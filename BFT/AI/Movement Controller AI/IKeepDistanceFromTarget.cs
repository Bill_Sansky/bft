﻿namespace BFT
{
    public interface IKeepDistanceFromTarget
    {
        float DistanceFromTarget { get; }
    }
}
