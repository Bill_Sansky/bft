﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class WaypointsController : MonoBehaviour
    {
        public enum WaypointsType
        {
            Looped,
            Line,
            Random
        }

        public WaypointsType DebugType;
#pragma warning disable 414
        private bool editMode = false;
#pragma warning restore 414
        [SerializeField, ShowInInspector, HideIf("editMode")]
        private List<Vector3> waypoints = new List<Vector3>();

        public int WaypointsCount => waypoints.Count;

        private void Awake()
        {
#if UNITY_EDITOR
            if (waypoints.Count < 2)
            {
                UnityEngine.Debug.LogError("Waypoint Controller requires minimum 2 waypoints!", this);
            }
#endif
        }

        public Vector3 GetWaypoint(int waypointIndex)
        {
            return waypoints[waypointIndex];
        }

        public Vector3 IncrementWaypoint(ref int currentWaypoint, ref bool forward, WaypointsType type)
        {
            switch (type)
            {
                case WaypointsType.Looped:
                    if (forward)
                    {
                        currentWaypoint = currentWaypoint == waypoints.Count - 1 ? 0 : currentWaypoint + 1;
                    }
                    else
                    {
                        currentWaypoint = currentWaypoint <= 0 ? waypoints.Count - 1 : currentWaypoint - 1;
                    }

                    break;
                case WaypointsType.Random:
                    if (waypoints.Count > 1)
                    {
                        int newWaypoint = currentWaypoint;
                        while (newWaypoint == currentWaypoint)
                        {
                            newWaypoint = Random.Range(0, waypoints.Count);
                        }

                        currentWaypoint = newWaypoint;
                    }

                    break;
                case WaypointsType.Line:
                    if (forward)
                    {
                        if (currentWaypoint == waypoints.Count - 1)
                        {
                            currentWaypoint--;
                            forward = false;
                        }
                        else
                        {
                            currentWaypoint++;
                        }
                    }
                    else
                    {
                        if (currentWaypoint <= 0)
                        {
                            currentWaypoint++;
                            forward = true;
                        }
                        else
                        {
                            currentWaypoint--;
                        }
                    }

                    break;
            }

            return GetWaypoint(currentWaypoint);
        }

        #region Editor

#if UNITY_EDITOR
        private List<GameObject> editModeWaypoints;

        [Button]
        private void AddWaypoint()
        {
            if (!editMode)
            {
                waypoints.Add(transform.position);
            }
            else
            {
                CreateEditorWaypoint(transform.position);
            }

            UnityEditor.SceneView.RepaintAll();
        }

        [Button, HideIf("editMode")]
        private void EditMode()
        {
            editModeWaypoints = new List<GameObject>();
            for (int i = 0; i < waypoints.Count; i++)
            {
                CreateEditorWaypoint(waypoints[i]);
            }

            editMode = true;
        }

        private void CreateEditorWaypoint(Vector3 position)
        {
            GameObject editorWP = GameObject.CreatePrimitive(PrimitiveType.Cube);
            editorWP.transform.parent = transform;
            editorWP.transform.position = position;
            editorWP.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            editModeWaypoints.Add(editorWP);
        }

        [Button, ShowIf("editMode"), GUIColor(0.45f, 1f, 0.65f)]
        private void SaveChanges()
        {
            waypoints = new List<Vector3>();
            for (int i = 0; i < editModeWaypoints.Count; i++)
            {
                waypoints.Add(editModeWaypoints[i].transform.position);
            }

            CloseEditMode();
        }

        [Button, ShowIf("editMode"), GUIColor(1f, 0.5f, 0.5f)]
        private void Cancel()
        {
            CloseEditMode();
        }

        private void CloseEditMode()
        {
            for (int i = 0; i < editModeWaypoints.Count; i++)
            {
                DestroyImmediate(editModeWaypoints[i]);
            }

            editModeWaypoints = null;
            editMode = false;
            UnityEditor.SceneView.RepaintAll();
        }

        private void OnDrawGizmosSelected()
        {
            if (!editMode)
                DrawDebug(DebugType, Color.green, Color.blue);
        }

        public void DrawDebug(WaypointsType type, Color waypointDebugColor, Color pathColor, List<Vector3> waypoints = null)
        {
            if (waypoints == null)
            {
                waypoints = this.waypoints;
            }

            Gizmos.color = waypointDebugColor;

            for (int i = 0; i < waypoints.Count; i++)
            {
                Gizmos.DrawSphere(waypoints[i], 0.25f);
            }

            Gizmos.color = pathColor;
            switch (type)
            {
                case WaypointsType.Looped:
                {
                    for (int i = 0; i < waypoints.Count; i++)
                    {
                        Gizmos.DrawLine(waypoints[i], i == waypoints.Count - 1 ? waypoints[0] : waypoints[i + 1]);
                    }
                }
                    break;
                case WaypointsType.Line:
                {
                    for (int i = 0; i < waypoints.Count - 1; i++)
                    {
                        Gizmos.DrawLine(waypoints[i], waypoints[i + 1]);
                    }
                }
                    break;
            }
        }

        private void OnDrawGizmos()
        {
            if (editMode)
            {
                UnityEngine.Debug.LogWarning("Waypoints controller edit mode is active, save changes or cancel editing!", this);

                Gizmos.color = Color.cyan;
                switch (DebugType)
                {
                    case WaypointsType.Looped:
                    {
                        for (int i = editModeWaypoints.Count - 1; i >= 0; i--)
                        {
                            if (editModeWaypoints[i] == null)
                            {
                                editModeWaypoints.RemoveAt(i);
                                continue;
                            }

                            while (i > 0 && editModeWaypoints[i - 1] == null)
                            {
                                i--;
                                editModeWaypoints.RemoveAt(i);
                            }

                            Gizmos.DrawLine(editModeWaypoints[i].transform.position,
                                i == 0
                                    ? editModeWaypoints[editModeWaypoints.Count - 1].transform.position
                                    : editModeWaypoints[i - 1].transform.position);
                        }
                    }
                        break;
                    case WaypointsType.Line:
                    {
                        for (int i = editModeWaypoints.Count - 1; i > 0; i--)
                        {
                            if (editModeWaypoints[i] == null)
                            {
                                editModeWaypoints.RemoveAt(i);
                                continue;
                            }

                            while (i > 0 && editModeWaypoints[i - 1] == null)
                            {
                                i--;
                                editModeWaypoints.RemoveAt(i);
                            }

                            if (editModeWaypoints.Count > 0)
                                Gizmos.DrawLine(editModeWaypoints[i].transform.position,
                                    editModeWaypoints[i - 1].transform.position);
                        }
                    }
                        break;
                }
            }
        }
#endif

        #endregion
    }
}
