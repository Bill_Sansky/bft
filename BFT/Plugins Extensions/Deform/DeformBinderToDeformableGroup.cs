#if BFT_DEFORM
using Deform;
using UnityEngine;

public class DeformBinderToDeformableGroup : MonoBehaviour
{
    public Deformer Deformer;
    public DeformableGroupValue Deformable;

    public bool BindOnEnable;

    private void OnEnable()
    {
        if (BindOnEnable)
            BindDeformer();
    }

    public void BindDeformer()
    {
        foreach (var deformable in Deformable.Value.Deformables)
        {
            deformable.AddDeformer(Deformer);
        }
    }

    public void UnBindDeformer()
    {
        foreach (var deformable in Deformable.Value.Deformables)
        {
            deformable.RemoveDeformer(Deformer);
        }
    }
}
#endif