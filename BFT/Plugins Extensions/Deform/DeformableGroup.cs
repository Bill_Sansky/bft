#if BFT_DEFORM
using System.Collections.Generic;
using Deform;
using UnityEngine;

public class DeformableGroup : MonoBehaviour
{
    public List<Deformable> Deformables = new List<Deformable>();

    public bool RetrieveDeformablesInChildrenOnAwake;

    private void Awake()
    {
        if (RetrieveDeformablesInChildrenOnAwake)
            Deformables.AddRange(GetComponentsInChildren<Deformable>());
    }
}

#endif