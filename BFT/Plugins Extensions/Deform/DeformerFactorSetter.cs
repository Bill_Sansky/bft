#if BFT_DEFORM
using System;
using BFT;
using UnityEngine;

public class DeformerFactorSetter : MonoBehaviour
{
    public DefomerFactorReference FactorDeformer;

    public FloatValue FactorValue;

    public bool SetOnEnable = true;

    public bool SetEveryFrame;

    private void OnEnable()
    {
        if (SetOnEnable)
            SetFactor();

        if (SetEveryFrame)
            this.CallEveryFrame(SetFactor);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    public void SetFactor()
    {
        FactorDeformer.Value.Factor = FactorValue.Value;
    }
}
#endif