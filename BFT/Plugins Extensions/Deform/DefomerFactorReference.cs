#if BFT_DEFORM
using System;
using BFT;
using Deform;

[Serializable]
public class DefomerFactorReference : InterfaceObjectReferenceValue<IFactor>
{
}
#endif