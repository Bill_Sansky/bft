#if BFT_DEFORM
using Deform;
using UnityEngine;

public class DeformBinderToDeformable : MonoBehaviour
{
    public Deformer Deformer;
    public DeformableValue Deformable;

    public bool BindOnEnable;

    private void OnEnable()
    {
        if (BindOnEnable)
            BindDeformer();
    }

    public void BindDeformer()
    {
        Deformable.Value.AddDeformer(Deformer);
    }
}

#endif