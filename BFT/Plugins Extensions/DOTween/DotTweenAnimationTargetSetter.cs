﻿#if BFT_DOTWEEN
using UnityEngine;
using DG.Tweening;

namespace BFT
{
    public class DotTweenAnimationTargetSetter : MonoBehaviour
    {
        public DOTweenAnimation[] anims;


        public void SwitchTargetAndRestart(UnityEngine.Transform target)
        {
            for (int i = 0; i < anims.Length; i++)
            {
                anims[i].targetGO = target.gameObject;
                anims[i].DORestart();
            }
        }
    }
}
#endif