﻿#if BFT_DOTWEEN
using Sirenix.OdinInspector;

namespace BFT
{
    public class DotTweenQuaternionTargetSetter : SerializedMonoBehaviour
    {
        public QuaternionTweener Tweener;

        public void SwitchTargetAndRestart(UnityEngine.Transform target)
        {
            //TODO FIX (I dont understand it anymore)
            //   TransformVariable vari = (TransformVariable)Tweener.EndValue;
            // vari.LocalValue = target;
            Tweener.RebuildTween();
            Tweener.Restart();
        }
    }
}
#endif
