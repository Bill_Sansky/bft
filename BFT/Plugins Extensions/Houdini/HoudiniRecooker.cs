﻿#if BFT_HOUDINI
using System;
using HoudiniEngineUnity;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class HoudiniRecooker : MonoBehaviour
    {
        
        public HEU_HoudiniAssetRoot houdiniAsset;

        public bool RecookOnMovement = false;
        
        [Button(ButtonSizes.Medium)]
        public void Recook()
        {
            if (!houdiniAsset)
                return;
            
            houdiniAsset._houdiniAsset.RequestCook(bCheckParametersChanged: true, bAsync: false, bSkipCookCheck: false, bUploadParameters: true);
        }

        [OnInspectorGUI]
        private void CheckIfShouldRecook()
        {
            if(transform.hasChanged && RecookOnMovement)
                Recook();
        }
    }
}
#endif