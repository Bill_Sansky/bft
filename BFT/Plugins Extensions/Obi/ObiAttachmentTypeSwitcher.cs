﻿#if BFT_OBI
using System;
using System.Collections.Generic;
using System.Linq;
using Obi;
using Sirenix.OdinInspector;
using UnityEngine;

public class ObiAttachmentTypeSwitcher : MonoBehaviour
{
    public List<ObiAttachmentValue> Attachment;

    public bool StartDynamic = true;

    public void OnEnable()
    {
        if (StartDynamic)
        {
            ConfigureDynamic();
        }
    }

   
    [Button(ButtonSizes.Medium)]
    public void ConfigureDynamic()
    {
        foreach (var attachmentValue in Attachment)
        {
            attachmentValue.Value.attachmentType = ObiParticleAttachment.AttachmentType.Dynamic;
        }
    } 
    
    [Button(ButtonSizes.Medium)]
    public void ConfigureStatic()
    {
        foreach (var attachmentValue in Attachment)
        {
            attachmentValue.Value.attachmentType = ObiParticleAttachment.AttachmentType.Static;
        }
    }

}
#endif