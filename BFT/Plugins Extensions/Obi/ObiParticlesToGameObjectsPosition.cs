﻿#if BFT_OBI
using System;
using BFT;
using Obi;
using Sirenix.OdinInspector;
using UnityEngine;


public class ObiParticlesToGameObjectsPosition : MonoBehaviour
{
    public ObiActor actor;

    public GameObject ToInstantiate;

    public GameObject[] CurrentGameObjects;

    public bool ReinstantiateOnStart = true;

    private void Start()
    {
        if (ReinstantiateOnStart)
            this.CallAfterOneFrame(ReInstantiate);
    }

    [Button(ButtonSizes.Medium)]
    public void ReInstantiate()
    {
        foreach (var currentGameObject in CurrentGameObjects)
        {
            if (Application.isPlaying)
                Destroy(currentGameObject);
            else
            {
                DestroyImmediate(currentGameObject);
            }
        }

        //TODO fix once you understand the new system

        CurrentGameObjects = new GameObject[actor.particleCount];
        for (int i = 0; i < actor.particleCount; i++)
        {
            var go = Instantiate(ToInstantiate, transform);
            CurrentGameObjects[i] = go;
        }
    }

    void LateUpdate()
    {
        //TODO fix once you understand the new system

        if (CurrentGameObjects.Length < actor.particleCount)
            return;

        for (var index = 0; index < actor.particleCount; index++)
        {
            var vector3 = actor.GetParticlePosition(index);
            CurrentGameObjects[index].transform.position = vector3;
        }
    }
}
#endif