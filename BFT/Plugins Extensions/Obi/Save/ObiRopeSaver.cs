#if BFT_OBI
using System.Collections.Generic;

using BFT;
using Obi;
using UnityEngine;


public class ObiRopeSaver : MonoBehaviour, ISavedObject
{
    public ObiRope Actor;

    public object Save()
    {
        if (!Actor.solver)
            return null;

        object[] savePerParticles = new object[Actor.solverIndices.Length];
        var solver = Actor.solver;
        for (var i = 0; i < Actor.solverIndices.Length; i++)
        {
            var index = Actor.solverIndices[i];
            savePerParticles[i] = (new object[]
            {
                solver.positions[index],
                solver.velocities[index]
            });
        }

        return savePerParticles;
    }

    public void Load(object saveFile)
    {
        if (saveFile == null)
            return;

        var solver = Actor.solver;
        object[] savePerParticles = (object[]) saveFile;

        for (var i = 0; i < Actor.solverIndices.Length; i++)
        {
            var index = Actor.solverIndices[i];
            var particleSave = (object[]) savePerParticles[i];
            solver.positions[index] = (Vector4) particleSave[0];
            solver.velocities[index] = (Vector4) particleSave[1];
        }
    }
}
#endif