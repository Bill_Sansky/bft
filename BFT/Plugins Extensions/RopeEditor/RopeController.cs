﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class RopeController : MonoBehaviour
    {
        [BoxGroup("Status"), ReadOnly] private int initialIterationAmount;

        [BoxGroup("Options/Override")] public int IterationOverride = 1;

        [BoxGroup("Options/Override")] public float IterationOverrideTime = 0.5f;

        [BoxGroup("Options/Override")] public float IterationRestoreTime = 0.5f;

        [BoxGroup("Status"), ReadOnly] private Rigidbody[] jointBodies;

        [BoxGroup("Options")] public float MaxForceApplyDelay;

        [BoxGroup("Rope")] public UltimateRope Rope;

        private Coroutine solverCountRestoreCoroutine;

        [BoxGroup("Options/Velocity")] public bool UseVelocityOverride = false;

        [BoxGroup("Options/Velocity")] public float VelocityLimit;

        [BoxGroup("Options/Velocity"), ShowIf("UseVelocityOverride")]
        public float VelocityOverrideValue;

        public void Awake()
        {
            jointBodies = Rope.GetComponentsInChildren<Rigidbody>();
            initialIterationAmount = Rope.LinkSolverIterationCount;

            foreach (var joint in Rope.GetComponentsInChildren<ConfigurableJoint>())
            {
                joint.projectionMode = JointProjectionMode.PositionAndRotation;
            }

            if (MaxForceApplyDelay > 0)
            {
                StartCoroutine(MaxForceDelay());
            }
        }

        private IEnumerator MaxForceDelay()
        {
            float value = Rope.LinkJointMaxForceValue;
            Rope.LinkJointMaxForceValue = int.MaxValue;
            yield return new WaitForSeconds(MaxForceApplyDelay);
            Rope.LinkJointMaxForceValue = value;
        }

        public void FixedUpdate()
        {
            if (solverCountRestoreCoroutine != null)
            {
                return;
            }

            foreach (var body in jointBodies)
            {
                if (body == null)
                    return;

                if (body.velocity.sqrMagnitude > VelocityLimit)
                {
                    if (solverCountRestoreCoroutine == null)
                    {
                        solverCountRestoreCoroutine = StartCoroutine(RestoreIterationsAfterAWhile());
                    }

                    if (UseVelocityOverride)
                    {
                        body.velocity = body.velocity.GetClampedMagnitude(0, VelocityOverrideValue);
                    }
                }
            }
        }

        private IEnumerator RestoreIterationsAfterAWhile()
        {
            Rope.LinkSolverIterationCount = IterationOverride;

            foreach (var body in jointBodies)
            {
                body.solverIterations = IterationOverride;
            }

            yield return new WaitForSeconds(IterationOverrideTime);

            float t = 0;
            while (t < IterationRestoreTime)
            {
                int newValue = (int) Mathf.Lerp(IterationOverride,
                    initialIterationAmount, t / IterationRestoreTime);
                Rope.LinkSolverIterationCount = newValue;

                foreach (var body in jointBodies)
                {
                    body.solverIterations = newValue;
                }

                t += UnityEngine.Time.deltaTime;
                yield return null;
            }

            solverCountRestoreCoroutine = null;
        }
    }
}
