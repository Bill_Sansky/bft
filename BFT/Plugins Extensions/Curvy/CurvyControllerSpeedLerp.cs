﻿#if BFT_CURVY
using BFT;
using FluffyUnderware.Curvy.Controllers;
using UnityEngine;

[ExecuteInEditMode]
public class CurvyControllerSpeedLerp :MonoBehaviour
{
    public float MaxSpeed;
    public float MinSpeed;
    public AnimationCurve SpeedProfile;

    public SplineController Controller;

    public void Update()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying && !Controller)
            return;
#endif
        Controller.Speed = MathExt.EvaluateCurve(MinSpeed, MaxSpeed, SpeedProfile, Controller.Position);
    }

}

#endif