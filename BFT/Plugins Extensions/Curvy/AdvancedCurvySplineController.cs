﻿#if BFT_CURVY
using FluffyUnderware.Curvy.Controllers;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(SplineController))]
public class AdvancedCurvySplineController : MonoBehaviour
{
    private SplineController controller;

    public UnityEvent OnEndReached;
    public UnityEvent OnStartReached;


    void Awake()
    {
        controller = GetComponent<SplineController>();
        controller.OnEndReached.AddListener(NotifyEndReached);
    }

    private void NotifyEndReached(CurvySplineMoveEventArgs arg0)
    {
        NotifyEndReached();
    }

    [Button(ButtonSizes.Medium)]
    public void RestartForward()
    {
        controller.Position = 0;
        controller.MovementDirection = MovementDirection.Forward;
        controller.Play();
    }

    [Button(ButtonSizes.Medium)]
    public void RestartBackwards()
    {
        controller.Position = 1;
        controller.MovementDirection = MovementDirection.Backward;
        controller.Play();
    }

    public void NotifyEndReached()
    {
        if (controller.MovementDirection == MovementDirection.Forward)
            OnEndReached.Invoke();
        else
        {
            OnStartReached.Invoke();
        }
    }
}
#endif