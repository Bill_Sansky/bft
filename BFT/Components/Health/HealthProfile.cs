﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [CreateAssetMenu(fileName = "Health Profile", menuName = "BFT/Data/Health/Health Profile")]
    public class HealthProfile : SerializedScriptableObject
    {
        [SerializeField, FoldoutGroup("Health")]
        private bool canBeRevived;

        [SerializeField, FoldoutGroup("Health"), ShowIf("UseReHit")]
        private float damageMultiplierOnReHit = 0.7f;

        [SerializeField, FoldoutGroup("Health"), ShowIf("UseInvincibilityFrames")]
        private float invincibilityFrameDuration = .7f;

        [SerializeField, FoldoutGroup("Health"), ShowIf("UseReHit")]
        private float reHitDuration = 0.7f;

        [SerializeField, FoldoutGroup("Health"), ShowIf("UseReHit")]
        private bool reHitPilingUp = true;

        [SerializeField, FoldoutGroup("Health")]
        private bool useInvincibilityFrames = true;

        [SerializeField, FoldoutGroup("Health")]
        private bool useReHit;

        public bool CanBeRevived => canBeRevived;
        public bool UseReHit => useReHit;
        public bool UseInvincibilityFrames => useInvincibilityFrames;
        public float InvincibilityFrameDuration => invincibilityFrameDuration;
        public float DamageMultiplierOnReHit => damageMultiplierOnReHit;
        public float ReHitDuration => reHitDuration;
        public bool ReHitPilingUp => reHitPilingUp;
    }
}
