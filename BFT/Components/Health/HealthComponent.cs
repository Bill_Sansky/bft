﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class HealthComponent : MonoBehaviour
    {
        private IEnumerator checkHitReductionTime;

        [SerializeField, FoldoutGroup("Health")]
        private IntVariable currentHealth;

        [FoldoutGroup("Status"), ReadOnly, ShowInInspector, ShowIf("UseReHit")]
        private float currentReHitMultiplier;

        [FoldoutGroup("Status")] public bool Damaged;

        [FoldoutGroup("Health")] public HealthProfile HealthProfile;

        [FoldoutGroup("Status"), ReadOnly, ShowInInspector, ShowIf("UseReHit")]
        private bool inHitReduction;

        [SerializeField, FoldoutGroup("Health")]
        private IntValue maxHealth;

        [FoldoutGroup("Events")] public UnityEvent OnDamagedEvent;

        [FoldoutGroup("Events")] public UnityEvent OnDeathEvent;

        [FoldoutGroup("Events")] public IntEvent OnHealthChangedEvent;

        [FoldoutGroup("Events")] public UnityEvent OnInvincibilityFramesEnded;

        [FoldoutGroup("Events")] public UnityEvent OnInvincibilityFramesStarted;

        [FoldoutGroup("Health")] [Range(0, 1)] public float StartingHealthPercent = 1;

        [ShowInInspector, ReadOnly, FoldoutGroup("Status")]
        public int Health => currentHealth.Value;

        [ShowInInspector, ReadOnly, FoldoutGroup("Status")]
        public int MaxHealth => maxHealth.Value;

        [ShowInInspector, ReadOnly, FoldoutGroup("Status")]
        public bool Dead { get; private set; }

        [ShowInInspector, ReadOnly, FoldoutGroup("Status")]
        public bool Invincible { get; set; }

        public bool UseReHit => HealthProfile && HealthProfile.UseReHit;

        public float HealthPercent
        {
            get => (float) currentHealth.Value / MaxHealth;
            set => currentHealth.Value = (int) (MaxHealth * value);
        }

        public void Initialize()
        {
            currentHealth.Value = (int) (MaxHealth * StartingHealthPercent);
            Dead = false;
            CheckDeath();
        }

        public void AddHealth(int added)
        {
            if (Dead && !HealthProfile.CanBeRevived)
                return;
            int previousHealth = currentHealth.Value;
            currentHealth.Value = Mathf.Clamp(added + currentHealth.Value, 0, MaxHealth);

            if (previousHealth != currentHealth.Value)
                OnHealthChangedEvent.Invoke(currentHealth.Value);

            if (added < 0)
            {
                if (currentHealth.Value > 0)
                    NotifyDamaged();
                else
                {
                    CheckDeath();
                }
            }
        }

        /// <summary>
        ///     difference with adding a negative health is that damage is subject to reHit
        /// </summary>
        /// <param name="damage"></param>
        public void Damage(int damage)
        {
            if (Dead || Invincible)
                return;
            if (HealthProfile.UseReHit)
            {
                if (!inHitReduction)
                {
                    inHitReduction = true;
                    currentReHitMultiplier = HealthProfile.DamageMultiplierOnReHit;
                    StartCoroutine(checkHitReductionTime = CheckHitReductionTime());
                }
                else if (HealthProfile.ReHitPilingUp)
                {
                    if (checkHitReductionTime != null)
                        StopCoroutine(checkHitReductionTime);
                    currentReHitMultiplier *= HealthProfile.DamageMultiplierOnReHit;
                    StartCoroutine(CheckHitReductionTime());
                }

                damage = (int) (damage * currentReHitMultiplier);
            }

            AddHealth(-damage);

            if (HealthProfile.UseInvincibilityFrames)
            {
                StartCoroutine(CheckInvincibilityFrames());
            }
        }

        private IEnumerator CheckInvincibilityFrames()
        {
            Invincible = true;
            yield return new WaitForSeconds(HealthProfile.InvincibilityFrameDuration);
            Invincible = false;
        }

        private IEnumerator CheckHitReductionTime()
        {
            yield return new WaitForSeconds(HealthProfile.ReHitDuration);
            inHitReduction = false;
        }

        private void NotifyDamaged()
        {
            Damaged = true;
            OnDamagedEvent.Invoke();
        }

        public void CheckDeath()
        {
            if (!Dead && currentHealth.Value == 0)
            {
                Dead = true;
                OnDeathEvent.Invoke();
            }
        }
    }
}
