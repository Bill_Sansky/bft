﻿namespace BFT
{
    public enum DataConnectorFlowStatus
    {
        INFLOW,
        OUTFLOW,
        BOTH,
    }
}
