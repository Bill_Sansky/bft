﻿using UnityEngine.Events;

namespace BFT
{
    public interface IConnectionDataNode : IConnectionNode
    {
        int PackageCapacity { get; }
        bool CanOverflow { get; }
        int StoredPackages { get; }

        int OutOverflowAmount { get; }

        /// <summary>
        ///     The time it takes for the Node to fill up before it can send the packages through the open output conenctors
        /// </summary>
        /// <returns></returns>
        float MinUpdateTime { get; }

        UnityEvent OnNodeFull { get; }
        UnityEvent OnNodeEmpty { get; }

        /// <summary>
        ///     returns true if the package could properly be received
        /// </summary>
        /// <param name="connector"></param>
        /// <param name="packages"></param>
        /// <returns></returns>
        bool ReceivePackageFrom(IConnector connector, int packages);

        void UpdateNode(float dt);
    }
}
