﻿using UnityEngine.Events;

 namespace BFT
{
    public interface IDataConnector : IConnector
    {
        /// <summary>
        ///     Can Data circulate through this connector
        /// </summary>
        bool IsOpen { get; set; }

        ConnectionDataType DataType { get; }
        DataConnectorFlowStatus ConnectorFlowStatus { get; set; }

        int PackageFlowCapacity { get; }

        /// <summary>
        ///     How far is the connector from its node
        ///     (i.e. how much time it will take for the data to be received)
        /// </summary>
        float DistanceToNode { get; }

        UnityEvent OnDataSent { get; }
        UnityEvent OnDataReceived { get; }

        bool ReceivePackage(int package);
        bool SendPackage(int package);
    }
}
