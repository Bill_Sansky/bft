﻿using System;
using UnityEngine.Events;

namespace BFT
{
    [Serializable]
    public class ConnectorEvent : UnityEvent<IConnector>
    {
    }

    /// <summary>
    ///     Can connect to ONE other connector, and belongs to a connection node
    /// </summary>
    public interface IConnector
    {
        IConnectionNode ConnectionNode { get; }

        EnumAsset[] ConnectionLayers { get; }

        IConnector ConnectedConnector { get; }

        bool IsConnected { get; }

        ConnectorEvent OnConnectionStarted { get; }

        ConnectorEvent OnConnectionEnded { get; }

        bool CanBeConnectedTo(IConnector receptor);

        bool ConnectTo(IConnector receptor);

        void DisconnectFrom(IConnector receptor);
    }
}
