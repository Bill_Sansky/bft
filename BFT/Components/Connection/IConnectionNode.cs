﻿namespace BFT
{
    /// <summary>
    ///     Can be connected to and has one or more connection slots
    /// </summary>
    public interface IConnectionNode
    {
        IConnector[] Connectors { get; }

        int ConnectorCount { get; }
    }
}
