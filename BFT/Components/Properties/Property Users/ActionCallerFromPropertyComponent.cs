﻿using UnityEngine;

namespace BFT
{
    public abstract class ActionCallerFromPropertyComponent<T, T3, T1, T2> : MonoBehaviour where T1 : BFTAction<T>
        where T2 : PropertyComponentWithValue<T, T3>
        where T3 : GenericValue<T>
    {
        public PropertyType PropertyType;
        public T1 Action;

        public void FindPropertyAndCallAction(GameObject obj)
        {
            var property = PropertyComponentGroup.FindPropertyOfType(PropertyType, obj);
            T2 myProp = (T2) property;
            if (!myProp)
                return;
            
            Debug.Assert(Action.TargetAction.MethodInfo != null,
                "The Method is undefined, please fix the action linked", this);
            Action.Invoke(myProp.Value);
        }
    }
}