using System;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BFT
{
    /// <summary>
    /// sets references to properties variables based on all the properties of given types that could be found on an object
    /// </summary>
    public class PropertyVariablesSetterFromPropertyTypes : MonoBehaviour
    {
        public PropertyTypeToPropertyVariableDictionary PropertiesToSet;

        public void SetPropertiesFromObject(Object go)
        {
            SetPropertiesFromGameObject((GameObject) go);
        }

        public void SetPropertiesFromGameObject(GameObject go)
        {
            var group = go.GetComponent<PropertyComponentGroup>();

            if (group)
            {
                foreach (var pair in PropertiesToSet)
                {
                    if (group.HasCategory(pair.Key))
                    {
                        pair.Value.Value = group.GetPropertyComponentForCategory(pair.Key);
                    }
                }

                return;
            }

            var components = go.GetComponents<PropertyComponent>();

            if (components.Length > 0)
            {
                foreach (var pair in PropertiesToSet)
                {
                    if (group.HasCategory(pair.Key))
                    {
                        pair.Value.Value = group.GetPropertyComponentForCategory(pair.Key);
                    }
                }


                return;
            }

            UnityEngine.Debug.LogWarning(
                $"No Property component or property component group was found on object {go}, this is invalid", go);
        }
    }
}