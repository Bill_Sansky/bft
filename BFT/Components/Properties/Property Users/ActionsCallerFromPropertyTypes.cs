﻿using UnityEngine;
using UnityEngine.Serialization;

namespace BFT
{
    /// <summary>
    ///calls actions using the data from the properties
    /// </summary>
    public class ActionsCallerFromPropertyTypes : MonoBehaviour
    {
        public PropertyTypeToObjectActionDictionary ActionsToCall;

        public void SetPropertiesFromObject(Object go)
        {
            SetPropertiesFromGameObject((GameObject) go);
        }

        public void SetPropertiesFromGameObject(GameObject go)
        {
            var group = go.GetComponent<PropertyComponentGroup>();

            if (group)
            {
                foreach (var pair in ActionsToCall)
                {
                    if (group.HasCategory(pair.Key))
                    {
                        pair.Value.Invoke(group.GetPropertyComponentForCategory(pair.Key).Data);
                    }
                }

                return;
            }

            var components = go.GetComponents<PropertyComponent>();

            if (components.Length > 0)
            {
                foreach (var pair in ActionsToCall)
                {
                    if (group.HasCategory(pair.Key))
                    {
                        pair.Value.Invoke(group.GetPropertyComponentForCategory(pair.Key).Data);
                    }
                }


                return;
            }

            UnityEngine.Debug.LogWarning(
                $"No Property component or property component group was found on object {go}, this is invalid", go);
        }
    }
}