using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public abstract class ValueFromPropertyComponent<T, T1> : MonoBehaviour, IValue<T> where T1 : GenericValue<T>
    {
        [SerializeField] private PropertyVariable currentProperty = new PropertyVariable() {UseReference = true};

        public T1 DefaultValue;

        [ShowInInspector]
        public T Value => currentProperty.Value ? ((IValue<T>) currentProperty.Value.Data).Value : DefaultValue.Value;

        public void AssignProperty(PropertyComponent component)
        {
            currentProperty.Value = component;
        }
    }
}