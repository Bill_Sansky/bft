using UnityEngine;

namespace BFT.Action_Caller_Types
{
    public class Vector3ActionCallerFromPropertyComponent : ActionCallerFromPropertyComponent<Vector3, Vector3Value,
        Vector3Action, Vector3PropertyComponent>
    {
        
    }
}