﻿using System;

namespace BFT
{
    [Serializable]
    public class PropertyTypeToObjectActionDictionary : SerializableDictionaryBase<PropertyType, ObjectAction>
    {
    }
}