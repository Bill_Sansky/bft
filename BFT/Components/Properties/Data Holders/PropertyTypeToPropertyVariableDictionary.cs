using System;

namespace BFT
{
    [Serializable]
    public class
        PropertyTypeToPropertyVariableDictionary : SerializableDictionaryBase<PropertyType, PropertyVariableComponent>
    {
    }
}