using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace BFT
{
    public class PropertyComponent : MonoBehaviour
    {
        [FormerlySerializedAs("Category")] [InlineEditor(InlineEditorModes.GUIAndHeader), OnValueChanged("EditorCheckType")]
        public PropertyType PropertyType;

        [OnValueChanged("EditorCheckType")] public virtual object Data { get; }

#if UNITY_EDITOR
        protected virtual void EditorCheckType()
        {
            if (!PropertyType.ExpectsDataType)
                return;
            var type = GetType();
            if (type != PropertyType.DataTypeExpected)
            {
                Debug.LogWarning(
                    $"The property type expects a property of type {PropertyType.DataTypeExpected} but the current property is of type {type}",
                    this);
                PropertyType = null;
            }
        }
#endif
    }
}
