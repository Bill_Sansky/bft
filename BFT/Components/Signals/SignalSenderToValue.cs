﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class SignalSenderToValue : MonoBehaviour
    {
        [AssetSelector] public SignalType SignalType;

        public bool IsDataSignal => SignalType && SignalType.ContainsData;

        [ShowIf("IsDataSignal")] public Object Data;

        public GameObjectValue Target;

        public bool SendSignalOnEnable = false;

        public void OnEnable()
        {
            if (SendSignalOnEnable)
                SendSignal();
        }

        public void SendSignal()
        {
            if (!Target.Value)
                return;

            SignalSender.SendSignalToGameObject(Target.Value, SignalType, Data);
        }
    }
}