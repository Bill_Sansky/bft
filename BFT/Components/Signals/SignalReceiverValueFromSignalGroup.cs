﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class SignalReceiverValueFromSignalGroup : MonoBehaviour, IValue<SignalReceiver>
    {
        [AssetSelector] 
        public SignalType SignalTypeToLookFor;
        
        [ShowInInspector,ReadOnly]
        public SignalReceiver Value { get; private set; }
        private SignalReceiver ovValue => Value;

        public void AssignFromGameObject(GameObject go)
        {
            Value = SignalSender.FindSignalReceiverOfType(go, SignalTypeToLookFor);
        }
        
        public void ReceiveSignal()
        {
            ovValue.ReceiveSignal(null);
        }
        public void ReceiveSignal(Object data)
        {
            ovValue.ReceiveSignal(data);
        }
    }
}