﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class SignalSenderToSignalReceiverValue : MonoBehaviour
    {
        [AssetSelector] public SignalType SignalType;

        public bool IsDataSignal => SignalType && SignalType.ContainsData;

        [ShowIf("IsDataSignal")] public Object Data;

        public SignalReceiverValue Target;

        public bool SendSignalOnEnable = false;

        public void OnEnable()
        {
            if (SendSignalOnEnable)
                SendSignal();
        }

        public void SendSignal()
        {
            if (!Target.Value)
                return;

            Target.Value.ReceiveSignal(Data);
        }

        public void StartToSendSignalRegularly(float timeInterval)
        {
            this.CallRegularly(SendSignal, timeInterval);
        }

        public void StopToSendSignalRegularly()
        {
            StopAllCoroutines();
        }

        public void StopToSendSignalRegularlyIfObjectHasReceiver(GameObject target)
        {
            var potentialReceiver = SignalSender.FindSignalReceiverOfType(target, SignalType);
            if (potentialReceiver == Target.Value)
                StopAllCoroutines();
        }
    }
}