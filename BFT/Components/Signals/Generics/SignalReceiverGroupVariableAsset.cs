namespace BFT.Generics
{
    public class SignalReceiverGroupVariableAsset : VariableAsset<SignalReceiverGroup>
    {
        public void SendSignal(SignalType signal)
        {
            Value.ReceiveSignal(signal);
        }
    }
}