using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Object = UnityEngine.Object;

namespace BFT
{
    public class SignalSender : MonoBehaviour
    {
        [AssetSelector] public SignalType SignalType;

        public bool IsDataSignal => SignalType && SignalType.ContainsData;

        [ShowIf("IsDataSignal")] public Object Data;

        public void SendSignal(Component component)
        {
            SendSignal(component.gameObject);
        }

        [InfoBox("Ideally, try to have a Signal Receiver or a signal receiver group directly on the object called," +
                 " and a SignalReceiverGroup whenever possible")]
        public void SendSignal(GameObject target)
        {
            //first check for Group on the object
            SendSignalToGameObject(target, SignalType, Data);
        }

        public static void SendSignalToGameObject(GameObject target, SignalType signalType, Object data = null)
        {
            var receiver = FindSignalReceiverOfType(target, signalType);
            if (receiver)
                receiver.ReceiveSignal(signalType.ContainsData ? data : null);
        }

        public static SignalReceiver FindSignalReceiverOfType(GameObject target, SignalType signalType)
        {
            var signalGroup = target.GetComponent<SignalReceiverGroup>();

            if (!signalGroup)
            {
                //then try to find normal receives on the object
                var receivers = target.GetComponents<SignalReceiver>();

                foreach (var receiver in receivers)
                {
                    if (receiver.SignalType == signalType)
                    {
                        return receiver;
                    }
                }
            }

            if (!signalGroup)
                signalGroup = target.GetComponentInChildren<SignalReceiverGroup>();

            if (signalGroup)
            {
                return signalGroup.GetReceiverOnType(signalType);
            }

            var allReceivers = target.GetComponentsInChildren<SignalReceiver>();

            foreach (var receiver in allReceivers)
            {
                if (receiver.SignalType == signalType)
                {
                    return receiver;
                }
            }

            return null;
        }

        public void SendSignalToAllChildren(GameObject go)
        {
            go.ForEachChildrenGameObject(SendSignal);
        }

        public void SendSignal(GameObjectReferenceAsset reference)
        {
            SendSignal(reference.Value);
        }
    }
}