using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class SignalReceiver : MonoBehaviour
    {
        [AssetSelector] public SignalType SignalType;

        public bool ReceiveSignalWhenInactive = false;

        [HideIf("IsDataSignal")] public UnityEvent OnSignalReceived;
        public bool IsDataSignal => SignalType && SignalType.ContainsData;

        [ShowIf("IsDataSignal")] public UnityObjectEvent OnSignalWithDataReceived;

        [Button(ButtonSizes.Medium)]
        public void ReceiveSignal(Object data = null)
        {
            if (!ReceiveSignalWhenInactive && (!enabled || !gameObject.activeInHierarchy))
                return;

            if (SignalType.ContainsData)
                OnSignalWithDataReceived.Invoke(data);
            else
                OnSignalReceived.Invoke();
        }
    }
}