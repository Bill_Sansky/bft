﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class StateMachineStateIDSwicth : MonoBehaviour
    {
        [BoxGroup("References")] public IntValue ID;

        [BoxGroup("References")] public StateMachine StateMachine;

        public void SwitchState()
        {
            StateMachine.TransitionToState(ID.Value);
        }
    }
}
