using UnityEngine;

namespace BFT
{
    public class OnDisableEvent : MonoBehaviour
    {
        public GameObjectEvent OnDisabled = new GameObjectEvent();

        private void OnDisable()
        {
            OnDisabled.Invoke(gameObject);
        }
    }
}