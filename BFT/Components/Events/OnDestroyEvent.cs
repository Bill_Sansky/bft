using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class OnDestroyEvent : MonoBehaviour
    {
        public UnityEvent OnDestroyEvt;
       
        void OnDestroy()
        {
            OnDestroyEvt.Invoke();
        }
        
    }
}