using UnityEngine;

namespace BFT
{
    public class GameObjectEnableDisableToggle : MonoBehaviour
    {
        public GameObjectValue GameObject;

        public void ToggleActive()
        {
            if (GameObject.Value.activeInHierarchy)
                GameObject.Value.SetActive(false);
            else
            {
                GameObject.Value.SetActive(true);
                ;
            }
        }
    }
}