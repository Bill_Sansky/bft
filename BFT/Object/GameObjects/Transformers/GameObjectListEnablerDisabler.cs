﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public enum EnablerDisablerEnableType
    {
        ALL,
        ONE_RANDOM,
        ALL_WITH_DELAY,
    }

    public class GameObjectListEnablerDisabler : MonoBehaviour
    {
        public bool ActivateOnEnable;

        [ShowIf("IsDelay")] public float DelayBetweenElements;

        public bool DisableAllOnAwake = true;

        private WaitForSeconds elementWait;

        public EnablerDisablerEnableType EnableType;
        [BoxGroup("Enable - Disable")] public List<GameObject> Objects = new List<GameObject>();

        public UnityEvent OnActivationDone;

        public bool SetInactiveOnDisable = true;

        private bool IsDelay => EnableType == EnablerDisablerEnableType.ALL_WITH_DELAY;

        public int ObjectCount => Objects.Count;

        public void Awake()
        {
            if (DisableAllOnAwake)
                DisableAll();
        }

        public void OnEnable()
        {
            if (ActivateOnEnable)
            {
                switch (EnableType)
                {
                    case EnablerDisablerEnableType.ALL:
                        EnableAll();
                        break;
                    case EnablerDisablerEnableType.ONE_RANDOM:
                        EnableRandomObject();
                        break;
                    case EnablerDisablerEnableType.ALL_WITH_DELAY:
                        EnableWithDelay();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public void OnDisable()
        {
            if (SetInactiveOnDisable)
                DisableAll();
        }

        [Button(ButtonSizes.Medium)]
        public void DisableAll()
        {
            foreach (var o in Objects)
            {
                if (o)
                    o.SetActive(false);
            }
        }

        public void DisableAllAfterTime(float time)
        {
            this.CallAfterSomeTime(time, DisableAll);
        }

        [Button(ButtonSizes.Medium)]
        public void EnableAll()
        {
            StopAllCoroutines();

            foreach (var o in Objects)
            {
                o.SetActive(true);
            }

            OnActivationDone.Invoke();
        }

        public void EnableObject(int id)
        {
            StopAllCoroutines();
            Objects[id].SetActive(true);
        }

        public void DisableObject(int id)
        {
            Objects[id].SetActive(false);
        }

        public void EnableWithDelay()
        {
            StopAllCoroutines();
            StartCoroutine(ActivateElementOneAtATime());
        }

        [Button(ButtonSizes.Medium)]
        public void EnableRandomObject()
        {
            Objects.Random().SetActive(true);
        }

        [Button(ButtonSizes.Medium)]
        public void DisableAllAndEnableRandomObject()
        {
            DisableAll();
            EnableRandomObject();
        }

        public IEnumerator ActivateElementOneAtATime()
        {
            elementWait = new WaitForSeconds(DelayBetweenElements);
            foreach (var o in Objects)
            {
                if (o)
                    o.SetActive(true);
                yield return elementWait;
            }

            OnActivationDone.Invoke();
        }

        private void OnValidate()
        {
            if (!Application.isPlaying)
                Objects.RemoveAll(_ => !_);
        }
    }
}
