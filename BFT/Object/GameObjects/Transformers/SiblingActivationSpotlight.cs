using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace BFT
{
    public class SiblingActivationSpotlight : MonoBehaviour
    {
        private int CurrentActiveID = -1;
        
        [FormerlySerializedAs("FirstObjectActive")] public IntValue FirstObjectIDActive = new IntValue(-1);
        public Transform ParentToManage;

        public GameObject CurrentlyActiveElement => ParentToManage.GetChild(CurrentActiveID).gameObject;
        
        public bool InitializeOnEnable = false;

        public bool HasActiveElement => CurrentActiveID != -1;
        
        public void OnEnable()
        {
            if (InitializeOnEnable)
                Initialize();
        }

        [Button(ButtonSizes.Medium)]
        private void Initialize()
        {
            CurrentActiveID = FirstObjectIDActive.Value;

           
            for (int i = 0; i < ParentToManage.childCount; i++)
            {
                var obj = ParentToManage.GetChild(i);
                if (i == CurrentActiveID)
                {
                    obj.gameObject.SetActive(true);
                }
                else
                {
                    obj.gameObject.SetActive(false);
                }
                    
            }
        }

        public void ShowId(int id)
        {
            if (HasActiveElement)
                CurrentlyActiveElement.SetActive(false);

            CurrentActiveID = ParentToManage.childCount.LoopID(id);

            UnityEngine.Debug.Assert(CurrentActiveID >= 0 && CurrentActiveID <  ParentToManage.childCount,
                $"The id ({CurrentActiveID}) is invalid", this);

            if (HasActiveElement)
                CurrentlyActiveElement.SetActive(true);
        }

        [Button(ButtonSizes.Medium)]
        public void ShowNoObject()
        {
            ShowId(-1);
        }

        [Button(ButtonSizes.Medium)]
        public void ShowNextObject()
        {
            ShowId(CurrentActiveID + 1);
        }

        [Button(ButtonSizes.Medium)]
        public void ShowPreviousObject()
        {
            ShowId(CurrentActiveID - 1);
        }
    }
}