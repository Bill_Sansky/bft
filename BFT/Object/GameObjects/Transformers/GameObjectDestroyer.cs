﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{

    public class GameObjectDestroyer : MonoBehaviour
    {
        [BoxGroup("Destroy Options")] public bool DestroyOnAwake;

        [BoxGroup("Destroy Options")] public FloatValue DestroyTimer;

        [BoxGroup("Destroy Options")] public GameObject GameObject;

        public void Awake()
        {
            if (DestroyOnAwake)
                Destroy();
        }

        public void Destroy()
        {
            Destroy(GameObject, DestroyTimer.Value);
        }
        
        public void DestroyExternal(GameObject go)
        {
            Destroy(go, DestroyTimer.Value);
        }
    }
}
