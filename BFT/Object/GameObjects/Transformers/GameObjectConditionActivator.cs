﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class GameObjectConditionActivator : MonoBehaviour
    {
        public bool AutoActivate = true;
        public BoolValue Condition;

        public GameObject ToActivate;

        void Reset()
        {
            ToActivate = gameObject;
        }

        void OnEnable()
        {
            ToggleActive();
            if (!AutoActivate)
                return;

            StopAllCoroutines();
            StartCoroutine(CheckEnable());
        }

        void OnDisable()
        {
            if (!AutoActivate)
                return;

            StopAllCoroutines();
        }

        private IEnumerator CheckEnable()
        {
            yield return CoroutineUtils.CallEveryFrame(ToggleActive);
        }

        public void ToggleActive()
        {
            ToActivate.SetActive(Condition == null || Condition.Value);
        }
    }
}
