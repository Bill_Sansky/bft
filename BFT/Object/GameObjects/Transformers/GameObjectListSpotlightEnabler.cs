using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class GameObjectListSpotlightEnabler : MonoBehaviour
    {
        private int CurrentActiveID = -1;

        [ValueDropdown("ObjectNames")] public int FirstObjectActive = -1;
        [OnValueChanged("RemoveNullMember")] public List<GameObject> GameObjects = new List<GameObject>();

        public GameObject CurrentlyActiveObject => GameObjects[CurrentActiveID];

        public bool HasObjectActive => CurrentActiveID != -1;
        public int ObjectCount => GameObjects.Count;

        private ValueDropdownList<int> ObjectNames
        {
            get
            {
                var dd = new ValueDropdownList<int>();
                dd.Add("None", -1);

                for (int i = 0; i < GameObjects.Count; i++)
                {
                    if (GameObjects[i])
                    {
                        dd.Add(GameObjects[i].name, i);
                    }
                }

                return dd;
            }
        }

        private void RemoveNullMember()
        {
            GameObjects.RemoveAll(_ => !_);
        }

        private void OnValidate()
        {
            RemoveNullMember();
        }

        public void Awake()
        {
            Initialize();
        }

        [Button(ButtonSizes.Medium)]
        private void Initialize()
        {
            CurrentActiveID = FirstObjectActive;

            foreach (var o in GameObjects)
            {
                o.SetActive(false);
            }

            if (HasObjectActive)
                CurrentlyActiveObject.SetActive(true);
        }

        public void ShowId(int id)
        {
            if (HasObjectActive)
                CurrentlyActiveObject.SetActive(false);

            CurrentActiveID = GameObjects.LoopID(id);

            UnityEngine.Debug.Assert(CurrentActiveID >= 0 && CurrentActiveID < GameObjects.Count,
                $"The id ({CurrentActiveID}) is invalid", this);

            if (HasObjectActive)
                CurrentlyActiveObject.SetActive(true);
        }

        [Button(ButtonSizes.Medium)]
        public void ShowNoObject()
        {
            ShowId(-1);
        }

        [Button(ButtonSizes.Medium)]
        public void ShowNextObject()
        {
            ShowId(CurrentActiveID + 1);
        }

        [Button(ButtonSizes.Medium)]
        public void ShowPreviousObject()
        {
            ShowId(CurrentActiveID - 1);
        }
    }
}
