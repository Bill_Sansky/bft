﻿using UnityEngine;

namespace BFT
{
    public class GameObjectDisableOnAwake : MonoBehaviour
    {
        void Awake()
        {
            gameObject.SetActive(false);
        }
    }
}
