﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace BFT
{
    public class GameObjectGroupEnableDisableToggle : MonoBehaviour
    {
        [FormerlySerializedAs("TriggerGroup2First")] [FormerlySerializedAs("Group1ActiveFirst")] [BoxGroup("Options")]
        public bool EnableGroup2First = true;

        [FormerlySerializedAs("ObjectsToEnable")] [BoxGroup("Enable-Disable")]
        public GameObject[] ObjectsGroup1;

        [FormerlySerializedAs("ObjectsToDisable")] [BoxGroup("Enable-Disable")]
        public GameObject[] ObjectsGroup2;

        public UnityEvent OnGroup1Enabled;
        public UnityEvent OnGroup2Enabled;

        [FormerlySerializedAs("ExecuteOnEnable")] [BoxGroup("Options")]
        public bool ToggleOnEnable = false;

        [ShowInInspector, BoxGroup("Status"), ReadOnly]
        private bool triggered;


        public void Awake()
        {
            if (!EnableGroup2First)
                triggered = true;
        }

        public void OnEnable()
        {
            if (ToggleOnEnable)
                ToggleEnableDisable();
        }

        public void EnableDisable(bool enable)
        {
            if (enable)
            {
                EnableGroup1DisableGroup2();
            }
            else
            {
                DisableGroup1EnableGroup2();
            }
        }

        [BoxGroup("Options")]
        [Button(ButtonSizes.Medium)]
        public void EnableGroup1DisableGroup2()
        {
            triggered = true;
            foreach (GameObject go in ObjectsGroup1)
            {
                go.SetActive(true);
            }

            OnGroup1Enabled.Invoke();

            foreach (GameObject go in ObjectsGroup2)
            {
                go.SetActive(false);
            }
        }

        [BoxGroup("Options")]
        [Button(ButtonSizes.Medium)]
        public void DisableGroup1EnableGroup2()
        {
            triggered = false;
            foreach (GameObject go in ObjectsGroup1)
            {
                go.SetActive(false);
            }

            foreach (GameObject go in ObjectsGroup2)
            {
                go.SetActive(true);
            }

            OnGroup2Enabled.Invoke();
        }

        [BoxGroup("Options")]
        [Button(ButtonSizes.Medium)]
        public void ToggleEnableDisable()
        {
            if (triggered)
                DisableGroup1EnableGroup2();
            else
            {
                EnableGroup1DisableGroup2();
            }
        }
    }
}