﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class SpawnPool : MonoBehaviour, ISavedObject
    {
        [BoxGroup("Pool Options")] public bool AutoDestroyObjectWhenMaxPoolSizeReached = true;

        [BoxGroup("Pool Options")] public bool AutoRaisePoolSize = true;


        [BoxGroup("Pool Options")] public int MaxPooledObjects = 30;

        [BoxGroup("Spawn Options"), SerializeField]
        private RespawnableComponent objectToSpawn;

        public RespawnableComponent LastObjectSpawned { get; private set; }
        public RespawnableComponent LastObjectDeSpawned { get; private set; }

        [BoxGroup("Events")] public RespawnableEvent OnObjectDeSpawned;
        [BoxGroup("Events")] public RespawnableEvent OnBeforeObjectSpawned;
        [BoxGroup("Events")] public RespawnableEvent OnObjectSpawned;

        [BoxGroup("Spawn Options")] public bool ParentDespawnedObjects;

        [ShowIf("ParentDespawnedObjects")] [BoxGroup("Spawn Options")]
        public Transform DespawnedObjectsParent;

        [BoxGroup("Status"), SerializeField] private List<RespawnableComponent> pooledObjects;
        [BoxGroup("Pool Options")] public int PoolSize = 1;

        [BoxGroup("Status"), SerializeField] private List<RespawnableComponent> spawnedObjects;

        [BoxGroup("Spawn Options")] public bool ParentSpawnedObject = false;

        [BoxGroup("Spawn Options"), ShowIf("ParentSpawnedObject")]
        public Transform SpawnedObjectsParent;

        [BoxGroup("Spawn Options")] public bool PositionSpawnedObject = false;

        [BoxGroup("Spawn Options"), ShowIf("PositionSpawnedObject")]
        public Vector3Value SpawnPosition;

        [BoxGroup("Spawn Options"), ShowIf("PositionSpawnedObject")]
        public QuaternionValue SpawnRotation;

        public List<RespawnableComponent> PooledObjects => pooledObjects;

        public RespawnableComponent this[int index] => Get(index);

        public List<RespawnableComponent> SpawnedObjects => spawnedObjects;

        public RespawnableComponent ObjectToSpawn
        {
            get => objectToSpawn;
            set => objectToSpawn = value;
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public virtual RespawnableComponent SpawnObject()
        {
            var obj = PoolTransfer();

            if (PositionSpawnedObject)
            {
                obj.transform.position = SpawnPosition.Value;
                obj.transform.rotation = SpawnRotation.Value;
            }

            OnBeforeObjectSpawned.Invoke(obj);
            LastObjectSpawned = obj;
            obj.NotifySpawn(this);
            OnObjectSpawned.Invoke(obj);

            if (ParentSpawnedObject)
                obj.transform.SetParent(SpawnedObjectsParent, true);

            return obj;
        }

        public void SpawnOneObject()
        {
            SpawnObject();
        }

        public void SpawnFewObjects(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                SpawnObject();
            }
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public void DeSpawnObject(RespawnableComponent obj)
        {
            UnityEngine.Debug.Assert(SpawnedObjects.Contains(obj),
                "The objects wasn't registered as one of the spawned object",
                obj as UnityEngine.Object);

#if UNITY_EDITOR

            if (Application.isPlaying)
            {
                if (obj == null)
                {
                    UnityEngine.Debug.LogWarning("A null object was Despawned: make sure you don't destroy " +
                                                 "any pooled object in your logic as this would cause run time exceptions");
                    return;
                }
            }

#endif
            spawnedObjects.Remove(obj);
            pooledObjects.Add(obj);

            if (ParentDespawnedObjects)
                obj.transform.SetParent(DespawnedObjectsParent);

            obj.NotifyDeSpawn();
            LastObjectDeSpawned = obj;
            OnObjectDeSpawned.Invoke(obj);
        }

        public void DeSpawnFirstObjects(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                if (SpawnedObjects.Count == 0)
                {
                    return;
                }

                DeSpawnObject(SpawnedObjects[0]);
            }
        }

        public void Register(RespawnableComponent obj)
        {
            if (!spawnedObjects.Contains(obj))
            {
                spawnedObjects.Add(obj);
            }
        }

        /// <summary>
        ///     Regenerates the pool of objects if not already generated, does nothing otherwise
        /// </summary>
        public void GeneratePool()
        {
            if (pooledObjects != null)
            {
                return;
            }

            pooledObjects = new List<RespawnableComponent>(PoolSize);
            spawnedObjects = new List<RespawnableComponent>(PoolSize);

            // ObjectToSpawn.NotifyDeSpawn();

            for (int i = 0; i < pooledObjects.Capacity; i++)
            {
                AddObjectToPool();
            }
        }

        private void AddObjectToPool()
        {
            RespawnableComponent newObject = (RespawnableComponent) ObjectToSpawn.Clone();
            newObject.NotifyDeSpawn();

            if (AutoDestroyObjectWhenMaxPoolSizeReached && pooledObjects.Count == MaxPooledObjects)
            {
                newObject.Destroy();
            }
            else
            {
                pooledObjects.Add(newObject);
            }
        }

        public void RaisePoolCapacity(int amount)
        {
            // ObjectToSpawn.NotifyDeSpawn();

            for (int i = 0; i < amount; i++)
            {
                AddObjectToPool();
            }
        }

        private RespawnableComponent PoolTransfer()
        {
            if (pooledObjects == null || spawnedObjects == null)
            {
                GeneratePool();
            }

            if (pooledObjects.IsEmpty())
            {
                if (AutoRaisePoolSize)
                {
                    RaisePoolCapacity(1);
                }
                else
                {
                    DeSpawnObject(spawnedObjects[0]);
                }
            }

            RespawnableComponent toSpawn = pooledObjects.First();
            spawnedObjects.Add(toSpawn);
            pooledObjects.Remove(toSpawn);

            return toSpawn;
        }

        public void DeSpawnAll()
        {
            DeSpawnFirstObjects(SpawnedObjects.Count);
        }

        public RespawnableComponent Get(int ID)
        {
            return spawnedObjects[ID];
        }

        public void UnRegister(RespawnableComponent obj)
        {
            spawnedObjects.Remove(obj);
        }

        public object Save()
        {
            return (spawnedObjects.Count, pooledObjects.Count, SpawnedObjects.ConvertAll(_ => _.Save()));
        }

        public void Load(object saveFile)
        {
            if (saveFile == null)
            {
                DeSpawnAll();
                return;
            }
             

            (int, int, List<object>) counts = ((int, int, List<object>)) saveFile;

            DeSpawnAll();
            
            if (spawnedObjects.Count < counts.Item1)
            {
                SpawnFewObjects(counts.Item1 - spawnedObjects.Count);
            }

            if (pooledObjects.Count < counts.Item2)
            {
                RaisePoolCapacity(counts.Item2 - pooledObjects.Count);
            }

            for (var index = 0; index < spawnedObjects.Count; index++)
            {
                var spawnedObject = spawnedObjects[index];
                spawnedObject.Load(counts.Item3[index]);
            }
        }
    }
}