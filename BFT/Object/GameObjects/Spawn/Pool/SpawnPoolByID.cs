﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
#if UNITY_EDITOR

using Sirenix.OdinInspector.Editor;
using UnityEditor;

#endif

namespace BFT
{
    public class SpawnPoolByID : MonoBehaviour, ISavedObject
    {
        [BoxGroup("Options")] public bool DefaultAutoRaisePoolSize = true;
        [BoxGroup("Options")] public int DefaultPoolSize;


        [BoxGroup("Pools")] public bool InitializeAllPoolsOnAwake = false;
        [BoxGroup("Pools")] public bool InitializePoolsOnTheFly = true;

        [BoxGroup("ToSpawn"), SerializeField] protected RespawnableDictionaryAsset objectsToSpawn;

        [BoxGroup("Options")] public bool ParentDespawnedObjects;

        [ShowIf("ParentDespawnedObjects")] [BoxGroup("Options")]
        public Transform DespawnedObjectsParent;

        [BoxGroup("Pools")] public SpawnPool DefaultSpawnPool;
        [BoxGroup("Pools")] public SpawnPoolDictionary SpawnPools = new SpawnPoolDictionary();

        public RespawnableEvent OnObjectSpawned;
        public RespawnableEvent OnObjectDeSpawned;

        public virtual RespawnableDictionaryAsset ObjectsToSpawn
        {
            get => objectsToSpawn;
            set => objectsToSpawn = value;
        }

        public IEnumerable<int> Ids => SpawnPools.Keys;

        public virtual void Awake()
        {
            if (InitializeAllPoolsOnAwake)
            {
                foreach (int key in objectsToSpawn.Keys)
                {
                    InitPool(key);
                }
            }
        }

        public IEnumerable<RespawnableComponent> SpawnedObjects(int id)
        {
            return SpawnPools[id].SpawnedObjects;
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public RespawnableComponent SpawnObject(int id)
        {
            if (InitializePoolsOnTheFly)
            {
                InitPool(id);
            }

            RespawnableComponent spawned = SpawnPools[id].SpawnObject();
            OnObjectSpawned.Invoke(spawned);
            return spawned;
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public void DeSpawnObject(int id, RespawnableComponent obj)
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlayingOrWillChangePlaymode)
            {
                DestroyImmediate(obj.gameObject);
                return;
            }
#endif

            if (InitializePoolsOnTheFly)
            {
                InitPool(id);
            }

            SpawnPools[id].DeSpawnObject(obj);
            OnObjectDeSpawned.Invoke(obj);
        }

        public void DeSpawnFirstObjects(int id, int amount)
        {
            if (InitializePoolsOnTheFly)
            {
                InitPool(id);
            }

            SpawnPools[id].DeSpawnFirstObjects(amount);
        }

        public RespawnableComponent ObjectToSpawnDyID(int id)
        {
            return ObjectsToSpawn[id];
        }

        public void SetObjectToSpawnDyID(int id, RespawnableComponent obj)
        {
            ObjectsToSpawn[id] = obj;
        }


        public SpawnPool InitAndGetSpawnPool(int id)
        {
            InitPool(id);
            return SpawnPools[id];
        }

        public void InitPool(int id)
        {
            if (SpawnPools == null)
                SpawnPools = new SpawnPoolDictionary();

            if (!SpawnPools.ContainsKey(id) || SpawnPools[id] == null)
            {
                SpawnPool pool = Instantiate(DefaultSpawnPool, transform);
                pool.ObjectToSpawn = ObjectsToSpawn[id];
                pool.PoolSize = DefaultPoolSize;
                pool.AutoRaisePoolSize = DefaultAutoRaisePoolSize;
                pool.ParentDespawnedObjects = ParentDespawnedObjects;
                pool.DespawnedObjectsParent = DespawnedObjectsParent;
                pool.GeneratePool();
                SpawnPools.AddOrReplace(id, pool);
            }
            else
            {
                SpawnPools[id].ObjectToSpawn = ObjectsToSpawn[id];
            }
        }

        public void RegisterObject(int id, RespawnableComponent respawnable)
        {
            if (!SpawnPools.ContainsKey(id))
            {
                InitPool(id);
            }

            UnityEngine.Debug.Assert(!SpawnPools[id].PooledObjects.Contains(respawnable),
                "The object is already in the spawn pool");

            SpawnPools[id].Register(respawnable);
        }

        public void DespawnRespawnable(RespawnableComponent component)
        {
            DeSpawnObject(component.ID, component);
        }

        public void DespawnAll()
        {
            foreach (var spawnPool in SpawnPools)
            {
                spawnPool.Value.DeSpawnAll();
            }
        }

        private void OnDrawGizmos()
        {
            if (SpawnPools == null)
                return;

            foreach (var spawnPool in SpawnPools.Values)
            {
                if (spawnPool == null || spawnPool.SpawnedObjects == null || spawnPool.PooledObjects == null)
                    continue;

                if (spawnPool.SpawnedObjects.Any((_ => !_)) || spawnPool.PooledObjects.Any((_ => !_)))
                {
                    spawnPool.PooledObjects.RemoveAll(_ => _ == null);
                    spawnPool.SpawnedObjects.RemoveAll(_ => _ == null);
#if UNITY_EDITOR
                    OdinPrefabUtility.UpdatePrefabInstancePropertyModifications(this, false);
#endif
                }
            }
        }

        public object Save()
        {
            var dic = new Dictionary<int, object>();

            foreach (var pool in SpawnPools)
            {
                dic.Add(pool.Key, pool.Value.Save());
            }

            return dic;
        }

        public void Load(object saveFile)
        {
            if (saveFile == null)
            {
                foreach (var pool in SpawnPools)
                {
                    pool.Value.Load(null);
                }

                return;
            }

            var dicSave = (Dictionary<int, object>) saveFile;
            foreach (var o in dicSave)
            {
                InitPool(o.Key);
                SpawnPools[o.Key].Load(o.Value);
            }
        }
    }
}