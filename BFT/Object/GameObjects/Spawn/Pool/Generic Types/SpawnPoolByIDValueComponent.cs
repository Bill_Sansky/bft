using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class SpawnPoolByIDValueComponent : ValueComponent<SpawnPoolByID, SpawnPoolByIDValue>, ISavedObject
    {
        public bool RepositionSpawnedObject;
        [ShowIf("RepositionSpawnedObject")] public TransformValue SpawnPosition;

        private SpawnPool currentSpawnPool;

        public void Spawn(int id)
        {
            currentSpawnPool = Value.InitAndGetSpawnPool(id);

            if (RepositionSpawnedObject)
            {
                currentSpawnPool.OnBeforeObjectSpawned.AddListener(Reposition);
            }

            Value.SpawnObject(id);
        }

        private void Reposition(RespawnableComponent obj)
        {
            obj.transform.Copy(SpawnPosition.Value, true, true, false);
            currentSpawnPool.OnBeforeObjectSpawned.RemoveListener(Reposition);
        }

        public object Save()
        {
            return Value.Save();
        }

        public void Load(object saveFile)
        {
            Value.Load(saveFile);
        }
    }
}