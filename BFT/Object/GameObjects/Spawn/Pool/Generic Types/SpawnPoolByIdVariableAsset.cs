using BFT;

namespace DefaultNamespace
{
    public class SpawnPoolByIdVariableAsset : VariableAsset<SpawnPoolByID>
    {
        public void SpawnObjectOnID(int id)
        {
            Value.SpawnObject(id);
        }
        
    }
}