using System;
using BFT;
using Sirenix.OdinInspector;

namespace DefaultNamespace
{
    [Serializable]
    public class RespawnableTypeSpawner
    {
        public RespawnableDictionaryAsset RespawnableDictionary;
        public SpawnPoolByIDValue SpawnPool;
        
        [ValueDropdown("RespKeys")] public int RespawnableID;

        private ValueDropdownList<int> RespKeys => RespawnableDictionary ? RespawnableDictionary.ContentIDs : new ValueDropdownList<int>();

        public void RespawnObject()
        {
            SpawnPool.Value.SpawnObject(RespawnableID);
        }
        
    }
}