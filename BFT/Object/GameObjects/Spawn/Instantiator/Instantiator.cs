﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

#if UNITY_EDITOR

#endif

namespace BFT
{
    /// <summary>
    ///     a direct instantiation class that can spawn objects over time
    ///     and has utilities for the positioning and the parenting of spawned object. does not pool any objects
    /// </summary>
    public class Instantiator : MonoBehaviour
    {
        public delegate void GameObjectEvt(RespawnableComponent instantiatedObject);

        public enum SpawningPosition
        {
            SELF,
            OTHER
        }

        [FoldoutGroup("Status", false), ReadOnly]
        private readonly List<GameObject> instantiatedObjects = new List<GameObject>();

        [BoxGroup("Spawn")] public bool CopySpawnerRotation;

        [BoxGroup("Stop")] public bool DestroyInstancesOnDestroy;

        [BoxGroup("Stop")] public bool DestroyInstancesOnDisable;

        [BoxGroup("Regular Spawn"), ShowIf("SpawnOverTime")]
        public float InitialTimeBeforeSpawn = 0;

        [BoxGroup("Init")] public bool InstantiateOnAwake;

        [BoxGroup("Init")] public bool InstantiateOnEnable;

        [BoxGroup("Events")] public UnityEvent OnInstanceGoingToBeDestroyed;

        [BoxGroup("Events")] public UnityEvent OnInstantiateDone;
        [BoxGroup("Events")] public GameObjectEvent OnInstantiateDoneReturn ;
        [BoxGroup("Events"), HideIf("UnlimitedSpawning")]
        public UnityEvent OnMaxInstances;

        [BoxGroup("Spawn")] public bool ParentInstantiatedObjects;

        [BoxGroup("Spawn"), ShowIf("CanUseDifferentParent"), PreviouslySerializedAs("parentTransform")]
        public Transform ParentTransform;

        [BoxGroup("Spawn")] public bool SpawnAsAFirstSibiling = false;

        [BoxGroup("Regular Spawn")] public bool SpawnOverTime;

        [BoxGroup("Spawn"), PreviouslySerializedAs("SpawnType"),
         InfoBox("Position origin will be set to world as long as you won't set up Spawn Transform!",
             InfoMessageType.Warning, "SpawnTransformOriginCheck")]
        public SpawningPosition SpawnPositionType = SpawningPosition.SELF;

        [BoxGroup("Spawn")] public float SpawnRange = 0;

        [BoxGroup("Spawn"), ShowIf("IsSpawningWorld")]
        public Transform SpawnTransform;

        [BoxGroup("Regular Spawn"), ShowIf("SpawnOverTime")]
        public float TimeBetweenSpawns = 1;

        [BoxGroup("Spawn")] public GameObjectValue ToInstantiate;

        [BoxGroup("Spawn"), ShowIf("ParentInstantiatedObjects")]
        public bool UseDifferentParentTransform;

        protected bool IsSpawningWorld => SpawnPositionType == SpawningPosition.OTHER;

        protected bool IsSpawnTransform => SpawnTransform;

        protected bool SpawnTransformOriginCheck => SpawnPositionType == SpawningPosition.OTHER && !IsSpawnTransform;

        protected bool CanUseDifferentParent => ParentInstantiatedObjects && UseDifferentParentTransform;

        public List<GameObject> InstantiatedObjects => instantiatedObjects;

        public GameObject SpawnObject()
        {
            return Instantiate();
        }

        public void DeSpawnObject(GameObject obj)
        {
            if (InstantiatedObjects.Contains(obj))
            {
                InstantiatedObjects.Remove(obj);
                OnInstanceGoingToBeDestroyed.Invoke();
                Destroy(gameObject);
            }
        }

        public void DeSpawnFirstObjects(int amount)
        {
            for (int i = 0; i < amount; i++)
            {
                if (instantiatedObjects.Count > 0)
                    Destroy(instantiatedObjects[0]);
            }
        }

        public void Register(GameObject obj)
        {
            instantiatedObjects.Add(obj);
        }

        public void Reset()
        {
            SpawnTransform = transform;
        }

        public void Awake()
        {
            if (InstantiateOnAwake)
                Instantiate();

            if (SpawnTransform == null && SpawnPositionType == SpawningPosition.OTHER)
            {
                SpawnPositionType = SpawningPosition.OTHER;
            }
        }

        public void OnEnable()
        {
            if (InstantiateOnEnable)
                Instantiate();
            if (SpawnOverTime)
            {
                StopAllCoroutines();
                StartCoroutine(SpawnObjectOverTime());
            }

            RefreshCheckForDestroyedObjects();
        }

        private void RefreshCheckForDestroyedObjects()
        {
            if (checkForDestroyedObjectsCoroutine != null)
            {
                StopCoroutine(checkForDestroyedObjectsCoroutine);
                checkForDestroyedObjectsCoroutine = null;
            }

            if (RefillDestroyedObjects)
            {
                StartCoroutine(checkForDestroyedObjectsCoroutine = CheckForDestroyedObjects());
            }
        }

        private IEnumerator SpawnObjectOverTime()
        {
            var elapsed = 0f;
            while (elapsed < InitialTimeBeforeSpawn)
            {
                yield return null;
                elapsed += UnityEngine.Time.deltaTime;
            }

            while (SpawnOverTime && (AbleToSpawn || RefillDestroyedObjects))
            {
                Instantiate();
                elapsed = 0f;
                yield return null;
                while (elapsed < TimeBetweenSpawns)
                {
                    elapsed += UnityEngine.Time.deltaTime;

                    yield return null;
                }
            }
        }

        private IEnumerator CheckForDestroyedObjects()
        {
            while (true)
            {
                if (!UnlimitedSpawning && RefillDestroyedObjects)
                {
                    for (int i = instantiatedObjects.Count - 1; i >= 0; i--)
                    {
                        if (instantiatedObjects[i] == null)
                            instantiatedObjects.RemoveAt(i);
                    }
                }

                yield return new WaitForSeconds(CheckForDestroyedObjectsTime);
            }
        }

        public void OnDisable()
        {
            if (DestroyInstancesOnDisable)
                DestroySpawnedObject();
        }

        public void OnDestroy()
        {
            if (DestroyInstancesOnDestroy)
            {
                DestroySpawnedObject();
            }
        }

        [BoxGroup("Spawn")]
        [Button(ButtonSizes.Medium)]
        public void DestroySpawnedObject()
        {
            foreach (GameObject o in instantiatedObjects)
            {
                if (o)
                {
                    OnInstanceGoingToBeDestroyed.Invoke();
                    Destroy(o);
                }
            }

            instantiatedObjects.Clear();
        }

        public void InstantiateNoReturn()
        {
            Instantiate();
        }


        [BoxGroup("Spawn")]
        [Button(ButtonSizes.Medium)]
        public void DestroyPreviousAndInstantiate()
        {
            DespawnAll();
            Instantiate();
        }

        [BoxGroup("Spawn")]
        [Button(ButtonSizes.Medium)]
        public GameObject Instantiate()
        {
            if (!AbleToSpawn)
                return null;

#if UNITY_EDITOR
            if (EditorInstantiate()) return null;
#endif
            GameObject go;
            if (ParentInstantiatedObjects)
            {
                if (UseDifferentParentTransform && ParentTransform)
                    go = Instantiate(ToInstantiate.Value, ParentTransform);
                else
                    go = Instantiate(ToInstantiate.Value,
                        (SpawnPositionType == SpawningPosition.OTHER && SpawnTransform) ? SpawnTransform : transform);
            }
            else
            {
                go = Instantiate(ToInstantiate.Value);
            }

            Vector3 pos = new Vector2();
            switch (SpawnPositionType)
            {
                case SpawningPosition.OTHER:
                    pos = SpawnTransform.position;
                    if (CopySpawnerRotation)
                        go.transform.rotation = SpawnTransform.rotation;
                    break;
                case SpawningPosition.SELF:
                    pos = transform.position;
                    if (CopySpawnerRotation)
                        go.transform.rotation = transform.rotation;
                    break;
            }

            if (SpawnRange > 0)
            {
                Vector2 pointInRange = Random.insideUnitCircle * SpawnRange;
                pos.x += pointInRange.x;
                pos.z += pointInRange.y;
            }

            go.transform.position = pos;

            if (SpawnAsAFirstSibiling)
                go.transform.SetAsFirstSibling();

            instantiatedObjects.Add(go);

            OnInstantiateDone.Invoke();
            OnInstantiateDoneReturn.Invoke(go);
            
            if (!AbleToSpawn)
            {
                OnMaxInstances.ExtendedInvoke(this);
            }

            if (SpawnOverTime && TimeBetweenSpawns <= 0 && !UnlimitedSpawning)
            {
                return Instantiate();
            }

            return go;
        }

#if UNITY_EDITOR

        private bool EditorInstantiate()
        {
            if (!Application.isPlaying)
            {
                if (PrefabUtility.GetPrefabAssetType(this) != PrefabAssetType.NotAPrefab ||
                    PrefabUtility.GetPrefabInstanceStatus(this) != PrefabInstanceStatus.NotAPrefab)
                {
                    GameObject obj =
                        (GameObject) PrefabUtility.InstantiatePrefab(ToInstantiate.Value, gameObject.scene);
                    if (SpawnTransform)
                    {
                        if (!ParentInstantiatedObjects)
                            obj.transform.CopyPositionRotation(SpawnTransform);
                        else
                        {
                            obj.transform.SetParent(SpawnTransform);
                            obj.transform.localPosition = Vector3.zero;
                        }
                    }

                    return true;
                }
            }

            return false;
        }
#endif

        public GameObject SpawnObject(Transform parent)
        {
            GameObject go = Instantiate();
            go.transform.SetParent(parent);
            return go;
        }

        public GameObject SpawnObject(Vector3 position, Transform parent)
        {
            GameObject go = Instantiate();
            go.transform.SetParent(parent);
            go.transform.position = position;
            return go;
        }

        public GameObject SpawnObject(Vector3 position, Quaternion rotation, UnityEngine.Transform parent)
        {
            GameObject go = Instantiate();
            go.transform.SetParent(parent);
            go.transform.position = position;
            go.transform.rotation = rotation;
            return go;
        }

        public void DespawnAll()
        {
            foreach (var component in InstantiatedObjects)
            {
                OnInstanceGoingToBeDestroyed.Invoke();
                Destroy(component.gameObject);
            }

            InstantiatedObjects.Clear();
        }

        public RespawnableComponent Get(int ID)
        {
            throw new NotImplementedException();
        }

        public void UnRegister(GameObject obj)
        {
            InstantiatedObjects.Remove(obj);
        }

        #region SPAWN LIMITATION

        [BoxGroup("Spawn Limitations")] public bool UnlimitedSpawning = true;

        [BoxGroup("Spawn Limitations"), HideIf("UnlimitedSpawning")]
        public IntValue MaxSpawnedObjects;

        [SerializeField, BoxGroup("Spawn Limitations"), HideIf("UnlimitedSpawning"),
         OnValueChanged("RefreshCheckForDestroyedObjects")]
        private bool refillDestroyedObjects = false;

        [SerializeField, BoxGroup("Spawn Limitations"), ShowIf("CheckForDestroyedObjectsTimeInspectorCondition")]
        public float CheckForDestroyedObjectsTime = 1f;

        private IEnumerator checkForDestroyedObjectsCoroutine;

        public bool RefillDestroyedObjects
        {
            get => refillDestroyedObjects;
            set
            {
                refillDestroyedObjects = value;
                RefreshCheckForDestroyedObjects();
            }
        }

        private bool CheckForDestroyedObjectsTimeInspectorCondition => !UnlimitedSpawning && RefillDestroyedObjects;

        public bool AbleToSpawn => UnlimitedSpawning || instantiatedObjects.Count < MaxSpawnedObjects.Value;

        #endregion
    }
}