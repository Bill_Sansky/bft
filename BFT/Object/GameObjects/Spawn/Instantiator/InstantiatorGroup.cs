﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    public class InstantiatorGroup : MonoBehaviour
    {
        public List<Instantiator> Instantiators = new List<Instantiator>();

        public bool IsAnyInstatiatorReady
        {
            get
            {
                foreach (var instantiator in Instantiators)
                {
                    if (instantiator.AbleToSpawn)
                        return true;
                }

                return false;
            }
        }

        public void UseFirstAvailable()
        {
            foreach (var instantiator in Instantiators)
            {
                if (instantiator.AbleToSpawn)
                {
                    instantiator.Instantiate();
                    return;
                }
            }
        }

        public void UseAllAvailable()
        {
            foreach (var instantiator in Instantiators)
            {
                if (instantiator.AbleToSpawn)
                {
                    instantiator.Instantiate();
                }
            }
        }
    }
}
