﻿using UnityEngine;

namespace BFT
{
    /// <summary>
    ///     Contains Respawnable and associates an ID to them
    /// </summary>
    public class RespawnableDictionaryAsset : DictionaryAsset<RespawnableComponent, RespawnableComponent>
    {
        public override RespawnableComponent CreateNewDataHolder(object data)
        {
            GameObject go = new GameObject("Respawnable");
            return go.AddComponent<RespawnableComponent>();
        }
    }
}
