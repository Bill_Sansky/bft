﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    /// <summary>
    ///     Can Respawn objects from a list of objects who know their own IDs
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Respawner: MonoBehaviour
    {
        [BoxGroup("Status")]
        public RespawnableListDictionary CurrentObjects;

        [NonSerialized] public bool IsInitialized = false;

        public bool RegisterCurrentObjects = true;

        [SerializeField, BoxGroup("Spawning")]
        protected SpawnPoolByID RespawnPools;

        public virtual void OnEnable()
        {
            if (!IsInitialized)
            {
                if (RegisterCurrentObjects)
                {
                    RespawnPools.OnObjectSpawned.AddListener(AddCurrentObject);
                    RespawnPools.OnObjectDeSpawned.AddListener(RemoveCurrentObject);
                }

                IsInitialized = true;
            }
        }

        public virtual void OnDisable()
        {
            if (RegisterCurrentObjects)
            {
                RespawnPools.OnObjectSpawned.RemoveListener(AddCurrentObject);
                RespawnPools.OnObjectDeSpawned.RemoveListener(RemoveCurrentObject);
            }

            IsInitialized = false;
        }

        public RespawnableComponent Respawn(int id)
        {
            return RespawnPools.SpawnObject(id);
        }

        public void RegisterObject(int id, RespawnableComponent respawnable)
        {
            RespawnPools.RegisterObject(id, respawnable);
            respawnable.Spawner = RespawnPools.SpawnPools[id];
            if (RegisterCurrentObjects)
                CurrentObjects.Add(id, respawnable);
        }

        public void Despawn(RespawnableComponent component)
        {
            RespawnPools.DespawnRespawnable(component);
        }
        
        protected virtual void RemoveCurrentObject(RespawnableComponent toRemove)
        {
            CurrentObjects.Remove(toRemove.ID, toRemove);
        }

        protected virtual void AddCurrentObject(RespawnableComponent toAdd)
        {
            CurrentObjects.Add(toAdd.ID, toAdd);
        }
    }
}
