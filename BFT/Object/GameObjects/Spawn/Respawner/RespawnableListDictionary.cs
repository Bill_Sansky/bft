﻿namespace BFT
{
    /// <summary>
    ///     contains lists of respawnable objects ordered by their dictionary ID
    /// </summary>
    public class RespawnableListDictionary : ListDictionaryComponent<int, RespawnableComponent>,
        IContainer<RespawnableComponent>
    {
        public RespawnableEvent OnRespawnableRegistered;
        public RespawnableEvent OnRespawnableUnRegistered;

        public virtual void Add(RespawnableComponent component)
        {
            Add(component.ID, component);
            OnRespawnableRegistered.Invoke(component);
        }

        public virtual void Remove(RespawnableComponent component)
        {
            Remove(component.ID, component);
            OnRespawnableUnRegistered.Invoke(component);
        }
    }
}
