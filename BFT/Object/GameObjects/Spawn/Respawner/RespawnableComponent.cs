﻿using System;
using BFT.Generics;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace BFT
{
    /// <summary>
    ///     A respawnable object that must have an ID from a dictionary in order to be respanable by a respawner
    /// </summary>
    public class RespawnableComponent : DictionaryEntryComponent<RespawnableComponent>, ISavedObject
    {
        [Tooltip("If true, the object will get deactivated when despawned, and activated when spawned")]
        [FormerlySerializedAs("SpawnSyncWithActivation")]
        [BoxGroup("Spawn Option")]
        public bool ActivationSyncedWithSpawn = true;

        [BoxGroup("Save")] public SavedInterfaceObjectReferenceReference RespawnableSave;

        [BoxGroup("Events")] public UnityEvent OnDespawned;

        [BoxGroup("Events")] public UnityEvent OnSpawned;

        [BoxGroup("Status")] [ShowInInspector, ReadOnly]
        private SpawnPool spawner;

        public override RespawnableComponent Data
        {
            get => this;
            set
            {
                //not allowed
            }
        }

        public SpawnPool Spawner
        {
            get => spawner;
            set => spawner = value;
        }

        [FoldoutGroup("Tools"), Button(ButtonSizes.Medium)]
        public virtual void NotifySpawn(SpawnPool spawner)
        {
            if (ActivationSyncedWithSpawn)
                gameObject.SetActive(true);
            this.spawner = spawner;
            OnSpawned.Invoke();
        }

        public virtual void NotifyDeSpawn()
        {
            if (gameObject && ActivationSyncedWithSpawn)
                gameObject.SetActive(false);

            OnDespawned.Invoke();
        }

        public RespawnableComponent Clone()
        {
#if UNITY_EDITOR

            RespawnableComponent comp;

            if ( !EditorApplication.isPlayingOrWillChangePlaymode && PrefabUtility.IsPartOfPrefabAsset(this))
            {
                comp = (RespawnableComponent) PrefabUtility.InstantiatePrefab(this);
            }
            else
            {
                comp = Instantiate(this);
            }

            comp.gameObject.name = name;
            return comp;
#else
        return Instantiate(this);
#endif
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }

        [FoldoutGroup("Tools"), Button(ButtonSizes.Medium)]
        public void Despawn()
        {
            if (Spawner != null)
            {
                Spawner.DeSpawnObject(this);
            }
            else
            {
                Destroy(gameObject);
                OnDespawned.Invoke();
            }
        }

        public override JsonData ExportJsonData()
        {
            throw new System.NotImplementedException();
        }

        public override void ParseJsonData(JsonData data)
        {
            throw new System.NotImplementedException();
        }

        public override void NotifyJSonDataDeleteRequest()
        {
            throw new System.NotImplementedException();
        }

        public object Save()
        {
            return new[]
            {
                transform.position.SerializedForm(), transform.rotation.SerializedForm(),
                RespawnableSave.Reference ? RespawnableSave.Value.Save() : null
            };
        }

        public void Load(object saveFile)
        {
            if (saveFile == null)
                return;
            var objs = (object[]) saveFile;
            transform.position.FromSerialized(objs[0]);
            transform.rotation.FromSerialized(objs[1]);
            if (RespawnableSave.Reference)
                RespawnableSave.Value.Load(objs[2]);
        }
    }

    [Serializable]
    public class RespawnableEvent : UnityEvent<RespawnableComponent>
    {
    }
}