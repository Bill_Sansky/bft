﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class PrefabUpdater : MonoBehaviour, IVariable<GameObject>
    {
        [BoxGroup("Options")] public bool AutoUpdate;

        [BoxGroup("References"), OnValueChanged("CheckIfSelf")]
        public IValue<GameObject> GameObjectReference;

        [SerializeField, ReadOnly] private GameObject instance;

        [BoxGroup("Options"), SerializeField] private bool normalizeScale = true;

        [SerializeField] private GameObjectEvent OnInstanceChanged;

        [SerializeField] private GameObjectEvent OnValueChange;

        [BoxGroup("References")] public UnityEngine.Transform Parent;

        [BoxGroup("Options")] public bool UpdateOnEnable;

        public GameObject Value
        {
            get => GameObjectReference.Value;

            set
            {
                IVariable<GameObject> variable = GameObjectReference as IVariable<GameObject>;
                if (variable != null)
                {
                    variable.Value = value;
                }

                OnValueChange.Invoke(value);
            }
        }

        private void CheckIfSelf()
        {
            if (GameObjectReference.Equals(this))
            {
                UnityEngine.Debug.LogWarning("You cannot reference an image updater to itself to avoid Stack Overflows", this);
                GameObjectReference = null;
            }
        }

        [BoxGroup("Options"), Button(ButtonSizes.Medium)]
        public void UpdateGameObject()
        {
            if (instance != null)
            {
                instance.SetActive(false);
                Destroy(instance);
            }

            if (GameObjectReference != null)
            {
                instance = Instantiate(GameObjectReference.Value, Parent);
                instance.transform.localPosition = Vector3.zero;
                instance.transform.localRotation = Quaternion.identity;
                if (normalizeScale)
                    instance.transform.localScale = Vector3.one;
                OnInstanceChanged.Invoke(instance);
            }
        }

        [OnInspectorGUI]
        private void EditorUpdate()
        {
            if (!Application.isPlaying && AutoUpdate)
                UpdateGameObject();
        }

        public void Reset()
        {
            Parent = transform;
        }

        public void OnEnable()
        {
            if (UpdateOnEnable)
                UpdateGameObject();
            if (AutoUpdate)
                StartCoroutine(UpdateRegularly());
        }

        public void OnDisable()
        {
            StopAllCoroutines();
        }

        private IEnumerator UpdateRegularly()
        {
            while (true)
            {
                UpdateGameObject();
                yield return null;
            }
        }
    }
}
