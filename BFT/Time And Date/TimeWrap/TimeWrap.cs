﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Time/Time Wrap")]
    public class TimeWrap : MonoBehaviour
    {
        public delegate void WrapAction();

        private static float? originalFixedUpdate;

#if UNITY_EDITOR

        [FoldoutGroup("Debug")] public bool DebugMessages;

#endif
        [TitleGroup("Transition")] public Timer EndTransitionTimer;

        [TitleGroup("Transition"), ShowInInspector, ReadOnly]
        private bool starting = false;

        [TitleGroup("Transition")] public Timer StartTransitionTimer;

        [TitleGroup("Transition"), ShowInInspector, ReadOnly]
        private bool stopping = false;

        [TitleGroup("Status")] public float TimeScale;

        private bool wrapping;

        [TitleGroup("Status"), ShowInInspector, ReadOnly]
        public float ActualTimeScale => UnityEngine.Time.timeScale;

        public float OriginalFixedUpdate
        {
            get
            {
                InitOriginalFixedUpdate();
                return originalFixedUpdate.Value;
            }
        }

        [TitleGroup("Status"), ShowInInspector, ReadOnly]
        public bool IsWrapping => wrapping;

        public event WrapAction OnWrapFullStarted;
        public event WrapAction OnWrapFullStopped;

        private void InitOriginalFixedUpdate()
        {
            if (!originalFixedUpdate.HasValue)
            {
                originalFixedUpdate = UnityEngine.Time.fixedDeltaTime;
            }
        }

        void Awake()
        {
            InitOriginalFixedUpdate();

            if (StartTransitionTimer)
                StartTransitionTimer.OnTimerDone.AddClean(InvokeStartingWrapEvents);
            if (EndTransitionTimer)
                EndTransitionTimer.OnTimerDone.AddClean(InvokeEndingWrapEvent);
        }

        [Button]
        public void StartWrap()
        {
            starting = true;
            wrapping = true;

            if (stopping)
            {
                stopping = false;
                if (StartTransitionTimer)
                    StartTransitionTimer.StartTimer(StartTransitionTimer.Duration - EndTransitionTimer.CurrentTime);
                else
                {
                    InvokeStartingWrapEvents();
                }

                if (EndTransitionTimer)
                    EndTransitionTimer.StopTimer();
            }
            else
            {
                if (StartTransitionTimer)
                    StartTransitionTimer.StartTimer();
                else
                {
                    InvokeStartingWrapEvents();
                }
            }
        }

        [Button]
        public void ImmediatelyWrap()
        {
            starting = true;
            InvokeStartingWrapEvents();
        }

        [Button]
        public void StopWrap()
        {
            stopping = true;

            if (starting)
            {
                starting = false;
                if (EndTransitionTimer)
                    EndTransitionTimer.StartTimer(EndTransitionTimer.Duration - StartTransitionTimer.CurrentTime);
                else
                {
                    InvokeEndingWrapEvent();
                }

                if (StartTransitionTimer)
                    StartTransitionTimer.StopTimer();
            }
            else
            {
                if (EndTransitionTimer)
                    EndTransitionTimer.StartTimer();
                else
                {
                    InvokeEndingWrapEvent();
                }
            }
        }

        public void BreakWrap()
        {
            stopping = true;

            if (starting)
            {
                starting = false;
                if (StartTransitionTimer)
                    StartTransitionTimer.StopTimer();
            }

            InvokeEndingWrapEvent();
        }

        private void InvokeStartingWrapEvents()
        {
            if (starting)
            {
                starting = false;
                UnityEngine.Time.timeScale = TimeScale;

                UnityEngine.Time.fixedDeltaTime = OriginalFixedUpdate * UnityEngine.Time.timeScale;
#if UNITY_EDITOR
                if (DebugMessages)
                    UnityEngine.Debug.Log("Transition To Start Wrap Done");
#endif

                if (OnWrapFullStarted != null) OnWrapFullStarted();
            }
        }

        private void InvokeEndingWrapEvent()
        {
            if (stopping)
            {
                stopping = false;
                wrapping = false;
                UnityEngine.Time.timeScale = 1;

                UnityEngine.Time.fixedDeltaTime = OriginalFixedUpdate;

#if UNITY_EDITOR
                if (DebugMessages)
                    UnityEngine.Debug.Log("Transition To End Wrap Done");
#endif
                if (OnWrapFullStopped != null) OnWrapFullStopped();
            }
        }

        void Update()
        {
            if (starting)
            {
                if (StartTransitionTimer && !StartTransitionTimer.IsDone)
                {
                    UnityEngine.Time.timeScale = Mathf.Lerp(1, TimeScale, StartTransitionTimer.Percent);
                    UnityEngine.Time.fixedDeltaTime = OriginalFixedUpdate * UnityEngine.Time.timeScale;
                }
            }
            else if (stopping)
            {
                if (EndTransitionTimer && !EndTransitionTimer.IsDone)
                {
                    UnityEngine.Time.timeScale = Mathf.Lerp(TimeScale, 1, EndTransitionTimer.Percent);
                    UnityEngine.Time.fixedDeltaTime = OriginalFixedUpdate * UnityEngine.Time.timeScale;
                }
            }
        }

        private void OnDestroy()
        {
            UnityEngine.Time.fixedDeltaTime = OriginalFixedUpdate;
        }
    }
}
