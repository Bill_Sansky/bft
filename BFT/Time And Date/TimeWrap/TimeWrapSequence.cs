﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    [RequireComponent(typeof(TimeWrap))]
    [AddComponentMenu("Time/Time Wrap Sequence")]
    public class TimeWrapSequence : MonoBehaviour, IValue<float>
    {
        private bool currentlyWrapping;

        public bool DebugMessages;
        public UnityEvent OnWrapEnd;

        public UnityEvent OnWrapStart;

        public bool StartOnAwake;
        public float TimeBetweenWraps;

        private int wrapCount = 0;
        public TimeWrap Wrapper;
        public float WrapTime;

        public bool IsWrapping => Wrapper.IsWrapping;

        [SerializeField, ShowInInspector]
        public bool InfiniteWrap
        {
            get => wrapCount == -1;
            set
            {
                if (value)
                    wrapCount = -1;
                else
                {
                    wrapCount = 0;
                }
            }
        }


        public float WrappedWrapTime => WrapTime * Wrapper.TimeScale;

        public float Percent { get; private set; }

        public float Value => Percent;

        // Use this for initialization
        void Awake()
        {
            Wrapper.OnWrapFullStarted += () => { StartCoroutine(StopToWrapAfterTime()); };

            Wrapper.OnWrapFullStopped += () => { StartCoroutine(WaitAndStartWrap()); };

            if (StartOnAwake)
                Wrapper.StartWrap();
        }

        [Button]
        public void LoopWraps()
        {
            Wrapper.StartWrap();
        }

        [Button]
        public void WrapOnce()
        {
            Wrap(1);
        }

        public void Wrap(int amount)
        {
            UnityEngine.Debug.Assert(amount > 0);

            if (DebugMessages)
                UnityEngine.Debug.Log("Wrapping " + amount + " times more", this);

            wrapCount += amount;
            if (!Wrapper.IsWrapping)
                Wrapper.StartWrap();
        }

        [Button]
        public void ForceStopWrap()
        {
            if (DebugMessages)
                UnityEngine.Debug.Log("Wrapping Stopped", this);
            if (currentlyWrapping)
            {
                StopAllCoroutines();
                Wrapper.StopWrap();
                currentlyWrapping = false;
                OnWrapEnd.Invoke();
            }
        }

        private IEnumerator StopToWrapAfterTime()
        {
#if UNITY_EDITOR
            if (DebugMessages)
                UnityEngine.Debug.Log("Waiting for end of time Wrap", this);
#endif
            Percent = 0;
            OnWrapStart.Invoke();
            currentlyWrapping = true;
            yield return new WaitForSecondsRealtime(WrapTime);
            currentlyWrapping = false;
            Wrapper.StopWrap();
            OnWrapEnd.Invoke();

#if UNITY_EDITOR
            if (DebugMessages)
                UnityEngine.Debug.Log("Wrapping Done", this);
#endif
        }

        private IEnumerator WaitAndStartWrap()
        {
#if UNITY_EDITOR
            if (DebugMessages)
                UnityEngine.Debug.Log("Waiting until next Wrap", this);
#endif
            if (wrapCount != -1)
            {
                wrapCount = Mathf.Clamp(wrapCount, 0, wrapCount - 1);
            }

            if (wrapCount == 0)
            {
                yield return 0;
            }

            yield return new WaitForSeconds(TimeBetweenWraps);
            Wrapper.StartWrap();

#if UNITY_EDITOR
            if (DebugMessages)
                UnityEngine.Debug.Log("Wrapping just Started", this);
#endif
        }

        void Update()
        {
            if (currentlyWrapping)
            {
                Percent += UnityEngine.Time.unscaledDeltaTime / WrapTime;
            }
        }
    }
}
