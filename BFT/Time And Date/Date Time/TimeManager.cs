﻿using System;
using System.Collections.Generic;
using UnityEngine.Serialization;

namespace BFT
{
    public class TimeManager : SingletonSO<TimeManager>, ITimeTicker, ISavedObject
    {
        public List<List<DurationWithCallBack>> CallBacksFromSecondToYears = new List<List<DurationWithCallBack>>(6);

        public TimeDateDurationAsset StartingTimeAndDate;

        public TimeAndDate TimeAndDate;
        public static TimeAndDate Time => Instance.TimeAndDate;

        public object Save()
        {
            return TimeAndDate.Save();
        }

        public void Load(object saveFile)
        {
            if (saveFile == null)
            {
                UnityEngine.Debug.Assert(StartingTimeAndDate != null,
                    "The starting time and date wasn't define: you need to define one in order to load a null save file",
                    this);
                TimeAndDate.CopyFromTimeStamp(StartingTimeAndDate.DateDuration);
                return;
            }

            TimeAndDate.Load(saveFile);
        }

        public void Tick(float deltaTime)
        {
            TimeAndDate.Tick(deltaTime);
        }

        public void Awake()
        {
            while (CallBacksFromSecondToYears.Count < 6)
            {
                CallBacksFromSecondToYears.Add(new List<DurationWithCallBack>());
            }
        }

        public void OnEnable()
        {
            TimeAndDate.OnSecondChanged.AddListener(CheckSecondBoundCallBacks);
            TimeAndDate.OnMinuteChanged.AddListener(CheckMinuteBoundCallBacks);
            TimeAndDate.OnHourChanged.AddListener(CheckHourBoundCallBacks);
            TimeAndDate.OnDayChanged.AddListener(CheckDayBoundCallBacks);
            TimeAndDate.OnMonthChanged.AddListener(CheckMonthBoundCallBacks);
            TimeAndDate.OnYearChanged.AddListener(CheckYearBoundCallBacks);
        }

        public void AddCallBack(TimeDateDuration timeStamp, ITimeStampCallBack callback)
        {
            if (timeStamp.TimeStampYearDifference(TimeAndDate) > 0)
            {
                CallBacksFromSecondToYears[5].Add(new DurationWithCallBack(timeStamp, callback));
            }
            else if (timeStamp.TimeStampMonthDifference(TimeAndDate) > 0)
            {
                CallBacksFromSecondToYears[4].Add(new DurationWithCallBack(timeStamp, callback));
            }
            else if (timeStamp.TimeStampDaysDifference(TimeAndDate) > 0)
            {
                CallBacksFromSecondToYears[3].Add(new DurationWithCallBack(timeStamp, callback));
            }
            else if (timeStamp.TimeStampHoursDifference(TimeAndDate) > 0)
            {
                CallBacksFromSecondToYears[2].Add(new DurationWithCallBack(timeStamp, callback));
            }
            else if (timeStamp.TimeStampMinuteDifference(TimeAndDate) > 0)
            {
                CallBacksFromSecondToYears[1].Add(new DurationWithCallBack(timeStamp, callback));
            }
            else if (timeStamp.TimeStampSecondDifference(TimeAndDate) > 0)
            {
                CallBacksFromSecondToYears[0].Add(new DurationWithCallBack(timeStamp, callback));
            }
            else
            {
                callback.NotifyTimeStampReached();
            }
        }

        public void AddCallBack(TimeDateDuration timeStamp, System.Action callback)
        {
            if (timeStamp.TimeStampYearDifference(TimeAndDate) > 0)
            {
                CallBacksFromSecondToYears[5].Add(new DurationWithCallBack(timeStamp, callback));
            }
            else if (timeStamp.TimeStampMonthDifference(TimeAndDate) > 0)
            {
                CallBacksFromSecondToYears[4].Add(new DurationWithCallBack(timeStamp, callback));
            }
            else if (timeStamp.TimeStampDaysDifference(TimeAndDate) > 0)
            {
                CallBacksFromSecondToYears[3].Add(new DurationWithCallBack(timeStamp, callback));
            }
            else if (timeStamp.TimeStampHoursDifference(TimeAndDate) > 0)
            {
                CallBacksFromSecondToYears[2].Add(new DurationWithCallBack(timeStamp, callback));
            }
            else if (timeStamp.TimeStampMinuteDifference(TimeAndDate) > 0)
            {
                CallBacksFromSecondToYears[1].Add(new DurationWithCallBack(timeStamp, callback));
            }
            else if (timeStamp.TimeStampSecondDifference(TimeAndDate) > 0)
            {
                CallBacksFromSecondToYears[0].Add(new DurationWithCallBack(timeStamp, callback));
            }
            else
            {
                UnityEngine.Debug.Log("The time stamp is now or in the past, callback called right away", this);
                callback();
            }
        }

        private void CheckCallbackTime(int id, TickMode mode)
        {
            if (CallBacksFromSecondToYears.Count <= id)
                return;

            List<DurationWithCallBack> callbacks = CallBacksFromSecondToYears[id];
            for (var i = callbacks.Count - 1; i >= 0; i--)
            {
                var callBack = callbacks[i];
                if (callBack.TimeStamp.IsTimeStampNow(TimeAndDate))
                {
                    callbacks.Remove(callBack);
                    callBack.Call();
                }
                else if (callBack.TimeStamp.TimeStampDifference(TimeAndDate, mode) <= 0)
                {
                    //then downgrade the difference to the more frequent time scale
                    callbacks.Remove(callBack);
                    if (id - 1 >= 0)
                        CallBacksFromSecondToYears[id - 1].Add(callBack);
                    else
                    {
                        callBack.Call();
                    }
                }
            }
        }

        public void CheckSecondBoundCallBacks()
        {
            CheckCallbackTime(0, TickMode.SECONDS);
        }

        public void CheckMinuteBoundCallBacks()
        {
            CheckCallbackTime(1, TickMode.MINUTES);
        }

        public void CheckHourBoundCallBacks()
        {
            CheckCallbackTime(1, TickMode.HOURS);
        }

        public void CheckDayBoundCallBacks()
        {
            CheckCallbackTime(1, TickMode.DAYS);
        }

        public void CheckMonthBoundCallBacks()
        {
            CheckCallbackTime(1, TickMode.MONTHS);
        }

        public void CheckYearBoundCallBacks()
        {
            CheckCallbackTime(1, TickMode.YEARS);
        }

        [Serializable]
        public class DurationWithCallBack
        {
            public System.Action Callback;
            [FormerlySerializedAs("Duration")] public TimeDateDuration TimeStamp;
            public ITimeStampCallBack TimeStampCallBack;

            public DurationWithCallBack(TimeDateDuration timeStamp, System.Action callback)
            {
                TimeStamp = timeStamp;
                Callback = callback;
            }

            public DurationWithCallBack(TimeDateDuration timeStamp, ITimeStampCallBack callback)
            {
                TimeStamp = timeStamp;
                TimeStampCallBack = callback;
            }

            public void Call()
            {
                Callback?.Invoke();
                TimeStampCallBack?.NotifyTimeStampReached();
            }
        }
    }
}
