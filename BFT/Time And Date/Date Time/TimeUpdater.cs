﻿using Sirenix.OdinInspector;
using UnityEngine.Serialization;

namespace BFT
{
    public class TimeUpdater : SerializedMonoBehaviour
    {
        private float currentTime;
        public ITimeTicker Ticker;

        [FormerlySerializedAs("TickFrequency")]
        public float TickTimeInterval = 0.1f;

        public float TimeScale = 1;

        public void Update()
        {
            currentTime += UnityEngine.Time.deltaTime;

            if (currentTime >= TickTimeInterval)
            {
                Ticker.Tick(currentTime * TimeScale);
                currentTime = 0;
            }
        }
    }
}
