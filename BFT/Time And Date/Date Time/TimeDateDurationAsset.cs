﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class TimeDateDurationAsset : ScriptableObject, ISavedObject
    {
        [ShowIf("UseTimeStampCallBack")] public bool AutoRegisterCallbackOnLoad = true;
        [ShowIf("UseTimeStampCallBack")] public bool AutoRegisterCallOnEnable = false;

        public UnityEvent OnTimeStampNowCallback;
        [SerializeField] private TimeDateDuration timeDateDuration = new TimeDateDuration();

        private TimeDateDuration timeStampUntilCallback;
        [ShowIf("UseTimeStampCallBack")] public bool UseTimeStampAsDurationFromCallBackRegister = false;

        public bool UseTimeStampCallBack = false;

        public TimeDateDuration DateDuration => timeDateDuration;

        public object Save()
        {
            if (!UseTimeStampCallBack)
                return timeDateDuration.Save();

            return (timeDateDuration.Save(), timeStampUntilCallback?.Save() ?? null);
        }

        public void Load(object saveFile)
        {
            if (saveFile == null)
            {
                timeDateDuration.Load(null);
                timeStampUntilCallback = null;

                if (UseTimeStampCallBack && AutoRegisterCallbackOnLoad)
                {
                    AutoRegister();
                }
            }
            else
            {
                if (!UseTimeStampCallBack)
                    timeDateDuration.Load(saveFile);
                else
                {
                    var tuple = ((object, object)) saveFile;
                    timeDateDuration.Load(tuple.Item1);
                    if (tuple.Item2 != null)
                        timeStampUntilCallback.Load(tuple.Item2);

                    if (timeStampUntilCallback != null)
                    {
                        TimeManager.Instance.AddCallBack(timeStampUntilCallback, NotifyTimeStampNow);
                    }
                    else if (AutoRegisterCallbackOnLoad)
                    {
                        AutoRegister();
                    }
                }
            }

            void AutoRegister()
            {
                var td = timeDateDuration;
                if (UseTimeStampAsDurationFromCallBackRegister)
                {
                    td = timeDateDuration.Clone().ConvertToTimeStamp();
                }

                if (!td.IsTimeStampNow(TimeManager.Time))
                {
                    //todo make sure I never need to have the callback when the tie is now on load (for now wont do anything
                    RegisterCallback();
                }
            }
        }

        public void OnEnable()
        {
            if (UseTimeStampCallBack && AutoRegisterCallOnEnable)
            {
                RegisterCallback();
            }
        }

        public void SetAsNowTimeStamp(TimeAndDate timeAndDate)
        {
            DateDuration.SetAsNowTimeStamp(timeAndDate);
        }

        public void ConvertToTimeStamp(TimeAndDate timeAndDate)
        {
            DateDuration.ConvertToTimeStamp(timeAndDate);
        }

        public bool IsTimeStampNow(TimeAndDate timeAndDate)
        {
            return DateDuration.IsTimeStampNow(timeAndDate);
        }

        public void MoveTimeStepForwardByDuration(TimeAndDate timeAndDate, TimeDateDuration duration)
        {
            DateDuration.MoveTimeStampForwardByDuration(timeAndDate, duration);
        }

        public void RegisterCallback()
        {
            UnityEngine.Debug.Assert(UseTimeStampCallBack,
                "The time date duration was now registered as using callback: are you sure you didn't do something wrong?",
                this);

            timeStampUntilCallback = UseTimeStampAsDurationFromCallBackRegister
                ? DateDuration.Clone().ConvertToTimeStamp(TimeManager.Time)
                : timeDateDuration.Clone();
            TimeManager.Instance.AddCallBack(timeStampUntilCallback, NotifyTimeStampNow);
        }

        public void NotifyTimeStampNow()
        {
            timeStampUntilCallback = null;
            OnTimeStampNowCallback.Invoke();
        }
    }
}
