﻿namespace BFT
{
    public interface ITimeStampCallBack
    {
        void NotifyTimeStampReached();
    }
}
