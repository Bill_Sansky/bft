﻿namespace BFT
{
    public interface ITimeTicker
    {
        void Tick(float deltaTime);
    }
}
