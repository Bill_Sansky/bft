using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace BFT
{
    public class TimeDateDurationView : SerializedMonoBehaviour
    {
        public UnityEvent OnBiggestValueDays;
        public UnityEvent OnBiggestValueHours;
        public UnityEvent OnBiggestValueMinutes;
        public UnityEvent OnBiggestValueMonths;
        public UnityEvent OnBiggestValueSeconds;

        public UnityEvent OnBiggestValueYears;
        public UnityEvent OnSmallestValueDays;
        public UnityEvent OnSmallestValueHours;
        public UnityEvent OnSmallestValueMinutes;
        public UnityEvent OnSmallestValueMonths;

        public UnityEvent OnSmallestValueSeconds;
        public UnityEvent OnSmallestValueYears;

        public UnityEvent OnUpdate;

        [SerializeField]
        private TimeDateDurationValue Duration;

        public bool UpdateOnEnable;

        public int Seconds => Duration.Value.Seconds;
        public int Minutes => Duration.Value.Minutes;
        public int Hours => Duration.Value.Hours;
        public int Days => Duration.Value.Days;
        public int Months => Duration.Value.Months;
        public int Years => Duration.Value.Years;

        public void Awake()
        {
            Init();
        }

        public void OnEnable()
        {
            if (UpdateOnEnable)
                UpdateView();
        }

        private void Init()
        {
           
        }

        public void InitAndUpdate()
        {
            Init();
            UpdateView();
        }

        public void UpdateView()
        {
            UnityEngine.Debug.Assert(Duration != null, "the duration wasn't set", this);

            var dateDuration = Duration.Value;

            if (dateDuration.Years > 0)
                OnBiggestValueYears.Invoke();
            else if (dateDuration.Months > 0)
                OnBiggestValueMonths.Invoke();
            else if (dateDuration.Days > 0)
                OnBiggestValueDays.Invoke();
            else if (dateDuration.Hours > 0)
                OnBiggestValueHours.Invoke();
            else if (dateDuration.Minutes > 0)
                OnBiggestValueMinutes.Invoke();
            else
                OnBiggestValueSeconds.Invoke();

            if (dateDuration.Seconds > 0)
                OnSmallestValueSeconds.Invoke();
            else if (dateDuration.Minutes > 0)
                OnSmallestValueMinutes.Invoke();
            else if (dateDuration.Hours > 0)
                OnSmallestValueHours.Invoke();
            else if (dateDuration.Days > 0)
                OnSmallestValueDays.Invoke();
            else if (dateDuration.Months > 0)
                OnSmallestValueMonths.Invoke();
            else
                OnSmallestValueYears.Invoke();

            OnUpdate.Invoke();
        }
    }
}
