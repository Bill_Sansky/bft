﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class TimeDateDuration : ISavedObject
    {
        [BoxGroup("Duration")] public int Minutes, Hours, Days, Months, Years;
        [BoxGroup("Duration")] public int Seconds;

        public TimeDateDuration(TimeDateDuration otherDuration)
        {
            Seconds = otherDuration.Seconds;
            Minutes = otherDuration.Minutes;
            Hours = otherDuration.Hours;
            Days = otherDuration.Days;
            Months = otherDuration.Months;
            Years = otherDuration.Years;
        }

        public TimeDateDuration()
        {
        }

        public bool IsDurationZeroOrNegative => Years < 0 ||
                                                (Years == 0 &&
                                                 (Months < 0 ||
                                                  (Months == 0 &&
                                                   (Days < 0 || (Days == 0 &&
                                                                 (Hours < 0 ||
                                                                  (Hours == 0 &&
                                                                   (Minutes < 0 || (Minutes == 0 && (Seconds <= 0))))))))));

        public object Save()
        {
            return new[] {Seconds, Minutes, Hours, Days, Months, Years};
        }

        public void Load(object saveFile)
        {
            if (saveFile == null)
                return;
            var objs = ((int[]) saveFile);
            Seconds = objs[0];
            Minutes = objs[1];
            Hours = objs[2];
            Days = objs[3];
            Months = objs[4];
            Years = objs[5];
        }

        public void SubtractDuration(TimeAndDate referenceScale, TimeDateDuration durationToRemove,
            bool zeroOnNegative = true)
        {
            /* Debug.Log(" --------------SUBTRACTION--------------");
 
         Debug.Log("Before Normalize");
         durationToRemove.Log();
         Log();*/

            durationToRemove.NormalizeDuration(referenceScale);
            NormalizeDuration(referenceScale);

            /* Debug.Log("After Normalize");
          durationToRemove.Log();
          Log();*/

            Seconds -= durationToRemove.Seconds;
            if (Seconds < 0)
            {
                Minutes--;
                Seconds = 60 + Seconds;
            }

            Minutes -= durationToRemove.Minutes;

            if (Minutes < 0)
            {
                //  Debug.Log("Minutes Lower than zero");

                Hours--;
                Minutes = 60 + Minutes;
                //  Log();
            }

            Hours -= durationToRemove.Hours;

            if (Hours < 0)
            {
                //Debug.Log("Hours Lower than zero");
                Days--;
                Hours = 24 + Hours;
                //  Log();
            }

            Days -= durationToRemove.Days;

            if (Days < 0)
            {
                Months--;
                Days = referenceScale.DaysPerMonth.Value + Days;
            }

            Months -= durationToRemove.Months;

            if (Months < 0)
            {
                Years--;
                Months = referenceScale.MonthsPerYear.Value + Months;
            }

            Years -= durationToRemove.Years;

            if (Years < 0 && zeroOnNegative)
            {
                //  Debug.Log("Reset");
                Reset();
            }

            /* Debug.Log("Before Normalize");
         Log();
 
         NormalizeDuration(referenceScale);
         Debug.Log("After Normalize");
         Log();
         
         Debug.Log(" --------------SUBTRACTION END--------------");*/
        }

        public void Reset()
        {
            Seconds = 0;
            Minutes = 0;
            Hours = 0;
            Days = 0;
            Months = 0;
            Years = 0;
        }

        public TimeDateDuration MultiplyDuration(TimeAndDate referenceScale, float multiplier)
        {
            float newSeconds = Seconds * multiplier;
            float newMinutes = multiplier * Minutes;
            float newHours = multiplier * Hours;
            float newDays = multiplier * Days;
            float newMonths = multiplier * Months;
            float newYears = multiplier * Years;

            Years = Mathf.FloorToInt(newYears);
            newMonths += (newYears - Years) * referenceScale.MonthsPerYear.Value;

            Months = Mathf.FloorToInt(newMonths);
            newDays += (newMonths - Months) * referenceScale.DaysPerMonth.Value;

            Days = Mathf.FloorToInt(newDays);
            newHours += (newDays - Days) * 24;

            Hours = Mathf.FloorToInt(newHours);
            newMinutes += (newHours - Hours) * 60;

            Minutes = Mathf.FloorToInt(newMinutes);
            newSeconds += (newMinutes - newMinutes) * 60;

            Seconds = Mathf.FloorToInt(newSeconds);

            return this;
        }

        [Button(ButtonSizes.Medium)]
        public void NormalizeDuration(TimeAndDate referenceScale)
        {
            int prevSec = Seconds;
            Seconds = Seconds % 60;

            int minutesToAdd = Mathf.FloorToInt((float) prevSec / 60);

            int prevMinutes = Minutes + minutesToAdd;
            Minutes = (prevMinutes) % 60;

            int hoursToAdd = Mathf.FloorToInt((float) (prevMinutes) / 60);

            int prevHours = Hours + hoursToAdd;
            Hours = (prevHours) % 24;

            int daysToAdd = Mathf.FloorToInt((float) (prevHours) / 24);

            int prevDays = Days + daysToAdd;
            Days = (prevDays) % referenceScale.DaysPerMonth.Value;

            int monthsToAdd = Mathf.FloorToInt((float) (prevDays) / referenceScale.DaysPerMonth.Value);

            int prevMonths = Months + monthsToAdd;
            Months = (prevMonths) % referenceScale.MonthsPerYear.Value;

            int yearsToAdd = Mathf.FloorToInt((float) (prevMonths) / referenceScale.MonthsPerYear.Value);

            Years += yearsToAdd;
        }

        public float GetLossyAmountInYear(TimeAndDate referenceScale)
        {
            return Years + GetLossyAmountInMonthsIgnoringYears(referenceScale) / referenceScale.MonthsPerYear.Value;
        }

        public float GetLossyAmountInMonthsIgnoringYears(TimeAndDate referenceScale)
        {
            return Months + GetLossyAmountInDaysIgnoringMonths(referenceScale) / referenceScale.DaysPerMonth.Value;
        }

        public float GetLossyAmountInDaysIgnoringMonths(TimeAndDate referenceScale)
        {
            return Days + GetLossyAmountInHoursIgnoringDays(referenceScale) / 24;
        }

        public float GetLossyAmountInHoursIgnoringDays(TimeAndDate referenceScale)
        {
            return Hours + GetLossyAmountInMinutesIgnoringHours(referenceScale) / 60;
        }

        public float GetLossyAmountInMinutesIgnoringHours(TimeAndDate referenceScale)
        {
            return Minutes + (float) Seconds / 60;
        }


        public float DivisionRatio(TimeAndDate referenceDate, TimeDateDuration dividerDuration)
        {
            float cumulatedAmount = 0;
            float otherCumulatedAmount = 0;

            if (Years != 0 || dividerDuration.Years != 0)
            {
                cumulatedAmount = GetLossyAmountInYear(referenceDate);
                otherCumulatedAmount = dividerDuration.GetLossyAmountInYear(referenceDate);
            }
            else if (Months != 0 || dividerDuration.Months != 0)
            {
                cumulatedAmount = GetLossyAmountInMonthsIgnoringYears(referenceDate);
                otherCumulatedAmount = dividerDuration.GetLossyAmountInMonthsIgnoringYears(referenceDate);
            }
            else if (Days != 0 || dividerDuration.Days != 0)
            {
                cumulatedAmount = GetLossyAmountInDaysIgnoringMonths(referenceDate);
                otherCumulatedAmount = dividerDuration.GetLossyAmountInDaysIgnoringMonths(referenceDate);
            }
            else if (Hours != 0 || dividerDuration.Hours != 0)
            {
                cumulatedAmount = GetLossyAmountInHoursIgnoringDays(referenceDate);
                otherCumulatedAmount = dividerDuration.GetLossyAmountInHoursIgnoringDays(referenceDate);
            }
            else if (Minutes != 0 || dividerDuration.Minutes != 0)
            {
                cumulatedAmount = GetLossyAmountInMinutesIgnoringHours(referenceDate);
                otherCumulatedAmount = dividerDuration.GetLossyAmountInMinutesIgnoringHours(referenceDate);
            }
            else
            {
                cumulatedAmount = Seconds;
                otherCumulatedAmount = dividerDuration.Seconds;
            }

            return cumulatedAmount / otherCumulatedAmount;
        }

        public void SetAsNowTimeStamp(TimeAndDate timeAndDate)
        {
            Seconds = timeAndDate.SecondsOfTheMinute;
            Minutes = timeAndDate.MinuteOfTheHour.Value;
            Hours = timeAndDate.HourOfTheDay.Value;
            Days = timeAndDate.DayOfTheMonth.Value;
            Months = timeAndDate.MonthOfTheYear.Value;
            Years = timeAndDate.CurrentYear.Value;
        }

        public TimeDateDuration ConvertToTimeStamp(TimeAndDate timeAndDate)
        {
            timeAndDate.ConvertDurationToTimeStamp(this);
            return this;
        }

        public TimeDateDuration ConvertToTimeStamp()
        {
            TimeManager.Instance.TimeAndDate.ConvertDurationToTimeStamp(this);
            return this;
        }

        public bool IsTimeStampNow(TimeAndDate timeAndDate)
        {
            return Years < timeAndDate.CurrentYear.Value
                   || (Years == timeAndDate.CurrentYear.Value
                       && (Months < timeAndDate.MonthOfTheYear.Value
                           || (Months == timeAndDate.MonthOfTheYear.Value
                               && (Days < timeAndDate.DayOfTheMonth.Value
                                   || (Days == timeAndDate.DayOfTheMonth.Value
                                       && (Hours < timeAndDate.HourOfTheDay.Value
                                           || (Hours == timeAndDate.HourOfTheDay.Value
                                               && (Minutes < timeAndDate.MinuteOfTheHour.Value
                                                   || Minutes == timeAndDate.MinuteOfTheHour.Value
                                                   && (Seconds <= timeAndDate.SecondsOfTheMinute)))))))));
            /*  return Seconds <= timeAndDate.SecondsOfTheMinute &&
                 Minutes <= timeAndDate.MinuteOfTheHour.Value &&
                 Hours <= timeAndDate.HourOfTheDay.Value &&
                 Days <= timeAndDate.DayOfTheMonth.Value &&
                 Months <= timeAndDate.MonthOfTheYear.Value &&
                 Years <= timeAndDate.CurrentYear.Value;*/
        }


        public int TimeStampDifference(TimeAndDate time, TickMode mode)
        {
            switch (mode)
            {
                case TickMode.SECONDS:
                    return TimeStampSecondDifference(time);
                case TickMode.MINUTES:
                    return TimeStampMinuteDifference(time);
                case TickMode.HOURS:
                    return TimeStampHoursDifference(time);
                case TickMode.DAYS:
                    return TimeStampDaysDifference(time);
                case TickMode.MONTHS:
                    return TimeStampMonthDifference(time);
                case TickMode.YEARS:
                    return TimeStampYearDifference(time);
                default:
                    throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
            }
        }

        public int TimeStampSecondDifference(TimeAndDate time)
        {
            return Seconds - time.SecondsOfTheMinute;
        }

        public int TimeStampYearDifference(TimeAndDate time)
        {
            return Years - time.CurrentYear.Value;
        }

        public int TimeStampMonthDifference(TimeAndDate time)
        {
            return Months - time.MonthOfTheYear.Value;
        }

        public int TimeStampDaysDifference(TimeAndDate time)
        {
            return Days - time.DayOfTheMonth.Value;
        }

        public int TimeStampHoursDifference(TimeAndDate time)
        {
            return Hours - time.HourOfTheDay.Value;
        }

        public int TimeStampMinuteDifference(TimeAndDate time)
        {
            return Minutes - time.MinuteOfTheHour.Value;
        }


        public void MoveTimeStampForwardByDuration(TimeAndDate timeAndDate, TimeDateDuration duration)
        {
            timeAndDate.AddDurationToTimeStamp(duration, this);
        }

        public TimeDateDuration Clone()
        {
            return new TimeDateDuration()
            {
                Seconds = Seconds,
                Minutes = Minutes,
                Hours = Hours,
                Days = Days,
                Months = Months,
                Years = Years,
            };
        }

        public void Log(UnityEngine.Object toPing = null)
        {
            UnityEngine.Debug.Log(
                $"Year: {Years} | Month: {Months} | Days: {Days} | Hours: {Hours} | Minutes: {Minutes} | Seconds: {Seconds}",
                toPing);
        }
    }
}
