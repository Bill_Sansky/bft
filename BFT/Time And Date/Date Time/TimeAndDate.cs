﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public enum TickMode
    {
        SECONDS,
        MINUTES,
        HOURS,
        DAYS,
        MONTHS,
        YEARS
    }

    public class TimeAndDate : SerializedScriptableObject, ITimeTicker, ISavedObject
    {
        [BoxGroup("Current Time"), ReadOnly] [SavedValue]
        private float currentSeconds;

        [BoxGroup("Current Time")] [SavedValue]
        public IVariable<int> CurrentYear;

        [BoxGroup("Current Time")] [SavedValue]
        public IVariable<int> DayOfTheMonth;

        [BoxGroup("Time Rate")] public IValue<int> DaysPerMonth;

        [BoxGroup("Current Time")] [SavedValue]
        public IVariable<int> HourOfTheDay;

        [BoxGroup("Current Time")] [SavedValue]
        public IVariable<int> MinuteOfTheHour;

        [BoxGroup("Current Time")] [SavedValue]
        public IVariable<int> MonthOfTheYear;

        [BoxGroup("Time Rate")] public IValue<int> MonthsPerYear;
        public UnityEvent OnDayChanged;
        public UnityEvent OnHourChanged;
        public UnityEvent OnMinuteChanged;
        public UnityEvent OnMonthChanged;

        public UnityEvent OnSecondChanged;
        public UnityEvent OnYearChanged;

        public TickMode TickMode = TickMode.SECONDS;

        public int SecondsOfTheMinute => Mathf.FloorToInt(currentSeconds);

        public object Save()
        {
            return SavedObjectUtils.GetSavedData(this);
        }

        public void Load(object saveFile)
        {
            SavedObjectUtils.LoadSaveData(this, (Dictionary<string, object>) saveFile);
        }

        public void Tick(float deltaTime)
        {
            switch (TickMode)
            {
                case TickMode.SECONDS:
                    AddSeconds(deltaTime);
                    break;
                case TickMode.MINUTES:
                    AddMinutes((int) deltaTime);
                    break;
                case TickMode.HOURS:
                    AddHours((int) deltaTime);
                    break;
                case TickMode.DAYS:
                    AddHours((int) deltaTime);
                    break;
                case TickMode.MONTHS:
                    AddMonths((int) deltaTime);
                    break;
                case TickMode.YEARS:
                    AddYears((int) deltaTime);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void CopyFromTimeStamp(TimeDateDuration timeStamp, bool silent = true)
        {
            currentSeconds = timeStamp.Seconds;
            MinuteOfTheHour.Value = timeStamp.Minutes;
            HourOfTheDay.Value = timeStamp.Hours;
            DayOfTheMonth.Value = timeStamp.Days;
            MonthOfTheYear.Value = timeStamp.Months;
            CurrentYear.Value = timeStamp.Years;

            if (!silent)
            {
                OnSecondChanged.Invoke();
                OnMinuteChanged.Invoke();
                OnHourChanged.Invoke();
                OnDayChanged.Invoke();
                OnMonthChanged.Invoke();
                OnYearChanged.Invoke();
            }
        }

        public void PassSomeTime(TimeDateDuration duration)
        {
            PassSomeTime(duration.Seconds,
                duration.Minutes, duration.Hours, duration.Days, duration.Months, duration.Years);
        }

        public void PassSomeTime(float seconds, int minutes, int hours, int days, int months, int years)
        {
            AddSeconds(seconds);
            AddMinutes(minutes);
            AddHours(hours, false);
            AddDays(days, false);
            AddMonths(months, false);
            AddYears(years, false);
        }

        public TimeDateDuration GetTimeDurationPassedFromTimeStamp(TimeDateDuration timeStamp)
        {
            /*
        Debug.Log("--------- DURATION PASSED FROM TIME STAMP -------------------");
        Debug.Log("current time");
        Log(this);
        Debug.Log("time stamp");
        timeStamp.Log();
*/

            int secondDifference = (int) currentSeconds - timeStamp.Seconds;
            int minuteDifference = MinuteOfTheHour.Value - timeStamp.Minutes;
            int hourDifference = HourOfTheDay.Value - timeStamp.Hours;
            int dayDifference = DayOfTheMonth.Value - timeStamp.Days;
            int monthDifference = MonthOfTheYear.Value - timeStamp.Months;
            int yearDifference = CurrentYear.Value - timeStamp.Years;

            /*
        Debug.Log("Seconds Difference: " + secondDifference);
        Debug.Log("Minutes Difference: " + minuteDifference);
        Debug.Log("Hours Difference: " + hourDifference);
        Debug.Log("Days Difference: " + dayDifference);
        Debug.Log("Months Difference: " + monthDifference);
        Debug.Log("Years Difference: " + yearDifference);
*/

            if (secondDifference < 0)
            {
                minuteDifference--;
                secondDifference = 60 + secondDifference;
            }

            if (minuteDifference < 0)
            {
                hourDifference--;
                minuteDifference = 60 + minuteDifference;
            }

            if (hourDifference < 0)
            {
                dayDifference--;
                hourDifference = 24 + hourDifference;
            }

            if (dayDifference < 0)
            {
                monthDifference--;
                dayDifference = DaysPerMonth.Value + dayDifference;
            }

            if (monthDifference < 0)
            {
                yearDifference--;
                monthDifference = MonthsPerYear.Value + monthDifference;
            }

            return new TimeDateDuration()
            {
                Seconds = secondDifference,
                Minutes = minuteDifference,
                Hours = hourDifference,
                Days = dayDifference,
                Months = monthDifference,
                Years = yearDifference,
            };
        }

        public void ConvertDurationToTimeStamp(TimeDateDuration duration)
        {
            AddDurationToTimeStamp(CreateNowTimeStamp(), duration);
        }

        public void AddDurationToTimeStamp(TimeDateDuration duration, TimeDateDuration timeStamp)
        {
            int second = timeStamp.Seconds;
            int minute = timeStamp.Minutes;
            int hour = timeStamp.Hours;
            int day = timeStamp.Days;
            int month = timeStamp.Months;
            int year = timeStamp.Years;

            IncrementTimeStampWithDuration(duration, ref second, ref minute, ref hour,
                ref day, ref month, ref year);

            timeStamp.Minutes = minute;
            timeStamp.Hours = hour;
            timeStamp.Days = day;
            timeStamp.Months = month;
            timeStamp.Years = year;
        }

        public TimeDateDuration GiveTimeStampAfterDuration(TimeDateDuration duration)
        {
            int second = SecondsOfTheMinute;
            int minute = MinuteOfTheHour.Value;
            int hour = HourOfTheDay.Value;
            int day = DayOfTheMonth.Value;
            int month = MonthOfTheYear.Value;
            int year = CurrentYear.Value;

            IncrementTimeStampWithDuration(duration, ref second, ref minute, ref hour,
                ref day, ref month, ref year);

            return new TimeDateDuration()
            {
                Seconds = second,
                Minutes = minute,
                Hours = hour,
                Days = day,
                Months = month,
                Years = year,
            };
        }

        public void GiveTimeStampAfterDuration(TimeDateDuration duration,
            out int second, out int minute, out int hour, out int day,
            out int month, out int year)
        {
            second = SecondsOfTheMinute;
            minute = MinuteOfTheHour.Value;
            hour = HourOfTheDay.Value;
            day = DayOfTheMonth.Value;
            month = MonthOfTheYear.Value;
            year = CurrentYear.Value;

            IncrementTimeStampWithDuration(duration, ref second, ref minute, ref hour,
                ref day, ref month, ref year);
        }

        private void IncrementTimeStampWithDuration(TimeDateDuration duration, ref int second,
            ref int minute, ref int hour, ref int day,
            ref int month, ref int year)
        {
            second += duration.Seconds % 60;

            int minutesToAdd = Mathf.FloorToInt((float) duration.Seconds / 60);

            minute += (duration.Minutes + minutesToAdd) % 60;

            int hoursToAdd = Mathf.FloorToInt((float) (duration.Minutes + minutesToAdd) / 60);

            hour += (duration.Hours + hoursToAdd) % 24;

            int daysToAdd = Mathf.FloorToInt((float) (duration.Hours + hoursToAdd) / 24);

            day += (duration.Days + daysToAdd) % DaysPerMonth.Value;

            int monthsToAdd = Mathf.FloorToInt((float) (duration.Days + daysToAdd) / DaysPerMonth.Value);

            month += (duration.Months + monthsToAdd) % MonthsPerYear.Value;

            int yearsToAdd = Mathf.FloorToInt((float) (duration.Months + monthsToAdd) / MonthsPerYear.Value);

            year += duration.Years + yearsToAdd;
        }

        public void AddSeconds(float seconds)
        {
            float prev = currentSeconds;
            float newSeconds = currentSeconds + seconds;
            currentSeconds = newSeconds;
            currentSeconds %= 60;

            if (currentSeconds >= 60)
            {
                AddMinutes(Mathf.FloorToInt(newSeconds / 60), false);
            }

            if (seconds > 1 || Mathf.FloorToInt(newSeconds) > Mathf.FloorToInt(prev))
            {
                OnSecondChanged.Invoke();
            }
        }

        public void AddMinutes(int amount, bool callAllTimeUnitEvent = true)
        {
            int newMinute = MinuteOfTheHour.Value + amount;
            MinuteOfTheHour.Value = newMinute;
            MinuteOfTheHour.Value %= 60;


            if (newMinute >= 60)
            {
                AddHours(Mathf.FloorToInt((float) newMinute / 60), false);
            }

            OnMinuteChanged.Invoke();

            if (callAllTimeUnitEvent)
            {
                OnSecondChanged.Invoke();
            }
        }

        public void AddSomeHours(int amount)
        {
            AddHours(amount);
        }

        public void AddHours(int amount, bool callAllTimeUnitEvent = true)
        {
            int newHour = HourOfTheDay.Value + amount;
            HourOfTheDay.Value = newHour;
            HourOfTheDay.Value %= 24;


            if (newHour >= 24)
            {
                AddDays(Mathf.FloorToInt((float) newHour / 24), false);
            }

            OnHourChanged.Invoke();

            if (callAllTimeUnitEvent)
            {
                OnMinuteChanged.Invoke();
                OnSecondChanged.Invoke();
            }
        }

        public void AddDays(int amount, bool callAllTimeUnitEvent = true)
        {
            int newDays = DayOfTheMonth.Value + amount;

            DayOfTheMonth.Value = newDays;

            if (DayOfTheMonth.Value > DaysPerMonth.Value)
                DayOfTheMonth.Value %= (DaysPerMonth.Value);

            if (newDays > DaysPerMonth.Value)
            {
                AddMonths(Mathf.FloorToInt((float) newDays / DaysPerMonth.Value), false);
            }

            OnDayChanged.Invoke();

            if (callAllTimeUnitEvent)
            {
                OnHourChanged.Invoke();
                OnMinuteChanged.Invoke();
                OnSecondChanged.Invoke();
            }
        }

        private void AddMonths(int amount, bool callAllTimeUnitEvent = true)
        {
            int newMonth = MonthOfTheYear.Value + amount;

            MonthOfTheYear.Value = newMonth;
            if (MonthOfTheYear.Value > MonthsPerYear.Value)
                MonthOfTheYear.Value %= (MonthsPerYear.Value);

            if (newMonth > MonthsPerYear.Value)
            {
                AddYears(Mathf.FloorToInt((float) newMonth / MonthsPerYear.Value), false);
            }

            OnMonthChanged.Invoke();

            if (callAllTimeUnitEvent)
            {
                OnDayChanged.Invoke();
                OnHourChanged.Invoke();
                OnMinuteChanged.Invoke();
                OnSecondChanged.Invoke();
            }
        }

        public void AddSomeYears(int amount)
        {
            AddYears(amount);
        }

        public void AddYears(int amount, bool callAllTimeUnitEvent = true)
        {
            CurrentYear.Value += amount;

            if (callAllTimeUnitEvent)
            {
                OnSecondChanged.Invoke();
                OnMinuteChanged.Invoke();
                OnHourChanged.Invoke();
                OnDayChanged.Invoke();
                OnMonthChanged.Invoke();
            }

            OnYearChanged.Invoke();
        }

        public float GetDayPercent()
        {
            return ((float) HourOfTheDay.Value + ((float) MinuteOfTheHour.Value + (float) SecondsOfTheMinute / 60) / 60) /
                   24;
        }

        public TimeDateDuration CreateNowTimeStamp()
        {
            return new TimeDateDuration()
            {
                Seconds = SecondsOfTheMinute,
                Days = DayOfTheMonth.Value,
                Hours = HourOfTheDay.Value,
                Minutes = MinuteOfTheHour.Value,
                Months = MonthOfTheYear.Value,
                Years = CurrentYear.Value
            };
        }

        public void Log(UnityEngine.Object toPing = null)
        {
            UnityEngine.Debug.Log(
                $"Year: {CurrentYear.Value} | Month: {MonthOfTheYear.Value} " +
                $"| Days: {DayOfTheMonth.Value} | Hours: {HourOfTheDay.Value} | Minutes: {MinuteOfTheHour.Value} | Seconds: {SecondsOfTheMinute}",
                toPing);
        }
    }
}
