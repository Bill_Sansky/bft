﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class TimeDateTimer : MonoBehaviour
    {
        [BoxGroup("References")] public TimeDateDurationAsset DurationAsset;

        [BoxGroup("Events")] public UnityEvent OnTimerDone;

        [BoxGroup("References")] public TimeAndDate TimeReference;

        [BoxGroup("Status"), ReadOnly] private TimeDateDurationAsset timeStamp;

        public void StartTimer()
        {
            if (timeStamp)
            {
                TimeReference.OnSecondChanged.RemoveListener(CheckTimeAndDate);
                Destroy(timeStamp);
            }

            timeStamp = ScriptableObject.CreateInstance<TimeDateDurationAsset>();
            timeStamp.SetAsNowTimeStamp(TimeReference);
            timeStamp.MoveTimeStepForwardByDuration(TimeReference, DurationAsset.DateDuration);

            TimeReference.OnSecondChanged.AddListener(CheckTimeAndDate);
        }

        private void CheckTimeAndDate()
        {
            if (timeStamp.IsTimeStampNow(TimeReference))
            {
                TimeReference.OnSecondChanged.RemoveListener(CheckTimeAndDate);
                OnTimerDone.Invoke();
                Destroy(timeStamp);
            }
        }
    }
}
