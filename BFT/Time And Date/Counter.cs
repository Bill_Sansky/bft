﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace BFT
{
    public class Counter : MonoBehaviour
    {
        [BoxGroup("Parameters")]
        public bool CountDown;

        [BoxGroup("Status"), ShowInInspector] private int currentCount;

        public UnityEvent OnCountDone;
        
        [FormerlySerializedAs("ResetOnCountDown")] [BoxGroup("Parameters")]
        public bool ResetOnCountDone = true;

        [BoxGroup("Parameters")]
        public IntValue StartCount;
        [BoxGroup("Parameters")]
        [HideIf("CountDown")] public IntValue TotalCount;

        public int CurrentCount => currentCount;

        void OnEnable()
        {
            currentCount = StartCount.Value;
        }

        private void InvokeCountDone()
        {
            OnCountDone.Invoke();
            if (ResetOnCountDone)
                currentCount = StartCount.Value;
        }

        public void Tick()
        {
            if (CountDown)
            {
                currentCount--;
            }
            else
            {
                currentCount++;
            }

            if ((CountDown && currentCount <= 0) || (currentCount >= TotalCount.Value))
                InvokeCountDone();
        }
    }
}
