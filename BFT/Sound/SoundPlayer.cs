﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [RequireComponent(typeof(AudioSource))]
    public class SoundPlayer : SerializedMonoBehaviour
    {
        [SerializeField, HideInInspector] private AudioSource audioSource;

        private IEnumerator fadeCoroutine;
        [SerializeField] private ISound sound;

        public void Awake()
        {
            AddAudioSource();
        }

        private void SetAudioProperSourceSettings()
        {
            if (sound == null)
            {
                UnityEngine.Debug.LogWarning("Sound file is missing!");
                return;
            }

            AddAudioSource();
            audioSource.volume = sound.Value.Volume;
            audioSource.pitch = sound.Value.Pitch;
            audioSource.clip = sound.Value.Clip;
        }

        [Button(ButtonSizes.Medium)]
        public void Play()
        {
            SetAudioProperSourceSettings();
            if (sound != null)
            {
                audioSource.Play();
            }
        }

        [Button(ButtonSizes.Medium)]
        public void PlayOneShot()
        {
            SetAudioProperSourceSettings();
            if (sound != null)
            {
                audioSource.PlayOneShot(sound.Value.Clip);
            }
        }

        [Button(ButtonSizes.Medium)]
        public void Stop()
        {
            audioSource.Stop();
        }

        [Button(ButtonSizes.Medium)]
        public void Pause()
        {
            audioSource.Pause();
        }

        [Button(ButtonSizes.Medium)]
        public void Resume()
        {
            audioSource.UnPause();
        }

        private void ClearFade()
        {
            if (fadeCoroutine != null)
            {
                StopCoroutine(fadeCoroutine);
                audioSource.volume = sound.Value.Volume;
            }
        }

        [Button(ButtonSizes.Medium)]
        public void FadeInAudio(float duration)
        {
            ClearFade();
            StartCoroutine(fadeCoroutine = FadeInAudioCoroutine(duration));
        }

        [Button(ButtonSizes.Medium)]
        public void FadeOutAudio(float duration)
        {
            if (!audioSource.isPlaying)
                return;
            ClearFade();
            StartCoroutine(fadeCoroutine = FadeOutAudioCoroutine(duration));
        }

        private IEnumerator FadeInAudioCoroutine(float duration)
        {
            if (!audioSource.isPlaying)
            {
                Play();
                audioSource.volume = 0;
                var timer = 0f;
                while (audioSource.volume < sound.Value.Volume)
                {
                    timer += UnityEngine.Time.deltaTime;
                    audioSource.volume = Mathf.Lerp(0, sound.Value.Volume, timer / duration);
                    yield return null;
                }
            }
        }

        private IEnumerator FadeOutAudioCoroutine(float duration)
        {
            if (audioSource.isPlaying)
            {
                var timer = duration;
                while (audioSource.volume > 0 && duration > 0)
                {
                    timer -= UnityEngine.Time.deltaTime;
                    audioSource.volume = Mathf.Lerp(0, sound.Value.Volume, timer / duration);
                    yield return null;
                }

                Stop();
                audioSource.volume = sound.Value.Volume;
            }
        }

        void Reset()
        {
            AddAudioSource();
        }

        private void AddAudioSource()
        {
            if (audioSource == null)
            {
                audioSource = gameObject.GetComponent<AudioSource>();
                if (audioSource == null) audioSource = gameObject.AddComponent<AudioSource>();
                audioSource.playOnAwake = false;
            }
#if UNITY_EDITOR
            UnityEditorInternal.ComponentUtility.MoveComponentUp(this);
#endif
        }
    }
}
