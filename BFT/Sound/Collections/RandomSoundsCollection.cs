﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class RandomSoundsCollection : ScriptableObject, ISound
    {
        [SerializeField] private List<Sound> Elements;

        public Sound Value => Elements.Random();


        [Button(ButtonSizes.Medium)]
        private void Play()
        {
            StopAll();
            SoundUtils.PlayClip(Value.Clip);
        }

        [Button(ButtonSizes.Medium)]
        private void StopAll()
        {
            SoundUtils.StopAllClips();
        }
    }
}
