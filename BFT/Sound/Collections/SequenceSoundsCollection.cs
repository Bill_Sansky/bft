﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class SequenceSoundsCollection : ScriptableObject, ISound
    {
        [NonSerialized] private int currentIndex;
        [SerializeField] private List<Sound> elements;

        public Sound Value => elements[currentIndex = ((++currentIndex) % elements.Count)];

        [Button(ButtonSizes.Medium)]
        public void ResetIndex()
        {
            currentIndex = 0;
        }


        [Button(ButtonSizes.Medium)]
        private void Play()
        {
            StopAll();
            SoundUtils.PlayClip(Value.Clip);
        }

        [Button(ButtonSizes.Medium)]
        private void StopAll()
        {
            SoundUtils.StopAllClips();
        }
    }
}
