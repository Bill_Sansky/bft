﻿namespace BFT
{
    public interface ISound
    {
        Sound Value { get; }
    }
}
