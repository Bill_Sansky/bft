﻿using Sirenix.OdinInspector;
using UnityEngine;
using Random = UnityEngine.Random;

namespace BFT
{
    public class Sound : SerializedScriptableObject, ISound
    {
        [SerializeField] private AudioClip clip;

        [FoldoutGroup("Pitch")] [HideIf("randomizePitch")] [SerializeField]
        private IValue<float> pitch = new FloatVariable(1f);

        [FoldoutGroup("Pitch")] [ShowIf("randomizePitch")] [SerializeField]
        private IValue<float> pitchMax = new FloatVariable(1f);

        [FoldoutGroup("Pitch")] [ShowIf("randomizePitch")] [SerializeField]
        private IValue<float> pitchMin = new FloatVariable(1f);

        [FoldoutGroup("Pitch")] [SerializeField]
        private bool randomizePitch = false;

        [FoldoutGroup("Volume")] [SerializeField]
        private bool randomizeVolume = false;

        [FoldoutGroup("Volume")] [HideIf("randomizeVolume")] [SerializeField]
        private IValue<float> volume = new FloatVariable(1f);

        [FoldoutGroup("Volume")] [ShowIf("randomizeVolume")] [SerializeField]
        private IValue<float> volumeMax = new FloatVariable(1f);

        [FoldoutGroup("Volume")] [ShowIf("randomizeVolume")] [SerializeField]
        private IValue<float> volumeMin = new FloatVariable(1f);

        public AudioClip Clip => clip;

        public float Volume =>
            !randomizeVolume ? volume.Value : Mathf.Clamp01(Random.Range(volumeMin.Value, volumeMax.Value));

        public float Pitch =>
            !randomizePitch ? pitch.Value : Mathf.Clamp(Random.Range(pitchMin.Value, pitchMax.Value), -3, 3);

        public Sound Value => this;

        [Button(ButtonSizes.Medium)]
        private void Play()
        {
            StopAll();
            SoundUtils.PlayClip(Clip);
        }

        [Button(ButtonSizes.Medium)]
        private void StopAll()
        {
            SoundUtils.StopAllClips();
        }
    }
}
