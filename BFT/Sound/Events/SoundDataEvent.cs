﻿using System;
using UnityEngine.Events;

namespace BFT
{
    [Serializable]
    public class SoundDataEvent : UnityEvent<SoundEventData>
    {
    }
}
