﻿namespace BFT
{
    public class SoundEventListener : GenericEventListener<SoundEventData, SoundDataEvent>
    {
        public SoundEventAsset SoundEvent;
        public override GenericEventAsset<SoundEventData, SoundDataEvent> Event => SoundEvent;
    }
}
