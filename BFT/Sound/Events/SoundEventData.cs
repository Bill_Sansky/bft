﻿using System;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class SoundEventData
    {
        [SerializeField] private UnityEngine.Transform invoker;

        public SoundEventData(UnityEngine.Transform invoker)
        {
            this.invoker = invoker;
        }

        public SoundEventData()
        {
        }
    }
}
