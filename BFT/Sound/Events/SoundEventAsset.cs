﻿using System;

namespace BFT
{
    [Serializable]
    public class SoundEventAsset : GenericEventAsset<SoundEventData, SoundDataEvent>
    {
    }
}
