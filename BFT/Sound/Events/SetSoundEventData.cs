﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class SetSoundEventData : SerializedMonoBehaviour
    {
        [SerializeField] private SoundEventData data;
        [SerializeField] private SoundDataEvent onInvoke;

        public void Invoke()
        {
            onInvoke.Invoke(data);
        }
    }
}
