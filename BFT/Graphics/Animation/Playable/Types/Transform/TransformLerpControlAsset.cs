﻿using UnityEngine;
using UnityEngine.Playables;

namespace BFT
{
    public class TransformLerpControlAsset : PlayableAsset
    {
        public AnimationCurve LerpCurve;
        public ExposedReference<Transform> Target;

        public bool LerpPosition = true;
        public bool LerpRotation = true;

        public bool HoldPositionOnPause = true;
        public float LerpSpeed;

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<TransformLerpPlayableBehaviour>.Create(graph);
            var behavior = playable.GetBehaviour();
            behavior.LerpCurve = LerpCurve;
            behavior.LerpPosition = LerpPosition;
            behavior.LerpRotation = LerpRotation;
            behavior.HoldPositionOnPause = HoldPositionOnPause;
            behavior.Target = Target.Resolve(graph.GetResolver());
            return playable;
        }
    }
}