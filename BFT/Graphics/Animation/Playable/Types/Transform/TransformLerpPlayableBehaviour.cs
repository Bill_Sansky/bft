﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

namespace BFT
{
    public class TransformLerpPlayableBehaviour : PlayableBehaviour
    {
        public struct PosRot
        {
            public Vector3 Position;
            public Quaternion Rotation;
        }

        public static Dictionary<Transform, PosRot> StartPositions = new Dictionary<Transform, PosRot>();

        public Transform ToLerp;
        public Transform Target;
        public AnimationCurve LerpCurve;
        public bool LerpRotation;
        public bool LerpPosition;

        private PosRot startPositionRotation;
        public bool HoldPositionOnPause;

        private double TimePercent(Playable playable)
        {
            return playable.GetTime() / playable.GetDuration();
        }


        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
        }

        public override void OnGraphStart(Playable playable)
        {
        }

        public override void PrepareFrame(Playable playable, FrameData info)
        {
        }

        public override void PrepareData(Playable playable, FrameData info)
        {
        }

        public override void OnGraphStop(Playable playable)
        {
            if (ToLerp)
            {
                if (LerpPosition)
                    ToLerp.position = StartPositions[ToLerp].Position;
                if (LerpRotation)
                    ToLerp.rotation = StartPositions[ToLerp].Rotation;
            }
        }


        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            if (ToLerp && (!HoldPositionOnPause || !Application.isPlaying))
            {
                if (LerpPosition)
                    ToLerp.position = StartPositions[ToLerp].Position;
                if (LerpRotation)
                    ToLerp.rotation = StartPositions[ToLerp].Rotation;
            }
        }

        private List<Transform> targets = new List<Transform>();
        private List<int> weigths = new List<int>();

        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            if (!ToLerp)
            {
                ToLerp = (Transform) playerData;
                startPositionRotation = new PosRot() {Position = ToLerp.position, Rotation = ToLerp.rotation};

                if (!StartPositions.ContainsKey(ToLerp))
                    StartPositions.Add(ToLerp, new PosRot() {Position = ToLerp.position, Rotation = ToLerp.rotation});
            }

            float percent = LerpCurve.Evaluate((float) TimePercent(playable));

            if (LerpPosition)
                ToLerp.position = Vector3.Lerp(startPositionRotation.Position, Target.position, percent);
            if (LerpRotation)
                ToLerp.rotation = Quaternion.Lerp(startPositionRotation.Rotation, Target.rotation, percent);


            //TODO add a blending ability
            /* int inputCount = playable.GetInputCount();
 
             if (inputCount > 1)
             {
                 weigths.Clear();
                 targets.Clear();
                 
                 for (int i = 0; i < inputCount; i++)
                 {
                     float inputWeight = playable.GetInputWeight(i);
 
                     ScriptPlayable<TransformLerpPlayableBehaviour> inputPlayable =
                         (ScriptPlayable<TransformLerpPlayableBehaviour>) playable.GetInput(i);
                     TransformLerpPlayableBehaviour input = inputPlayable.GetBehaviour();
 
                     // Use the above variables to process each frame of this playable.
                     finalIntensity += input.intensity * inputWeight;
                     finalColor += input.color * inputWeight;
                 }
 
                 //assign the result to the bound object
                 trackBinding.intensity = finalIntensity;
                 trackBinding.color = finalColor;
             }*/
        }
    }
}