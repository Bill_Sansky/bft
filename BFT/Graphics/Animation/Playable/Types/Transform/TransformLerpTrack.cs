﻿using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;


namespace BFT
{
    [TrackClipType(typeof(TransformLerpControlAsset))]
    [TrackBindingType(typeof(Transform))]
    public class TransformLerpTrack : TrackAsset
    {
        /* public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
         {
             // before building, update the binding field in the clips assets;
            var director = go.GetComponent<PlayableDirector>();
             var binding = director.GetGenericBinding(this);
 
             foreach (var c in GetClips())
             {
                 var myAsset = c.asset as TransformLerpControlAsset;
                 if (myAsset != null)
                     myAsset.Target = (ExposedReference<Tr>) binding;
             }
 
             return base.CreateTrackMixer(graph, go, inputCount);
         }*/

        public override void GatherProperties(PlayableDirector director, IPropertyCollector driver)
        {
            base.GatherProperties(director, driver);
        }
    }
}