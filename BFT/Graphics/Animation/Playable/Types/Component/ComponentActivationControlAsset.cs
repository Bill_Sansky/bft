using UnityEngine;
using UnityEngine.Playables;

namespace BFT
{
    public class ComponentActivationControlAsset : PlayableAsset
    {
        public bool EnableInClip=true;

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<ComponentActivationPlayableBehaviour>.Create(graph);
            var behavior = playable.GetBehaviour();
            behavior.EnableInClip = EnableInClip;
            return playable;
        }
       
    }
}