using BFT;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;

namespace BFT
{
    [TrackClipType(typeof(ComponentActivationControlAsset))]
    [TrackBindingType(typeof(Behaviour))]
    public class ComponentActivationTrack : TrackAsset
    {
        public override void GatherProperties(PlayableDirector director, IPropertyCollector driver)
        {
         
        }
    }
}