using UnityEngine;
using UnityEngine.Playables;

namespace BFT
{
    public class ComponentActivationPlayableBehaviour : PlayableBehaviour
    {
        public Behaviour Target;
        public bool EnableInClip;

        public override void OnBehaviourPlay(Playable playable, FrameData info)
        {
            if (!Target)
                return;

            if (playable.GetTime() >= playable.GetDuration() || playable.GetTime() < 0)
                Target.enabled = !EnableInClip;
            else
            {
                Target.enabled = EnableInClip;
            }
        }

        public override void OnBehaviourPause(Playable playable, FrameData info)
        {
            base.OnBehaviourPause(playable, info);

            if (!Target)
                return;

            Target.enabled = !EnableInClip;
        }

        public override void OnGraphStart(Playable playable)
        {
            if (Target)
                Target.enabled = !EnableInClip;
        }

        public override void PrepareFrame(Playable playable, FrameData info)
        {
        }

        public override void PrepareData(Playable playable, FrameData info)
        {
        }

        public override void OnGraphStop(Playable playable)
        {
            if (Target)
                Target.enabled = !EnableInClip;
        }


        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            if (!Target)
                Target = (Behaviour) playerData;

            if (playable.GetTime() >= playable.GetDuration())
                Target.enabled = !EnableInClip;
            else
            {
                Target.enabled = EnableInClip;
            }
        }
    }
}