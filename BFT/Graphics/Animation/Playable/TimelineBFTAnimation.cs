﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class TimelineBFTAnimation : MonoBehaviour, IAnimation
    {
        private AnimationValue animationReference;

        public bool IsPlaying => animationReference.Value.IsPlaying;

        public float Duration => animationReference.Value.Duration;

        public UnityEvent OnPlay => animationReference.Value.OnPlay;

        public UnityEvent OnEnd => animationReference.Value.OnEnd;

        public UnityEvent OnPause => animationReference.Value.OnPause;

        public void Play()
        {
            animationReference.Value.Play();
        }

        public void Stop()
        {
            animationReference.Value.Stop();
        }

        public AnimationUpdateType UpdateMode
        {
            get => animationReference.Value.UpdateMode;
            set => animationReference.Value.UpdateMode = value;
        }

        public void Pause()
        {
            animationReference.Value.Pause();
        }

        public void Restart()
        {
            animationReference.Value.Restart();
        }

        public void GoToTime(float time)
        {
            animationReference.Value.GoToTime(time);
        }

        public void ManualUpdate(float deltaTime)
        {
            animationReference.Value.ManualUpdate(deltaTime);
        }

        public void SetTarget(string id, GameObject target)
        {
            animationReference.Value.SetTarget(id, target);
        }
    }
}