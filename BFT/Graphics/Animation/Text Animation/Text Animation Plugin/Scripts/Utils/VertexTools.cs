﻿using UnityEngine;

namespace BFT
{
    public struct CharacterInfo
    {
        public Vector2 Min, Max;
        public char Character;
        public float TopY, BaselineY, Height;

        public int iCharacterInText,
            iCharacterInLine,
            iLine,
            iWord,
            iWordInLine,
            iParagraph;
    }

    public struct VertexInfo
    {
        public Vector3 position;
        public Vector2 uv;
        public Color32 color;

        public Vector2 uv1;
        public Vector3 normal;
        public Vector4 tangent;

        public int characterID;

        public static VertexInfo Get(UIVertex vert, byte characterID)
        {
            info.position = vert.position;
            info.color = vert.color;
            info.uv = vert.uv0;
            info.characterID = characterID;
            return info;
        }

        public static VertexInfo info = new VertexInfo();
    }
}
