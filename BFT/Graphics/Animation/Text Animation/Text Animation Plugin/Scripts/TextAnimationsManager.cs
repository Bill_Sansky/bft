﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if BFT_TEXTMESHPRO
using UnityEditor;

#if UNITY_EDITOR
#endif

namespace BFT
{
    [ExecuteInEditMode]
    [AddComponentMenu("")]
    public class TextAnimationsManager : MonoBehaviour
    {
        // TextAnimation components that are running animations
        private readonly List<TextAnimationComponent> animationsToUpdate = new List<TextAnimationComponent>();

        IEnumerator UpdateAnimations()
        {
            while (true)
            {
                if (AnimationsToUpdate.Count == 0)
                {
                    while (AnimationsToUpdate.Count <= 0)
                    {
                        yield return null;
                    }
                }

                AnimLogic();

                yield return null;
            }
        }

        private void AnimLogic()
        {
            // Update all animations and then remove the ones that are not longer playing
            for (int i = AnimationsToUpdate.Count - 1; i >= 0; i--)
            {
                AnimationsToUpdate[i].UpdateAnimations();
            }
        }

        #region Setup

        void Initialize()
        {
            if (Application.isPlaying)
            {
                StartCoroutine(UpdateAnimations());
            }
            else
            {
#if UNITY_EDITOR
                EditorApplication.update -= EditorUpdate;
                EditorApplication.update += EditorUpdate;
#endif
            }
        }

        private void EditorUpdate()
        {
            AnimLogic();
        }

        public static void RegisterAnimation(TextAnimationComponent se)
        {
            if (!Singleton.AnimationsToUpdate.Contains(se))
            {
                Singleton.AnimationsToUpdate.Add(se);
            }

#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                EditorApplication.update -= Singleton.EditorUpdate;
                EditorApplication.update += Singleton.EditorUpdate;
            }
#endif
        }

        public static void UnregisterAnimation(TextAnimationComponent se)
        {
            if (mSingleton == null)
            {
                return;
            }

            var manager = Singleton;
            manager.AnimationsToUpdate.Remove(se);
        }

        #endregion

        #region Singleton

        static TextAnimationsManager mSingleton;

        public static TextAnimationsManager Singleton
        {
            get
            {
                if (mSingleton == null)
                {
                    mSingleton = (TextAnimationsManager) FindObjectOfType(typeof(TextAnimationsManager));

                    if (mSingleton == null)
                    {
                        GameObject go = new GameObject();
                        go.hideFlags = go.hideFlags | HideFlags.HideAndDontSave;
                        //go.hideFlags = go.hideFlags | HideFlags.DontSave;
                        go.name = "[singleton] TextAnimationsManager";

                        mSingleton = go.AddComponent<TextAnimationsManager>();
                        mSingleton.Initialize();
                    }
                }

                return mSingleton;
            }
        }

        public List<TextAnimationComponent> AnimationsToUpdate => animationsToUpdate;

        void OnDestroy()
        {
            if (mSingleton == this)
            {
                mSingleton = null;
            }
        }

        void Awake()
        {
            if (mSingleton == null)
            {
                mSingleton = this;
                if (Application.isPlaying)
                {
                    DontDestroyOnLoad(gameObject);
                }
            }
            else if (mSingleton != this)
            {
                Destroy(gameObject);
            }
        }

        #endregion
    }
}
#endif
