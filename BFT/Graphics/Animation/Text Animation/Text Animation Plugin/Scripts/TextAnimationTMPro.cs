#if BFT_TEXTMESHPRO
using TMPro;
using UnityEngine;

namespace BFT
{
    public partial class TextAnimationComponent
    {
        RectTransform mRectTransform;

        public TMP_Text TextMeshProLabel;

        void Reset()
        {
            TextMeshProLabel = GetComponent<TextMeshProUGUI>();
        }

        protected void OnEnableTMPro()
        {
            if (!TextMeshProLabel)
            {
                TextMeshProLabel = GetComponent<TextMeshProUGUI>();
            }

            EnableTextModificationTMPro();
        }

        protected void OnDisableTMPro()
        {
            DisableTextModification_TMPro();
        }

        public void MarkWidgetAsChangedTMPro(bool MarkVertices = true, bool MarkMaterial = false)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying && TextMeshProLabel == null)
            {
                TextMeshProLabel = GetComponent<TextMeshProUGUI>();
            }
#endif

            if (TextMeshProLabel != null)
            {
                TextMeshProLabel.SetAllDirty();
            }
        }

        void SetWidgetColorTMPro(Color32 color)
        {
            if (TextMeshProLabel != null)
            {
                TextMeshProLabel.color = color;
            }
        }

        protected void EnableTextModificationTMPro()
        {
            TMPro_EventManager.TEXT_CHANGED_EVENT.Add(OnTextChangedTMPro);
        }

        protected void DisableTextModification_TMPro()
        {
            TMPro_EventManager.TEXT_CHANGED_EVENT.Remove(OnTextChangedTMPro);
        }

        void OnTextChangedTMPro(UnityEngine.Object obj)
        {
            if (obj != TextMeshProLabel)
            {
                return;
            }

            if (!isActiveAndEnabled)
            {
                return;
            }

            var textInfo = TextMeshProLabel.textInfo;

            if (textInfo.characterCount == 0)
            {
                return;
            }

            if (mRectTransform == null)
            {
                mRectTransform = transform as RectTransform;
            }

            if (mRectTransform == null)
            {
                return;
            }

            mRectTransform = TextMeshProLabel.rectTransform;
            Rect = mRectTransform.rect;
            RectPivot = mRectTransform.pivot;

            CharacterSize = TextMeshProLabel.fontSize;
            LineHeight = (textInfo.lineInfo.Length > 0) ? textInfo.lineInfo[0].lineHeight : CharacterSize;

            //--[ Characters ]--------------------------------------

            AllCharactersMin.x = AllCharactersMin.y = float.MaxValue;
            AllCharactersMax.x = AllCharactersMax.y = float.MinValue;
            Characters.Reset(textInfo.characterCount);

            int iLine = 0, nCharactersInLine = 0, iCharacter = 0;
            int iWord = 0, nWordsInLine = 0;
            int iParagraph = 0;

            int iFirstCharNextLine = 0;
            int iFirstCharNextWord = 0;

            OriginalVertices.Clear();
            OriginalVertices.Reset(4 * textInfo.characterCount);
            int iVert = 0;

            for (int c = 0; c < textInfo.characterCount; ++c)
            {
                if (textInfo.characterInfo.Length <= c)
                    break;
                if (!textInfo.characterInfo[c].isVisible)
                {
                    continue;
                }

                Characters.Buffer[iCharacter].Min = textInfo.characterInfo[c].bottomLeft;
                Characters.Buffer[iCharacter].Max = textInfo.characterInfo[c].topRight;
                if (AllCharactersMin.x > Characters.Buffer[iCharacter].Min.x)
                {
                    AllCharactersMin.x = Characters.Buffer[iCharacter].Min.x;
                }

                if (AllCharactersMin.y > Characters.Buffer[iCharacter].Min.y)
                {
                    AllCharactersMin.y = Characters.Buffer[iCharacter].Min.y;
                }

                if (AllCharactersMax.x < Characters.Buffer[iCharacter].Max.x)
                {
                    AllCharactersMax.x = Characters.Buffer[iCharacter].Max.x;
                }

                if (AllCharactersMax.y < Characters.Buffer[iCharacter].Max.y)
                {
                    AllCharactersMax.y = Characters.Buffer[iCharacter].Max.y;
                }

                if (c >= iFirstCharNextLine)
                {
                    iFirstCharNextLine = textInfo.lineInfo[iLine].lastCharacterIndex + 1;
                    iLine++;
                    nCharactersInLine = 0;
                    nWordsInLine = -1;
                    iFirstCharNextWord = c - 1;
                }

                if (c >= iFirstCharNextWord)
                {
                    iFirstCharNextWord = textInfo.wordInfo.Length > iWord
                        ? textInfo.wordInfo[iWord].lastCharacterIndex + 1
                        : int.MaxValue;
                    iWord++;
                    nWordsInLine++;
                }

                // Find the mesh and vertex
                int materialIndex = textInfo.characterInfo[c].materialReferenceIndex;
                int vertexIndex = textInfo.characterInfo[c].vertexIndex;
                VertexInfo info = new VertexInfo();
                for (int i = 0; i < 4; ++i)
                {
                    info.position = textInfo.meshInfo[materialIndex].vertices[vertexIndex + i];
                    info.color = textInfo.meshInfo[materialIndex].colors32[vertexIndex + i];
                    info.uv = textInfo.meshInfo[materialIndex].uvs0[vertexIndex + i];
                    info.normal = textInfo.meshInfo[materialIndex].normals[vertexIndex + i];
                    info.tangent = textInfo.meshInfo[materialIndex].tangents[vertexIndex + i];
                    info.characterID = c;

                    OriginalVertices.Buffer[iVert++] = info;
                }

                Characters.Buffer[iCharacter].Character = textInfo.characterInfo[c].character;
                Characters.Buffer[iCharacter].iLine = iLine;
                Characters.Buffer[iCharacter].iCharacterInText = iCharacter;
                Characters.Buffer[iCharacter].iCharacterInLine = nCharactersInLine++;
                Characters.Buffer[iCharacter].iWord = iWord;
                Characters.Buffer[iCharacter].iWordInLine = nWordsInLine;
                Characters.Buffer[iCharacter].iParagraph = iParagraph;
                Characters.Buffer[iCharacter].TopY = textInfo.lineInfo.Length > iLine
                    ? textInfo.lineInfo[iLine].lineExtents.min.y
                    : Characters.Buffer[iCharacter].Min.y;
                Characters.Buffer[iCharacter].BaselineY = textInfo.lineInfo.Length > iLine
                    ? textInfo.lineInfo[iLine].lineExtents.max.y
                    : Characters.Buffer[iCharacter].Max.y;
                iCharacter++;
            }

            Characters.Size = iCharacter;

            CurrentVertices = new ArrayBuffer<VertexInfo>();
            CurrentVertices.CopyFrom(OriginalVertices);
        }

        private void ExportVerticesToTMPro(TMP_TextInfo textInfo)
        {
            int iVert = 0;
            for (int c = 0; c < textInfo.characterCount; ++c)
            {
                if (textInfo.characterInfo.Length <= c)
                    break;

                if (!textInfo.characterInfo[c].isVisible)
                {
                    continue;
                }

                // Find the mesh and vertex
                int materialIndex = textInfo.characterInfo[c].materialReferenceIndex;
                int vertexIndex = textInfo.characterInfo[c].vertexIndex;

                for (int i = 0; i < 4; ++i)
                {
                    textInfo.meshInfo[materialIndex].vertices[vertexIndex + i] = CurrentVertices.Buffer[iVert].position;
                    textInfo.meshInfo[materialIndex].colors32[vertexIndex + i] = CurrentVertices.Buffer[iVert].color;
                    textInfo.meshInfo[materialIndex].uvs0[vertexIndex + i] = CurrentVertices.Buffer[iVert].uv;
                    textInfo.meshInfo[materialIndex].normals[vertexIndex + i] = CurrentVertices.Buffer[iVert].normal;
                    textInfo.meshInfo[materialIndex].tangents[vertexIndex + i] = CurrentVertices.Buffer[iVert].tangent;
                    iVert++;
                }
            }

            TextMeshProLabel.UpdateVertexData(TMP_VertexDataUpdateFlags.All);
        }
    }
}

#endif
