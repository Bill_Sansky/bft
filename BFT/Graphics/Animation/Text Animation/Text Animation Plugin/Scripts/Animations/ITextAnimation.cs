#if BFT_TEXTMESHPRO

namespace BFT
{
    public interface ITextAnimation
    {
        bool IsBackgroundAnim(TextAnimationComponent component);
        TextAnimationStatus CreateNewStatus();
        bool IsMainlyDone(TextAnimationComponent component);
        void ApplyCharacters(TextAnimationComponent component);
        void AnimUpdate(float dt, TextAnimationComponent component, ref bool makeMaterialDirty, ref bool makeVerticesDirty);
        void PlayAnimation(TextAnimationComponent component);
        void StopAnimation(TextAnimationComponent component, bool executeCallbacks = true);

        void UpdateTimeStatuses(TextAnimationComponent component, bool force = false,
            int overrideStringStartID = 0, int overrideStringLength = -1);

        float Duration(TextAnimationComponent component);
        void SetCurrentTime(TextAnimationComponent component, float time);
        float GetCurrentTime(TextAnimationComponent component);
    }
}
#endif
