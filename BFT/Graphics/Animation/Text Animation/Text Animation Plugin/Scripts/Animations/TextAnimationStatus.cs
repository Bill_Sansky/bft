﻿#if BFT_TEXTMESHPRO

using System;
using UnityEngine;

namespace BFT
{
    public class TextAnimationStatus
    {
        public float InitWaitingTime = 0;

        public bool IsPlaying;

        public float LoopDuration; // Total time of a single loop

        public float LoopTime, // Time within the loop duration: mTime=Repeat(mRealTime, loopDuration)
            Time; // total time that go bigger than loop duration

        public int
            NumElements; // Num elements in the TextAnimation component (used to know when the InitTime should be executed again if the text changes)

        public int RandomSeed;

        public bool IsFinishedEvent => OnFinished != null;

        public event Action<TextAnimationComponent, TextAnimation> OnFinished = delegate { };

        public void InvokeFinishedEvent(TextAnimationComponent se, TextAnimation anim)
        {
            OnFinished(se, anim);
        }

        public void SetFinishEventNull()
        {
            OnFinished = null;
        }

        public int GetCurrentLoop(EPlayback playback)
        {
            float loopDuration = playback == EPlayback.PING_PONG ? 2 * LoopDuration : LoopDuration;
            float t = Time;
            if (t < 0.0001f)
            {
                t = 0.001f;
            }

            return Mathf.CeilToInt(t / loopDuration) - 1;
        }
    }
}
#endif
