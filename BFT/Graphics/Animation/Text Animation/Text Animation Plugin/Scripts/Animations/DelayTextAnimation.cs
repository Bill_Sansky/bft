﻿#if BFT_TEXTMESHPRO
using System;
using System.Collections.Generic;

namespace BFT
{
    [Serializable]
    public class DelayTextAnimation : ITextAnimation
    {
        public float Delay;

        private Dictionary<TextAnimationComponent, DelayTextAnimationStatus> statuses
            = new Dictionary<TextAnimationComponent, DelayTextAnimationStatus>();

        public bool IsBackgroundAnim(TextAnimationComponent component)
        {
            return false;
        }

        public TextAnimationStatus CreateNewStatus()
        {
            return new TextAnimationStatus();
        }

        public bool IsMainlyDone(TextAnimationComponent component)
        {
            CheckStatuses(component);

            return statuses[component].CurrentTime >= Delay;
        }

        public void ApplyCharacters(TextAnimationComponent component)
        {
            //nothing to do, just wait
        }

        public void AnimUpdate(float dt, TextAnimationComponent component, ref bool makeMaterialDirty,
            ref bool makeVerticesDirty)
        {
            CheckStatuses(component);
            statuses[component].CurrentTime += dt;
        }

        public void PlayAnimation(TextAnimationComponent component)
        {
            CheckStatuses(component);
            statuses[component].CurrentTime = 0;
        }

        public void StopAnimation(TextAnimationComponent component, bool executeCallbacks = true)
        {
            statuses[component].CurrentTime = 0;
            //nothing to do
        }

        public void UpdateTimeStatuses(TextAnimationComponent component, bool force = false,
            int overrideStringStartID = 0,
            int overrideStringLength = -1)
        {
            //nothing to do
        }

        public float Duration(TextAnimationComponent component)
        {
            return Delay;
        }

        public void SetCurrentTime(TextAnimationComponent component, float time)
        {
            statuses[component].CurrentTime = time;
        }

        public float GetCurrentTime(TextAnimationComponent component)
        {
            return statuses[component].CurrentTime;
        }

        private void CheckStatuses(TextAnimationComponent component)
        {
            if (statuses == null)
            {
                statuses = new Dictionary<TextAnimationComponent, DelayTextAnimationStatus>();
            }

            if (!statuses.ContainsKey(component))
            {
                statuses.Add(component, new DelayTextAnimationStatus());
            }
        }
    }
}
#endif
