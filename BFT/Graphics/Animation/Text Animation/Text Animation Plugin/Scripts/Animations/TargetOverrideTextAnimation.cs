﻿#if BFT_TEXTMESHPRO
using System;

namespace BFT
{
    [Serializable]
    public class TargetOverrideTextAnimation : ITextAnimation
    {
        public ITextAnimation Animation;
        public int Length;
        public int StartID;

        public bool IsBackgroundAnim(TextAnimationComponent component)
        {
            return Animation.IsBackgroundAnim(component);
        }

        public TextAnimationStatus CreateNewStatus()
        {
            return Animation.CreateNewStatus();
        }

        public bool IsMainlyDone(TextAnimationComponent component)
        {
            return Animation.IsMainlyDone(component);
        }

        public void ApplyCharacters(TextAnimationComponent component)
        {
            Animation.ApplyCharacters(component);
        }

        public void AnimUpdate(float dt, TextAnimationComponent component, ref bool makeMaterialDirty,
            ref bool makeVerticesDirty)
        {
            Animation.AnimUpdate(dt, component, ref makeMaterialDirty, ref makeVerticesDirty);
        }

        public void PlayAnimation(TextAnimationComponent component)
        {
            Animation.PlayAnimation(component);
        }

        public void StopAnimation(TextAnimationComponent component, bool executeCallbacks = true)
        {
            Animation.StopAnimation(component, executeCallbacks);
        }

        public void UpdateTimeStatuses(TextAnimationComponent component, bool force = false, int overrideStringStartID = 0,
            int overrideStringLength = -1)
        {
            Animation.UpdateTimeStatuses(component, force, StartID, Length);
        }

        public float Duration(TextAnimationComponent component)
        {
            TextAnimation anim = Animation as TextAnimation;
            if (anim != null && anim.CreateStatusIfMissing(component))
            {
                anim.UpdateTimeStatuses(component, false, StartID, Length);
            }

            return Animation.Duration(component);
        }

        public void SetCurrentTime(TextAnimationComponent component, float time)
        {
            Animation.SetCurrentTime(component, time);
        }

        public float GetCurrentTime(TextAnimationComponent component)
        {
            return Animation.GetCurrentTime(component);
        }
    }
}
#endif
