﻿using UnityEngine;
#if BFT_TEXTMESHPRO
using System;

#if UNITY_EDITOR
#endif


namespace BFT
{
    [Serializable]
    public class TextAnimOperationRotation : TextAnimOperationScale
    {
        public override string GetTypeName()
        {
            return "Rotation";
        }

        // Copy from SE_AnimSequence_Position, except where noted
        public override void ApplyCharacters(TextAnimationComponent se, TextAnimation anim,
            int sequenceIndex)
        {
            if (anim.Statuses == null || anim.Statuses[se].LoopTime < Statuses[se].Delay && !SetFromValueOnStart)
            {
                return;
            }

            bool applyRandomFrom = HasRandom(FromRandom);
            bool applyRandomTo = HasRandom(ToRandom);
            DRandom.mCurrentSeed = GetRandomSeed(se, anim, sequenceIndex);

            Vector3
                from = From; // * se.mCharacterSize;                                                              // REMOVED *se.mCharacterSize
            Vector3
                to = To; // * se.mCharacterSize;                                                                // REMOVED *se.mCharacterSize
            Vector3 newValue = TextAnimMathUtils.v3zero;

            // Iterate through all the valid range
            for (int iElement = Statuses[se].ElementRangeStartID; iElement < Statuses[se].ElementRangeEndID; ++iElement)
            {
                float progress = GetProgress(anim.Statuses[se].LoopTime, se, anim, iElement);
                if (!SetFromValueOnStart && progress < 0)
                {
                    continue;
                }

                progress = progress < 0 ? 0 : progress;

                float tx = EasingCurveX.Evaluate(progress);
                float ty = UseAxisEasingCurves ? EasingCurveY.Evaluate(progress) : tx;
                float tz = UseAxisEasingCurves ? EasingCurveZ.Evaluate(progress) : tx;

                var currentValue = TextAnimMathUtils.v3one; // MODIFIED

                var vFrom = (AnimBlendFrom == EAnimBlendMode.REPLACE) ? from : (currentValue + from);
                var vTo = (AnimBlendTo == EAnimBlendMode.REPLACE) ? to : (currentValue + to);

                if (applyRandomFrom)
                {
                    vFrom += GetRandom(FromRandom /* se.mCharacterSize*/, iElement); // REMOVED *se.mCharacterSize
                }

                if (applyRandomTo)
                {
                    vTo += GetRandom(ToRandom /* se.mCharacterSize*/, iElement * 2 + 90); // REMOVED *se.mCharacterSize
                }

                if (ApplyX)
                {
                    newValue.x = vFrom.x + (vTo.x - vFrom.x) * tx;
                }
                else
                {
                    newValue.x = 0;
                }

                if (ApplyY)
                {
                    newValue.y = vFrom.y + (vTo.y - vFrom.y) * ty;
                }
                else
                {
                    newValue.y = 0;
                }

                if (ApplyZ)
                {
                    newValue.z = vFrom.z + (vTo.z - vFrom.z) * tz;
                }
                else
                {
                    newValue.z = 0;
                }

                Vector3 vPivot = TextAnimMathUtils.v3half;
                if (PivotType == EPivotType.RELATIVE_LETTER || PivotType == EPivotType.RELATIVE_WORD ||
                    PivotType == EPivotType.RELATIVE_LINE)
                {
                    vPivot.x = TextAnimMathUtils.LerpUnclamped(se.Characters.Buffer[iElement].Min.x,
                        se.Characters.Buffer[iElement].Max.x, Pivot.x);
                    vPivot.y = TextAnimMathUtils.LerpUnclamped(se.Characters.Buffer[iElement].Min.y,
                        se.Characters.Buffer[iElement].Max.y, Pivot.y);
                }
                else if (PivotType == EPivotType.RELATIVE_ALL)
                {
                    vPivot.x = TextAnimMathUtils.LerpUnclamped(se.AllCharactersMin.x, se.AllCharactersMax.x, Pivot.x);
                    vPivot.y = TextAnimMathUtils.LerpUnclamped(se.AllCharactersMin.y, se.AllCharactersMax.y, Pivot.y);
                }
                else if (PivotType == EPivotType.RELATIVE_RECT)
                {
                    vPivot.x = TextAnimMathUtils.LerpUnclamped(se.WidgetRectMin.x, se.WidgetRectMax.x, Pivot.x);
                    vPivot.y = TextAnimMathUtils.LerpUnclamped(se.WidgetRectMin.y, se.WidgetRectMax.y, Pivot.y);
                }
                else
                {
                    vPivot = Pivot * se.CharacterSize;
                }
                // END NEW CODE


                Quaternion qRot = Quaternion.Euler(newValue);

                ArrayBuffer<VertexInfo> infos =
                    BaseOnOriginalVertices ? se.OriginalVertices : se.CurrentVertices;

                // Apply to all Vertices
                for (int v = iElement * 4; v < iElement * 4 + 4; ++v)
                {
                    se.CurrentVertices.Buffer[v].position = qRot *
                                                            (infos.Buffer[v].position - vPivot) + vPivot;
                }
            }
        }
    }
}
#endif
