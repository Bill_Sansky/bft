﻿#if BFT_TEXTMESHPRO
using System;
using Sirenix.OdinInspector;

#if UNITY_EDITOR
#endif

namespace BFT
{
    [Serializable]
    public class TextAnimOperationAlpha : TextAnimOperationFloat
    {
        [BoxGroup("Start")]
        public bool
            OnFinishPutBackInitialValues; // When the animation finishes, sets the TextAnimation.color to the final value

        public override string GetTypeName()
        {
            return "Alpha";
        }

        public override void OnStop(TextAnimationComponent se, TextAnimation anim, int sequenceIndex)
        {
            if (!OnFinishPutBackInitialValues)
            {
                return;
            }

            for (int iElement = Statuses[se].ElementRangeStartID;
                iElement < Statuses[se].ElementRangeEndID;
                ++iElement)
            {
                // Apply to all Vertices
                for (int v = iElement * 4; v < iElement * 4 + 4; ++v)
                {
                    se.CurrentVertices.Buffer[v].color.a = se.OriginalVertices.Buffer[v].color.a;
                }
            }

            base.OnStop(se, anim, sequenceIndex);
        }
    }
}
#endif
