﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
#if BFT_TEXTMESHPRO
using Sirenix.Serialization;

#if UNITY_EDITOR

#endif

namespace BFT
{
    [Serializable]
    public class TextAnimOperationColor : TextAnimOperation
    {
        public enum EColorBlendMode
        {
            REPLACE,
            ALPHA_BLEND,
            MULTIPLY,
            ADDITIVE
        };

        [BoxGroup("Target/Color")] public bool ApplyR = true, ApplyG = true, ApplyB = true, ApplyA = true;

        [BoxGroup("Target/Color")] public EColorBlendMode ColorBlend = EColorBlendMode.ALPHA_BLEND;

        [BoxGroup("Target/Easing Curve")]
        public AnimationCurve EasingCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        [NonSerialized, OdinSerialize] [BoxGroup("Target/Color")]
        public IValue<UnityEngine.Gradient> Gradient;

        [BoxGroup("Start")]
        public bool
            OnFinishSetColorToInitialValue; // When the animation finishes, sets the TextAnimation.color to the final value

        public override string GetTypeName()
        {
            return "Color";
        }

        public override void UpdateSequence(float dt, TextAnimationComponent se, TextAnimation anim, int sequenceIndex,
            ref bool makeMaterialDirty, ref bool makeVerticesDirty)
        {
            base.UpdateSequence(dt, se, anim, sequenceIndex, ref makeMaterialDirty, ref makeVerticesDirty);
            makeVerticesDirty = true;
        }

        public override void ApplyCharacters(TextAnimationComponent se, TextAnimation anim, int sequenceIndex)
        {
            if (anim.Statuses == null || anim.Statuses[se].LoopTime < Statuses[se].Delay && !SetFromValueOnStart)
                return;

            // Iterate through all the valid range
            for (int iElement = Statuses[se].ElementRangeStartID; iElement < Statuses[se].ElementRangeEndID; ++iElement)
            {
                float progress = GetProgress(anim.Statuses[se].LoopTime, se, anim, iElement);
                if (!SetFromValueOnStart && progress < 0)
                    continue;
                progress = progress < 0 ? 0 : progress;

                progress = EasingCurve.Evaluate(progress);
                var currentColor = se.CurrentVertices.Buffer[iElement * 4].color;

                // Find Color for this Character
                var newColor = Gradient.Value.Evaluate(progress); //Color32.Lerp(cFrom, cTo, progress);

                //--[ Blend ]----------------------------
                switch (ColorBlend)
                {
                    case EColorBlendMode.REPLACE: break;
                    case EColorBlendMode.MULTIPLY:
                        newColor = newColor * currentColor;
                        break;
                    case EColorBlendMode.ADDITIVE:
                        newColor = newColor + currentColor;
                        break;
                    case EColorBlendMode.ALPHA_BLEND:
                        newColor = Color.Lerp(currentColor, newColor, newColor.a);
                        break;
                }

                if (ApplyR) currentColor.r = (byte) (newColor.r * 255);
                if (ApplyG) currentColor.g = (byte) (newColor.g * 255);
                if (ApplyB) currentColor.b = (byte) (newColor.b * 255);
                if (ApplyA) currentColor.a = (byte) (newColor.a * 255);

                // Apply to all Vertices
                for (int v = iElement * 4; v < iElement * 4 + 4; ++v)
                    se.CurrentVertices.Buffer[v].color = currentColor;
            }
        }

        public bool HasRandom(float maxRandom)
        {
            return maxRandom < -0.001f || maxRandom > 0.001f;
        }


        public override void OnStop(TextAnimationComponent se, TextAnimation anim, int sequenceIndex)
        {
            if (!OnFinishSetColorToInitialValue)
                return;

            for (int iElement = Statuses[se].ElementRangeStartID; iElement < Statuses[se].ElementRangeEndID; ++iElement)
            {
                // Apply to all Vertices
                for (int v = iElement * 4; v < iElement * 4 + 4; ++v)
                    se.CurrentVertices.Buffer[v].color = se.OriginalVertices.Buffer[v].color;
            }
        }
    }
}
#endif
