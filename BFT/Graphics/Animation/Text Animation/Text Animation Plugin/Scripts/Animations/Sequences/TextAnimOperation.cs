﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
#if BFT_TEXTMESHPRO
using System.Collections.Generic;

namespace BFT
{
    public enum AnimSequenceType
    {
        ALPHA,
        COLOR,
        POSITION,
        ROTATION,
        SCALE,
    }

    public enum EStartType
    {
        ON_ANIM_START,
        WITH_PREVIOUS,
        AFTER_PREVIOUS,
        ALL_PREVIOUS,
        ALL_USED_ELEMENTS
    }

    public enum EDurationType
    {
        TOTAL_TIME,
        PER_ELEMENT
    };

    public enum EElements
    {
        LETTERS,
        WORDS,
        LINES
    }

    public enum ETargetRange
    {
        ALL,
        FIRST,
        LAST,
        RANGE,
        CUSTOM
    };

    public class AnimOperationStatus
    {
        [HideInInspector] public float ElementDuration, ElementDelay;

        [HideInInspector] public int ElementRangeStartID, ElementRangeEndID;

        [HideInInspector]
        public float
            TotalLoopDuration; // 2*mLoopDuration if its PingPong  (Anim sequence goes from  mDelay and last for mTotalLoopDuration)

        //calculated variables, not shown
        // This is the total time counting the delay and loops: mTotalTime = mDelay + mLoopDuration * _PlaybackTimes;
        [HideInInspector] public float TotalTime, LoopDuration, Delay;
    }

    [Serializable]
    public abstract class TextAnimOperation
    {
        [NonSerialized] public Dictionary<TextAnimationComponent, AnimOperationStatus> Statuses =
            new Dictionary<TextAnimationComponent, AnimOperationStatus>();

        public bool Enabled
        {
            get => enabled;
            set => enabled = value;
        }

        public static TextAnimOperation CreateAnimSequence(AnimSequenceType type)
        {
            switch (type)
            {
                case AnimSequenceType.ALPHA:
                    return new TextAnimOperationAlpha();
                case AnimSequenceType.COLOR:
                    return new TextAnimOperationColor();
                case AnimSequenceType.POSITION:
                    return new TextAnimOperationPosition();
                case AnimSequenceType.ROTATION:
                    return new TextAnimOperationRotation();
                case AnimSequenceType.SCALE:
                    return new TextAnimOperationScale();
                default:
                    throw new ArgumentOutOfRangeException(nameof(type), type, null);
            }
        }

        public virtual string GetTypeName()
        {
            return "None";
        }

        public bool HasFinished()
        {
            return false;
        }

        public virtual void OnStart(TextAnimationComponent se)
        {
            if (Statuses == null)
                Statuses = new Dictionary<TextAnimationComponent, AnimOperationStatus>();

            Statuses.AddOrReplace(se, new AnimOperationStatus());
        }

        public virtual void OnStop(TextAnimationComponent se, TextAnimation anim, int sequenceIndex)
        {
        }

        public virtual void CalculateTimes(TextAnimationComponent se, TextAnimation anim,
            int sequenceIndex, int nElements, int startID = 0, int maxLength = Int32.MaxValue)
        {
            if (Statuses == null)
                Statuses = new Dictionary<TextAnimationComponent, AnimOperationStatus>();

            if (!Statuses.ContainsKey(se))
                Statuses.Add(se, new AnimOperationStatus());

            if (maxLength < 0)
                maxLength = nElements;

            AnimOperationStatus status = Statuses[se];

            switch (StartType)
            {
                case EStartType.AFTER_PREVIOUS:
                {
                    if (sequenceIndex > 0)
                    {
                        status.Delay = anim.Sequences[sequenceIndex - 1].Statuses[se].TotalTime;
                    }

                    break;
                }

                case EStartType.ALL_PREVIOUS:
                {
                    status.Delay = 0;
                    for (int i = 0; i < sequenceIndex; ++i)
                    {
                        status.Delay = Mathf.Max(status.Delay, anim.Sequences[sequenceIndex].Statuses[se].TotalTime);
                    }

                    break;
                }

                case EStartType.WITH_PREVIOUS:
                {
                    if (sequenceIndex > 0)
                    {
                        status.Delay = anim.Sequences[sequenceIndex - 1].Statuses[se].Delay;
                    }

                    break;
                }

                case EStartType.ALL_USED_ELEMENTS:
                {
                    status.Delay = 0;
                    for (int i = 0; i < sequenceIndex; ++i)
                    {
                        if (anim.Sequences[sequenceIndex].IsUsingCommonElements(se, this))
                        {
                            status.Delay = Mathf.Max(status.Delay,
                                anim.Sequences[sequenceIndex].Statuses[se].TotalTime);
                        }
                    }

                    break;
                }

                default:
                    //case eStartType.OnAnimStart:
                {
                    status.Delay = 0;
                    break;
                }
            }

            status.Delay += StartDelay;


            //--[ Get Animation Range ]--------------------------------

            status.ElementRangeStartID = 0;
            status.ElementRangeEndID = nElements;
            switch (TargetRangeType)
            {
                case ETargetRange.ALL:
                    status.ElementRangeStartID = startID;
                    status.ElementRangeEndID = startID + maxLength;
                    break;

                case ETargetRange.FIRST:
                    status.ElementRangeStartID = startID;
                    status.ElementRangeEndID = startID + Mathf.Min(StringTargetRange, maxLength);
                    break;
                case ETargetRange.LAST:
                    status.ElementRangeStartID = Mathf.Max(startID, startID + maxLength - StringTargetRange);
                    status.ElementRangeEndID = startID + maxLength;
                    break;
                case ETargetRange.RANGE:
                    status.ElementRangeStartID = Mathf.Min(startID + maxLength, startID + StringTargetIDStart);
                    status.ElementRangeEndID = Mathf.Min(startID + maxLength,
                        startID + StringTargetIDStart + StringTargetRange);
                    break;
            }

            nElements = status.ElementRangeEndID - status.ElementRangeStartID;

            //--[ Find Times ]--------------------------------

            if (DurationType == EDurationType.PER_ELEMENT)
            {
                status.ElementDuration = Duration;
                status.LoopDuration = status.ElementDuration + (status.ElementDuration * Separation * (nElements - 1));
            }
            else
            {
                status.LoopDuration = Duration;
                status.ElementDuration = status.LoopDuration / (Separation * nElements - Separation + 1);
            }

            status.ElementDelay = status.ElementDuration * Separation;
            status.TotalLoopDuration = Playback == EPlayback.PING_PONG ? 2 * status.LoopDuration : status.LoopDuration;

            if (Playback == EPlayback.LOOP || Playback == EPlayback.PING_PONG)
            {
                if (PlaybackTimes > 0)
                {
                    status.TotalTime = status.Delay + status.LoopDuration * PlaybackTimes;
                }
                else
                {
                    status.TotalTime = float.MaxValue;
                }
            }
            else
            {
                status.TotalTime = status.Delay + status.LoopDuration;
            }
        }

        public virtual void UpdateSequence(float dt, TextAnimationComponent se, TextAnimation anim, int sequenceIndex,
            ref bool makeMaterialDirty, ref bool makeVerticesDirty)
        {
        }

        public virtual void ApplyCharacters(TextAnimationComponent se, TextAnimation anim, int sequenceIndex)
        {
        }

        public float GetProgress(float time, TextAnimationComponent se, TextAnimation anim, int characterID)
        {
            AnimOperationStatus status = Statuses[se];

            //--[ Verify that the animation is playing ]--------------------------------
            if (time < status.Delay)
            {
                return -1;
            }

            if (time > status.TotalTime)
            {
                time = status.TotalTime;
            }

            //--[ Only accept elements in the Characters Range ]--------------------------------

            int elementId = characterID;

            if (status.ElementRangeEndID <= status.ElementRangeStartID || elementId < status.ElementRangeStartID
                                                                       || elementId >= status.ElementRangeEndID)
            {
                return -1;
            }

            //--[ only accept those elements in the Custom array ]--------------------------------

            if (TargetRangeType == ETargetRange.CUSTOM && TargetRangeCustom != null)
            {
                bool found = false;
                for (int i = 0, imax = TargetRangeCustom.Length; i < imax; ++i)
                {
                    if (TargetRangeCustom[i] == characterID)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    return -1;
                }
            }

            if (time >= status.TotalTime)
            {
                return 1;
            }

            time -= status.Delay;

            var nElements = status.ElementRangeEndID - status.ElementRangeStartID;
            elementId -= status.ElementRangeStartID;


            //--[ Apply Playback ]--------------------------------

            switch (Playback)
            {
                case EPlayback.LOOP:
                    time = Mathf.Repeat(time, status.LoopDuration);
                    status.ElementDelay -= 0.5f * status.ElementDelay / (float) nElements;
                    break;

                case EPlayback.PING_PONG:
                {
                    time = Mathf.Repeat(time, 2 * status.LoopDuration);
                    status.ElementDelay -= 0.5f * status.ElementDelay / (float) nElements;
                    if (time > status.LoopDuration)
                    {
                        time = status.LoopDuration - (time - status.LoopDuration);
                        time -= 0.25f * status.ElementDelay;
                    }

                    break;
                }

                default: //case ePlayback.Single:
                    break;
            }


            //--[ Find element progress and apply Easing ]--------------------------------
            float startTime, elementDuration;
            GetElementTimes(se, anim, elementId, out startTime, out elementDuration);


            float t = (time - startTime) / elementDuration;
            t = t > 1 ? 1 : t;
            return t;
        }

        public void GetElementTimes(TextAnimationComponent se, TextAnimation anim, int index, out float startTime,
            out float elementDuration)
        {
            AnimOperationStatus status = Statuses[se];
            startTime = index * status.ElementDelay;
            elementDuration = status.ElementDuration;
            float loopTime = status.TotalTime - status.Delay;

            int randomSeedBase = anim.Statuses[se].RandomSeed + 10 *
                                 anim.Statuses[se].GetCurrentLoop(anim.Playback) - DRandom.mCurrentSeed;

            if (DurationRandomSlower > 0 || DurationRandomFaster > 0)
            {
                float durRangeSlower = DurationRandomSlower * loopTime;
                float durRangeFaster = DurationRandomFaster * elementDuration;

                float minDuration = elementDuration - durRangeFaster;
                if (minDuration < 0.001f)
                {
                    minDuration = 0.001f;
                }

                float maxDuration = elementDuration + durRangeSlower;
                if (maxDuration > loopTime - startTime)
                {
                    maxDuration = loopTime - startTime;
                }

                elementDuration = DRandom.Get(index + randomSeedBase, minDuration, maxDuration);
            }

            if (RandomStart > 0)
            {
                float randRange = RandomStart * loopTime;
                float minStart = startTime - randRange;
                if (minStart < 0)
                {
                    minStart = 0;
                }

                float maxStart = startTime + randRange;
                if (maxStart > loopTime - elementDuration)
                {
                    maxStart = loopTime - elementDuration;
                }

                startTime = DRandom.Get(index + randomSeedBase, minStart, maxStart);
            }

            if (Backwards)
            {
                startTime = status.TotalTime - (startTime + status.Delay) - elementDuration;
            }
        }

        public float GetFinalTime(TextAnimationComponent se, TextAnimation anim, int index)
        {
            float start, eDuration;
            GetElementTimes(se, anim, index, out start, out eDuration);
            return start + eDuration;
        }


        public virtual int GetRandomSeed(TextAnimationComponent se, TextAnimation anim, int sequenceIndex)
        {
            AnimOperationStatus status = Statuses[se];

            float t = anim.Statuses[se].LoopTime - status.Delay;
            if (t < 0.0001f)
            {
                t = 0.0001f;
            }

            return anim.Statuses[se].RandomSeed +
                   (anim.Statuses[se].GetCurrentLoop(anim.Playback)
                    * anim.Sequences.Count + Mathf.CeilToInt(t / status.TotalLoopDuration)) * 20 * sequenceIndex;
        }


        private bool IsUsingCommonElements(TextAnimationComponent se, TextAnimOperation operation)
        {
            AnimOperationStatus status = Statuses[se];

            if (status.ElementRangeStartID >= operation.Statuses[se].ElementRangeEndID || status.ElementRangeEndID
                <= operation.Statuses[se].ElementRangeStartID)
            {
                return false;
            }

            //TODO custom wont be correct in that case because of the override ID and length
            if (TargetRangeType == ETargetRange.CUSTOM && TargetRangeCustom != null &&
                operation.TargetRangeCustom == null)
            {
                for (int i = 0; i < TargetRangeCustom.Length; ++i)
                {
                    if (TargetRangeCustom[i] >= operation.Statuses[se].ElementRangeStartID && TargetRangeCustom[i]
                        < operation.Statuses[se].ElementRangeEndID)
                    {
                        return true;
                    }
                }
            }

            if (TargetRangeCustom == null && operation.TargetRangeCustom != null &&
                operation.TargetRangeType == ETargetRange.CUSTOM)
            {
                for (int i = 0; i < operation.TargetRangeCustom.Length; ++i)
                {
                    if (operation.TargetRangeCustom[i] >= status.ElementRangeStartID
                        && operation.TargetRangeCustom[i] < status.ElementRangeEndID)
                    {
                        return true;
                    }
                }
            }

            if (TargetRangeType == ETargetRange.CUSTOM && TargetRangeCustom != null &&
                operation.TargetRangeCustom != null && operation.TargetRangeType == ETargetRange.CUSTOM)
            {
                for (int i = 0; i < TargetRangeCustom.Length; ++i)
                {
                    for (int j = 0; j < TargetRangeCustom.Length; ++j)
                    {
                        if (TargetRangeCustom[i] == operation.TargetRangeCustom[j])
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        public TextAnimOperation Clone()
        {
            return this.MemberwiseClone() as TextAnimOperation;
        }

#if UNITY_EDITOR
        public virtual void InspectorGUI()
        {
        }
#endif

        #region Variables

        [BoxGroup("Sequence")] public string Name = "Sequence";

        [BoxGroup("Start")] public EStartType StartType = EStartType.ON_ANIM_START;

        [BoxGroup("Start")] public float StartDelay;

        [FoldoutGroup("Time Parameters")] public EDurationType DurationType = EDurationType.TOTAL_TIME;

        [FoldoutGroup("Time Parameters")] public float Duration = 1;

        [FoldoutGroup("Time Parameters"), Range(0, 1)]
        public float DurationRandomSlower = 0;

        [FoldoutGroup("Time Parameters"), Range(0, 1)]
        public float DurationRandomFaster = 0;

        [FoldoutGroup("Time Parameters")]
        //TODO figure out why the hell 4
        [Tooltip("At what progress, the next element will start animating"), Range(0, 4)]
        public float Separation;

        [FoldoutGroup("Time Parameters"),
         Tooltip("How much to randomize the start of animating each element"), Range(0, 1)]
        public float RandomStart;

        [FoldoutGroup("Time Parameters")] public bool Backwards;

        [FoldoutGroup("Time Parameters")] public EPlayback Playback;

        [FoldoutGroup("Time Parameters"), ShowIf("IsMoreThanOnce")]
        public int PlaybackTimes = 1;

        private bool IsMoreThanOnce => Playback == EPlayback.LOOP;


        [FoldoutGroup("Target")] public EElements TargetType = EElements.LETTERS;

        [FoldoutGroup("Target")] public ETargetRange TargetRangeType;

        private bool IsAllOrCustom => TargetRangeType == ETargetRange.ALL
                                      || TargetRangeType == ETargetRange.CUSTOM;

        private bool IsCustom => TargetRangeType == ETargetRange.CUSTOM;

        [FoldoutGroup("Target"), HideIf("IsAllOrCustom")]
        public int StringTargetIDStart, // used for targetRage.Range
            StringTargetRange;
        // targetRage.First, targetRage.Last, targetRage.Range, targetRage.Range

        [FoldoutGroup("Target"), ShowIf("IsCustom")]
        public int[] TargetRangeCustom; // only elements 3rd, 5th, 10th   {3, 5, 10}

        [BoxGroup("Start")]
        public bool SetFromValueOnStart = false; // Set From value in all elements when the animation starts playing

        [SerializeField] [BoxGroup("Sequence")]
        private bool enabled = true;

        #endregion
    }
}
#endif
