﻿using System;
using Sirenix.OdinInspector;
#if BFT_TEXTMESHPRO
using UnityEngine;

#if UNITY_EDITOR

#endif

namespace BFT
{
    [Serializable]
    public class TextAnimOperationFloat : TextAnimOperation
    {
        public enum EAnimBlendMode
        {
            OFFSET,
            REPLACE,
            BLEND
        };

        [BoxGroup("Target/Easing From")] public EAnimBlendMode AnimBlendFrom = EAnimBlendMode.REPLACE;

        [BoxGroup("Target/Easing To")] public EAnimBlendMode AnimBlendTo = EAnimBlendMode.REPLACE;

        [BoxGroup("Target/Easing Curve")]
        public AnimationCurve EasingCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        [BoxGroup("Target/Easing From")] public float From, FromRandom;

        [BoxGroup("Target/Easing To")] public float To = 1, ToRandom;

        public override void UpdateSequence(float dt, TextAnimationComponent se, TextAnimation anim, int sequenceIndex,
            ref bool makeMaterialDirty, ref bool makeVerticesDirty)
        {
            base.UpdateSequence(dt, se, anim, sequenceIndex, ref makeMaterialDirty, ref makeVerticesDirty);
            makeVerticesDirty = true;
        }

        public override void ApplyCharacters(TextAnimationComponent se, TextAnimation anim, int sequenceIndex)
        {
            if (anim.Statuses == null || anim.Statuses[se].LoopTime < Statuses[se].Delay && !SetFromValueOnStart)
            {
                return;
            }

            bool applyRandomFrom = HasRandom(FromRandom);
            bool applyRandomTo = HasRandom(ToRandom);
            DRandom.mCurrentSeed = GetRandomSeed(se, anim, sequenceIndex);

            // Iterate through all the valid range
            for (int iElement = Statuses[se].ElementRangeStartID; iElement < Statuses[se].ElementRangeEndID; ++iElement)
            {
                if (se.CurrentVertices.Buffer.Length <= iElement * 4)
                    break;

                float progress = GetProgress(anim.Statuses[se].LoopTime, se, anim, iElement);
                if (!SetFromValueOnStart && progress < 0)
                {
                    continue;
                }

                progress = progress < 0 ? 0 : progress;

                progress = EasingCurve.Evaluate(progress);
                var currentAlpha = se.CurrentVertices.Buffer[iElement * 4].color.a;

                //--[ From ]----------------------------
                float aFrom = From * 255;
                if (AnimBlendFrom == EAnimBlendMode.OFFSET)
                {
                    aFrom = aFrom + currentAlpha;
                }

                if (AnimBlendFrom == EAnimBlendMode.BLEND)
                {
                    aFrom = From * currentAlpha;
                }

                if (applyRandomFrom)
                {
                    aFrom += 255 * FromRandom * DRandom.GetUnit(iElement);
                }

                //--[ To ]----------------------------
                float aTo = 255 * To;
                if (AnimBlendTo == EAnimBlendMode.OFFSET)
                {
                    aTo = (currentAlpha + To);
                }

                if (AnimBlendTo == EAnimBlendMode.BLEND)
                {
                    aTo = (currentAlpha * To);
                }

                if (applyRandomTo)
                {
                    aTo += 255 * ToRandom * DRandom.GetUnit(iElement * 2 + 90);
                }


                // Find Alpha for this Character
                float falpha = (aFrom + (aTo - aFrom) * progress);
                byte alpha = (byte) (falpha < 0 ? 0 : falpha > 255 ? 255 : falpha);


                // Apply to all Vertices
                for (int v = iElement * 4; v < iElement * 4 + 4; ++v)
                {
                    se.CurrentVertices.Buffer[v].color.a = alpha;
                }
            }
        }

        public bool HasRandom(float maxRandom)
        {
            return maxRandom < -0.001f || maxRandom > 0.001f;
        }
    }
}
#endif
