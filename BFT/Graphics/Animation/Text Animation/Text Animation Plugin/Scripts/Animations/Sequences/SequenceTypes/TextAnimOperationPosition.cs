﻿using System;
using Sirenix.OdinInspector;
#if BFT_TEXTMESHPRO
using UnityEngine;

#if UNITY_EDITOR
#endif


namespace BFT
{
    [Serializable]
    public class TextAnimOperationPosition : TextAnimOperation
    {
        public enum EAnimBlendMode
        {
            OFFSET,
            REPLACE
        };

        [BoxGroup("Target/Easing From")] public EAnimBlendMode AnimBlendFrom = EAnimBlendMode.OFFSET;

        [BoxGroup("Target/Easing To")] public EAnimBlendMode AnimBlendTo = EAnimBlendMode.OFFSET;

        [BoxGroup("Target/Easing Axis")] public bool ApplyX = true;

        [BoxGroup("Target/Easing Axis")] public bool ApplyY = true;

        [BoxGroup("Target/Easing Axis")] public bool ApplyZ = true;

        [FoldoutGroup("Target")]
        [Tooltip("If true, will base all the vertex calculations on the text unchanged," +
                 " otherwise, will work on the current vertex positions")]
        public bool BaseOnOriginalVertices = true;

        [HideReferenceObjectPicker] [BoxGroup("Target/Easing Curves")]
        public AnimationCurve EasingCurveX = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        [HideReferenceObjectPicker] [BoxGroup("Target/Easing Curves")] [ShowIf("UseAxisEasingCurves")]
        public AnimationCurve EasingCurveY = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        [HideReferenceObjectPicker] [BoxGroup("Target/Easing Curves")] [ShowIf("UseAxisEasingCurves")]
        public AnimationCurve EasingCurveZ = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        [BoxGroup("Target/Easing From")] public Vector3 From = new Vector3(0, 0, 0);

        [BoxGroup("Target/Easing From")] public Vector3 FromRandom;

        [BoxGroup("Target/Easing To")] public Vector3 To = new Vector3(0, 1, 0);

        [BoxGroup("Target/Easing To")] public Vector3 ToRandom;

        [BoxGroup("Target/Easing Curves")] public bool UseAxisEasingCurves = false;

        public override string GetTypeName()
        {
            return "Position";
        }

        public override void UpdateSequence(float dt, TextAnimationComponent se, TextAnimation anim, int sequenceIndex,
            ref bool makeMaterialDirty, ref bool makeVerticesDirty)
        {
            base.UpdateSequence(dt, se, anim, sequenceIndex, ref makeMaterialDirty, ref makeVerticesDirty);
            makeVerticesDirty = true;
        }


        public bool HasRandom(Vector3 maxRandom)
        {
            return maxRandom.sqrMagnitude > 0.001f;
        }

        public Vector3 GetRandom(Vector3 maxRandom, int iElement)
        {
            TextAnimMathUtils.tempV3.x = maxRandom.x * (DRandom.GetUnit(iElement));
            TextAnimMathUtils.tempV3.y = maxRandom.y * (DRandom.GetUnit(iElement + 13 * iElement));
            TextAnimMathUtils.tempV3.z = maxRandom.z * (DRandom.GetUnit(iElement + 29 * iElement));
            return TextAnimMathUtils.tempV3;
        }

        public override void ApplyCharacters(TextAnimationComponent se, TextAnimation anim, int sequenceIndex)
        {
            if (anim.Statuses == null || !anim.Statuses.ContainsKey(se) ||
                anim.Statuses[se].LoopTime < Statuses[se].Delay && !SetFromValueOnStart)
            {
                return;
            }

            bool applyRandomFrom = HasRandom(FromRandom);
            bool applyRandomTo = HasRandom(ToRandom);
            DRandom.mCurrentSeed = GetRandomSeed(se, anim, sequenceIndex);

            Vector3 from = From * se.CharacterSize;
            Vector3 to = To * se.CharacterSize;
            Vector3 newValue = TextAnimMathUtils.v3zero;

            if (AnimBlendFrom == EAnimBlendMode.REPLACE)
            {
                from.x = TextAnimMathUtils.LerpUnclamped(se.AllCharactersMin.x, se.AllCharactersMax.x, From.x);
                from.y = TextAnimMathUtils.LerpUnclamped(se.AllCharactersMin.y, se.AllCharactersMax.y, From.y);
            }

            if (AnimBlendTo == EAnimBlendMode.REPLACE)
            {
                to.x = TextAnimMathUtils.LerpUnclamped(se.AllCharactersMin.x, se.AllCharactersMax.x, To.x);
                to.y = TextAnimMathUtils.LerpUnclamped(se.AllCharactersMin.y, se.AllCharactersMax.y, To.y);
            }

            // Iterate through all the valid range
            for (int iElement = Statuses[se].ElementRangeStartID; iElement < Statuses[se].ElementRangeEndID; ++iElement)
            {
                float progress = GetProgress(anim.Statuses[se].LoopTime, se, anim, iElement);
                if (!SetFromValueOnStart && progress < 0)
                {
                    continue;
                }

                progress = progress < 0 ? 0 : progress;

                float tx = EasingCurveX.Evaluate(progress);
                float ty = UseAxisEasingCurves ? EasingCurveY.Evaluate(progress) : tx;
                float tz = UseAxisEasingCurves ? EasingCurveZ.Evaluate(progress) : tx;

                var currentValue = se.CurrentVertices.Buffer[iElement * 4].position;

                var vFrom = (AnimBlendFrom == EAnimBlendMode.OFFSET) ? (currentValue + from) : from;
                var vTo = (AnimBlendTo == EAnimBlendMode.OFFSET) ? (currentValue + to) : to;

                if (applyRandomFrom)
                {
                    vFrom += GetRandom(FromRandom * se.CharacterSize, iElement);
                }

                if (applyRandomTo)
                {
                    vTo += GetRandom(ToRandom * se.CharacterSize, iElement * 2 + 90);
                }

                if (ApplyX)
                {
                    newValue.x = vFrom.x + (vTo.x - vFrom.x) * tx;
                }

                if (ApplyY)
                {
                    newValue.y = vFrom.y + (vTo.y - vFrom.y) * ty;
                }

                if (ApplyZ)
                {
                    newValue.z = vFrom.z + (vTo.z - vFrom.z) * tz;
                }

                ArrayBuffer<VertexInfo> infos =
                    BaseOnOriginalVertices ? se.OriginalVertices : se.CurrentVertices;

                // Apply to all Vertices
                for (int v = iElement * 4; v < iElement * 4 + 4; ++v)
                {
                    // Apply Lerp(vFrom, vTo, t) + offset_from_currentValue   (the offset is applied to avoid changing the character's size)
                    if (ApplyX)
                    {
                        se.CurrentVertices.Buffer[v].position.x =
                            newValue.x + (infos.Buffer[v].position.x - currentValue.x);
                    }

                    if (ApplyY)
                    {
                        se.CurrentVertices.Buffer[v].position.y =
                            newValue.y + (infos.Buffer[v].position.y - currentValue.y);
                    }

                    if (ApplyZ)
                    {
                        se.CurrentVertices.Buffer[v].position.z =
                            newValue.z + (infos.Buffer[v].position.z - currentValue.z);
                    }
                }
            }
        }
    }
}
#endif
