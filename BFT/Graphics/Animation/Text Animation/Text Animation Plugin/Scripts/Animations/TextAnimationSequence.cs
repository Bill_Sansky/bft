using System;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
#if BFT_TEXTMESHPRO
using System.Collections.Generic;

namespace BFT
{
    //TODO make sure this works again, later
    public class TextAnimationSequenceStatus
    {
        public List<ITextAnimation> CurrentAnimations = new List<ITextAnimation>();

        public int CurrentID = 0;
        public float CurrentTime;

        public List<ITextAnimation> FinishingAnimations = new List<ITextAnimation>();
        public bool IsDone;
        public Dictionary<ITextAnimation, float> StartingTimesPerAnimations;

        public bool Stopping = true;
    }

    [Serializable]
    public class TextAnimationSequence : ITextAnimation
    {
        [BoxGroup("Animations"), ListDrawerSettings(ShowIndexLabels = false)]
        public List<ITextAnimation> AnimationSequence = new List<ITextAnimation>();

        [BoxGroup("Options")] public bool ApplyAllAnimationOnceOnStart;

        [SerializeField] [BoxGroup("Options")] private bool forceBackgroundAnim;

        [BoxGroup("Options")] public float OverlapTime = 0;

        [BoxGroup("Options")] public AnimationSequencePlayType PlayType;

        [ShowInInspector, BoxGroup("Status")]
        private Dictionary<TextAnimationComponent, TextAnimationSequenceStatus> statuses
            = new Dictionary<TextAnimationComponent, TextAnimationSequenceStatus>();

        private bool IsLinear => PlayType == AnimationSequencePlayType.LINEAR;

        public bool IsBackgroundAnim(TextAnimationComponent component)
        {
            if (forceBackgroundAnim)
            {
                return true;
            }

            if (statuses == null || !statuses.ContainsKey(component))
            {
                return false;
            }

            if (statuses[component].CurrentAnimations.Count == 0)
            {
                return false;
            }

            return statuses[component].CurrentAnimations.All(_ => _.IsBackgroundAnim(component));
        }

        public TextAnimationStatus CreateNewStatus()
        {
            return new TextAnimationStatus();
        }

        public bool IsMainlyDone(TextAnimationComponent component)
        {
            return statuses == null || !statuses.ContainsKey(component) ||
                   statuses[component].CurrentAnimations.Count == 0
                   || statuses[component].CurrentAnimations.All(_ => _.IsBackgroundAnim(component));
        }

        public void ApplyCharacters(TextAnimationComponent component)
        {
            if (statuses == null || !statuses.ContainsKey(component))
            {
                return;
            }

            foreach (var animation in statuses[component].CurrentAnimations)
            {
                animation.ApplyCharacters(component);
            }
        }

        public void AnimUpdate(float dt, TextAnimationComponent component, ref bool makeMaterialDirty,
            ref bool makeVerticesDirty)
        {
            if (component.UpdateMode == AnimationUpdateType.AUTO)
            {
                AutoPlay(dt, component, ref makeMaterialDirty, ref makeVerticesDirty);
            }
            else
            {
                ManualPlay(dt, component, ref makeMaterialDirty, ref makeVerticesDirty);
            }
        }

        public void PlayAnimation(TextAnimationComponent se)
        {
            if (statuses == null)
            {
                statuses = new Dictionary<TextAnimationComponent, TextAnimationSequenceStatus>();
            }

            if (!statuses.ContainsKey(se))
            {
                statuses.Add(se, new TextAnimationSequenceStatus());
            }

            TextAnimationSequenceStatus status = statuses[se];
            status.Stopping = false;
            status.IsDone = false;
            status.CurrentID = 0;
            status.CurrentAnimations.Clear();
            status.FinishingAnimations.Clear();

            if (ApplyAllAnimationOnceOnStart)
            {
                foreach (var animation in AnimationSequence)
                {
                    animation.ApplyCharacters(se);
                }
            }

            if (se.UpdateMode == AnimationUpdateType.MANUAL)
            {
                CreateStartingTimeDictionary(se, status);
            }

            StartAnimations(se, PlayType == AnimationSequencePlayType.PARALLEL);
        }

        public void StopAnimation(TextAnimationComponent component, bool executeCallbacks = true)
        {
            statuses[component].Stopping = true;
            foreach (var animation in statuses[component].CurrentAnimations)
            {
                animation.StopAnimation(component, executeCallbacks);
            }
        }


        public void UpdateTimeStatuses(TextAnimationComponent component, bool force, int overrideStringStartID = 0,
            int overrideStringLength = -1)
        {
            if (statuses != null)
            {
                /*  foreach (var slot in statuses[component].FinishingAnimations)
                  {
                      slot.UpdateTimeStatuses(component, force, overrideStringStartID, overrideStringLength);
                  }*/

                foreach (var slot in statuses[component].CurrentAnimations)
                {
                    slot.UpdateTimeStatuses(component, force, overrideStringStartID, overrideStringLength);
                }
            }
        }

        public float Duration(TextAnimationComponent component)
        {
            if (IsBackgroundAnim(component))
            {
                return 0;
            }

            if (PlayType == AnimationSequencePlayType.LINEAR)
            {
                float duration = 0;
                int i = 0;
                foreach (var animation in AnimationSequence)
                {
                    float animDuration = animation.Duration(component);
                    if (animDuration < 0)
                    {
                        return -1;
                    }
                    else
                    {
                        duration += animDuration - ((i > 0) ? OverlapTime : 0);
                    }

                    i++;
                }

                return duration;
            }
            else
            {
                float duration = 0;
                foreach (var animation in AnimationSequence)
                {
                    float animDuration = animation.Duration(component);
                    if (animDuration == -1)
                    {
                        return -1;
                    }
                    else
                    {
                        duration = Mathf.Max(duration, animDuration);
                    }
                }

                return duration;
            }
        }

        public void SetCurrentTime(TextAnimationComponent component, float time)
        {
            if (statuses == null)
            {
                statuses = new Dictionary<TextAnimationComponent, TextAnimationSequenceStatus>();
            }

            if (!statuses.ContainsKey(component))
            {
                statuses.Add(component, new TextAnimationSequenceStatus());
                CreateStartingTimeDictionary(component, statuses[component]);
            }

            statuses[component].CurrentTime = time;
        }

        public float GetCurrentTime(TextAnimationComponent component)
        {
            return statuses[component].CurrentTime;
        }

        public event System.Action OnPlayAnimationSequenceElement;
        public event System.Action OnFinishedAnimationSequenceElement;

        private void ManualPlay(float dt, TextAnimationComponent component, ref bool makeMaterialDirty,
            ref bool makeVerticesDirty)
        {
            if (statuses == null || !statuses.ContainsKey(component))
            {
                return;
            }

            statuses[component].CurrentAnimations.Clear();
            foreach (var animPair in statuses[component].StartingTimesPerAnimations)
            {
                float duration = animPair.Key.Duration(component);
                float startTime = animPair.Value;
                if (startTime <= statuses[component].CurrentTime
                    && (duration < 0 || startTime + duration >= statuses[component].CurrentTime))
                {
                    if (animPair.Key.IsMainlyDone(component))
                    {
                        animPair.Key.PlayAnimation(component);
                    }

                    statuses[component].CurrentAnimations.Add(animPair.Key);
                    animPair.Key.SetCurrentTime(component, statuses[component].CurrentTime - startTime);
                    animPair.Key.AnimUpdate(dt, component, ref makeMaterialDirty, ref makeVerticesDirty);
                }
            }
        }

        private void AutoPlay(float dt, TextAnimationComponent component, ref bool makeMaterialDirty,
            ref bool makeVerticesDirty)
        {
            //TODO ensure this never happen, or mark only for editor when application isnt playing
            if (statuses == null || !statuses.ContainsKey(component))
            {
                return;
            }

            List<ITextAnimation> currentAnims = statuses[component].CurrentAnimations;
            List<ITextAnimation> finishingAnims = statuses[component].FinishingAnimations;

            for (int i = finishingAnims.Count - 1; i >= 0; i--)
            {
                finishingAnims[i]
                    .AnimUpdate(dt, component, ref makeMaterialDirty, ref makeVerticesDirty);

                if (finishingAnims[i].IsMainlyDone(component))
                {
                    finishingAnims.RemoveAt(i);
                    OnFinishedAnimationSequenceElement?.Invoke();
                }
            }

            for (int i = currentAnims.Count - 1; i >= 0; i--)
            {
                currentAnims[i].AnimUpdate(dt, component, ref makeMaterialDirty, ref makeVerticesDirty);
                if (currentAnims[i].IsMainlyDone(component))
                {
                    if (!currentAnims[i].IsBackgroundAnim(component))
                    {
                        currentAnims.RemoveAt(i);
                        OnFinishedAnimationSequenceElement?.Invoke();
                    }
                }
                else if (PlayType == AnimationSequencePlayType.LINEAR && OverlapTime > 0)
                {
                    if (currentAnims[i].GetCurrentTime(component)
                        >= currentAnims[i].Duration(component) - OverlapTime)
                    {
                        if (!currentAnims[i].IsBackgroundAnim(component))
                        {
                            finishingAnims.Add(currentAnims[i]);
                            currentAnims.RemoveAt(i);
                            OnFinishedAnimationSequenceElement?.Invoke();
                        }
                    }
                }
            }

            //TODO the "all" call for background anims could be replaced with a bool flag
            if (statuses[component].CurrentID != -1 &&
                PlayType == AnimationSequencePlayType.LINEAR
                && (currentAnims.Count == 0 || currentAnims.All(_ => _.IsBackgroundAnim(component))))
            {
                //then play the next anim
                statuses[component].CurrentID++;

                if (statuses[component].CurrentID >= AnimationSequence.Count)
                {
                    statuses[component].CurrentID = -1;
                    return;
                }

                statuses[component].CurrentAnimations.Add(AnimationSequence[statuses[component].CurrentID]);
                AnimationSequence[statuses[component].CurrentID].PlayAnimation(component);
                OnPlayAnimationSequenceElement?.Invoke();
            }
        }

        private void CreateStartingTimeDictionary(TextAnimationComponent se, TextAnimationSequenceStatus status)
        {
            status.StartingTimesPerAnimations = new Dictionary<ITextAnimation, float>();
            int currentID = 0;
            foreach (var animation in AnimationSequence)
            {
                float time = 0;
                if (PlayType == AnimationSequencePlayType.LINEAR)
                {
                    float timeCorrection = 0;
                    for (int i = 0; i < currentID; i++)
                    {
                        if (AnimationSequence[i].Duration(se) > 0)
                        {
                            timeCorrection += AnimationSequence[i].Duration(se)
                                              - ((i > 0) ? OverlapTime : 0);
                        }
                        else
                        {
                            //arbitrary large amount to prevent the animation from firing
                            timeCorrection = 99999;
                        }
                    }

                    time += timeCorrection;
                }

                status.StartingTimesPerAnimations.Add(animation, time);
                currentID++;
            }
        }


        private void StartAnimations(TextAnimationComponent se, bool inParallel)
        {
            if (inParallel)
            {
                statuses[se].CurrentAnimations.AddRange(AnimationSequence);
            }
            else if (AnimationSequence.Count > 0)
            {
                statuses[se].CurrentAnimations.Add(AnimationSequence[0]);
            }

            foreach (var animation in statuses[se].CurrentAnimations)
            {
                animation.PlayAnimation(se);
                OnPlayAnimationSequenceElement?.Invoke();
            }
        }
    }
}
#endif
