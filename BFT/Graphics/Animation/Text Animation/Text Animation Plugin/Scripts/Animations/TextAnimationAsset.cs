﻿using System;
using Sirenix.OdinInspector;
#if BFT_TEXTMESHPRO
using Sirenix.Serialization;

namespace BFT
{
    public class TextAnimationAsset : SerializedScriptableObject, ITextAnimation
    {
        [NonSerialized, OdinSerialize] private ITextAnimation textAnimation;

        public TextAnimationStatus CreateNewStatus()
        {
            return textAnimation.CreateNewStatus();
        }

        public void AnimUpdate(float dt, TextAnimationComponent se, ref bool makeMaterialDirty,
            ref bool makeVerticesDirty)
        {
            textAnimation.AnimUpdate(dt, se, ref makeMaterialDirty, ref makeVerticesDirty);
        }

        public void PlayAnimation(TextAnimationComponent component)
        {
            textAnimation.PlayAnimation(component);
        }

        public void StopAnimation(TextAnimationComponent component, bool executeCallbacks = true)
        {
            textAnimation.StopAnimation(component, executeCallbacks);
        }

        public bool IsBackgroundAnim(TextAnimationComponent component)
        {
            return textAnimation.IsBackgroundAnim(component);
        }

        public bool IsMainlyDone(TextAnimationComponent component)
        {
            return textAnimation.IsMainlyDone(component);
        }

        public void ApplyCharacters(TextAnimationComponent se)
        {
            textAnimation.ApplyCharacters(se);
        }

        public void UpdateTimeStatuses(TextAnimationComponent se, bool force = false, int overrideStringStartID = 0,
            int overrideStringLength = -1)
        {
            textAnimation.UpdateTimeStatuses(se, force, overrideStringStartID, overrideStringLength);
        }

        public float Duration(TextAnimationComponent component)
        {
            return textAnimation.Duration(component);
        }

        public void SetCurrentTime(TextAnimationComponent component, float time)
        {
            textAnimation.SetCurrentTime(component, time);
        }

        public float GetCurrentTime(TextAnimationComponent component)
        {
            return textAnimation.GetCurrentTime(component);
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void PlayAndAssignAnimation(TextAnimationComponent component)
        {
            component.Animation = this;
            PlayAnimation(component);
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void AddAnimationToDictionary(IEntryDictionary<ITextAnimation> entryDictionary)
        {
            entryDictionary.Add(this);
        }

        public override string ToString()
        {
            return name;
        }
    }
}
#endif
