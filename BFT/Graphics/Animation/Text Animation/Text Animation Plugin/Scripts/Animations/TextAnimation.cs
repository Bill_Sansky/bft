﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
#if BFT_TEXTMESHPRO
using System.Collections.Generic;

namespace BFT
{
    public enum EPlayback
    {
        SINGLE,
        LOOP,
        PING_PONG
    };

    [Serializable]
    public class TextAnimation : ITextAnimation
    {
        [BoxGroup("Playback Options")] public bool Backwards;

        [BoxGroup("Playback Options/Delays")] public float DelayOnStart, DelayOnEnd;

        [BoxGroup("Playback Options/Loop"), HideIf("IsSingle")]
        public float ExtraTimePerLoop;

        [Tooltip("The duration of this animation will be ignored in case" +
                 " it is in a structure that waits for all animation to be done")]
        [BoxGroup("Playback Options")]
        [SerializeField]
        private bool isBackgroundAnimation;

        public string Name = "Custom";

        [BoxGroup("Playback Options/Loop")] public EPlayback Playback = EPlayback.SINGLE;

        [BoxGroup("Playback Options/Loop"), HideIf("IsSingle")]
        public int PlaybackTimes = 1;

        [BoxGroup("Sequences"), ListDrawerSettings(ListElementLabelName = "Name")]
        public List<TextAnimOperation> Sequences = new List<TextAnimOperation>(3);

        [NonSerialized, ShowInInspector, ReadOnly, FoldoutGroup("Status")]
        public Dictionary<TextAnimationComponent, TextAnimationStatus> Statuses
            = new Dictionary<TextAnimationComponent, TextAnimationStatus>();

        public bool IsLoop => Playback == EPlayback.LOOP;
        public bool IsSingle => (Playback == EPlayback.SINGLE);

        public void AnimUpdate(float dt, TextAnimationComponent se, ref bool makeMaterialDirty,
            ref bool makeVerticesDirty)
        {
            if (se == null || !Statuses.ContainsKey(se))
            {
                return;
            }

            TextAnimationStatus status = Statuses[se];

            if (status.InitWaitingTime < DelayOnStart)
            {
                status.InitWaitingTime += dt;
                return;
            }

            if (status.NumElements <= 0)
            {
                return;
            }

            if (status.Time >= 0)
            {
                status.Time += dt;

                switch (Playback)
                {
                    case EPlayback.LOOP:
                        status.LoopTime = Mathf.Repeat(status.Time, status.LoopDuration);
                        break;

                    case EPlayback.PING_PONG:
                    {
                        status.LoopTime = Mathf.Repeat(status.Time, 2 * status.LoopDuration);
                        if (status.LoopTime > status.LoopDuration)
                        {
                            status.LoopTime = status.LoopDuration - (status.LoopTime - status.LoopDuration);
                        }

                        break;
                    }

                    default: //case ePlayback.Single:
                        status.LoopTime = status.Time;
                        break;
                }

                if (Backwards)
                {
                    status.LoopTime = status.LoopDuration - status.LoopTime;
                }

                float mFinalTime = status.LoopDuration;
                if (Playback != EPlayback.SINGLE)
                {
                    mFinalTime = PlaybackTimes <= 0 ? -1 : (status.LoopDuration * PlaybackTimes);
                }

                if (mFinalTime > 0 && status.Time >= (mFinalTime + DelayOnEnd))
                {
                    status.Time = mFinalTime;
                    Stop(se);
                    return;
                }
            }

            for (int i = 0, imax = Sequences.Count; i < imax; ++i)
            {
                if (Sequences[i].Enabled)
                {
                    Sequences[i].UpdateSequence(dt, se, this, i, ref makeMaterialDirty, ref makeVerticesDirty);
                }
            }
        }

        public void PlayAnimation(TextAnimationComponent component)
        {
            Play(component);
        }

        public void StopAnimation(TextAnimationComponent component, bool executeCallbacks = true)
        {
            Stop(component, executeCallbacks);
        }

        public bool IsBackgroundAnim(TextAnimationComponent component)
        {
            return isBackgroundAnimation;
        }

        public TextAnimationStatus CreateNewStatus()
        {
            return new TextAnimationStatus();
        }

        public bool IsMainlyDone(TextAnimationComponent component)
        {
            if (Statuses == null || !Statuses.ContainsKey(component))
            {
                return true;
            }

            TextAnimationStatus status = Statuses[component];


            float mFinalTime = status.LoopDuration;
            if (Playback != EPlayback.SINGLE)
            {
                mFinalTime = PlaybackTimes <= 0 ? -1 : (status.LoopDuration * PlaybackTimes);
            }

            return mFinalTime > 0 &&
                   status.Time >= (mFinalTime + DelayOnEnd);
        }

        public void ApplyCharacters(TextAnimationComponent se)
        {
            if (!isBackgroundAnimation && IsMainlyDone(se))
            {
                return;
            }

            if (Statuses[se].NumElements <= 0)
            {
                return;
            }

            if (Sequences == null)
            {
                return;
            }

            for (int i = 0, imax = Sequences.Count; i < imax; ++i)
            {
                if (Sequences[i].Enabled)
                {
                    Sequences[i].ApplyCharacters(se, this, i);
                }
            }
        }

        public void UpdateTimeStatuses(TextAnimationComponent se, bool force = false,
            int overrideStringStartID = 0, int overrideStringLength = -1)
        {
            if (se == null)
            {
                return;
            }

            CreateStatusIfMissing(se);

            TextAnimationStatus status = Statuses[se];

            if (status.RandomSeed < 0)
            {
                status.RandomSeed = DRandom.GetSeed();
            }

            int nElements = se.Characters.Size;

            if (status.NumElements == nElements && !force)
            {
                //if the number of elements in the string did not change, do nothing
                return;
            }

            status.NumElements = nElements;

            status.LoopDuration = 0;
            if (Sequences != null)
            {
                //todo probably a way to cache this? probably would work if put once at the start
                for (int i = 0, imax = Sequences.Count; i < imax; ++i)
                {
                    //if (_Sequences[i]._Enabled || force)

                    Sequences[i].CalculateTimes(se, this, i, nElements, overrideStringStartID, overrideStringLength);

                    float seqDuration = Sequences[i].Statuses[se].TotalTime;
                    if (status.LoopDuration < 0 || seqDuration < 0)
                    {
                        status.LoopDuration = float.MaxValue;
                    }
                    else
                    {
                        status.LoopDuration = status.LoopDuration > seqDuration ? status.LoopDuration : seqDuration;
                    }
                }
            }

            status.LoopDuration += ExtraTimePerLoop;
        }

        public float Duration(TextAnimationComponent component)
        {
            if (CreateStatusIfMissing(component))
            {
                UpdateTimeStatuses(component);
            }

            if (IsBackgroundAnim(component))
            {
                return 0;
            }

            switch (Playback)
            {
                case EPlayback.SINGLE:
                    return Statuses[component].LoopDuration;
                case EPlayback.LOOP:
                    if (PlaybackTimes <= 0)
                    {
                        return -1;
                    }

                    return (Statuses[component].LoopDuration + ExtraTimePerLoop) * PlaybackTimes;
                case EPlayback.PING_PONG:
                    if (PlaybackTimes <= 0)
                    {
                        return -1;
                    }

                    return (Statuses[component].LoopDuration + ExtraTimePerLoop) * PlaybackTimes * 2;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void SetCurrentTime(TextAnimationComponent component, float time)
        {
            CreateStatusIfMissing(component);
            Statuses[component].Time = time;
            Statuses[component].LoopTime = Statuses[component].Time -
                                           Statuses[component].GetCurrentLoop(Playback) *
                                           Statuses[component].LoopDuration;
        }

        public float GetCurrentTime(TextAnimationComponent component)
        {
            if (!Statuses.ContainsKey(component))
                return 0;
            return Statuses[component].Time;
        }

        public void Play(TextAnimationComponent se)
        {
            if (se != null)
            {
                CreateStatusIfMissing(se);

                TextAnimationStatus status = Statuses[se];
                status.SetFinishEventNull();
                //  status.OnFinished += NotifyAnimationDone;
                status.IsPlaying = true;
                status.LoopTime = status.Time = 0;
                status.NumElements = -1;
                status.RandomSeed = -1;
                status.InitWaitingTime = 0;

                foreach (var operation in Sequences)
                {
                    operation.OnStart(se);
                }
            }
        }

        public bool CreateStatusIfMissing(TextAnimationComponent se)
        {
            if (Statuses == null)
            {
                Statuses = new Dictionary<TextAnimationComponent, TextAnimationStatus>();
            }

            if (!Statuses.ContainsKey(se))
            {
                Statuses.Add(se, CreateNewStatus());
                return true;
            }

            return false;
        }

        private void NotifyAnimationDone(TextAnimationComponent se, TextAnimation config)
        {
            se.OnAnimationFinished.Invoke(config);
        }

        public void Stop(TextAnimationComponent se, bool executeCallback = true)
        {
            if (Statuses != null)
                Statuses[se].LoopTime = Statuses[se].LoopDuration;

            for (int i = 0; i < Sequences.Count; ++i)
            {
                if (Sequences[i].Enabled)
                {
                    Sequences[i].ApplyCharacters(se, this, i);
                    Sequences[i].OnStop(se, this, i);
                }
            }

            if (Statuses != null)
            {
                if (executeCallback && Statuses.ContainsKey(se) && Statuses[se].IsFinishedEvent)
                {
                    Statuses[se].InvokeFinishedEvent(se, this);
                }

                Statuses.Remove(se);
            }
        }

        public void InitSequencesTimes()
        {
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
#endif
