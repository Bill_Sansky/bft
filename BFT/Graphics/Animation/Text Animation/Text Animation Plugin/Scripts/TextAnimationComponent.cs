using System;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Events;

#if BFT_TEXTMESHPRO

namespace BFT
{
    public enum ETimeSource
    {
        GAME,
        REAL
    };

    [Serializable]
    public class TextAnimationEvent : UnityEvent<ITextAnimation>
    {
    }

    [ExecuteInEditMode]
    public partial class TextAnimationComponent : MonoBehaviour, IAnimation
    {
        [NonSerialized, OdinSerialize, BoxGroup("Animation")]
        public ITextAnimation Animation;

        private float lastRealtimeUpdate;

        [BoxGroup("Events")] public TextAnimationEvent OnAnimationFinished = new TextAnimationEvent();

        [BoxGroup("Events")] [SerializeField] private UnityEvent onEnd;

        [BoxGroup("Events")] [SerializeField] private UnityEvent onPause;

        [BoxGroup("Events")] [SerializeField] private UnityEvent onPlay;

        [BoxGroup("Options")] public bool PlayOnEnable = false;

        [BoxGroup("Options")] public bool ResetStateOnStop = true;

        [SerializeField] [BoxGroup("Options")] private AnimationUpdateType updateMode = AnimationUpdateType.AUTO;

        private bool IsAnim => Animation != null;

        public AnimationUpdateType UpdateMode
        {
            get => updateMode;
            set => updateMode = value;
        }

        public bool IsPlaying { get; private set; }
        public float Duration => Animation.Duration(this);

        public UnityEvent OnPlay => onPlay;
        public UnityEvent OnEnd => onEnd;
        public UnityEvent OnPause => onPause;

        public void Play()
        {
            PlayAnimation();
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium), ShowIf("IsAnim")]
        public void Stop()
        {
            StopAnimation();
        }

        public void Pause()
        {
            IsPlaying = false;
            OnPause.Invoke();
        }


        public void Restart()
        {
            Stop();
            Play();
        }

        public void GoToTime(float time)
        {
            Animation.SetCurrentTime(this, time);
            ManuaUpdate(0);
        }

        public void ManualUpdate(float deltaTime)
        {
            ManuaUpdate(deltaTime);
        }

        public void SetTarget(string id, GameObject target)
        {
            throw new NotImplementedException();
        }

        public void InitAllTimes(bool force = false)
        {
            Animation.UpdateTimeStatuses(this, force);
        }

        public bool UpdateAnimations()
        {
            if (Animation == null)
            {
                return true;
            }

            if (!IsPlaying)
            {
                return false;
            }

            return ManuaUpdate(UnityEngine.Time.deltaTime);
        }

        private bool ManuaUpdate(float dt)
        {
            bool makeMaterialDirty = false;
            bool makeVerticesDirty = false;

            Animation.UpdateTimeStatuses(this);
            Animation.AnimUpdate(dt, this, ref makeMaterialDirty, ref makeVerticesDirty);
            Animation.ApplyCharacters(this);

            if (makeMaterialDirty || makeVerticesDirty)
            {
                ExportVerticesToTMPro(TextMeshProLabel.textInfo);
                TextMeshProLabel.UpdateVertexData();
            }

#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                UnityEditor.EditorUtility.SetDirty(this);
            }
#endif
            if (Animation.IsMainlyDone(this))
            {
                if (Animation.IsBackgroundAnim(this))
                {
                    return false;
                }

                ExportVerticesToTMPro(TextMeshProLabel.textInfo);
                TextMeshProLabel.UpdateVertexData();

                TextAnimationsManager.Singleton.AnimationsToUpdate.Remove(this);
                OnAnimationFinished.Invoke(Animation);
                OnEnd.Invoke();
                return true;
            }

            return false;
        }

        public void StopAnimation(bool executeCallbacks = true)
        {
            Animation.StopAnimation(this, executeCallbacks);

            if (ResetStateOnStop)
            {
                CurrentVertices = OriginalVertices;
            }

            ExportVerticesToTMPro(TextMeshProLabel.textInfo);
            TextMeshProLabel.UpdateVertexData();
            TextAnimationsManager.UnregisterAnimation(this);
            IsPlaying = false;
            if (executeCallbacks)
            {
                OnEnd.Invoke();
            }
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium), ShowIf("IsAnim")]
        public void ResetTextStatus()
        {
            CurrentVertices = OriginalVertices;
            Animation.UpdateTimeStatuses(this);
            bool refbool1 = true;
            bool refbool2 = true;

            Animation.AnimUpdate(100, this, ref refbool1, ref refbool2);
            Animation.ApplyCharacters(this);
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium), ShowIf("IsAnim")]
        public void PlayAnimation()
        {
            if (!Animation.IsMainlyDone(this) && Characters.Size > 0)
            {
                StopAnimation(false);
            }

            OnTextChangedTMPro(TextMeshProLabel);

            Animation.PlayAnimation(this);
            Animation.UpdateTimeStatuses(this);

            if (UpdateMode != AnimationUpdateType.MANUAL)
            {
                TextAnimationsManager.RegisterAnimation(this);
            }

            IsPlaying = true;
            OnPlay.Invoke();
        }

        public void Resume()
        {
            IsPlaying = true;
        }

        protected void OnEnable()
        {
            OnEnableTMPro();

            if (Application.isPlaying && PlayOnEnable && Animation != null)
            {
                PlayAnimation();
            }
        }

        protected void OnDisable()
        {
            TextAnimationsManager.UnregisterAnimation(this);
            StopAllCoroutines();
            OnDisableTMPro();
        }

        public void SetWidgetColor(Color32 color)
        {
            SetWidgetColorTMPro(color);
        }

        #region CACHED TEXTMESHPRO GEOM VALUES

        [NonSerialized]
        public ArrayBuffer<VertexInfo>
            OriginalVertices =
                new ArrayBuffer<VertexInfo>(); // This its a direct access to mSEMesh.mLayers[SEVerticesLayers.Original]

        [NonSerialized] public ArrayBuffer<CharacterInfo> Characters = new ArrayBuffer<CharacterInfo>();

        [NonSerialized] public ArrayBuffer<VertexInfo> CurrentVertices = new ArrayBuffer<VertexInfo>();

        [NonSerialized]
        public float CharacterSize = 1; // When using a text, it is the Font Size, otherwise the Image Height

        [NonSerialized]
        public float LineHeight = 1; // When using a text, it is the Font LineHeight, otherwise the Image Height

        [NonSerialized]
        public Vector2 AllCharactersMin, AllCharactersMax; // Bounding Rect of all vertices in mCharacters

        [NonSerialized] public Rect Rect;
        [NonSerialized] public Vector2 RectPivot;
        [NonSerialized] public Vector2 WidgetRectMin, WidgetRectMax; // Cached from mRect

        #endregion
    }
}
#endif
