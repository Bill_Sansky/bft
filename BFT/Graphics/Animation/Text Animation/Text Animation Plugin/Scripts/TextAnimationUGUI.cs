//TODO maybe reactivate if found that it's needed
/*namespace BFT
{
   #if UNITY_5_3_OR_NEWER || UNITY_5_3 || UNITY_5_2
  public partial class TextAnimationComponent : IMeshModifier
   {
       public void ModifyMesh(Mesh mesh)
       {
           if (!enabled) return;

           //if (!this.IsActive()) return;

           using (VertexHelper vertexHelper = new VertexHelper(mesh))
           {
               this.ModifyMesh(vertexHelper);
               vertexHelper.FillMesh(mesh);
           }
       }

       public void ModifyMesh (VertexHelper vh)
       {
           if (!isActiveAndEnabled) return;

           //--[ Cache ]------------
           if (mGraphic==null)	mGraphic=GetComponent<Graphic>();
           if (mGraphic==null)	return;

           if (mRectTransform==null) mRectTransform = transform as RectTransform;
           if (mRectTransform==null) return;

           if (vh.currentVertCount == 0)
               return;

           Rect = mRectTransform.rect;
           RectPivot = mRectTransform.pivot;
           WidgetColor = mGraphic.color;

           if (vh.currentVertCount<=0 )
               return;

           ImportVerticesFromUGUI(vh);

           ModifyVertices();

           ExportVerticesToUGUI(vh);

       }

       private void ImportVerticesFromUGUI(VertexHelper vh)
       {
           Text label = mGraphic as Text;
           CharacterSize = (label == null ? 1 : (float)label.fontSize);
           LineHeight = CharacterSize;

           var fontScale = (label == null ? 1 : (CharacterSize / (float)label.font.fontSize));
           var ascender = (label == null ? LineHeight : (label.font.ascent*fontScale));

           string strText = (label==null ? null : label.text);
           float Pixel2Units = (label == null ? 1 : 1/label.pixelsPerUnit);

           //--[ Populate Original Mesh ]-----------------

           OriginalVertices.Clear();


           OriginalVertices.Reset(vh.currentVertCount);
           Characters.Reset(OriginalVertices.Size / 4);

           AllCharactersMin = TextAnimMathUtils.v2max;
           AllCharactersMax = TextAnimMathUtils.v2min;
           var cIndex = 0;

           int iLine = 0, nCharactersInLine=0;
           int iWord = 0, nWordsInLine = 0;
           int iParagraph = 0;
           bool inWord = false;
           IList<UILineInfo> lineInfos = null;

           if (label != null)
           {
               if (label.cachedTextGeneratorForLayout.characterCount != strText.Length)
                   label.cachedTextGeneratorForLayout.Populate(strText, label.GetGenerationSettings(Rect.size));
               lineInfos = label.cachedTextGeneratorForLayout.lines;
           }
           //else
             //  mLines.Clear();

           var iFirstCharOfNextLine = (lineInfos == null || lineInfos.Count <= 1) ? int.MaxValue : lineInfos[1].startCharIdx;
           float lineHeight = (lineInfos == null || lineInfos.Count <= 0) ? 1 : lineInfos[0].height*Pixel2Units;
           float linePosY = (lineInfos == null || lineInfos.Count <= 0) ? 0 : lineInfos[0].topY * Pixel2Units;

           // The array is cleared to initialize all RichText values
           System.Array.Clear(Characters.Buffer, 0, Characters.Size);

           for (int c = 0; c < Characters.Size; ++c)
           {
               Characters.Buffer[cIndex].Min = TextAnimMathUtils.v2max;
               Characters.Buffer[cIndex].Max = TextAnimMathUtils.v2min;

               for (int i = 0; i < 4; i++)
               {
                   vh.PopulateUIVertex(ref uiVertex, c * 4 + i);

                   VertexInfo.position    = uiVertex.position;
                   VertexInfo.color       = uiVertex.color;
                   VertexInfo.uv          = uiVertex.uv0;
                   VertexInfo.normal      = uiVertex.normal;
                   VertexInfo.tangent     = uiVertex.tangent;
                   VertexInfo.characterID = cIndex;

                   if (Characters.Buffer[cIndex].Min.x > uiVertex.position.x) Characters.Buffer[cIndex].Min.x = uiVertex.position.x;
                   if (Characters.Buffer[cIndex].Min.y > uiVertex.position.y) Characters.Buffer[cIndex].Min.y = uiVertex.position.y;
                   if (Characters.Buffer[cIndex].Max.x < uiVertex.position.x) Characters.Buffer[cIndex].Max.x = uiVertex.position.x;
                   if (Characters.Buffer[cIndex].Max.y < uiVertex.position.y) Characters.Buffer[cIndex].Max.y = uiVertex.position.y;

                   OriginalVertices.Buffer[cIndex*4 + i] = VertexInfo;
               }

               char chr = strText == null ? (char)0 : strText[c];
               bool isWhiteSpace = char.IsWhiteSpace(chr);
               if (inWord && (isWhiteSpace || c >= iFirstCharOfNextLine))  // is a new word if we reach a whitespace or started another line
               {
                   iWord++;
                   nWordsInLine++;
               }
               inWord = !isWhiteSpace;
               if (chr == '\n')
                   iParagraph++;

               while (c >= iFirstCharOfNextLine)
               {
                   iLine++;
                   nCharactersInLine = 0;
                   nWordsInLine = 0;
                   iFirstCharOfNextLine = (lineInfos.Count <= (iLine + 1)) ? int.MaxValue : lineInfos[iLine + 1].startCharIdx;

                   if (lineInfos != null && lineInfos.Count > iLine)
                   {
                       lineHeight = lineInfos[iLine].height * Pixel2Units;
                       linePosY = lineInfos[iLine].topY * Pixel2Units;
                   }
               }

               if (Characters.Buffer[cIndex].Min.x < Characters.Buffer[cIndex].Max.x && Characters.Buffer[cIndex].Min.y < Characters.Buffer[cIndex].Max.y)
               {
                   if (Characters.Buffer[cIndex].Min.x < AllCharactersMin.x) AllCharactersMin.x = Characters.Buffer[cIndex].Min.x;
                   if (Characters.Buffer[cIndex].Min.y < AllCharactersMin.y) AllCharactersMin.y = Characters.Buffer[cIndex].Min.y;
                   if (Characters.Buffer[cIndex].Max.x > AllCharactersMax.x) AllCharactersMax.x = Characters.Buffer[cIndex].Max.x;
                   if (Characters.Buffer[cIndex].Max.y > AllCharactersMax.y) AllCharactersMax.y = Characters.Buffer[cIndex].Max.y;

                   Characters.Buffer[cIndex].Character = chr;
                   Characters.Buffer[cIndex].iLine = iLine;
                   Characters.Buffer[cIndex].iCharacterInText = cIndex;
                   Characters.Buffer[cIndex].iCharacterInLine = nCharactersInLine++;
                   Characters.Buffer[cIndex].iWord = iWord;
                   Characters.Buffer[cIndex].iWordInLine = nWordsInLine;
                   Characters.Buffer[cIndex].iParagraph = iParagraph;
                   Characters.Buffer[cIndex].TopY = linePosY;
                   Characters.Buffer[cIndex].BaselineY = linePosY - ascender;
                   Characters.Buffer[cIndex].Height = lineHeight;

                   cIndex++;
               }
           }
           Characters.Size = cIndex;
           OriginalVertices.Size = cIndex*4;
       }

       private void ExportVerticesToUGUI(VertexHelper vh)
       {
           vh.Clear();
           var ioffset = 0;

           for (int i = 0; i < OriginalVertices.Size; ++i)
               vh.AddVert(OriginalVertices.Buffer[i].position, OriginalVertices.Buffer[i].color, OriginalVertices.Buffer[i].uv, OriginalVertices.Buffer[i].uv1, OriginalVertices.Buffer[i].normal, OriginalVertices.Buffer[i].tangent);

           for (int i = 0; i < OriginalVertices.Size; i += 4)
           {
               vh.AddTriangle(ioffset + i, ioffset + i + 1, ioffset + i + 2);
               vh.AddTriangle(ioffset + i + 2, ioffset + i + 3, ioffset + i);
           }
       }



#else
   public partial class TextAnimation
   {
   #endif


       void OnEnableUGUI()
       {
           if (mGraphic == null) mGraphic = GetComponent<Graphic>();
       }

       void OnDisableUGUI()
       {
       }

       // When useVertices==true, it updates the Character min and max from its vertices and then updates the mAllCharacterMinMax
       // otherwise, it just updates the mAllCharacterMinMax from the min/max of each character
       public void UpdateCharactersMinMax( bool useVertices=true )
       {
           if (useVertices)
           {
               for (var i = 0; i < OriginalVertices.Size; i++)
               {
                   var cIndex = i / 4;
                   if (i % 4 == 0)
                   {
                       Characters.Buffer[cIndex].Min = TextAnimMathUtils.v2max;
                       Characters.Buffer[cIndex].Max = TextAnimMathUtils.v2min;
                   }
                   var pos = OriginalVertices.Buffer[i].position;

                   if (Characters.Buffer[cIndex].Min.x > pos.x) Characters.Buffer[cIndex].Min.x = pos.x;
                   if (Characters.Buffer[cIndex].Min.y > pos.y) Characters.Buffer[cIndex].Min.y = pos.y;
                   if (Characters.Buffer[cIndex].Max.x < pos.x) Characters.Buffer[cIndex].Max.x = pos.x;
                   if (Characters.Buffer[cIndex].Max.y < pos.y) Characters.Buffer[cIndex].Max.y = pos.y;
               }
           }

           AllCharactersMin = TextAnimMathUtils.v2max;
           AllCharactersMax = TextAnimMathUtils.v2min;
           for (var c = 0; c < Characters.Size; c++)
           {
               if (Characters.Buffer[c].Min.x < AllCharactersMin.x) AllCharactersMin.x = Characters.Buffer[c].Min.x;
               if (Characters.Buffer[c].Min.y < AllCharactersMin.y) AllCharactersMin.y = Characters.Buffer[c].Min.y;
               if (Characters.Buffer[c].Max.x > AllCharactersMax.x) AllCharactersMax.x = Characters.Buffer[c].Max.x;
               if (Characters.Buffer[c].Max.y > AllCharactersMax.y) AllCharactersMax.y = Characters.Buffer[c].Max.y;
           }

       }

       void SetWidgetColor_UGUI ( Color32 color )
       {
           if (mGraphic!=null)
               mGraphic.color = color;
       }
   }
}*/

