﻿using Sirenix.OdinInspector;
#if BFT_TEXTMESHPRO
using System;

namespace BFT
{
    [Serializable]
    public class AnimatedText : IValue<string>
    {
        [BoxGroup("Data")] public ITextAnimation Animation;

        [BoxGroup("Raw Data"), MultiLineProperty(5)]
        public string RawText;

        [BoxGroup("Data"), MultiLineProperty(5)]
        public string Text;

        public string Value => Text;

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void UpdateAnimation(IEntryDictionary<ITextAnimation> animationEntryDictionary,
            ITextAnimation defaultAnimation)
        {
            AnimationTagHelper.UpdateAnimatedTextAnimations(RawText, this, animationEntryDictionary,
                defaultAnimation);
        }
    }
}
#endif
