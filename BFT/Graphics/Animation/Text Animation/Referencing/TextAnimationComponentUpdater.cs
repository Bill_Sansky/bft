﻿#if BFT_TEXTMESHPRO
using Sirenix.OdinInspector;

namespace BFT
{
    public class TextAnimationComponentUpdater : SerializedMonoBehaviour, IVariable<AnimatedText>
    {
        [BoxGroup("References")] public TextAnimationComponent AnimationComponent;

        [BoxGroup("Options")] public bool AssignOnEnable = false;

        [BoxGroup("Options")] public bool AssignTextOnReferenceChange = true;

        [BoxGroup("Options")] public bool PlayAnimationOnAssign = true;

        [BoxGroup("References")] public IValue<AnimatedText> TextReference;

        public AnimatedText Value
        {
            get => TextReference.Value;
            set
            {
                IVariable<AnimatedText> variable = TextReference as IVariable<AnimatedText>;
                if (variable != null)
                {
                    variable.Value = value;
                    if (AssignTextOnReferenceChange)
                        UpdateTextAnimation();
                }
            }
        }


        void Reset()
        {
            AnimationComponent = GetComponent<TextAnimationComponent>();
        }

        public void OnEnable()
        {
            if (AssignOnEnable)
                UpdateTextAnimation();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public void UpdateTextAnimation()
        {
            if (TextReference?.Value != null)
            {
                AnimationComponent.Animation = TextReference.Value.Animation;
                AnimationComponent.TextMeshProLabel.SetText(TextReference.Value.Text);
                AnimationComponent.TextMeshProLabel.SetAllDirty();
                if (PlayAnimationOnAssign)
                    AnimationComponent.PlayAnimation();
            }
            else
            {
                AnimationComponent.Animation = null;
                AnimationComponent.TextMeshProLabel.text = "";
            }
        }
    }
}
#endif
