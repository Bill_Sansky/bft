using UnityEngine;

namespace BFT
{
    public class TextAnimationDurationAsset : ScriptableObject
    {
        public TextAnimationDuration Duration;
    }
}
