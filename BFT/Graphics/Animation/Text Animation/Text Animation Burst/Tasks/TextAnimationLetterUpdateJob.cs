#if BFT_TEXTMESHPRO

using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace BFT
{
    [BurstCompile]
    public struct TextAnimationLetterUpdateJob : IJobParallelFor
    {
        public NativeSlice<TextLetterAnimInfo> LetterInfos;

        public TextAnimationData AnimationData;

        public void Execute(int index)
        {
            TextLetterAnimInfo info = LetterInfos[index];

            var lerpPercent = CalculateLerpPercent(index);

            UpdateColor(ref info, lerpPercent);

            UpdateTranslate(ref info, lerpPercent);
            UpdateRotation(ref info, lerpPercent);
            UpdateScale(ref info, lerpPercent);

            LetterInfos[index] = info;
        }

        private void UpdateColor(ref TextLetterAnimInfo info, float lerpPercent)
        {
            info.TargetColor = math.lerp(AnimationData.StartColor,
                AnimationData.TargetColor, lerpPercent).ToColor32();
        }

        private void UpdateTranslate(ref TextLetterAnimInfo info, float lerpPercent)
        {
            info.Translate = math.lerp(AnimationData.StartTranslate, AnimationData.TargetTranslate,
                    AnimationData.TranslateProfileCurve.Evaluate(lerpPercent))
                .ToVector3();

            info.Scale = Vector3.one;
        }

        private void UpdateRotation(ref TextLetterAnimInfo info, float lerpPercent)
        {
            info.Rotation = Quaternion.Lerp(AnimationData.StartRotation, AnimationData.TargetRotation,
                AnimationData.RotationProfileCurve.Evaluate(lerpPercent));
        }

        private void UpdateScale(ref TextLetterAnimInfo info, float lerpPercent)
        {
            info.Scale = math.lerp(AnimationData.StartScale, AnimationData.TargetScale,
                AnimationData.ScaleProfileCurve.Evaluate(lerpPercent));
        }

        private float CalculateLerpPercent(int index)
        {
            float lerpPercent = 0;
            if (AnimationData.SeparationType == TextAnimElementSeparationType.ALL_AT_ONCE)
            {
                lerpPercent = AnimationData.TotalTimePercent;
            }
            else
            {
                lerpPercent = AnimationData.GetElementTimePercent(index);
            }

            return AnimationData.TimeProfileCurve.Evaluate(lerpPercent);
        }
    }
}
#endif