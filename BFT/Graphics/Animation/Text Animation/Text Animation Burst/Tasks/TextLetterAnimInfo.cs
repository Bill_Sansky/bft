#if BFT_TEXTMESHPRO

using Unity.Mathematics;
using UnityEngine;

namespace BFT
{
    public struct TextLetterAnimInfo
    {
        public float4 StartColor;
        public Color32 TargetColor;

        public Vector3 Pivot;
        public Vector2 LetterDimension;

        public Vector3 TopLeft;
        public Vector3 TopRight;
        public Vector3 BottomLeft;
        public Vector3 BottomRight;

        public Vector3 StartTranslation;
        public Vector3 Translate;

        public Vector3 StartScale;
        public Vector3 Scale;

        public Quaternion StartRotation;
        public Quaternion Rotation;

        public int VertexIndex;
    }
}
#endif