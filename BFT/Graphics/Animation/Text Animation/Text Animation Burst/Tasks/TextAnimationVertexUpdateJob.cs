

#if BFT_TEXTMESHPRO && BFT_NATIVE
using Unity.Mathematics;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

namespace BFT
{
    [BurstCompile]
    public struct TextAnimationVertexUpdateJob : IJobParallelFor
    {
        public Matrix4x4 TextTransform;

        //TODO for multi text animation this is a problem: the data must come from each individual job.
        //the vertex job could be split per task as well
        public TextAnimationData LetterAnimationData;

        [Unity.Collections.ReadOnly] public NativeSlice<TextLetterAnimInfo> LetterInfos;
        [Unity.Collections.ReadOnly] public NativeSlice<TextVertexAnimInfo> VertexInfos;

        [WriteOnly] public NativeSlice<Color32> OutColors;
        [WriteOnly] public NativeSlice<Vector3> OutPositions;

        public void Execute(int index)
        {
            int letterIndex = VertexInfos[index].LetterID;

            TextLetterAnimInfo info = LetterInfos[letterIndex];
            Color32 letterColor = info.TargetColor;
            OutColors[index] = letterColor;

            Vector3 vertexOffset = new Vector3();
            int split4Index = VertexInfos[index].SplitID;

            switch (split4Index)
            {
                case 0:
                    vertexOffset = new Vector3(-LetterAnimationData.ElementPivotPercents.x * info.LetterDimension.x,
                        -LetterAnimationData.ElementPivotPercents.y * info.LetterDimension.y, 0);

                    vertexOffset = info.BottomLeft - info.Pivot;
                    break;
                case 1:
                    vertexOffset = new Vector3(-LetterAnimationData.ElementPivotPercents.x * info.LetterDimension.x,
                        (1 - LetterAnimationData.ElementPivotPercents.y) * info.LetterDimension.y, 0);

                    vertexOffset = info.TopLeft - info.Pivot;

                    break;
                case 2:
                    vertexOffset = new Vector3(
                        (1 - LetterAnimationData.ElementPivotPercents.x) * info.LetterDimension.x,
                        (1 - LetterAnimationData.ElementPivotPercents.y) * info.LetterDimension.y, 0);

                    vertexOffset = info.TopRight - info.Pivot;

                    break;
                case 3:
                    vertexOffset = new Vector3(
                        (1 - LetterAnimationData.ElementPivotPercents.x) * info.LetterDimension.x,
                        -LetterAnimationData.ElementPivotPercents.y * info.LetterDimension.y, 0);

                    vertexOffset = info.BottomRight - info.Pivot;

                    break;
            }

            Vector3 pivotOffset = new Vector3(
                info.LetterDimension.x * 0.5f * (0.5f - LetterAnimationData.ElementPivotPercents.x),
                info.LetterDimension.y * 0.5f * (0.5f - LetterAnimationData.ElementPivotPercents.y), 0);

            Vector3 originalPosition = vertexOffset + pivotOffset;

            Matrix4x4 rotationMat = Matrix4x4.Rotate(info.Rotation);

            Vector3 newPos = rotationMat.MultiplyPoint3x4(originalPosition);

            newPos -= pivotOffset;

            Matrix4x4 trsMat = Matrix4x4.TRS(info.Pivot + info.Translate, quaternion.identity, info.Scale);

            newPos = trsMat.MultiplyPoint3x4(newPos);


            OutPositions[index] = newPos;
        }
    }
}
#endif