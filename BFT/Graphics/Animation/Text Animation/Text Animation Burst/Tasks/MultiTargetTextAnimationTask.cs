#if BFT_TEXTMESHPRO

using System;

using System.Collections.Generic;
using TMPro;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

namespace BFT
{
    public class MultiTargetTextAnimationTask : TextAnimationLetterTask
    {
        private readonly List<JobHandle> letterHandles = new List<JobHandle>();
        private readonly List<JobHandle> vertexHandles = new List<JobHandle>();
        public float CurrentTime;

        public List<TextAnimationLetterTask> Tasks;

        public List<float> TimeStampsPerTasks;

        public void Initialize(List<SingleTargetedAnimation> animatioms, TextAnimElementSeparationType separation,
            TMP_Text text,
            NativeArray<TextLetterAnimInfo> letterInfos, float timeDelay = 0, Vector2 randomRange = default)
        {
            CurrentTime = 0;
            TimeStampsPerTasks = new List<float>(animatioms.Count);
            Tasks = new List<TextAnimationLetterTask>(animatioms.Count);

            float cumulativeDuration = 0;
            for (int i = 0; i < animatioms.Count; i++)
            {
                Tasks.Add(animatioms[i].CreateLetterTask(text, letterInfos));
                cumulativeDuration += Tasks[i].AnimationData.TotalLoopDuration;
            }

            for (int i = 0; i < Tasks.Count; i++)
            {
                switch (separation)
                {
                    case TextAnimElementSeparationType.RANDOM:
                        TimeStampsPerTasks.Add(UnityEngine.Random.Range(0,
                            cumulativeDuration - Tasks[i].AnimationData.TotalLoopDuration));
                        break;
                    case TextAnimElementSeparationType.AFTER_PREVIOUS:

                        TimeStampsPerTasks.Add(
                            i == 0 ? 0 : TimeStampsPerTasks[i - 1] + Tasks[i - 1].AnimationData.TotalLoopDuration);
                        break;
                    case TextAnimElementSeparationType.AFTER_PREVIOUS_RANDOM_TIME_DELAY:
                        float endOfPrevious =
                            i == 0 ? 0 : TimeStampsPerTasks[i - 1] + Tasks[i - 1].AnimationData.TotalLoopDuration;
                        TimeStampsPerTasks.Add(endOfPrevious +
                                               UnityEngine.Random.Range(timeDelay + randomRange.x,
                                                   timeDelay + randomRange.y));
                        break;
                    case TextAnimElementSeparationType.AFTER_PREVIOUS_WITH_DELAY:
                        endOfPrevious =
                            i == 0 ? 0 : TimeStampsPerTasks[i - 1] + Tasks[i - 1].AnimationData.TotalLoopDuration;
                        TimeStampsPerTasks.Add(endOfPrevious + timeDelay);

                        break;
                    case TextAnimElementSeparationType.ALL_AT_ONCE:
                        TimeStampsPerTasks.Add(0);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(separation), separation, null);
                }
            }
        }

        public override JobHandle UpdateLetterTask(float dt, JobHandle handle1)
        {
            CurrentTime += dt;
            letterHandles.Clear();

            letterHandles.Add(handle1);

            for (int i = 0; i < TimeStampsPerTasks.Count; i++)
            {
                float stamp = TimeStampsPerTasks[i];
                TextAnimationLetterTask task = Tasks[i];

                if (CurrentTime >= stamp && CurrentTime < Tasks[i].AnimationData.TotalLoopDuration + stamp)
                {
                    letterHandles.Add(task.UpdateLetterTask(dt, letterHandles[letterHandles.Count - 1]));
                }
            }

            return letterHandles[letterHandles.Count - 1];
        }

        public override JobHandle UpdateVertexTask(JobHandle handle, TextMeshAnimationComponent component)
        {
            vertexHandles.Clear();

            vertexHandles.Add(handle);

            for (int i = 0; i < TimeStampsPerTasks.Count; i++)
            {
                TextAnimationLetterTask task = Tasks[i];

                vertexHandles.Add(task.UpdateVertexTask(vertexHandles[vertexHandles.Count - 1], component));
            }

            return vertexHandles[vertexHandles.Count - 1];
        }


        public override TextAnimationLetterUpdateJob EditorLetterUpdateJob()
        {
            //todo EDITOR STUFF
            return default;
        }
    }
}
#endif