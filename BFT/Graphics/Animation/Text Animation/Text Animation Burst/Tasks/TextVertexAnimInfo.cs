namespace BFT
{
    public struct TextVertexAnimInfo
    {
        public int LetterID;
        public int SplitID;
    }
}
