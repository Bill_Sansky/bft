#if BFT_TEXTMESHPRO

using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

namespace BFT
{
    public abstract class TextAnimationLetterTask
    {
        public TextAnimationData AnimationData;
        public NativeSlice<TextLetterAnimInfo> LetterInfos;
        public NativeSlice<Color32> VertexColor;

        public NativeSlice<TextVertexAnimInfo> VertexInfos;
        public NativeSlice<Vector3> VertexPositions;


        public abstract JobHandle UpdateLetterTask(float dt, JobHandle handle);

        public abstract JobHandle UpdateVertexTask(JobHandle handle, TextMeshAnimationComponent component);

        public abstract TextAnimationLetterUpdateJob EditorLetterUpdateJob();

        public virtual void Dispose()
        {
            AnimationData.Dispose();
        }
    }
}
#endif
