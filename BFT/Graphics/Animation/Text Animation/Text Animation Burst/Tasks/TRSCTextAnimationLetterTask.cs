#if BFT_TEXTMESHPRO

using Unity.Jobs;

namespace BFT
{
    public class TRSCTextAnimationLetterTask : TextAnimationLetterTask
    {
        public override JobHandle UpdateLetterTask(float dt, JobHandle handle)
        {
            if (AnimationData.isDone)
                return new JobHandle();

            AnimationData.UpdateTime(dt);

            TextAnimationLetterUpdateJob job = new TextAnimationLetterUpdateJob()
            {
                LetterInfos = LetterInfos,
                AnimationData = AnimationData,
            };

            return job.Schedule(LetterInfos.Length, 64, handle);
        }

        public override JobHandle UpdateVertexTask(JobHandle handle, TextMeshAnimationComponent component)
        {
            var job = new TextAnimationVertexUpdateJob()
            {
                OutColors = component.vertexColor,
                LetterInfos = component.letterInfos,
                VertexInfos = component.vertexInfos,
                LetterAnimationData = AnimationData,
                OutPositions = component.vertexPositions,
                TextTransform = component.Text.transform.localToWorldMatrix
            };

            return job.Schedule(component.vertexColor.Length, 64, handle);
        }

        public override TextAnimationLetterUpdateJob EditorLetterUpdateJob()
        {
            return new TextAnimationLetterUpdateJob()
            {
                LetterInfos = LetterInfos,
                AnimationData = AnimationData,
            };
        }
    }
}
#endif