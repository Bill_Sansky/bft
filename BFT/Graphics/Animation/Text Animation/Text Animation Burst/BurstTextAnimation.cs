#if BFT_TEXTMESHPRO

using System;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace BFT
{
    [Serializable]
    public abstract class BurstTextAnimation
    {
        [BoxGroup("Duration")] [HideIf("DurationFromAsset")]
        public TextAnimationDuration Duration;

        [BoxGroup("Duration")] [ShowIf("DurationFromAsset")]
        public TextAnimationDurationAsset DurationAsset;

        [BoxGroup("Duration")] public bool DurationFromAsset = false;

        public Vector2 PivotPercent = new Vector2(0.5f, 0.5f);

        public AnimationCurve ProfileCurve;

        [ShowIf("IsDelayedSeparation")] public float SeparationDelay;

        [ShowIf("IsRandomDelay")] public Vector2 SeparationDelayRandomOffsetRange;
        [HideIf("IsAllTargeted")] public TextAnimElementSeparationType SeparationType;

        [FormerlySerializedAs("StartValueLogic")]
        public TextAnimStartValueLogicType startValueLogicType;

        public TextAnimationTargetType TargetType;

        private bool IsAllTargeted => TargetType == TextAnimationTargetType.EVERYTHING;

        protected bool IsDelayedSeparation =>
            SeparationType == TextAnimElementSeparationType.AFTER_PREVIOUS_WITH_DELAY ||
            SeparationType == TextAnimElementSeparationType.AFTER_PREVIOUS_RANDOM_TIME_DELAY;

        protected bool IsRandomDelay =>
            SeparationType == TextAnimElementSeparationType.AFTER_PREVIOUS_RANDOM_TIME_DELAY;

        protected bool IsOverrideStart => startValueLogicType == TextAnimStartValueLogicType.OVERRIDE;

        public abstract TextAnimationLetterTask CreateAnimationTasks(TextMeshAnimationComponent component,
            int endLetterID,
            int i);

        public abstract TextAnimationData CreateAnimationData(int startID, int endID, TMP_Text text);

        public void Awake()
        {
            if (DurationFromAsset)
                Duration = DurationAsset.Duration;
        }
    }
}
#endif
