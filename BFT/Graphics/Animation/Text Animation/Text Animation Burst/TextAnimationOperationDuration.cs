using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace BFT
{
    [Serializable]
    public class TextAnimationDuration
    {
        public float Duration;
        public bool DurationPerElement;
        [BoxGroup("Loop"), ShowIf("Loop")] public bool InfiniteLoop = true;

        [BoxGroup("Loop")] public bool Loop;

        [BoxGroup("Loop"), ShowIf("IsLoopAndNotInfinite")]
        public int LoopAmount;

        //TODO add loop type
        //  [BoxGroup("Loop"),ShowIf("Loop")]
        //   public TextAnimationLoopType LoopType;

        public bool LoopPerElement = false;
        [FormerlySerializedAs("ProfileCurve")] public AnimationCurve TimeCurve;

        private bool IsLoopAndNotInfinite => Loop && !InfiniteLoop;
    }

    public enum TextAnimationLoopType
    {
        RESTART,
        PING_PONG,
    }
}
