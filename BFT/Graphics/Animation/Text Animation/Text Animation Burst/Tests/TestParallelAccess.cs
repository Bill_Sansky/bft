#if BFT_TEXTMESHPRO

using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

namespace BFT
{
    public class TestParallelAccess : MonoBehaviour
    {
        private NativeArray<Common> Commons;
        private JobHandle handle;
        private JobHandle handle2;
        private NativeArray<Lerp> Lerps;

        private void OnEnable()
        {
            Commons = new NativeArray<Common>(3, Allocator.Persistent)
            {
                [0] = new Common() {Start = Vector3.up},
                [1] = new Common() {Start = Vector3.right},
                [2] = new Common() {Start = Vector3.forward}
            };

            Lerps = new NativeArray<Lerp>(6, Allocator.Persistent)
            {
                [0] = new Lerp() {CommonID = 2,},
                [1] = new Lerp() {CommonID = 0,},
                [2] = new Lerp() {CommonID = 1,},
                [3] = new Lerp() {CommonID = 0,},
                [4] = new Lerp() {CommonID = 1,},
                [5] = new Lerp() {CommonID = 2,}
            };
        }

        private void Update()
        {
            NativeSlice<Lerp> slice1 = Lerps.Slice(0, 2);
            NativeSlice<Lerp> slice2 = Lerps.Slice(2, 3);

            TestJob job = new TestJob()
            {
                Lerps = slice1,
                Commons = Commons
            };

            TestJob job2 = new TestJob()
            {
                Lerps = slice2,
                Commons = Commons
            };

            handle = job.Schedule(slice1.Length, 64);
            handle = job2.Schedule(slice2.Length, 64, handle);
        }

        public void LateUpdate()
        {
            handle.Complete();

            UnityEngine.Debug.Log("------------------- RESULT -------------------");
            for (int i = 0; i < Lerps.Length; i++)
            {
                UnityEngine.Debug.Log(Lerps[i].Current);
            }
        }

        public void OnDestroy()
        {
            handle.Complete();
            Commons.Dispose();
            Lerps.Dispose();
        }

        public struct Common
        {
            public Vector3 Start;
            public float duration;
        }

        public struct Lerp
        {
            public Vector3 Current;
            public int CommonID;
            public float currentTime;
        }

        [BurstCompile]
        public struct TestJob : IJobParallelFor
        {
            [ReadOnly] public NativeArray<Common> Commons;

            public NativeSlice<Lerp> Lerps;

            //enums are fine for burst
            public AxisBFT axis;

            public void Execute(int index)
            {
                Lerp lerp = Lerps[index];
                Common common = Commons[lerp.CommonID];
                lerp.Current += common.Start;
                Lerps[index] = lerp;
            }

            //RESULT: reading a different index is totally legal, just you cant write to a different index
        }
    }
}
#endif