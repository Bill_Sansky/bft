#if BFT_TEXTMESHPRO
using Sirenix.OdinInspector;

namespace BFT
{
    public partial class BurstTextAnimationAsset : SerializedScriptableObject
    {
        public TRSCTextAnimation trscTextAnimation = new TRSCTextAnimation();

        private void Awake()
        {
            trscTextAnimation.Awake();
        }
    }
}
#endif
