#if BFT_TEXTMESHPRO
using System;
using Sirenix.OdinInspector;
using TMPro;

namespace BFT
{
    [Serializable]
    public class TextAnimationTarget
    {
        [HideIf("TargetsWholeText")] public int EndIndex;
        [HideIf("TargetsWholeText")] public int StartIndex;
        public bool TargetsWholeText;

        public int GetLetterStart(TMP_Text text)
        {
            return TargetsWholeText ? 0 : StartIndex;
        }

        public int GetLetterEnd(TMP_Text text)
        {
            return TargetsWholeText ? text.textInfo.characterCount - 1 : EndIndex;
        }
    }
}
#endif
