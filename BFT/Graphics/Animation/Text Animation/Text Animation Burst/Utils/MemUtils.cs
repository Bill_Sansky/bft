﻿#if BFT_NATIVE
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;
using UnityEngine;
using Vector3 = System.Numerics.Vector3;
using Vector4 = System.Numerics.Vector4;

namespace BFT
{
    public static class MemUtils
    {
        public static unsafe void CopyVector3NativeToManaged(Vector3[] managed, NativeArray<float3> native)
        {
            fixed (void* managedArrayPointer = managed)
            {
                UnsafeUtility.MemCpy
                (
                    destination: managedArrayPointer,
                    source: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(native),
                    size: managed.Length * (long) UnsafeUtility.SizeOf<Vector3>()
                );
            }
        }

        public static unsafe void CopyColorNativeToManaged(Color32[] managed, NativeArray<float4> native)
        {
            fixed (void* managedArrayPointer = managed)
            {
                UnsafeUtility.MemCpy
                (
                    destination: managedArrayPointer,
                    source: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(native),
                    size: managed.Length * (long) UnsafeUtility.SizeOf<Vector3>()
                );
            }
        }

        public unsafe static void CopyManagedToNative(Vector3[] managed, NativeArray<float3> native)
        {
            fixed (void* managedArrayPointer = managed)
            {
                UnsafeUtility.MemCpy
                (
                    destination: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(native),
                    source: managedArrayPointer,
                    size: managed.Length * (long) UnsafeUtility.SizeOf<Vector3>()
                );
            }
        }

        public unsafe static void CopyManagedToNative(Vector4[] managed, NativeArray<float4> native)
        {
            fixed (void* managedArrayPointer = managed)
            {
                UnsafeUtility.MemCpy
                (
                    destination: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(native),
                    source: managedArrayPointer,
                    size: managed.Length * (long) UnsafeUtility.SizeOf<Vector4>()
                );
            }
        }

        public unsafe static void CopyManagedToNative(Color32[] managed, NativeArray<float4> native)
        {
            fixed (void* managedArrayPointer = managed)
            {
                UnsafeUtility.MemCpy
                (
                    destination: NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(native),
                    source: managedArrayPointer,
                    size: managed.Length * (long) UnsafeUtility.SizeOf<Color>()
                );
            }
        }
    }
}
#endif