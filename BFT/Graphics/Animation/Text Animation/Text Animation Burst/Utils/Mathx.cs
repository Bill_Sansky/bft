#if BFT_NATIVE
using System.Runtime.CompilerServices;
using Unity.Mathematics;

namespace BFT
{
    public static class Mathx
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float repeat(float t, float length)
        {
            return math.clamp(t - math.floor(t / length) * length, 0, length);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float pingpong(float t, float length)
        {
            t = repeat(t, length * 2f);
            return length - math.abs(t - length);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float2 bezier(float2 p1, float2 p2, float2 p3, float2 p4, float2 t)
        {
            var omt = 1f - t;
            var omt2 = omt * omt;
            var t2 = t * t;
            return
                p1 * (omt2 * omt) +
                p2 * (3f * omt2 * t) +
                p3 * (3f * omt * t2) +
                p4 * (t2 * t);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float2 beziertangent(float2 p1, float2 p2, float2 p3, float2 p4, float2 t)
        {
            var omt = 1f - t;
            var omt2 = omt * omt;
            var t2 = t * t;

            return math.normalize
            (
                p1 * (-omt2) +
                p2 * (3f * omt2 - 2f * omt) +
                p3 * (-3f * t2 + 2f * t) +
                p4 * (t2)
            );
        }
    }
}
#endif
