#if BFT_TEXTMESHPRO
using System;
using Sirenix.OdinInspector;
using TMPro;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class TRSCTextAnimation : BurstTextAnimation
    {
        public AnimationCurve ColorProfile;
        public AnimationCurve RotationCurve;
        public AnimationCurve ScaleCurve;
        [ShowIf("IsOverrideStart")] public Color StartColor;

        public Color TargetColor;

        public Vector3 TargetRotation;

        public Vector3 TargetScale;

        public Vector3 TargetTranslation;
        public AnimationCurve TranslationCurve;

        public override TextAnimationLetterTask CreateAnimationTasks(TextMeshAnimationComponent component,
            int startLetterID, int endLetterID)
        {
            NativeSlice<TextLetterAnimInfo>
                slice = component.letterInfos.Slice(startLetterID, endLetterID - startLetterID + 1);

            TRSCTextAnimationLetterTask task = new TRSCTextAnimationLetterTask()
            {
                LetterInfos = slice,
                AnimationData = CreateAnimationData(startLetterID, endLetterID, component.Text),
                // VertexColor = component.vertexColor.Slice(component.letterInfos[startLetterID])
            };

            return task;
        }

        public override TextAnimationData CreateAnimationData(int startID, int endID, TMP_Text text)
        {
            int elementNumber = endID - startID + 1;

            var data = new TextAnimationData()
            {
                DurationValue = Duration.Duration,
                IsDurationPerElement = Duration.DurationPerElement,
                CurrentTime = 0,
                TargetColor = TargetColor.ToFloat4(),
                TargetTranslate = TargetTranslation,
                StartRotation = quaternion.identity,
                TargetRotation = Quaternion.Euler(TargetRotation),
                TargetScale = TargetScale,
                StartScale = new float3(1, 1, 1),
                TimeProfileCurve = new NativeCurve(ProfileCurve),
                TranslateProfileCurve = new NativeCurve(TranslationCurve),
                RotationProfileCurve = new NativeCurve(RotationCurve),
                ScaleProfileCurve = new NativeCurve(ScaleCurve),
                ColorProfileCurve = new NativeCurve(ColorProfile),
                TextAnimationType = TextAnimationType.COLOR,
                ElementNumber = elementNumber,
                StartValueLogicType = startValueLogicType,
                StartColor = StartColor.ToFloat4(),
                SeparationType = SeparationType,
                SeparationDelay = SeparationDelay,
                RandomDelayRange = SeparationDelayRandomOffsetRange,
                ElementPivotPercents = PivotPercent,
                elementTimeStamps = new NativeArray<float>(elementNumber, Allocator.Persistent),
                IsLoop = Duration.Loop,
                LoopPerElement = Duration.LoopPerElement,
                LoopNumber = Duration.InfiniteLoop ? -1 : Duration.LoopAmount,
            };
            data.Initialize();
            return data;
        }
    }
}

#endif
