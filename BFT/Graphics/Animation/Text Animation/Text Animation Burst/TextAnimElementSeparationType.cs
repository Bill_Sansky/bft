namespace BFT
{
    public enum TextAnimElementSeparationType
    {
        RANDOM,
        AFTER_PREVIOUS,
        AFTER_PREVIOUS_RANDOM_TIME_DELAY,
        AFTER_PREVIOUS_WITH_DELAY,

        //TODO: add this delay option
        //AFTER_PREVIOUS_WITH_DELAY_LOOP_INCLUDED,
        ALL_AT_ONCE
    }
}
