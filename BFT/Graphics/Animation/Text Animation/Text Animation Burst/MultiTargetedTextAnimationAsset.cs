#if BFT_TEXTMESHPRO

using UnityEngine;

namespace BFT
{
    public class MultiTargetedTextAnimationAsset : ScriptableObject
    {
        public MultiTargetedTextAnimation TextAnimation;
    }
}
#endif