
#if BFT_TEXTMESHPRO
using System;
using Sirenix.OdinInspector;
using TMPro;
using Unity.Collections;

namespace BFT
{
    [Serializable]
    public class SingleTargetedAnimation : TargetedTextAnimation
    {
        [ShowIf("AnimationFromAsset")] public TextAnimationAsset AnimationAsset;
        public bool AnimationFromAsset;
        public TextAnimationTarget Target;

        [HideIf("AnimationFromAsset")] public TRSCTextAnimation TextAnimation;

        public override TextAnimationLetterTask CreateLetterTask(TMP_Text text,
            NativeArray<TextLetterAnimInfo> letterInfos)
        {
            /*    if (AnimationFromAsset)
                   {
                       return AnimationAsset.trscTextAnimation.CreateAnimationTasks(Target.GetLetterStart(text),
                           Target.GetLetterEnd(text),
                           letterInfos, text);
                   }*/

            /* return TextAnimation.CreateAnimationTasks(Target.GetLetterStart(text), Target.GetLetterEnd(text),
                 letterInfos, text);*/
            return null;
        }
    }
}
#endif
