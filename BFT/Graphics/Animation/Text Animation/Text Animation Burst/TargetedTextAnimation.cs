#if BFT_TEXTMESHPRO

using System;
using TMPro;
using Unity.Collections;

namespace BFT
{
    [Serializable]
    public abstract class TargetedTextAnimation
    {
        public abstract TextAnimationLetterTask CreateLetterTask(TMP_Text text,
            NativeArray<TextLetterAnimInfo> letterInfos);
    }
}
#endif