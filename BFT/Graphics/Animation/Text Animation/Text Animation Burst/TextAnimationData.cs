#if BFT_TEXTMESHPRO
using System;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;

namespace BFT
{
    public struct TextAnimationData
    {
        public float CurrentTime;
        public bool isDone;

        public int ElementNumber;

        public TextAnimElementSeparationType SeparationType;

        public float SeparationDelay;

        public Vector2 RandomDelayRange;

        public Vector2 ElementPivotPercents;

        /// <summary>
        ///     WARNING: this value depends on wether it's expressed per element or total: use Element Duration or Total Duration
        ///     to have the good amount
        /// </summary>
        public float DurationValue;

        public bool IsDurationPerElement;

        [ReadOnly] public NativeCurve TimeProfileCurve;

        public TextAnimationType TextAnimationType;

        public TextAnimStartValueLogicType StartValueLogicType;

        public Quaternion TargetRotation;
        public Quaternion StartRotation;

        [ReadOnly] public NativeCurve RotationProfileCurve;

        public float3 TargetTranslate;
        public float3 StartTranslate;

        [ReadOnly] public NativeCurve TranslateProfileCurve;

        public float3 TargetScale;
        public float3 StartScale;

        [ReadOnly] public NativeCurve ScaleProfileCurve;

        public float4 TargetColor;
        public float4 StartColor;

        [ReadOnly] public NativeCurve ColorProfileCurve;

        [ReadOnly] public NativeArray<float> elementTimeStamps;

        public NativeArray<byte> IsLoopDonePerElement;

        //TODO support stack of animation
        //public NativeArray<TextAnimationData> animationStack;

        public bool IsLoop;

        public int CurrentLoop;

        public TextAnimationLoopType LoopType;
        public int LoopNumber;

        public bool LoopPerElement;

        public float TotalTimePercent => math.clamp(CurrentTime / TotalLoopDuration, 0, 1);
        public float ElementDuration => IsDurationPerElement ? DurationValue : DurationValue / ElementNumber;

        public float TotalLoopDuration
        {
            get
            {
                if (SeparationType != TextAnimElementSeparationType.ALL_AT_ONCE)
                {
                    return elementTimeStamps[ElementNumber - 1] + ElementDuration;
                }
                else
                {
                    return IsDurationPerElement
                        ? DurationValue * ElementNumber
                        : DurationValue;
                }
            }
        }

        //TODO add loop


        public void UpdateTime(float delta)
        {
            CurrentTime += delta;
            if (IsLoop)
            {
                if (!LoopPerElement)
                {
                    if (CurrentTime < TotalLoopDuration)
                        return;

                    CurrentTime -= TotalLoopDuration;
                    CurrentLoop++;

                    if (LoopNumber > 0 && CurrentLoop >= LoopNumber)
                        isDone = true;
                }
                else
                {
                    for (int i = 0; i < elementTimeStamps.Length; i++)
                    {
                        if (IsLoopDonePerElement[i] == 1 || LoopNumber == -1)
                            continue;

                        float timeStamp = elementTimeStamps[i];
                        int loopAmount = (int) ((CurrentTime - timeStamp) / ElementDuration);

                        IsLoopDonePerElement[i] = (byte) (loopAmount > LoopNumber ? 1 : 0);
                    }
                }
            }
            else if (CurrentTime >= TotalLoopDuration)
                isDone = true;
        }

        public float GetElementTimePercent(int index)
        {
            if (IsLoop && LoopPerElement)
            {
                if (IsLoopDonePerElement[index] == 1)
                    return 1;

                float timeStamp = elementTimeStamps[index];
                int loopAmount = (int) ((CurrentTime - timeStamp) / ElementDuration);

                return math.clamp(
                    (CurrentTime - (GetTimeStampForElement(index) + ElementDuration * loopAmount)) / ElementDuration, 0,
                    1);
            }
            else
                return math.clamp((CurrentTime - GetTimeStampForElement(index)) / ElementDuration, 0, 1);
        }

        public float GetTimeStampForElement(int index)
        {
            return elementTimeStamps[index];
        }

        public void Initialize()
        {
            IsLoopDonePerElement = new NativeArray<byte>(IsLoop ? elementTimeStamps.Length : 1, Allocator.Persistent);

            for (int i = 0; i < elementTimeStamps.Length; i++)
            {
                switch (SeparationType)
                {
                    case TextAnimElementSeparationType.RANDOM:
                        elementTimeStamps[i] = UnityEngine.Random.Range(0, TotalLoopDuration - ElementDuration);
                        break;
                    case TextAnimElementSeparationType.AFTER_PREVIOUS:
                        elementTimeStamps[i] = ElementDuration * i;
                        break;
                    case TextAnimElementSeparationType.AFTER_PREVIOUS_RANDOM_TIME_DELAY:
                        elementTimeStamps[i] = ElementDuration * i +
                                               UnityEngine.Random.Range(SeparationDelay + RandomDelayRange.x,
                                                   SeparationDelay + RandomDelayRange.y) * i;
                        break;
                    case TextAnimElementSeparationType.AFTER_PREVIOUS_WITH_DELAY:
                        elementTimeStamps[i] = ElementDuration * i + SeparationDelay * i;
                        break;
                    case TextAnimElementSeparationType.ALL_AT_ONCE:
                        elementTimeStamps[i] = 0;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (IsLoop && LoopPerElement)
                {
                    IsLoopDonePerElement[i] = 0;
                }
            }
        }

        public void Dispose()
        {
            if (elementTimeStamps.IsCreated)
                elementTimeStamps.Dispose();
            if (TimeProfileCurve.IsCreated)
                TimeProfileCurve.Dispose();
            if (TranslateProfileCurve.IsCreated)
                TranslateProfileCurve.Dispose();
            if (RotationProfileCurve.IsCreated)
                RotationProfileCurve.Dispose();
            if (ScaleProfileCurve.IsCreated)
                ScaleProfileCurve.Dispose();
            if (ColorProfileCurve.IsCreated)
                ColorProfileCurve.Dispose();
            if (IsLoopDonePerElement.IsCreated)
                IsLoopDonePerElement.Dispose();
        }
    }
}
#endif