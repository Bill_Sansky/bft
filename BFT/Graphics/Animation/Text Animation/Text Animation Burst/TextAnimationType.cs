using System;

namespace BFT
{
    [Flags]
    public enum TextAnimationType
    {
        COLOR = 0,
        ROTATION = 1 << 0,
        TRANSLATION = 1 << 1,
        SCALE = 1 << 2,
        ALPHA = 1 << 3
    }
}
