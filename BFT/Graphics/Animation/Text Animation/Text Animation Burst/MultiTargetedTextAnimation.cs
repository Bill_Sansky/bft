#if BFT_TEXTMESHPRO

using System;
using System.Collections.Generic;
using TMPro;
using Unity.Collections;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class MultiTargetedTextAnimation : TargetedTextAnimation
    {
        public TextAnimElementSeparationType AnimationSeparationType;

        public Vector2 RandomDelayRange;
        public List<SingleTargetedAnimation> TargetedAnimations;

        public float TimeDelay;

        public override TextAnimationLetterTask CreateLetterTask(TMP_Text text,
            NativeArray<TextLetterAnimInfo> letterInfos)
        {
            MultiTargetTextAnimationTask animationTask = new MultiTargetTextAnimationTask();

            animationTask.Initialize(TargetedAnimations, AnimationSeparationType, text, letterInfos, TimeDelay,
                RandomDelayRange);
            return animationTask;
        }
    }
}
#endif
