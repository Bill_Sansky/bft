#if BFT_TEXTMESHPRO
using Sirenix.OdinInspector;
using TMPro;
using Unity.Collections;
using Unity.Jobs;
using UnityEditor;
using UnityEngine;
#if UNITY_EDITOR
#endif

namespace BFT
{
    public class TextMeshAnimationComponent : MonoBehaviour
    {
        [HideIf("UseTargetedAnimation")] public TextAnimationTarget AnimationTarget;

        private TextAnimationLetterTask animationTask;

        private bool isPlaying;


        public NativeArray<TextLetterAnimInfo> letterInfos;

        private JobHandle letterJobHandle = new JobHandle();

        public bool PlayAnimationOnEnable;

        public MultiTargetedTextAnimationAsset TargetedAnimationAsset;

        public bool TargetedAnimationFromAsset;

        [ShowIf("IsNonAssetAndTargeted")] public TargetedTextAnimation TargetedTextAnimation;
        public TMP_Text Text;

        [HideIf("UseTargetedAnimation")] public BurstTextAnimationAsset TextAnimationAsset;

        public bool UseTargetedAnimation = true;

        public NativeArray<Color32> vertexColor;

        public NativeArray<TextVertexAnimInfo> vertexInfos;
        private JobHandle vertexJobHandle = new JobHandle();
        public NativeArray<Vector3> vertexPositions;

        private bool IsNonAssetAndTargeted => UseTargetedAnimation && !TargetedAnimationFromAsset;

        public void Awake()
        {
            if (!UseTargetedAnimation)
            {
                TargetedTextAnimation = new SingleTargetedAnimation()
                {
                    Target = AnimationTarget,
                    TextAnimation = TextAnimationAsset.trscTextAnimation
                };
            }
            else if (TargetedAnimationFromAsset)
            {
                TargetedTextAnimation = TargetedAnimationAsset.TextAnimation;
            }
        }

        public void OnEnable()
        {
            AssignTextMesh(Text);
            if (PlayAnimationOnEnable)
                PlayAnimation();
        }

        public void AssignTextMesh(TMP_Text textMesh)
        {
            if (letterInfos.IsCreated)
                letterInfos.Dispose();

            Text.ForceMeshUpdate();
            Text.UpdateVertexData();

            //TODO modify the structure to work with multiple meshes

            vertexInfos =
                new NativeArray<TextVertexAnimInfo>(textMesh.textInfo.meshInfo[0].colors32.Length,
                    Allocator.Persistent);

            vertexColor = new NativeArray<Color32>(textMesh.textInfo.meshInfo[0].colors32.Length, Allocator.Persistent);
            vertexColor.CopyFrom(textMesh.textInfo.meshInfo[0].colors32);

            vertexPositions =
                new NativeArray<Vector3>(textMesh.textInfo.meshInfo[0].vertices.Length, Allocator.Persistent);
            vertexPositions.CopyFrom(textMesh.textInfo.meshInfo[0].vertices);

            //TODO apply targets (for now all characters affected
            letterInfos =
                new NativeArray<TextLetterAnimInfo>(textMesh.textInfo.characterCount,
                    Allocator.Persistent);

            for (var index = 0; index < textMesh.textInfo.characterCount; index++)
            {
                TMP_CharacterInfo cinfo = textMesh.textInfo.characterInfo[index];

                float width = (cinfo.topLeft - cinfo.topRight).magnitude;
                float height = (cinfo.topLeft - cinfo.bottomLeft).magnitude;

                Vector3 pivot = (cinfo.topLeft + cinfo.bottomRight) * 0.5f;
                TextLetterAnimInfo info = new TextLetterAnimInfo()
                {
                    StartColor = cinfo.color.ToFloat4(),
                    TargetColor = new Color(1, 1, 1, 1),
                    Pivot = pivot,
                    LetterDimension = new Vector2(width, height),
                    TopLeft = cinfo.topLeft,
                    TopRight = cinfo.topRight,
                    BottomLeft = cinfo.bottomLeft,
                    BottomRight = cinfo.bottomRight,
                    VertexIndex = cinfo.vertexIndex,
                };

                vertexInfos[cinfo.vertexIndex] = new TextVertexAnimInfo()
                {
                    LetterID = cinfo.index,
                    SplitID = 0,
                };

                vertexInfos[cinfo.vertexIndex + 1] = new TextVertexAnimInfo()
                {
                    LetterID = cinfo.index,
                    SplitID = 1,
                };

                vertexInfos[cinfo.vertexIndex + 2] = new TextVertexAnimInfo()
                {
                    LetterID = cinfo.index,
                    SplitID = 2,
                };

                vertexInfos[cinfo.vertexIndex + 3] = new TextVertexAnimInfo()
                {
                    LetterID = cinfo.index,
                    SplitID = 3,
                };

                letterInfos[index] = info;
            }
        }

        [Button(ButtonSizes.Medium)]
        public void PlayAnimation()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                UpdateTextMeshEditor();
                return;
            }
#endif
            animationTask?.Dispose();

            animationTask = TargetedTextAnimation.CreateLetterTask(Text, letterInfos);

            isPlaying = true;
        }

        public void OnDisable()
        {
            Dispose();
            isPlaying = false;
        }

        private void Dispose()
        {
            letterJobHandle.Complete();
            vertexJobHandle.Complete();

            if (letterInfos.IsCreated)
                letterInfos.Dispose();
            if (vertexColor.IsCreated)
                vertexColor.Dispose();
            if (vertexPositions.IsCreated)
                vertexPositions.Dispose();
            if (vertexInfos.IsCreated)
                vertexInfos.Dispose();
            animationTask?.Dispose();
        }

        public void UpdateTextMesh(TMP_Text text)
        {
            letterJobHandle = animationTask.UpdateLetterTask(UnityEngine.Time.deltaTime, new JobHandle());
            vertexJobHandle = animationTask.UpdateVertexTask(letterJobHandle, this);
        }

        public void Update()
        {
            if (isPlaying)
                UpdateTextMesh(Text);
        }

        public void LateUpdate()
        {
            vertexJobHandle.Complete();

            TMP_CharacterInfo[] charInfos = Text.textInfo.characterInfo;

            if (vertexColor.IsCreated)
            {
                vertexColor.CopyTo(Text.textInfo.meshInfo[0].colors32);
            }

            if (vertexPositions.IsCreated)
            {
                vertexPositions.CopyTo(Text.textInfo.meshInfo[0].vertices);
            }

            Text.UpdateVertexData();
        }

#if UNITY_EDITOR

        private bool isEditorUpdating = false;
        private double previousEditorTime;

        protected void UpdateTextMeshEditor()
        {
            Dispose();

            AssignTextMesh(Text);

            //TODO fix
            /*    animationTask = TextAnimationAsset.trscTextAnimation.CreateAnimationTasks(
                    AnimationTarget.GetLetterStart(Text),
                    AnimationTarget.GetLetterEnd(Text), letterInfos, Text);*/

            editorLetterJob = animationTask.EditorLetterUpdateJob();
            /*   vertexUpdateJob = new TextAnimationVertexUpdateJob()
               {
                   LetterAnimationData = animationTask.AnimationData,
                   TextTransform = Text.transform.localToWorldMatrix,
                   OutPositions = vertexPositions,
                   OutColors = vertexColor,
                   LetterInfos = letterInfos,
                   VertexInfos = vertexInfos
               };*/
            isEditorUpdating = true;
            previousEditorTime = EditorApplication.timeSinceStartup;

            EditorApplication.update -= EditorUpdate;
            EditorApplication.update += EditorUpdate;
        }

        private TextAnimationLetterUpdateJob editorLetterJob;

        public void EditorUpdate()
        {
            if (!isEditorUpdating)
                return;
            double delta = EditorApplication.timeSinceStartup - previousEditorTime;
            previousEditorTime = EditorApplication.timeSinceStartup;

            editorLetterJob.AnimationData.UpdateTime((float) delta);
            for (int i = 0; i < Text.textInfo.characterCount; i++)
            {
                editorLetterJob.Execute(i);
            }
/*
            for (int i = 0; i < vertexInfos.Length; i++)
            {
                vertexUpdateJob.Execute(i);
            }*/

            TMP_CharacterInfo[] charInfos = Text.textInfo.characterInfo;

            if (vertexColor.IsCreated)
            {
                vertexColor.CopyTo(Text.textInfo.meshInfo[0].colors32);
            }

            if (vertexPositions.IsCreated)
            {
                vertexPositions.CopyTo(Text.textInfo.meshInfo[0].vertices);
            }

            Text.UpdateVertexData();

            UnityEditor.EditorUtility.SetDirty(this);

            if (editorLetterJob.AnimationData.isDone)
            {
                StopAnimation();
            }
        }

        [Button(ButtonSizes.Medium), HideInPlayMode]
        private void StopAnimation()
        {
            isEditorUpdating = false;
            Dispose();
            EditorApplication.update -= EditorUpdate;
            Text.ForceMeshUpdate();
        }

#endif
    }

    //TODO add more job types for more target types
}
#endif