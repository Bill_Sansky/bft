﻿using System.Collections.Generic;
using System.Linq;
#if BFT_TEXTMESHPRO
using System.Text.RegularExpressions;

namespace BFT
{
    public static class AnimationTagHelper
    {
        private const string TagsRegex = @"\[a( [^\]]*){0,1}](.*?)\[/a]";

        public const string AnimationRegex = @"[[][/]{0,1}[a]( [^\]]*){0,1}[\]]";
        public const string AnimationOpenRegex = @"[[][a]( [^\]]*){0,1}[\]]";
        public const string AnimationCloseRegex = @"[[][/][a]( [^\]]*){0,1}[\]]";

        private const string animationStartRegex = "(?<!" + AnimationCloseRegex + "|^)" + AnimationOpenRegex;

        private const string animationEndsRegex = AnimationCloseRegex + "(?!" + AnimationOpenRegex + "|$)";
        //private const string HtmlTagRegex = "(.*?)\\<.*?>(.*?)<\\/.*?>(.*?)$";

        private static string RemoveHTMLTags(string text)
        {
            return Regex.Replace(text, "[<][^\\>]*[>]", "");
        }

        private static string FillMissingTextAnimationsTags(string rawText)
        {
            if (rawText.Contains("[a]") || rawText.Contains("[a "))
            {
                if (!rawText.StartsWith("[a]"))
                {
                    rawText = "[a]" + rawText;
                }

                if (!rawText.EndsWith("[/a]"))
                {
                    rawText = rawText + "[/a]";
                }

                rawText = Regex.Replace(rawText, animationStartRegex, delegate(Match m) { return "[/a]" + m.Value; });
                rawText = Regex.Replace(rawText, animationEndsRegex, delegate(Match m) { return m.Value + "[a]"; });
            }

            return rawText;
        }

        public static void UpdateAnimatedTextAnimations(string rawString, AnimatedText text,
            IEntryDictionary<ITextAnimation> animationEntryDictionary, ITextAnimation defaultAnimation)
        {
            rawString = FillMissingTextAnimationsTags(rawString);
            List<TextAnimationInfo> infos;
            string nonTaggedString = GetAnimatedStrings(rawString, out infos);

            text.Animation = null;
            text.Text = nonTaggedString;

            if (infos.Count == 0)
            {
                if (defaultAnimation == null)
                {
                    return;
                }

                //removing the HTML tags to calculate the string length
                string textWithoutHTML = RemoveHTMLTags(nonTaggedString);
                //MatchCollection htmlLookUpCollection =
                //    Regex.Matches(nonTaggedString, HtmlTagRegex);

                /*foreach (Match match in htmlLookUpCollection)
            {
                for (int i = 1; i < match.Groups.Count; i++)
                {
                    textWithoutHTML += match.Groups[i].Value;
                }
            }*/

                TargetOverrideTextAnimation overrideAnim =
                    new TargetOverrideTextAnimation
                    {
                        Length = textWithoutHTML.Length,
                        StartID = 0,
                        Animation = defaultAnimation
                    };
                text.Animation = overrideAnim;
                return;
            }


            TextAnimationSequence sequence = new TextAnimationSequence();
            sequence.PlayType = AnimationSequencePlayType.LINEAR;
            foreach (var info in infos)
            {
                TargetOverrideTextAnimation overrideAnim = new TargetOverrideTextAnimation();
                overrideAnim.Length = info.StringLength;
                overrideAnim.StartID = info.StartIndex;

                if (info.AnimationName.IsNullOrEmpty())
                {
                    if (defaultAnimation == null)
                    {
                        continue;
                    }

                    overrideAnim.Animation = defaultAnimation;
                }
                else
                {
                    overrideAnim.Animation = animationEntryDictionary
                        [animationEntryDictionary.ContentIDs.GetValueFromString(info.AnimationName)];
                }

                if (info.StartDelay > 0)
                {
                    sequence.AnimationSequence.Add(new DelayTextAnimation() {Delay = info.StartDelay});
                }

                sequence.AnimationSequence.Add(overrideAnim);
            }

            text.Animation = sequence;
        }

        public static string GetUntaggedString(string taggedData)
        {
            taggedData = FillMissingTextAnimationsTags(taggedData);
            MatchCollection matches = Regex.Matches(taggedData, TagsRegex);
            string sentenceWithoutTags = "";
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    if (match.Groups.Count > 2)
                    {
                        sentenceWithoutTags += match.Groups[2].Value;
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            else
            {
                return taggedData;
            }

            return sentenceWithoutTags;
        }


        public static string GetAnimatedStrings(string taggedData, out List<TextAnimationInfo> infos)
        {
            taggedData = FillMissingTextAnimationsTags(taggedData);
            //matches texts between animation tags, and returns matches for the anim parameters,
            //and for the text between the animation tags
            MatchCollection matches = Regex.Matches(taggedData, TagsRegex);

            infos = new List<TextAnimationInfo>();

            string sentenceWithoutTags = "";
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    if (match.Groups.Count > 2)
                    {
                        //if not some parameters were ignore, and the string is not valid

                        TextAnimationInfo info = new TextAnimationInfo();

                        string sentenceWithoutHTMLTags = RemoveHTMLTags(sentenceWithoutTags);
                        /*MatchCollection htmlLookUpCollection =
                        Regex.Matches(sentenceWithoutTags, HtmlTagRegex);

                    foreach (Match htmlMatch in htmlLookUpCollection)
                    {
                        for (int i = 1; i < htmlMatch.Groups.Count; i++)
                        {
                            sentenceWithoutHTMLTags += htmlMatch.Groups[i].Value;
                        }
                    }*/

                        // MatchCollection insideTagLookUpCollection =
                        //     Regex.Matches(sentenceWithoutTags, match.Groups[2].Value);

                        string insideTagWithoutHTML = RemoveHTMLTags(match.Groups[2].Value);

                        /*foreach (Match htmlMatch in insideTagLookUpCollection)
                    {
                        for (int i = 1; i < htmlMatch.Groups.Count; i++)
                        {
                            insideTagWithoutHTML += htmlMatch.Groups[i].Value;
                        }
                    }*/

                        info.StartIndex = sentenceWithoutHTMLTags.Count(_ => _ != ' ');
                        info.StringLength = insideTagWithoutHTML.Count(_ => _ != ' ');

                        string parameters = match.Groups[1].Value;

                        //matches any name parameter identified by n= and ending with either a whitespace or a bracket end
                        Match nameMatch = Regex.Match(parameters, "n=(.*)[ |\\]]?");
                        if (nameMatch.Groups.Count > 0)
                        {
                            info.AnimationName = nameMatch.Groups[1].Value;
                        }

                        //matches any numbers identified by d= and ending with either a whitespace or a bracket end
                        Match delayMatch = Regex.Match(parameters, "d=(\\d*\\.\\d*|\\d*)[ |\\]]?");

                        if (nameMatch.Groups.Count > 0)
                        {
                            double result;
                            if (double.TryParse(delayMatch.Groups[1].Value, out result))
                            {
                                info.StartDelay = (float) result;
                            }
                        }

                        infos.Add(info);
                        sentenceWithoutTags += match.Groups[2].Value;
                    }
                    else
                    {
                        /*     Debug.LogErrorFormat("The string {0} could not be parsed as an animated string:" +
                                                " make sure all the sentence pieces are in between [a n=XX d=00(optional)] " +
                                                "and [/a] for it to be valid", taggedData);*/
                        return "";
                    }
                }
            }
            else
            {
                //then the string does not contain any animation, return the string and no infos
                return taggedData;
            }

            return sentenceWithoutTags;
        }
    }
}
#endif
