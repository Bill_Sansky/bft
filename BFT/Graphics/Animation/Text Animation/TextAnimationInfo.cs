﻿//for now does not support setting up animations in parallel:
//the animations are going to come in sequence from the reading order

namespace BFT
{
    public class TextAnimationInfo
    {
        public string AnimationName = "";
        public float StartDelay = 0;
        public int StartIndex;
        public int StringLength;
    }
}
