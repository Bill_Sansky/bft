﻿#if BFT_TEXTMESHPRO
using UnityEngine;

namespace BFT
{
    public class TextAnimationEntry : DictionaryEntry<ITextAnimation>
    {
        [SerializeField] private ITextAnimation data;

        [SerializeField] private string nameID;

        public override string NameID
        {
            get => nameID;
            set => nameID = value;
        }

        public override ITextAnimation Data
        {
            get => data;
            set => data = value;
        }

        public override JsonData ExportJsonData()
        {
            throw new System.NotImplementedException();
        }

        public override void ParseJsonData(JsonData data)
        {
            throw new System.NotImplementedException();
        }

        public override void NotifyJSonDataDeleteRequest()
        {
            throw new System.NotImplementedException();
        }
    }
}

#endif
