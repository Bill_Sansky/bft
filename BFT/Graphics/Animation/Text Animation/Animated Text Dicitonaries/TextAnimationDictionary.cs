﻿#if BFT_TEXTMESHPRO

namespace BFT
{
    public class TextAnimationDictionary : DictionaryAsset<ITextAnimation, TextAnimationEntry>
    {
        public override TextAnimationEntry CreateNewDataHolder(object data)
        {
            return new TextAnimationEntry();
        }
    }
}

#endif
