﻿#if BFT_TEXTMESHPRO
using UnityEngine;

namespace BFT
{
    public class AnimatedTextEntry : DictionaryEntry<AnimatedText>
    {
        public static string ValueDicKey = "Value";

        [SerializeField] private AnimatedText data;

        [SerializeField] private string nameID;

        public override string NameID
        {
            get => nameID;
            set => nameID = value;
        }

        public override AnimatedText Data
        {
            get => data;
            set => data = value;
        }

        public override JsonData ExportJsonData()
        {
            JsonData jData = new JsonData();
            //for now does not export the animation values!
            jData.AddData("Text", data.Text);
            return jData;
        }

        public void RegenerateAnimations()
        {
            AnimationTagHelper.UpdateAnimatedTextAnimations(data.RawText, data,
                AnimatedTextDictionary.CurrentParsingDicitonary.textAnimationEntryDictionary,
                AnimatedTextDictionary.CurrentParsingDicitonary.DefaultAnimation);
        }

        public override void ParseJsonData(JsonData jData)
        {
            if (!jData.DataByID.ContainsKey(ValueDicKey))
            {
                UnityEngine.Debug.LogWarning("The parsed data did not contain data on the expected key '" + ValueDicKey + "'");
                return;
            }

            string rawString = jData.DataByID[ValueDicKey];
            if (data == null)
            {
                data = new AnimatedText();
            }

            data.RawText = rawString;
            AnimationTagHelper.UpdateAnimatedTextAnimations(rawString, data,
                AnimatedTextDictionary.CurrentParsingDicitonary.textAnimationEntryDictionary,
                AnimatedTextDictionary.CurrentParsingDicitonary.DefaultAnimation);
        }


        public override void NotifyJSonDataDeleteRequest()
        {
            data = null;
        }
    }
}
#endif
