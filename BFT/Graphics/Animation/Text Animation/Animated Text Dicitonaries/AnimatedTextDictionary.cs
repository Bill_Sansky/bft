﻿using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine.Serialization;

#if BFT_TEXTMESHPRO
#if UNITY_EDITOR

#endif

namespace BFT
{
    public class AnimatedTextDictionary : DictionaryAsset<AnimatedText, AnimatedTextEntry>
    {
        public static AnimatedTextDictionary CurrentParsingDicitonary;

        public ITextAnimation DefaultAnimation;

        [FormerlySerializedAs("TextAnimationDictionary")]
        public IEntryDictionary<ITextAnimation> textAnimationEntryDictionary;

        public override AnimatedTextEntry CreateNewDataHolder(object data)
        {
            return new AnimatedTextEntry();
        }

        [FoldoutGroup("Tools"), Button(ButtonSizes.Medium)]
        public void RegenerateAnimations()
        {
            CurrentParsingDicitonary = this;

            foreach (var pair in InternalDictionary)
            {
                pair.Value.RegenerateAnimations();
            }

#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }

        public void UpdateAnimation(int id, string rawText)
        {
            CurrentParsingDicitonary = this;
            if (!InternalDictionary.ContainsKey(id))
                Set(id, new AnimatedText());
            InternalDictionary[id].Data.RawText = rawText;
            InternalDictionary[id].RegenerateAnimations();

#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }

        public override void ParseTableData(string jsonTableData, bool createMissingData = false,
            bool deleteMissingData = false)
        {
            CurrentParsingDicitonary = this;
            base.ParseTableData(jsonTableData, createMissingData, deleteMissingData);
        }
    }
}
#endif
