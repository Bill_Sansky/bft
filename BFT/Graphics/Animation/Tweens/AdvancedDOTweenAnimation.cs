﻿#if BFT_DOTWEEN

using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

using DG.Tweening;

namespace BFT
{
    [RequireComponent(typeof(DOTweenAnimation))]
    public class AdvancedDOTweenAnimation : SerializedMonoBehaviour, ITweenValue
    {
        [BoxGroup("Animation")] public DOTweenAnimation Animation;

        [SerializeField] [BoxGroup("Events")] private UnityEvent onEnd;

        [SerializeField] [BoxGroup("Events")] private UnityEvent onPause;

        [SerializeField] [BoxGroup("Events")] private UnityEvent onPlay;


        public Tween Value
        {
            get
            {
                if (Animation.tween == null)
                {
                    RebuildTween();
                }

                return Animation.tween;

#if UNITY_EDITOR
                /*if (!Application.isPlaying)
            {
                return Animation.CreateEditorPreview();
            }*/
#endif
            }
        }

        public AnimationUpdateType UpdateMode
        {
            get => Animation.updateType == UpdateType.Manual ? AnimationUpdateType.MANUAL : AnimationUpdateType.AUTO;
            set
            {
                if (value == AnimationUpdateType.MANUAL)
                {
                    Animation.updateType = UpdateType.Manual;
                    if (Animation.tween != null)
                    {
                        Animation.tween.SetUpdate(UpdateType.Manual);
                    }
                }
                else
                {
                    if (Animation.updateType == UpdateType.Manual)
                    {
                        Animation.updateType = UpdateType.Normal;
                        if (Animation.tween != null)
                        {
                            Animation.tween.SetUpdate(UpdateType.Normal);
                        }
                    }
                }
            }
        }

        public bool IsPlaying => this && Value.IsPlaying();
        public float Duration => Animation.duration;

        public UnityEvent OnPlay => onPlay;

        public UnityEvent OnEnd => onEnd;

        public UnityEvent OnPause => onPause;

        public void GoToTime(float time)
        {
            Value.Goto(time);
        }

        public void ManualUpdate(float deltaTime)
        {
            Animation.tween.SetUpdate(UpdateType.Manual);
            DOTween.ManualUpdate((float) deltaTime, (float) deltaTime);
        }

        public void SetTarget(string id, GameObject target)
        {
            Animation.targetGO = target;
        }

        public void Play()
        {
            Value.Play();
            Value.OnComplete(OnEnd.Invoke);
            onPlay.Invoke();
        }

        public void Stop()
        {
            Value.Complete();
            Value.Rewind();
        }

        public void Pause()
        {
            Value.Pause();
            onPause.Invoke();
        }

        public void Restart()
        {
            Value.Restart();
            onPlay.Invoke();
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void RebuildTween()
        {
            if (!this || !gameObject)
                return;

            Animation.CreateTween();
        }

        void Reset()
        {
            Animation = GetComponent<DOTweenAnimation>();
        }

        void Awake()
        {
            Animation.CreateTween();
        }

        public void RecreateAndPlay()
        {
            Animation.CreateTween();
            Animation.DOPlay();
        }
    }
}
#endif
