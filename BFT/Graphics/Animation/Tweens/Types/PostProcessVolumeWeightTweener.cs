#if BFT_POSTPROCESS

using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

namespace BFT
{
    public class PostProcessVolumeWeightTweener : FloatTweener
    {
        [SerializeField] private PostProcessVolume volume;

        private void Reset()
        {
            volume = GetComponent<PostProcessVolume>();
        }

        private float GetWeight()
        {
            return volume.weight;
        }

        private void SetWeight(float value)
        {
            volume.weight = value;
        }
    }
}
#endif