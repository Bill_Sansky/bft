﻿
#if BFT_DOTWEEN
using UnityEngine;
using DG.Tweening;
namespace BFT
{
    public class QuaternionTweener : GeneralTweener<Quaternion, QuaternionBFTFunction, QuaternionAction, QuaternionValue>
    {
        public Vector3 End => EndValue.Value.eulerAngles;

        protected override Tween BuildTween()
        {
            Tween = DOTween.To(DoGetter, DoSetter, End, TweenDuration.Value);
            return Tween;
        }
    }
}
#endif
