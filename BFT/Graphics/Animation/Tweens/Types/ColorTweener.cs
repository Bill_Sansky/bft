﻿#if BFT_DOTWEEN
using UnityEngine;
using DG.Tweening;

namespace BFT
{
    public class ColorTweener : GeneralTweener<Color, ColorBFTFunction, ColorAction, ColorValue>
    {
        protected override Tween BuildTween()
        {
            Tween = DOTween.To(DoGetter, DoSetter, EndValue.Value, TweenDuration.Value);
            return Tween;
        }
    }
}
#endif
