﻿#if BFT_DOTWEEN
using UnityEngine;
using DG.Tweening;

namespace BFT
{
    public class Vector3Tweener : GeneralTweener<Vector3, Vector3BFTFunction, Vector3Action, Vector3Value>
    {
        protected override Tween BuildTween()
        {
            Tween = DOTween.To(DoGetter, DoSetter, EndValue.Value, TweenDuration.Value);
            return Tween;
        }
    }
}

#endif
