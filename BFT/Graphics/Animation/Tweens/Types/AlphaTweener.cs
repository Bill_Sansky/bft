﻿#if BFT_DOTWEEN
using UnityEngine;
using DG.Tweening;

namespace BFT
{
    public class AlphaTweener : GeneralTweener<Color, ColorBFTFunction, ColorAction, ColorValue>
    {
        protected override Tween BuildTween()
        {
            Tween = DOTween.ToAlpha(DoGetter, DoSetter, EndValue.Value.a, TweenDuration.Value);
            return Tween;
        }
    }
}
#endif
