#if BFT_DOTWEEN

using Sirenix.OdinInspector;
using UnityEngine;
using DG.Tweening;

namespace BFT
{
    public class DOTweenAnimationDelayRandomizer : MonoBehaviour
    {
        public DOTweenAnimation Animation;

        [MinMaxSlider(0, 10)] public Vector2 Range;

        public bool SetDelayOnAwake = true;

        public void Reset()
        {
            Animation = GetComponent<DOTweenAnimation>();
        }

        public void Awake()
        {
            if (SetDelayOnAwake)
                SetDelay();
        }

        private void SetDelay()
        {
            Animation.delay = UnityEngine.Random.Range(Range.x, Range.y);
        }
    }
}
#endif
