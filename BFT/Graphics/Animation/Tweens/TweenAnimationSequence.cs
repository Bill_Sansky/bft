﻿#if BFT_DOTWEEN
using System;

using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using DG.Tweening;
namespace BFT
{
    public class TweenAnimationSequence : SerializedMonoBehaviour, ITweenValue
    {
        [BoxGroup("Options")] public bool AutoKill = false;

        [BoxGroup("Options")] public bool GenerateSequenceOnEnable = true;

        public bool LogDebug;

        [BoxGroup("Tweens")] public bool NestedPrefabUsage = false;

        public UnityEvent OnAnimationComplete;

        [SerializeField] private UnityEvent onPause;

        [SerializeField] private UnityEvent onPlay;

        [BoxGroup("Options")] public bool PlayOnEnable;

        [InfoBox("Tweens can only belong to ONE sequence at the same time," +
                 " and won't be usable outside of the sequence if added.")]
        [BoxGroup("Tweens"), ShowIf("NestedPrefabUsage")]
        public List<AdvancedDOTweenAnimation> PrefabTweens = new List<AdvancedDOTweenAnimation>();

/*
 *  WARNING : Not working properly anymore : investigate what's not working whenever possible
 * 
 * 
  */
        private Sequence sequence;

        public AnimationSequencePlayType SequenceType = AnimationSequencePlayType.LINEAR;

        [InfoBox("Tweens can only belong to ONE sequence at the same time," +
                 " and won't be usable outside of the sequence if added.")]
        [BoxGroup("Tweens"), OnValueChanged("CheckIfNotSelf"), HideIf("NestedPrefabUsage")]
        public List<ITweenValue> Tweens = new List<ITweenValue>();

        [BoxGroup("Options")] public UpdateType UpdateType = UpdateType.Normal;

        [ShowInInspector, FoldoutGroup("Status", false)]
        public bool IsInitialized => sequence?.IsInitialized() ?? false;

        [ShowInInspector, FoldoutGroup("Status", false)]
        public bool IsNull => sequence?.IsNull() ?? true;

        [ShowInInspector, FoldoutGroup("Status", false)]
        public bool IsComplete => sequence?.IsComplete() ?? false;

        public AnimationUpdateType UpdateMode
        {
            get => UpdateType == UpdateType.Manual ? AnimationUpdateType.MANUAL : AnimationUpdateType.AUTO;
            set
            {
                if (value == AnimationUpdateType.MANUAL)
                {
                    UpdateType = UpdateType.Manual;
                    if (sequence != null)
                    {
                        sequence.SetUpdate(UpdateType.Manual);
                    }
                }
                else
                {
                    if (UpdateType == UpdateType.Manual)
                    {
                        UpdateType = UpdateType.Normal;
                        if (sequence != null)
                        {
                            sequence.SetUpdate(UpdateType.Normal);
                        }
                    }
                }
            }
        }

        [ShowInInspector, FoldoutGroup("Status", false)]
        public bool IsPlaying => sequence?.IsPlaying() ?? false;

        public float Duration
        {
            get
            {
                if (sequence == null)
                {
                    RegenerateSequence();
                }

                return sequence.Duration();
            }
        }

        public UnityEvent OnPlay => onPlay;
        public UnityEvent OnEnd => OnAnimationComplete;
        public UnityEvent OnPause => onPause;

        public Tween Value
        {
            get
            {
#if UNITY_EDITOR
                if (!Application.isPlaying)
                {
                    RegenerateSequence();
                }
                else
#endif
                {
                    GenerateSequence();
                }

                return sequence;
            }
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools"), HideIf("IsEditMode")]
        public void Play()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                PrepareEditorPlay();
                return;
            }
#endif
            if (LogDebug)
            {
                UnityEngine.Debug.LogFormat(this, "sequence {0} started", this);
            }

            if (AutoKill)
            {
                RegenerateSequence();
            }

            sequence.Play();
            onPlay.Invoke();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools"), HideIf("IsEditMode")]
        public void Stop()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                PrepareEditorStop();
                return;
            }
#endif
            sequence.Complete();
            sequence.Rewind();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools"), HideIf("IsEditMode")]
        public void Pause()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                PrepareEditorStop();
                return;
            }
#endif
            sequence.Pause();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools"), HideIf("IsEditMode")]
        public void Restart()
        {
            sequence.Restart();
        }

        public void GoToTime(float time)
        {
            sequence.Goto(time);
        }

        public void ManualUpdate(float deltaTime)
        {
            sequence.SetUpdate(UpdateType.Manual);
            DOTween.ManualUpdate((float) deltaTime, (float) deltaTime);
        }

        public void SetTarget(string id, GameObject target)
        {
            foreach (List<ITweenValue> list in Tweens)
            {
                foreach (var value in list)
                {
                    value.SetTarget(id, target);
                }
            }
        }

        public void OnEnable()
        {
            if (GenerateSequenceOnEnable)
            {
                GenerateSequence();
            }

            if (PlayOnEnable)
            {
                Play();
            }
        }

        private void GenerateSequence()
        {
            if (sequence != null)
            {
                return;
            }

            RegenerateSequence();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public void RegenerateSequence()
        {
            DOTween.Init();

            sequence = DOTween.Sequence();
            sequence.SetUpdate(UpdateType);
            sequence.SetAutoKill(AutoKill);
            IEnumerable<ITweenValue> tweens = !NestedPrefabUsage ? Tweens : PrefabTweens.Convert(_ => _ as ITweenValue);
            foreach (ITweenValue tween in tweens)
            {
                if (SequenceType == AnimationSequencePlayType.LINEAR)
                {
                    sequence.Append(tween.Value);
                }
                else
                {
                    sequence.Insert(0, tween.Value);
                }
            }

            sequence.Complete(false);
            sequence.Rewind(false);
            sequence.AppendCallback(InvokeDone);
        }

        private void InvokeDone()
        {
            OnAnimationComplete.Invoke();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools"), HideIf("IsEditMode")]
        public void Complete()
        {
            sequence.Complete();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools"), HideIf("IsEditMode")]
        public void Rewind()
        {
            sequence.Rewind();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public void AddTween(ITweenValue tween)
        {
            Tweens.Add(tween);
        }

        public void PlayReversed()
        {
            sequence.Complete();
            sequence.PlayBackwards();
        }

#if UNITY_EDITOR

        private bool IsEditMode => !Application.isPlaying;

        public void CheckIfNotSelf()
        {
            Tweens.RemoveAll(_ => _.IsNull());

            foreach (List<ITweenValue> list in Tweens)
            {
                list.RemoveAll(_ => _.IsNull() || ReferenceEquals(_, this));
            }
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools"), ShowIf("IsEditMode")]
        public void PlayPreview()
        {
            PrepareEditorPlay();
            EditorUpdate.EditorUpdatePlug(EditorPlay);
        }

        public void PrepareEditorPlay()
        {
            RegenerateSequence();
            sequence.SetUpdate(UpdateType.Manual);

            sequence.ForceInit();
            sequence.SetAutoKill(false);
            sequence.Restart(false);
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools"), ShowIf("IsEditMode")]
        public void StopPreview()
        {
            PrepareEditorStop();
            EditorUpdate.RemoveEditorUpdatePlug(EditorPlay);
        }

        public void PrepareEditorStop()
        {
            try
            {
                sequence?.Complete();
                sequence.Rewind();
            }
            catch (Exception)
            {
                // ignored
            }
        }

        public void EditorPlay(double deltaTime)
        {
            DOTween.ManualUpdate((float) deltaTime, (float) deltaTime);
            if (sequence.IsComplete())
            {
                sequence.Rewind(false);
                EditorUpdate.RemoveEditorUpdatePlug(EditorPlay);
            }
        }
#endif
    }
}
#endif
