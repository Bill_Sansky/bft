﻿#if BFT_TEXTMESHPRO
using Sirenix.OdinInspector;

using TMPro;

using UnityEngine;

namespace BFT
{
    public class UIFontMaterialTextureUpdater : MonoBehaviour
    {
        public bool CloneSharedMaterialOnStart = false;
        public string PropertyName;

        //note that this class does not use material property blocks to be usable in the canvas renderer

        public TextMeshProUGUI Target;

        public IValue<Texture2D> Texture;

        private void Start()
        {
            if (CloneSharedMaterialOnStart)
            {
                CloneSharedMaterial();
            }
        }

        public void CloneSharedMaterial()
        {
            Target.fontSharedMaterial = new Material(Target.fontSharedMaterial);
        }

        public void UpdateMaterial()
        {
            UpdateMaterial(Target.fontSharedMaterial);
        }

        protected virtual void UpdateMaterial(Material mat)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                Target.fontSharedMaterial.SetTexture(PropertyName, Texture.Value);
                return;
            }
#endif
            mat.SetTexture(PropertyName, Texture.Value);
        }
    }
}
#endif
