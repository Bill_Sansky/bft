﻿using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Mesh/Mesh Reference")]
    public class MeshRendererReference : MonoBehaviour
    {
        public MeshRenderer MRenderer;
    }
}
