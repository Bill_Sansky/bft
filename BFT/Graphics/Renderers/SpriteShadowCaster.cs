﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Rendering;

namespace BFT
{
    public class SpriteShadowCaster : MonoBehaviour
    {
        public void Awake()
        {
            EnableShadows();
        }

        [Button(ButtonSizes.Medium)]
        private void EnableShadows()
        {
            Renderer renderer = GetComponent<Renderer>();
            renderer.shadowCastingMode = ShadowCastingMode.On;
        }
    }
}
