﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class TiledSpriteRendererSizeSetter : MonoBehaviour
    {
        public bool SetOnAwake = false;
        public SpriteRenderer SpriteRenderer;
        public FloatValue Width, Height;

        private void Awake()
        {
            if (SetOnAwake)
                SetSizes();
        }

        private void Reset()
        {
            if (SpriteRenderer == null)
            {
                SpriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            }
        }

        private void SetSizes()
        {
            if (SpriteRenderer && SpriteRenderer.drawMode == SpriteDrawMode.Tiled && Width != null && Height != null)
                SpriteRenderer.size = new Vector2(Width.Value, Height.Value);
        }

#if UNITY_EDITOR
        private void Update()
        {
            SetSizes();
        }
#endif
    }
}
