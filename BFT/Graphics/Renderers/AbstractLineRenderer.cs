﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public abstract class AbstractLineRenderer : MonoBehaviour
    {
        public LineRenderer lineRenderer;

        public Vector3Value offset;

        protected abstract int PositionsCount();
        protected abstract Vector3 GetPosition(int index);

        public bool UpdatePositionOnLateUpdate = true;

        void Reset()
        {
            lineRenderer = GetComponent<LineRenderer>();
        }

        private void LateUpdate()
        {
            if (UpdatePositionOnLateUpdate)
                UpdatePositions();
        }

        public void UpdatePositions()
        {
            int positionsCount = PositionsCount();
            if (positionsCount != lineRenderer.positionCount)
                lineRenderer.positionCount = positionsCount;

            for (int i = 0; i < positionsCount; i++)
            {
                lineRenderer.SetPosition(i, GetPosition(i) + offset.Value);
            }
        }
    }
}