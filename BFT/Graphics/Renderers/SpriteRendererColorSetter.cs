﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class SpriteRendererColorSetter : MonoBehaviour
    {
        [BoxGroup("References")] public ColorValue Color;

        [BoxGroup("References")] public SpriteRenderer Renderer;

        void Reset()
        {
            Renderer = GetComponent<SpriteRenderer>();
        }

        void OnEnable()
        {
            UpdateColor();
        }

        [BoxGroup("Utils"), Button(ButtonSizes.Medium)]
        public void UpdateColor()
        {
            if (!Renderer || Color == null)
                return;

            Renderer.color = Color.Value;
        }

        [OnInspectorGUI]
        public void OnDrawGizmos()
        {
            if (!Application.isPlaying)
                UpdateColor();
        }
    }
}
