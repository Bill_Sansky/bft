﻿using UnityEngine;

namespace BFT
{
    public class TransformsLineRenderer : AbstractLineRenderer
    {
        public TransformValue[] transforms;

        protected override Vector3 GetPosition(int index)
        {
            return transforms[index].position;
        }

        protected override int PositionsCount()
        {
            return transforms.Length;
        }
    }
}
