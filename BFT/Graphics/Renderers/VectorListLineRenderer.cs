﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    public class VectorListLineRenderer : AbstractLineRenderer
    {
        public List<Vector3Value> positions;

        protected override Vector3 GetPosition(int index)
        {
            return positions[index].Value;
        }

        protected override int PositionsCount()
        {
            return positions.Count;
        }
    }
}