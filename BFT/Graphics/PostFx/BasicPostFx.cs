﻿using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class BasicPostFx : MonoBehaviour
    {
        public Material testMat;

        void OnRenderImage(RenderTexture sourceImage, RenderTexture destinationImage)
        {
            UnityEngine.Graphics.Blit(sourceImage, destinationImage, testMat);
        }
    }
}
