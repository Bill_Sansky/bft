﻿using UnityEngine;

namespace BFT
{
    public class LocalPitch : MonoBehaviour
    {
        public UnityEngine.Transform trans;

        // Update is called once per frame
        void Update()
        {
            transform.localRotation = Quaternion.Euler(new Vector3(-trans.rotation.eulerAngles.z, 0, 0));
        }
    }
}
