﻿using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class LightSunlight : MonoBehaviour
    {
        public float DayAtmosphereThickness = 0.4f;

        public Vector3 DayRotateAxis;

        public Timer DayTimer;

#if UNITY_EDITOR

        public bool debugShowSunInEditor;

#endif
        public AnimationCurve FogDensityCurve;
        public float FogScale = 1f;


        UnityEngine.Light mainLight;

        public float MaxAmbient = 1f;

        public float MaxIntensity = 3f;
        public float MinAmbient = 0f;
        public float MinAmbientPoint = -0.2f;
        public float MinIntensity = 0f;
        public float MinPoint = -0.2f;
        public float NightAtmosphereThickness = 0.87f;

        public UnityEngine.Gradient NightDayColor;


        public UnityEngine.Gradient NightDayFogColor;
        public UnityEngine.Gradient NightlightColor;
        public Vector3 NightRotateAxis;
        public Timer NightTimer;
        Material skyMat;

        float skySpeed = 1;

        public UnityEngine.Gradient SunlightColor;

        void Start()
        {
            mainLight = GetComponent<UnityEngine.Light>();
            skyMat = RenderSettings.skybox;
        }

        void Update()
        {
#if UNITY_EDITOR

            if (!Application.isPlaying && !debugShowSunInEditor)
                return;
#endif

            float tRange = 1 - MinPoint;
            float dot = Mathf.Clamp01((Vector3.Dot(mainLight.transform.forward, Vector3.down) - MinPoint) / tRange);
            float i = ((MaxIntensity - MinIntensity) * dot) + MinIntensity;

            mainLight.intensity = i;

            tRange = 1 - MinAmbientPoint;
            dot = Mathf.Clamp01((Vector3.Dot(mainLight.transform.forward, Vector3.down) - MinAmbientPoint) / tRange);
            i = ((MaxAmbient - MinAmbient) * dot) + MinAmbient;
            RenderSettings.ambientIntensity = i;

            mainLight.color = NightDayColor.Evaluate(dot);
            RenderSettings.ambientLight = mainLight.color;

            RenderSettings.fogColor = NightDayFogColor.Evaluate(dot);
            RenderSettings.fogDensity = FogDensityCurve.Evaluate(dot) * FogScale;

            i = ((DayAtmosphereThickness - NightAtmosphereThickness) * dot) + NightAtmosphereThickness;
            skyMat.SetFloat("_AtmosphereThickness", i);

            RotateSun(dot, UnityEngine.Time.deltaTime);


#if UNITY_EDITOR
            RotateSun(dot, 0.1f);
#endif
        }

        private void RotateSun(float dot, float delta)
        {
            if (dot > 0)
                transform.Rotate(DayRotateAxis * delta * skySpeed);
            else
                transform.Rotate(NightRotateAxis * delta * skySpeed);
        }
    }
}
