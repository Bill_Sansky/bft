﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class LightIntensityLerpByPercent : MonoBehaviour
    {
#if UNITY_EDITOR
        [Range(0, 1)] public float DebugRange;
#endif

        public AnimationCurve IntensityProfile;
        private UnityEngine.Light lightToLerp;
        public float MaxIntensity;
        public float MinIntensity;

        [SerializeField] private IValue<float> percent;

        void Start()
        {
            lightToLerp = GetComponent<UnityEngine.Light>();
        }

        void Update()
        {
#if UNITY_EDITOR

            if (!Application.isPlaying)
            {
                lightToLerp.intensity = MathExt.EvaluateCurve(MinIntensity, MaxIntensity, IntensityProfile, DebugRange);
                return;
            }

#endif
            lightToLerp.intensity = MathExt.EvaluateCurve(MinIntensity, MaxIntensity, IntensityProfile, percent.Value);
        }
    }
}
