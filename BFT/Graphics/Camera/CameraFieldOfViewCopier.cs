﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class CameraFieldOfViewCopier : MonoBehaviour
    {
        private UnityEngine.Camera myCamera;

        public UnityEngine.Camera ToCopy;

        void Awake()
        {
            myCamera = GetComponent<UnityEngine.Camera>();
        }

        void OnEnable()
        {
            CopyFieldOfView();
        }

        void Update()
        {
            CopyFieldOfView();
        }

        private void CopyFieldOfView()
        {
            myCamera.fieldOfView = ToCopy.fieldOfView;
        }

        [OnInspectorGUI]
        void OnDrawGizmos()
        {
            if (Application.isPlaying)
                return;
            if (myCamera)
                CopyFieldOfView();
        }
    }
}
