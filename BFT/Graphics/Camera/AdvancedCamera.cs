﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [RequireComponent(typeof(UnityEngine.Camera))]
    public class AdvancedCamera : MonoBehaviour, IValue<UnityEngine.Camera>
    {
        [SerializeField, HideInInspector] private UnityEngine.Camera cam;

        [ShowIf("IsReferenced")] public CameraVariable CameraReference;

        [Tooltip("If true, the camera will register itself on a variable on enable, and unregister itself on disable")]
        public bool IsReferenced;

        public UnityEngine.Camera Value => cam;

        void Reset()
        {
            cam = GetComponent<UnityEngine.Camera>();
        }

        void Awake()
        {
            if (!cam)
                cam = GetComponent<UnityEngine.Camera>();
        }

        void OnEnable()
        {
            if (IsReferenced)
                CameraReference.Value = cam;
        }

        void OnDisable()
        {
            if (IsReferenced)
            {
                CameraReference.Value = null;
            }
        }
    }
}
