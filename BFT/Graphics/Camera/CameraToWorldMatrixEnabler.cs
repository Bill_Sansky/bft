﻿using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class CameraToWorldMatrixEnabler : MonoBehaviour
    {
        private UnityEngine.Camera cam;

        void Reset()
        {
            cam = GetComponent<UnityEngine.Camera>();
            Shader.SetGlobalMatrix("_Camera2World", cam.cameraToWorldMatrix);
        }

        // Use this for initialization
        void Start()
        {
            cam = GetComponent<UnityEngine.Camera>();
            Shader.SetGlobalMatrix("_Camera2World", cam.cameraToWorldMatrix);
        }

        void OnPreCull()
        {
            Shader.SetGlobalMatrix("_Camera2World", cam.cameraToWorldMatrix);
        }
    }
}
