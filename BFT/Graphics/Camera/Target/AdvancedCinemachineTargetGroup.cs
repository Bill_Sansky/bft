﻿#if BFT_CINE
using System.Collections;
using System.Linq;
using Cinemachine;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [RequireComponent(typeof(CinemachineTargetGroup))]
    public class AdvancedCinemachineTargetGroup : MonoBehaviour
    {
        [FoldoutGroup("Options")] public float DefaultAddingTime = .5f;

        [FoldoutGroup("Options")] public float DefaultRemovingTime = .5f;

        private CinemachineTargetGroup group;

        [FoldoutGroup("Options")] public bool UseUnscaledTime;

        void Awake()
        {
            group = GetComponent<CinemachineTargetGroup>();
            RemoveNullTarget();
        }

        void OnDestroy()
        {
            RemoveNullTarget();
        }

        [OnInspectorGUI]
        private void RemoveNullTarget()
        {
            if (!group)
                group = GetComponent<CinemachineTargetGroup>();
            if (group)
                group.m_Targets = group.m_Targets.Where(_ => _.target != null).ToArray();
        }

        public void AddTarget(UnityEngine.Transform trans, float weight, float radius, float time = -1)
        {
            if (Mathf.Approximately(time, -1))
            {
                StartCoroutine(AddTransformOverTime(trans, weight, radius, DefaultAddingTime));
            }
            else if (Mathf.Approximately(time, 0))
            {
                group.m_Targets = group.m_Targets.Add(new CinemachineTargetGroup.Target
                {
                    target = trans,
                    radius = radius,
                    weight = weight
                });
            }
            else
            {
                StartCoroutine(AddTransformOverTime(trans, weight, radius, time));
            }
        }

        private IEnumerator AddTransformOverTime(UnityEngine.Transform trans, float weight,
            float radius, float time)
        {
            group.m_Targets = group.m_Targets.Add(new CinemachineTargetGroup.Target
            {
                target = trans,
                radius = 0,
                weight = 0
            });

            float currentTime = 0;
            while (currentTime < time)
            {
                int index = IndexOf(trans);
                if (index == -1)
                    break;

                group.m_Targets[index].radius = Mathf.Lerp(0, radius, currentTime / time);
                group.m_Targets[index].weight = Mathf.Lerp(0, weight, currentTime / time);
                currentTime += UnityEngine.Time.deltaTime;
                yield return null;
            }
        }

        public int IndexOf(UnityEngine.Transform target)
        {
            for (int i = 0; i < group.m_Targets.Length; i++)
            {
                if (group.m_Targets[i].target == target)
                    return i;
            }

            return -1;
        }

        public void RemoveTarget(UnityEngine.Transform toRemove, float time = -1)
        {
            if (Mathf.Approximately(time, -1))
                StartCoroutine(RemoveTransformOverTime(toRemove, DefaultRemovingTime));
            else if (Mathf.Approximately(time, 0))
            {
                group.m_Targets = group.m_Targets.Where(_ => _.target != toRemove).ToArray();
            }
            else
            {
                StartCoroutine(RemoveTransformOverTime(toRemove, time));
            }
        }

        private IEnumerator RemoveTransformOverTime(UnityEngine.Transform trans, float time)
        {
            int index = IndexOf(trans);
            if (index == -1)
                yield break;

            CinemachineTargetGroup.Target target = group.m_Targets[index];
            float originalWeight = target.weight;
            float originalRadius = target.radius;

            float currentTime = 0;
            while (currentTime < time)
            {
                index = IndexOf(trans);
                if (index == -1)
                    break;

                group.m_Targets[index].radius = Mathf.Lerp(originalRadius, 0, currentTime / time);
                group.m_Targets[index].weight = Mathf.Lerp(originalWeight, 0, currentTime / time);
                currentTime += UnityEngine.Time.deltaTime;

                yield return null;
            }

            group.m_Targets = group.m_Targets.Where(_ => _.target != trans).ToArray();
        }

        public bool Contains(UnityEngine.Transform trans)
        {
            return group.m_Targets.Any(_ => _.target == trans);
        }
    }
}
#endif