#if BFT_CINE
using Cinemachine;
using UnityEngine;

namespace BFT
{
    public class VirtualCameraTargetSetter : MonoBehaviour
    {
        public CinemachineVirtualCamera VirtualCam;

        public GameObjectValue Target;
        public bool LookAtTarget;
        public bool FollowTarget;

        public bool SetTargetOnEnable = false;

        public void OnEnable()
        {
            if (SetTargetOnEnable)
                SetTarget();
        }

        public void SetTarget()
        {
            if (LookAtTarget)
                VirtualCam.LookAt = Target.Value.transform;
            if (FollowTarget)
                VirtualCam.Follow = Target.Value.transform;
        }
    }
}
#endif