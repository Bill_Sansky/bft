#if BFT_CINE
using Cinemachine;
using UnityEngine;

namespace BFT
{
    public class CinemachineDollyTrackPositionFromFloatValue : MonoBehaviour
    {
        public CinemachineVirtualCamera Camera;

        private CinemachineTrackedDolly dolly;
        
        public FloatValue DollyPosition;
        
        void Reset()
        {
            Camera = GetComponent<CinemachineVirtualCamera>();
        }

        void Awake()
        {
            dolly = Camera.GetCinemachineComponent<CinemachineTrackedDolly>();
        }

        void Update()
        {
            dolly.m_PathPosition = DollyPosition.Value;
        }
    }
}
#endif