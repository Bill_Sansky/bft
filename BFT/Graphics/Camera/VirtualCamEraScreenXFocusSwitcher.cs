﻿#if BFT_CINE
using System.Collections;
using UnityEngine;
using Sirenix.OdinInspector;
using Cinemachine;

namespace BFT
{
    [RequireComponent(typeof(Timer))]
    public class VirtualCameraScreenXFocusSwitcher : SerializedMonoBehaviour
    {
        public float CheckFrequency = 0.5f;
        private CinemachineComposer composer;

        public IValue<bool> Condition;

        public AnimationCurve EasingProfile;

        private bool previousCondition;
        public float ScreenXOnFalse;

        public float ScreenXOnTrue;

        private IEnumerator switchScreenOverTime;

        private Timer timer;

        private CinemachineVirtualCamera virtualCam;

        void Awake()
        {
            virtualCam = GetComponent<CinemachineVirtualCamera>();
            previousCondition = Condition.Value;
            composer = virtualCam.GetCinemachineComponent<CinemachineComposer>();
            timer = GetComponent<Timer>();
            if (Condition.Value)
                SwitchToTrue();
            else
            {
                SwitchToFalse();
            }
        }

        void OnEnable()
        {
            StopAllCoroutines();
            StartCoroutine(CheckBool());
        }

        [Button]
        private void SwitchToTrue()
        {
#if UNITY_EDITOR

            if (composer == null)
            {
                Awake();
            }
#endif
            composer.m_ScreenX = ScreenXOnTrue;
        }

        [Button]
        private void SwitchToFalse()
        {
#if UNITY_EDITOR

            if (composer == null)
            {
                Awake();
            }
#endif

            composer.m_ScreenX = ScreenXOnFalse;
        }

        [Button]
        private void SwitchScreenX()
        {
#if UNITY_EDITOR

            if (composer == null)
            {
                Awake();
            }

            if (!Application.isPlaying)
            {
                composer.m_ScreenX = (previousCondition) ? ScreenXOnTrue : ScreenXOnFalse;
                return;
            }
#endif
            if (switchScreenOverTime != null)
                StopCoroutine(switchScreenOverTime);
            StartCoroutine(switchScreenOverTime = SwitchScreenOverTime());
        }

        public IEnumerator SwitchScreenOverTime()
        {
            timer.StartTimer();
            float previousScreenX = composer.m_ScreenX;
            float newScreenX = (previousCondition) ? ScreenXOnTrue : ScreenXOnFalse;
            while (!timer.IsDone)
            {
                composer.m_ScreenX = Mathf.Lerp(previousScreenX, newScreenX, EasingProfile.Evaluate(timer.Percent));
                yield return null;
            }

            composer.m_ScreenX = newScreenX;
        }

        private IEnumerator CheckBool()
        {
            while (true)
            {
                if (Condition.Value != previousCondition)
                {
                    previousCondition = Condition.Value;
                    SwitchScreenX();
                }

                yield return new WaitForSeconds(CheckFrequency);
            }
        }
    }
}
#endif
