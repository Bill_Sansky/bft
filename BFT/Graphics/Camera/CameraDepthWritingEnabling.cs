﻿using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class CameraDepthWritingEnabling : MonoBehaviour
    {
        public UnityEngine.Camera cam;
        public DepthTextureMode mode;


        void Reset()
        {
            if (!cam)
                cam = GetComponent<UnityEngine.Camera>();
            cam.depthTextureMode = mode;
        }

        // Use this for initialization
        void Awake()
        {
            cam.depthTextureMode = mode;
        }

#if UNITY_EDITOR

        void Update()
        {
            if (!Application.isPlaying)
                cam.depthTextureMode = mode;
        }


#endif
    }
}
