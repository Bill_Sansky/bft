﻿#if BFT_CINE
using UnityEngine;

namespace BFT
{
    public class CinemachineLocalTargetGroupUpdater : MonoBehaviour
    {
        public bool AddOnEnter = true;
        public float RadiusToAdd = 1;
        public bool RemoveOnExit = true;

        public AdvancedCinemachineTargetGroup targetGroup;

        public float WeightToAdd = 1;

        public void OnTriggerEnter(Collider col)
        {
            if (!targetGroup || !AddOnEnter || targetGroup.Contains(col.transform))
                return;

            targetGroup.AddTarget(col.transform, WeightToAdd, RadiusToAdd);
        }

        public void OnTriggerExit(Collider col)
        {
            if (!targetGroup || !RemoveOnExit || !targetGroup.Contains(col.transform))
                return;

            targetGroup.RemoveTarget(col.transform);
        }
    }
}
#endif
