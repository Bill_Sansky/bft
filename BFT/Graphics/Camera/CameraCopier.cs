﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class CameraCopier : MonoBehaviour
    {
        public bool CopyMainCamera = true;

        public bool CopyPosition = true;
        public bool CopyRotation = true;

        [HideIf("CopyMainCamera")] public UnityEngine.Camera ToCopy;

        public void Start()
        {
            if (CopyMainCamera)
                ToCopy = UnityEngine.Camera.main;
        }

        public void Update()
        {
            if (!ToCopy)
                return;

            if (CopyPosition)
                transform.position = ToCopy.transform.position;
            if (CopyRotation)
                transform.rotation = ToCopy.transform.rotation;
        }
    }
}
