﻿using UnityEngine;

namespace BFT
{
    /// <summary>
    ///     Works with only one camera on the scene so far
    /// </summary>
    [ExecuteInEditMode]
    public class CameraFacingBillboard : MonoBehaviour
    {
        private UnityEngine.Camera cam;


        public bool OrientInEditMode;

        void Reset()
        {
            if (!cam)
                cam = UnityEngine.Camera.main;
        }

        // Use this for initialization
        void Awake()
        {
            if (!cam)
                cam = UnityEngine.Camera.main;
        }

        // Update is called once per frame
        void Update()
        {
#if UNITY_EDITOR

            if (!Application.isPlaying && !cam)
                cam = UnityEngine.Camera.main;

            if (!Application.isPlaying && (!cam || !OrientInEditMode))
                return;
#endif

            transform.LookAt(transform.position + cam.transform.rotation * Vector3.forward,
                cam.transform.rotation * Vector3.up);
        }
    }
}
