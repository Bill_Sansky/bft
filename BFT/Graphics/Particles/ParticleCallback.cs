﻿using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class ParticleCallback : MonoBehaviour
    {
        [SerializeField] private UnityEvent onParticleSystemStopped;

        public void OnParticleSystemStopped()
        {
            onParticleSystemStopped.ExtendedInvoke(this);
        }
    }
}
