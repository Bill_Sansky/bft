﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    
    /// <summary>
    /// TODO: split into a threshold activator
    /// </summary>
    public class PercentageThresholdParticleActivator : MonoBehaviour
    {
        public ParticleSystem Particles;
        public PercentValue Percent;

        [Range(0, 1)] public float PercentThreshold;

        public void Update()
        {
            if (Percent.Value > PercentThreshold)
            {
                if (!Particles.isEmitting)
                    Particles.Play();
            }
            else
            {
                if (Particles.isEmitting)
                    Particles.Stop();
            }
        }
    }
}
