﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR
#endif

namespace BFT
{
    public class AdditiveLinkedSceneLoader : MonoBehaviour
    {
        [FoldoutGroup("Status"), ReadOnly, ShowInInspector]
        private readonly Dictionary<LinkedSceneComponent, Transform> linkedSceneAttachmentSlots
            = new Dictionary<LinkedSceneComponent, Transform>();

        [BoxGroup("Attachment")] public bool ActivateSceneOnLoaded = true;

        [BoxGroup("Limits")] public bool AllowScenesToLoadSeveralTimes = false;

        [BoxGroup("Limits")] public bool LimitSceneCount = false;

        [BoxGroup("Init"), ShowIf("LoadScenesOnEnable")]
        public bool LoadScenesAfterOneFrame = true;

        [BoxGroup("Init")] public bool LoadScenesOnEnable = false;

        [BoxGroup("Attachment"), ShowIf("HasAttachmentPoints")]
        public bool LoopAttachmentOnFull = false;

        [BoxGroup("Events")] public UnityEvent OnAllLinkedSceneLoaded;

        [BoxGroup("Events")] public UnityEvent OnLinkedSceneLoaded;

        [BoxGroup("Attachment")] public List<Transform> SceneAttachmentPoints = new List<Transform>();

        [BoxGroup("Limits"), ShowIf("LimitSceneCount")]
        public int SceneCountLimit = 2;

        [FoldoutGroup("Status"), ReadOnly, ShowInInspector]
        private List<int> scenesLeftToLoad = new List<int>();

        [BoxGroup("Loading"), OnValueChanged("CheckIfSelf")]
        public List<LinkedSceneData> ScenesToLoad;

        [BoxGroup("Init")] public bool UnloadScenesOnDestroy = false;

        public bool HasAttachmentPoints => SceneAttachmentPoints.Count > 0;

#if UNITY_EDITOR
        protected void CheckIfSelf()
        {
            if (ScenesToLoad.Any(_ =>
                _.EditorSceneReference == SceneUtil.GetAssetFromScene(gameObject.scene)))
            {
                ScenesToLoad = ScenesToLoad
                    .Where(_ => _.EditorSceneReference !=
                                SceneUtil.GetAssetFromScene(gameObject.scene)).ToList();
                UnityEngine.Debug.LogWarning("WARNING: You cannot ask a scene loader to load its " +
                                 "own scene to avoid loops", this);
            }
        }
#endif

        public void NotifyLinkedSceneReloaded(LinkedSceneComponent reloadScene, LinkedSceneComponent oldScene)
        {
            if (!linkedSceneAttachmentSlots.ContainsKey(oldScene))
            {
                return;
            }

            if (!reloadScene)
            {
                LinkedSceneManager.Instance.NotifyAttachmentRequest(this, oldScene.SceneData.LinkedSceneID);
            }
            else
            {
                linkedSceneAttachmentSlots.Add(reloadScene, linkedSceneAttachmentSlots[oldScene]);

                if (!reloadScene.IsAttachAlways)
                {
                    reloadScene.Activate(linkedSceneAttachmentSlots[reloadScene]);
                }
            }

            NotifySceneUnloaded(oldScene);
        }

        public void ReassignLinkedSceneReloaded(AdditiveLinkedSceneLoader oldLoader)
        {
            if (oldLoader != null)
            {
                foreach (var sceneData in oldLoader.linkedSceneAttachmentSlots)
                {
                    linkedSceneAttachmentSlots.Add(sceneData.Key, sceneData.Value);
                }
            }
        }

        public UnityEngine.Transform NotifyLinkedSceneLoaded(LinkedSceneComponent linkedScene)
        {
            if (SceneAttachmentPoints.IsEmpty())
            {
                if (linkedScene.UseTransformAttachment)
                {
                    UnityEngine.Debug.LogWarningFormat(this, "Warning: the scene {0} requires an attachment," +
                                                 " but none was available", linkedScene);
                }

                linkedSceneAttachmentSlots.Add(linkedScene, null);

                if (ActivateSceneOnLoaded)
                {
                    linkedScene.Activate(null);
                }

                return null;
            }

            if (linkedScene.UseTransformAttachment)
            {
                UnityEngine.Transform attached = SceneAttachmentPoints[0];
                SceneAttachmentPoints.RemoveAt(0);

                if (LoopAttachmentOnFull)
                {
                    SceneAttachmentPoints.Add(attached);
                }

                linkedSceneAttachmentSlots.Add(linkedScene, attached);

                if (ActivateSceneOnLoaded)
                {
                    linkedScene.Activate(attached);
                }

                return attached;
            }

            linkedSceneAttachmentSlots.Add(linkedScene, null);


            if (ActivateSceneOnLoaded)
            {
                linkedScene.Activate(null);
            }

            return null;
        }

        public void NotifySceneUnloaded(LinkedSceneComponent linkedScene)
        {
            if (!linkedSceneAttachmentSlots.ContainsKey(linkedScene))
            {
                return;
            }

            UnityEngine.Transform trans = linkedSceneAttachmentSlots[linkedScene];

            linkedSceneAttachmentSlots.Remove(linkedScene);

            if (LoopAttachmentOnFull)
            {
                return;
            }

            SceneAttachmentPoints.Add(trans);
        }

        public void ActivateAllScenes()
        {
            foreach (var slot in linkedSceneAttachmentSlots)
            {
                slot.Key.Activate(slot.Value);
            }
        }

        [FoldoutGroup("Tools"), Button(ButtonSizes.Medium)]
        public void LoadAllScenes()
        {
            LinkedSceneManager.Instance.OnSceneLoaded.AddListener(NotifyGlobalSceneLoaded);
            scenesLeftToLoad.Clear();

            foreach (var sceneData in ScenesToLoad)
            {
#if UNITY_EDITOR
                if (!AllowScenesToLoadSeveralTimes &&
                    SceneManager.GetSceneByName(sceneData.EditorSceneReference.name).isLoaded)
                    continue;
#endif


                if (!AllowScenesToLoadSeveralTimes && (sceneData.IsLoaded || sceneData.IsLoading))
                {
                    continue;
                }

                if (LimitSceneCount)
                {
                    if (SceneManager.sceneCount >= SceneCountLimit)
                    {
                        break;
                    }
                }

                scenesLeftToLoad.Add(sceneData.LinkedSceneID);

                sceneData.LoadLinkedScene();

                LinkedSceneManager.Instance.NotifyAttachmentRequest(this, sceneData.LinkedSceneID);
            }

            if (scenesLeftToLoad.Count == 0)
            {
                LinkedSceneManager.Instance.OnSceneLoaded.RemoveListener(NotifyGlobalSceneLoaded);
                OnAllLinkedSceneLoaded.Invoke();
            }
        }

        private void NotifyGlobalSceneLoaded(int arg0)
        {
            scenesLeftToLoad.Remove(arg0);

            if (scenesLeftToLoad.Count > 0)
                OnLinkedSceneLoaded.Invoke();
            else
            {
                LinkedSceneManager.Instance.OnSceneLoaded.RemoveListener(NotifyGlobalSceneLoaded);
                OnAllLinkedSceneLoaded.Invoke();
            }
        }

        [FoldoutGroup("Tools"), Button(ButtonSizes.Medium)]
        public void UnLoadAllScenes()
        {
            foreach (var sceneData in ScenesToLoad)
            {
                sceneData.UnLoadFirstLinkedSceneMatchingBuildID();
            }
        }

        [FoldoutGroup("Tools"), Button(ButtonSizes.Medium)]
        public void ReloadAllScenes()
        {
            foreach (var sceneData in ScenesToLoad)
            {
                sceneData.UnLoadFirstLinkedSceneMatchingBuildID();
                sceneData.LoadLinkedScene();
            }
        }

        [FoldoutGroup("Tools"), Button(ButtonSizes.Medium)]
        public void ActivateAllLoadedScenes()
        {
            foreach (var slot in linkedSceneAttachmentSlots)
            {
                slot.Key.Activate(slot.Value);
            }
        }

        public void OnEnable()
        {
            if (LoadScenesOnEnable)
            {
                if (LoadScenesAfterOneFrame)
                    this.CallAfterOneFrame(LoadAllScenes);
                else
                    LoadAllScenes();
            }
        }

        public void OnDestroy()
        {
            if (UnloadScenesOnDestroy)
            {
                UnLoadAllScenes();
            }

            foreach (var sceneData in ScenesToLoad)
            {
                LinkedSceneManager.Instance.NotifyEndOfAttachmentRequest(this, sceneData.LinkedSceneID);
            }
        }
    }
}
