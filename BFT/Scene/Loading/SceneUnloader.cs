﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BFT
{
    public class SceneUnloader : MonoBehaviour
    {
        [Button(ButtonSizes.Medium), DisableInEditorMode]
        public void UnLoadScene()
        {
            SceneManager.UnloadSceneAsync(gameObject.scene);
        }
    }
}
