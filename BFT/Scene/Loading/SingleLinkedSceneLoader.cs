﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class SingleLinkedSceneLoader : MonoBehaviour
    {
        public bool LoadAdditive = false;

        [BoxGroup("Init")] public bool LoadSceneOnEnable = false;

        [BoxGroup("Loading"), OnValueChanged("CheckIfSelf")]
        public LinkedSceneData SceneToLoad;

#if UNITY_EDITOR
        protected void CheckIfSelf()
        {
            if (SceneToLoad.EditorSceneReference == SceneUtil.GetAssetFromScene(gameObject.scene))
            {
                SceneToLoad = null;
                UnityEngine.Debug.LogWarning("WARNING: You cannot ask a scene loader to load its " +
                                 "own scene to avoid loops", this);
            }
        }
#endif

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void LoadScene()
        {
            SceneToLoad.LoadLinkedScene(LoadAdditive);
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void UnloadScene()
        {
            SceneToLoad.UnLoadFirstLinkedSceneMatchingBuildID();
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void ReloadScene()
        {
            SceneToLoad.UnLoadFirstLinkedSceneMatchingBuildID();
            SceneToLoad.LoadLinkedScene(false);
        }

        public void OnEnable()
        {
            if (LoadSceneOnEnable)
            {
                LoadScene();
            }
        }
    }
}
