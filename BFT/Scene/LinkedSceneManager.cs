﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

#if UNITY_EDITOR

#endif

namespace BFT
{
    [CreateAssetMenu(fileName = "LinkedSceneManager", menuName = "BFT/Scene Management/Linked Scene Manager")]
    public class LinkedSceneManager : SingletonSO<LinkedSceneManager>
    {
        [TabGroup("Utils")] private readonly Dictionary<int, List<AdditiveLinkedSceneLoader>> attachmentRequestsPerScene
            = new Dictionary<int, List<AdditiveLinkedSceneLoader>>();

        [TabGroup("Utils")] private readonly Dictionary<int, List<LinkedSceneComponent>> linkedSceneWaitingForReloads
            = new Dictionary<int, List<LinkedSceneComponent>>();

        [TabGroup("IDs"), SerializeField] private Dictionary<int, int> linkedIDsToBuildIDs
            = new Dictionary<int, int>();

        [NonSerialized, ShowInInspector] public List<int> LinkedScenesLoading = new List<int>();

        public IntEvent OnSceneLoaded;

        [TabGroup("Utils")] public Dictionary<int, string> SceneNamesByLinkedSceneIds = new Dictionary<int, string>();

        public ValueDropdownList<int> SceneIDs
        {
            get
            {
                var dd = new ValueDropdownList<int>();

                foreach (var sceneId in SceneNamesByLinkedSceneIds)
                {
                    dd.Add(sceneId.Value, sceneId.Key);
                }

                return dd;
            }
        }

        public Dictionary<int, int> LinkedIDsToBuildIDs => linkedIDsToBuildIDs;

        public void NotifyLinkedSceneLoadingStarted(int linkedSceneID)
        {
            LinkedScenesLoading.Add(linkedSceneID);
        }

        public void NotifyLinkedSceneLoadingDone(int linkedSceneID)
        {
            LinkedScenesLoading.Remove(linkedSceneID);
            OnSceneLoaded.Invoke(linkedSceneID);
        }

        public bool IsLinkedSceneLoading(int id)
        {
            return LinkedScenesLoading.Contains(id);
        }

        public LinkedSceneComponent PullSceneWaitingForCachedReload(int linkedSceneID)
        {
            if (!linkedSceneWaitingForReloads.ContainsKey(linkedSceneID)
                || linkedSceneWaitingForReloads[linkedSceneID].Count == 0)
                return null;

            LinkedSceneComponent scene = linkedSceneWaitingForReloads[linkedSceneID][0];
            linkedSceneWaitingForReloads[linkedSceneID].RemoveAt(0);

            return scene;
        }

        public void AddLinkedSceneWaitingForReload(LinkedSceneComponent scene)
        {
            if (!linkedSceneWaitingForReloads.ContainsKey(scene.SceneData.LinkedSceneID))
                linkedSceneWaitingForReloads.Add(scene.SceneData.LinkedSceneID, new List<LinkedSceneComponent>());
            linkedSceneWaitingForReloads[scene.SceneData.LinkedSceneID].Add(scene);
        }

        public AdditiveLinkedSceneLoader PullLoaderForID(int id)
        {
            if (!attachmentRequestsPerScene.ContainsKey(id)
                || attachmentRequestsPerScene[id] == null ||
                attachmentRequestsPerScene[id].Count == 0)
                return null;

            AdditiveLinkedSceneLoader loader = attachmentRequestsPerScene[id][0];
            attachmentRequestsPerScene[id].RemoveAt(0);
            return loader;
        }

        public void NotifyAttachmentRequest(AdditiveLinkedSceneLoader loader, int id)
        {
            if (!attachmentRequestsPerScene.ContainsKey(id))
                attachmentRequestsPerScene.Add(id, new List<AdditiveLinkedSceneLoader>());
            attachmentRequestsPerScene[id].Add(loader);
        }

        public void NotifyEndOfAttachmentRequest(AdditiveLinkedSceneLoader loader, int id)
        {
            if (!attachmentRequestsPerScene.ContainsKey(id))
                return;

            attachmentRequestsPerScene[id].Remove(loader);
        }

        public bool ContainsKey(int linkedSceneID)
        {
            return LinkedIDsToBuildIDs.ContainsKey(linkedSceneID);
        }

        public int GetSceneBuildID(int linkedSceneID)
        {
            if (!LinkedIDsToBuildIDs.ContainsKey(linkedSceneID))
                return -1;
            return LinkedIDsToBuildIDs[linkedSceneID];
        }

        public void RemoveLinkedSceneID(int id)
        {
            LinkedIDsToBuildIDs.Remove(id);

#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }

        public List<int> GetLinkedSceneIdsInBuild()
        {
            List<int> ids = new List<int>();

            foreach (var kv in LinkedIDsToBuildIDs)
            {
                if (kv.Value != -1)
                {
                    ids.Add(kv.Key);
                }
            }

            return ids;
        }

        public void LoadScene(int linkedSceneID, bool async = true)
        {
            if (!LinkedIDsToBuildIDs.ContainsKey(linkedSceneID))
                return;

            if (async)
            {
                SceneManager.LoadSceneAsync(LinkedIDsToBuildIDs[linkedSceneID], LoadSceneMode.Additive);
            }
            else
            {
                SceneManager.LoadScene(LinkedIDsToBuildIDs[linkedSceneID], LoadSceneMode.Additive);
            }
        }

#if UNITY_EDITOR

        public void UpdateBuildId(int linkedSceneID, int newBuildID, string sceneName)
        {
            if (!LinkedIDsToBuildIDs.ContainsKey(linkedSceneID))
                LinkedIDsToBuildIDs.Add(linkedSceneID, newBuildID);
            else
                LinkedIDsToBuildIDs[linkedSceneID] = newBuildID;

            SceneNamesByLinkedSceneIds.AddOrReplace(linkedSceneID, sceneName);

            EditorUtility.SetDirty(this);
        }

        [Button(ButtonSizes.Medium)]
        public void OpenEditorLinkedSceneManager()
        {
            Selection.SetActiveObjectWithContext(EditorLinkedSceneManager.Instance,
                EditorLinkedSceneManager.Instance);
        }
#endif
    }
}
