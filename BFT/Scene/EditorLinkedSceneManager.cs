﻿#if UNITY_EDITOR
using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

namespace BFT
{
    [InitializeOnLoad]
    [CreateAssetMenu(fileName = "EditorLinkedSceneManager",
        menuName = "BFT/Scene Management/Editor Linked Scene Manager")]
    public class EditorLinkedSceneManager : SingletonSO<EditorLinkedSceneManager>
    {
        [TabGroup("Scene Utilities"), ReadOnly]
        public readonly List<LinkedSceneComponent> OpenedLinkedScene = new List<LinkedSceneComponent>();

        [TabGroup("Tags"), SerializeField] private List<string> buildTags = new List<string>();

        [TabGroup("Tags", false, 999), SerializeField]
        private Dictionary<SceneAsset, TagList> buildTagsPerScene
            = new Dictionary<SceneAsset, TagList>();

        [TabGroup("Scene IDs"), SerializeField, ReadOnly]
        private Dictionary<SceneAsset, int> linkedSceneIDsBySceneAsset
            = new Dictionary<SceneAsset, int>();

        [TabGroup("Scene Preprocess", false, 999), SerializeField, ValueDropdown("ObjectTagsDropDown")]
        public List<string> ObjectsTagsToRemove;

        [TabGroup("Scene Utilities", false, -1)]
        public List<SceneAsset> ScenesToAlwaysOpen = new List<SceneAsset>();

        [TabGroup("Tags"), ValueDropdown("BuildTagsDropDown"), SerializeField, PropertyOrder(0)]
        private List<string> tagsToBuild = new List<string>();


        [TabGroup("Scene Utilities", false, -1)]
        public bool UseAlwaysOpenedScenes = true;

        static EditorLinkedSceneManager()
        {
            IsEditor = true;
        }

        [TabGroup("Scene Utilities"), ReadOnly]
        public List<SceneAsset> CurrentlyOpenedScene
        {
            get
            {
                List<SceneAsset> list = new List<SceneAsset>(EditorSceneManager.loadedSceneCount);
                for (int i = 0; i < EditorSceneManager.loadedSceneCount; i++)
                {
                    list.Add(AssetDatabase.LoadAssetAtPath<SceneAsset>(SceneManager.GetSceneAt(i).path));
                }

                return list;
            }
        }

        public ValueDropdownList<string> BuildTagsDropDown
        {
            get
            {
                ValueDropdownList<string> dropDown = new ValueDropdownList<string>();
                foreach (var key in buildTags)
                {
                    dropDown.Add(key);
                }

                return dropDown;
            }
        }

        public Dictionary<SceneAsset, int> LinkedSceneIDsBySceneAsset => linkedSceneIDsBySceneAsset;

        public IEnumerable<string> TagsToBuild => tagsToBuild;

        private ValueDropdownList<string> ObjectTagsDropDown()
        {
            ValueDropdownList<string> list = new ValueDropdownList<string>();
            foreach (string tag in UnityEditorInternal.InternalEditorUtility.tags)
            {
                list.Add(tag);
            }

            return list;
        }

        public bool IsLinkedSceneOpenedInEditor(int linkedSceneID)
        {
            foreach (LinkedSceneComponent linkedScene in OpenedLinkedScene)
            {
                if (linkedScene.SceneData.LinkedSceneID == linkedSceneID)
                    return true;
            }

            return false;
        }

        [Button(ButtonSizes.Medium)]
        public void SyncTagsToBuildSettings()
        {
            List<SceneAsset> toRemove = new List<SceneAsset>();
            List<SceneAsset> toAdd = new List<SceneAsset>();

            foreach (var pair in buildTagsPerScene)
            {
                if (pair.Value.Tags.ContainsAny(TagsToBuild))
                    toAdd.Add(pair.Key);
                else
                {
                    toRemove.Add(pair.Key);
                }
            }

            foreach (var sceneAsset in toAdd)
            {
                if (sceneAsset)
                    SceneUtil.AddSceneToBuildSettings(sceneAsset);
            }

            foreach (SceneAsset sceneAsset in toRemove)
            {
                SceneUtil.RemoveSceneFromBuildSettings(sceneAsset);
            }

            SceneUtil.RemoveNullScenesFromBuildSetting();
            UpdateSceneReferences();
            EditorUtility.SetDirty(this);
        }

        public int RegisterSceneEditor(LinkedSceneData data)
        {
            EditorUtility.SetDirty(this);

            return AddSceneAssetToManage(data.EditorSceneReference);
        }

        public int AddSceneAssetToManage(SceneAsset asset)
        {
            int id;
            if (LinkedSceneIDsBySceneAsset.TryGetValue(asset, out id))
            {
                return id;
            }

            int newId = linkedSceneIDsBySceneAsset.Values.AsEnumerable().NextAvailableID();
            if (LinkedSceneIDsBySceneAsset.ContainsValue(newId))
            {
                Debug.LogError("ID automatically generated was" +
                               " already present: error with the ID generation", this);
                return -1;
            }

            LinkedSceneIDsBySceneAsset.Add(asset, newId);
            LinkedSceneManager.Instance.UpdateBuildId(newId,
                SceneUtil.GetSceneBuildID(asset), asset.name);

            TagScene(asset);

            EditorUtility.SetDirty(this);

            return newId;
        }

        public void TagScene(SceneAsset asset)
        {
            if (!buildTagsPerScene.ContainsKey(asset))
            {
                if (!buildTags.Contains(NoTagTag))
                    buildTags.Add(NoTagTag);
                buildTagsPerScene.Add(asset, new TagList());
            }

            EditorUtility.SetDirty(this);
        }

        public void NotifySceneDeleted(SceneAsset asset)
        {
            if (LinkedSceneIDsBySceneAsset.ContainsKey(asset))
            {
                LinkedSceneManager.Instance.RemoveLinkedSceneID(LinkedSceneIDsBySceneAsset[asset]);
                LinkedSceneIDsBySceneAsset.Remove(asset);
            }

            EditorUtility.SetDirty(this);
        }

        public bool IsSceneRegistered(SceneAsset asset)
        {
            return LinkedSceneIDsBySceneAsset.ContainsKey(asset);
        }

        public List<string> GetBuildTags(SceneAsset asset)
        {
            if (!buildTagsPerScene.ContainsKey(asset))
                return new List<string>() {"No Tag"};

            return buildTags.Where(_ => buildTagsPerScene[asset].Tags.Contains(_)).ToList();
        }

        public TagList GetTagListObject(SceneAsset asset)
        {
            if (asset == null)
                return new TagList();

            if (!buildTagsPerScene.ContainsKey(asset))
                return new TagList();

            return buildTagsPerScene[asset];
        }

        public void SetTagList(SceneAsset asset, TagList tagList)
        {
            if (!buildTagsPerScene.ContainsKey(asset))
                buildTagsPerScene.Add(asset, tagList);

            buildTagsPerScene[asset] = tagList;

            EditorUtility.SetDirty(this);
        }

        public void SetTagList(SceneAsset asset, List<string> tagList)
        {
            if (!buildTagsPerScene.ContainsKey(asset))
                buildTagsPerScene.Add(asset, new TagList() {Tags = tagList});

            buildTagsPerScene[asset] = new TagList() {Tags = tagList};

            EditorUtility.SetDirty(this);
        }

        public List<string> GetUnBoundTags(SceneAsset asset)
        {
            if (!buildTagsPerScene.ContainsKey(asset))
                return new List<string>(0);

            return buildTags.Where(_ => !buildTagsPerScene[asset].Tags.Contains(_)).ToList();
        }

        [Button(ButtonSizes.Medium)]
        public void OpenLinkedSceneManager()
        {
            Selection.SetActiveObjectWithContext(LinkedSceneManager.Instance,
                LinkedSceneManager.Instance);
        }

        #region STATIC

        public static readonly string NoTagTag = "No Tag";

        [InitializeOnLoadMethod]
        static void UpdateSceneReferencesCallBack()
        {
            EditorBuildSettings.sceneListChanged -= UpdateSceneReferences;
            EditorBuildSettings.sceneListChanged += UpdateSceneReferences;

            if (IsRegistered)
            {
                List<string> tags = Instance.buildTags;
                if (!tags.Contains(NoTagTag))
                    tags.Add(NoTagTag);
            }

            EditorApplication.playModeStateChanged -= NotifyPlayEnter;
            EditorApplication.playModeStateChanged += NotifyPlayEnter;
        }

        private static void NotifyPlayEnter(PlayModeStateChange obj)
        {
            if (obj == PlayModeStateChange.ExitingEditMode)
            {
                for (int i = 0; i < SceneManager.sceneCount; i++)
                {
                    LinkedScenesEditorStatusChecks.CheckSave(SceneManager.GetSceneAt(i), null);
                }

                if (!Instance.UseAlwaysOpenedScenes)
                    return;

                foreach (var sceneAsset in Instance.ScenesToAlwaysOpen)
                {
                    if (!Instance.CurrentlyOpenedScene.Contains(sceneAsset))
                    {
                        EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(sceneAsset),
                            OpenSceneMode.Additive);
                    }
                }
            }
        }

        private static void UpdateSceneReferences()
        {
            int i = 0;
            foreach (var scene in EditorBuildSettings.scenes)
            {
                int id;
                if (Instance.LinkedSceneIDsBySceneAsset.TryGetValue(
                    AssetDatabase.LoadAssetAtPath<SceneAsset>(scene.path), out id))
                {
                    if (!scene.enabled)
                        Debug.Log(
                            String.Format("Build ID of scene at {0} is now -1 because it is disabled in the build",
                                scene.path), Instance);
                    LinkedSceneManager.Instance.UpdateBuildId(id, scene.enabled ? i : -1,
                        AssetDatabase.LoadAssetAtPath<SceneAsset>(scene.path).name);
                }

                i++;
            }

            RemoveUnusedIds();

            foreach (var sceneId in Instance.LinkedSceneIDsBySceneAsset)
            {
                if (EditorBuildSettings.scenes.All(_ => _.path !=
                                                        AssetDatabase.GetAssetPath(sceneId.Key)))
                {
                    LinkedSceneManager.Instance.UpdateBuildId(sceneId.Value, -1, sceneId.Key.name);
                }
            }
        }

        private static void RemoveUnusedIds()
        {
            List<int> idsToRemove = new List<int>();

            foreach (var instanceLinkedIDsToBuildID in LinkedSceneManager.Instance.LinkedIDsToBuildIDs.Keys)
            {
                if (!Instance.LinkedSceneIDsBySceneAsset.ContainsValue(instanceLinkedIDsToBuildID))
                {
                    idsToRemove.Add(instanceLinkedIDsToBuildID);
                }
            }

            foreach (var id in idsToRemove)
            {
                LinkedSceneManager.Instance.LinkedIDsToBuildIDs.Remove(id);
            }
        }

        #endregion
    }


    public class LinkedScenesEditorStatusChecks : UnityEditor.AssetModificationProcessor,
        IPostprocessBuildWithReport, IPreprocessBuildWithReport, IProcessSceneWithReport
    {
        public static bool IsBuilding = false;
        public static bool IsSaving = false;


        public int callbackOrder => 0;

        public void OnPostprocessBuild(BuildReport report)
        {
            IsBuilding = false;
        }

        public void OnPreprocessBuild(BuildReport report)
        {
            IsBuilding = true;
        }

        public void OnProcessScene(Scene scene, BuildReport report)
        {
            if (Application.isPlaying)
                return;

            if (EditorLinkedSceneManager.Instance.ObjectsTagsToRemove.IsEmpty())
                return;

            foreach (var rootGo in scene.GetRootGameObjects())
            {
                foreach (var transform in rootGo.transform.GetAllSubTransforms(true))
                {
                    if (!transform)
                        continue;

                    if (EditorLinkedSceneManager.Instance.ObjectsTagsToRemove.Contains(transform.tag))
                    {
                        for (int i = 0; i < transform.childCount; i++)
                        {
                            transform.GetChild(i).SetParent(transform.parent);
                        }

                        UnityEngine.Object.DestroyImmediate(transform.gameObject);
                    }
                }
            }
        }

        public static AssetDeleteResult OnWillDeleteAsset(string assetPath, RemoveAssetOptions options)
        {
            SceneAsset asset = AssetDatabase.LoadAssetAtPath<SceneAsset>(assetPath);
            if (asset)
            {
                //then it's a scene
                if (LinkedSceneManager.IsRegistered)
                    EditorLinkedSceneManager.Instance.NotifySceneDeleted(asset);
            }

            return AssetDeleteResult.DidNotDelete;
        }

        [InitializeOnLoadMethod]
        public static void PlugToSave()
        {
            EditorSceneManager.sceneSaving -= CheckSave;
            EditorSceneManager.sceneSaving += CheckSave;

            EditorSceneManager.sceneSaved -= CheckSaved;
            EditorSceneManager.sceneSaved += CheckSaved;

            EditorSceneManager.sceneOpened -= CheckOpened;
            EditorSceneManager.sceneOpened += CheckOpened;

            EditorSceneManager.sceneClosed -= CheckClosed;
            EditorSceneManager.sceneClosed += CheckClosed;
        }

        private static void CheckClosed(Scene scene)
        {
            CheckSave(scene, null);
            IsSaving = false;
        }

        private static void CheckOpened(Scene scene, OpenSceneMode mode)
        {
            if (Application.isPlaying)
                return;

            foreach (var go in scene.GetRootGameObjects())
            {
                LinkedSceneComponent lScene = go.GetComponent<LinkedSceneComponent>();

                if (!lScene)
                {
                    lScene = go.GetComponentInChildren<LinkedSceneComponent>();
                }

                if (lScene)
                {
                    if (lScene.CanBeActivated)
                    {
                        foreach (var holder in lScene.SceneHolders)
                        {
                            holder.SetActive(!IsBuilding);
                        }
                    }

                    break;
                }
            }
        }

        public static void CheckSave(Scene scene, string path)
        {
            if (!scene.IsValid() || !scene.isLoaded || IsBuilding || Application.isPlaying)
                return;

            IsSaving = true;

            foreach (var go in scene.GetRootGameObjects())
            {
                LinkedSceneComponent lScene = go.GetComponent<LinkedSceneComponent>();

                if (!lScene)
                {
                    LinkedSceneComponent[] lScenes = go.GetComponentsInChildren<LinkedSceneComponent>();
                    if (lScenes.Length > 1)
                        Debug.LogWarningFormat("WARNING: Two Linked Scene " +
                                               "objects were found in scene {0}: only one is supported at a time.");
                    if (lScenes.Length > 0)
                        lScene = lScenes[0];
                }

                if (lScene && lScene.CanBeActivated)
                {
                    foreach (var holder in lScene.SceneHolders)
                    {
                        holder.SetActive(false);
                    }

                    break;
                }
            }
        }

        private static void CheckSaved(Scene scene)
        {
            if (Application.isPlaying || !scene.IsValid() || IsBuilding)
                return;

            foreach (var go in scene.GetRootGameObjects())
            {
                LinkedSceneComponent lScene = go.GetComponent<LinkedSceneComponent>();

                if (!lScene)
                {
                    LinkedSceneComponent[] lScenes = go.GetComponentsInChildren<LinkedSceneComponent>();
                    if (lScenes.Length > 1)
                        Debug.LogWarningFormat("WARNING: Two Linked Scene " +
                                               "objects were found in scene {0}: only one is supported at a time.");
                    if (lScenes.Length > 0)
                        lScene = lScenes[0];
                }

                if (lScene && lScene.CanBeActivated)
                {
                    foreach (var holder in lScene.SceneHolders)
                    {
                        holder.SetActive(true);
                    }

                    break;
                }
            }
        }
    }

    [Serializable]
    public class TagList
    {
        [ValueDropdown("PotentialTags"), OnValueChanged("SaveManagerOnChange")]
        public List<string> Tags;

        public TagList()
        {
            Tags = new List<string> {EditorLinkedSceneManager.NoTagTag};
        }

        private ValueDropdownList<string> PotentialTags => EditorLinkedSceneManager.Instance.BuildTagsDropDown;

        private void SaveManagerOnChange()
        {
            EditorUtility.SetDirty(EditorLinkedSceneManager.Instance);
            EditorUtility.SetDirty(LinkedSceneManager.Instance);
        }
    }
}
#endif