﻿using System;
using System.IO;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.SceneManagement;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;

#endif

namespace BFT
{
    [Serializable]
    public class LinkedSceneData
    {
        [TabGroup("Scene")] public bool LoadAsync = true;

        [TabGroup("Status")] [SerializeField, ReadOnly]
        private int linkedSceneID = -1;


        [NonSerialized] public AsyncOperation LoadingOp;

        public LinkedSceneData(int linkedSceneID)
        {
            this.linkedSceneID = linkedSceneID;
        }

        public LinkedSceneData(LinkedSceneData other)
        {
            linkedSceneID = other.linkedSceneID;
            LoadAsync = other.LoadAsync;
#if UNITY_EDITOR
            EditorSceneReference = other.EditorSceneReference;
#endif
        }

        //TODO expend for Asset bundles

        public int LinkedSceneID => linkedSceneID;

        [TabGroup("Status")] public int SceneBuildID => LinkedSceneManager.Instance.GetSceneBuildID(linkedSceneID);

        [TabGroup("Status")]
        public virtual bool IsLoaded
        {
            get
            {
                if (SceneBuildID != -1)
                    return SceneManager.GetSceneByBuildIndex(SceneBuildID).isLoaded;
                return false;
            }
        }

        public bool IsLoading => LinkedSceneManager.Instance.IsLinkedSceneLoading(linkedSceneID);

        public event System.Action OnLoadingStarted;
        public event System.Action OnLoadingDone;

        #region LOADING

        [TabGroup("Utils"), Button(ButtonSizes.Medium), HideIf("IsLinkedSceneUnDefinedOrLoadedEditor")]
        public AsyncOperation LoadLinkedScene(bool additive = true)
        {
            if (SceneBuildID == -1)
            {
                UnityEngine.Debug.LogWarning("Attempting to load a scene that is not in the build!");
                return null;
            }

            OnLoadingStarted?.Invoke();

#if UNITY_EDITOR

            if (!Application.isPlaying)
            {
                if (!additive)
                {
                    if (!EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
                    {
                        return null;
                    }
                }

                EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(EditorSceneReference),
                    additive ? OpenSceneMode.Additive : OpenSceneMode.Single);
                return null;
            }
#endif


            if (LoadAsync)
            {
                if (IsLoading)
                {
                    if (!LinkedSceneManager.Instance.LinkedScenesLoading.Contains(linkedSceneID))
                        LinkedSceneManager.Instance.NotifyLinkedSceneLoadingStarted(linkedSceneID);
                    return LoadingOp;
                }

                LinkedSceneManager.Instance.NotifyLinkedSceneLoadingStarted(linkedSceneID);
                LoadingOp = SceneManager.LoadSceneAsync(SceneBuildID,
                    additive ? LoadSceneMode.Additive : LoadSceneMode.Single);

                LoadingOp.completed += NotifyLoaded;

                return LoadingOp;
            }

            SceneManager.LoadScene(SceneBuildID,
                additive ? LoadSceneMode.Additive : LoadSceneMode.Single);
            NotifyLoaded(null);

            return null;
        }

        private void NotifyLoaded(AsyncOperation obj)
        {
            LinkedSceneManager.Instance.NotifyLinkedSceneLoadingDone(linkedSceneID);
            OnLoadingDone?.Invoke();
        }

        [TabGroup("Utils"), Button(ButtonSizes.Medium), ShowIf("IsLinkedSceneDefinedAndLoadedEditor")]
        public void UnLoadFirstLinkedSceneMatchingBuildID()
        {
            if (!IsLoaded)
                return;

            if (SceneBuildID == -1)
            {
                UnityEngine.Debug.LogWarning("Attempting to unload a scene that is not in the build!");
                return;
            }

#if UNITY_EDITOR

            if (!Application.isPlaying)
            {
                EditorSceneManager.CloseScene(SceneManager.GetSceneByBuildIndex(SceneBuildID), true);
                return;
            }
#endif
            SceneManager.UnloadSceneAsync(SceneBuildID);
        }

        public void UnloadScene(UnityEngine.SceneManagement.Scene scene)
        {
            SceneManager.UnloadSceneAsync(scene);
        }

        #endregion

        #region EDITOR

#if UNITY_EDITOR
        [TabGroup("Scene", order: -1), AssetsOnly, OnValueChanged("ValidateLinkedScene")]
        public SceneAsset EditorSceneReference;

        [OnInspectorGUI]
        private void CheckSceneRegistered()
        {
            if (EditorSceneReference)
            {
                if (!EditorLinkedSceneManager.Instance.IsSceneRegistered(EditorSceneReference))
                    linkedSceneID = EditorLinkedSceneManager.Instance.RegisterSceneEditor(this);
                linkedSceneID = EditorLinkedSceneManager.Instance.LinkedSceneIDsBySceneAsset[EditorSceneReference];

                if (Event.current.type == EventType.Repaint)
                {
                    TagList = EditorLinkedSceneManager.Instance.GetTagListObject(EditorSceneReference);
                    if (EditorLinkedSceneManager.Instance.TagsToBuild.ContainsAny(TagList.Tags)
                        && SceneBuildID == -1)
                    {
                        EditorLinkedSceneManager.Instance.SyncTagsToBuildSettings();
                    }
                }
            }
        }

        [TabGroup("Scene"), ShowInInspector, ShowIf("IsLinkedSceneDefinedEditor")]
        public TagList TagList
        {
            get => EditorLinkedSceneManager.Instance.GetTagListObject(EditorSceneReference);
            set => EditorLinkedSceneManager.Instance.SetTagList(EditorSceneReference, value);
        }

        [TabGroup("Utils"), Button(ButtonSizes.Medium)]
        private void OpenSceneManager()
        {
            EditorLinkedSceneManager.Instance.TagScene(EditorSceneReference);
            Selection.SetActiveObjectWithContext(EditorLinkedSceneManager.Instance, LinkedSceneManager.Instance);
        }

        public LinkedSceneData(UnityEngine.SceneManagement.Scene scene) : this(SceneUtil.GetAssetFromScene(scene))
        {
        }

        public LinkedSceneData(SceneAsset editorScene)
        {
            EditorSceneReference = editorScene;

            if (EditorSceneReference)
            {
                linkedSceneID = EditorLinkedSceneManager.Instance.RegisterSceneEditor(this);
                EditorLinkedSceneManager.Instance.TagScene(EditorSceneReference);
            }

            if (linkedSceneID == -1)
            {
                EditorSceneReference = null;
            }
        }

        public void ValidateLinkedScene()
        {
            if (EditorSceneReference)
            {
                linkedSceneID = EditorLinkedSceneManager.Instance.RegisterSceneEditor(this);
                EditorLinkedSceneManager.Instance.TagScene(EditorSceneReference);
            }
            else
            {
                linkedSceneID = -1;
            }
        }


        [TabGroup("Utils"), Button(ButtonSizes.Medium), ShowIf("IsLinkedSceneDefinedEditor")]
        public void EditLinkedSceneAlone()
        {
            EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(EditorSceneReference)
                , OpenSceneMode.Single);
        }

        public bool IsLinkedSceneLoadedEditor()
        {
            return SceneManager.GetSceneByName(EditorSceneReference.name).isLoaded;
        }

        public bool IsLinkedSceneDefinedEditor()
        {
            return EditorSceneReference;
        }

        public bool IsLinkedSceneDefinedAndLoadedEditor()
        {
            return (EditorSceneReference && IsLinkedSceneLoadedEditor());
        }

        public bool IsLinkedSceneUnDefinedOrLoadedEditor()
        {
            return !EditorSceneReference || IsLinkedSceneLoadedEditor();
        }


        public static LinkedSceneData CreateNewLinkedScene(UnityEngine.Object owner, string linkedSceneName,
            bool relativePath = true, string path = "", params GameObject[] objectsToAttach)
        {
            SceneUtil.SaveAllScenes();
            UnityEngine.SceneManagement.Scene newScene =
                EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Additive);

            foreach (GameObject go in objectsToAttach)
            {
                SceneManager.MoveGameObjectToScene(go, newScene);
            }

            string scenePath = "";

            if (relativePath)
            {
                if (PrefabUtility.GetPrefabInstanceStatus(owner) != PrefabInstanceStatus.NotAPrefab)
                    scenePath = Path.GetDirectoryName(AssetDatabase.GetAssetPath(
                        PrefabUtility.GetPrefabInstanceHandle(owner)));
                else if (PrefabUtility.GetPrefabAssetType(owner) != PrefabAssetType.NotAPrefab)
                {
                    scenePath = Path.GetDirectoryName(AssetDatabase.GetAssetPath(owner));
                }
                else
                {
                    GameObject own = (GameObject) owner;
                    if (own)
                        scenePath = own.scene.path;
                    else
                    {
                        AssetDatabase.GetAssetPath(owner);
                    }
                }

                if (!path.IsNullOrWhitespace())
                    scenePath += "/" + path;
            }
            else
            {
                scenePath = path;
            }

            scenePath = scenePath.Directorized();

            scenePath += linkedSceneName + ".unity";

            while (File.Exists(scenePath))
                scenePath = scenePath.Replace(".unity", "(Clone).unity");

            EditorSceneManager.SaveScene(newScene, scenePath);

            SceneUtil.RemoveNullScenesFromBuildSetting();

            SceneUtil.AddSceneToBuildSettings(AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath));

            return new LinkedSceneData(AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath));
        }

        public static LinkedSceneData CreateNewLinkedScene(GameObject owner, string linkedSceneName,
            params GameObject[] objectsToAttach)
        {
            return CreateNewLinkedScene(owner, linkedSceneName, true, "", objectsToAttach);
        }

#endif

        #endregion
    }
}