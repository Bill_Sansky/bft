﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Sirenix.Utilities;

using UnityEngine.SceneManagement;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace BFT
{
    public static class SceneUtil
    {
        #region EDITOR

#if UNITY_EDITOR

        public static void RemoveNullScenesFromBuildSetting()
        {
            List<EditorBuildSettingsScene> scenes = EditorBuildSettings.scenes.ToList();
            scenes.RemoveAll(x => x == null || x.path.IsNullOrWhitespace() || !File.Exists(x.path));
            EditorBuildSettings.scenes = scenes.ToArray();
        }

        public static void SaveAllScenes()
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                EditorSceneManager.SaveScene(SceneManager.GetSceneAt(i));
            }
        }

        public static int GetSceneBuildID(SceneAsset sceneAsset)
        {
            return SceneManager.GetSceneByPath(AssetDatabase.GetAssetPath(sceneAsset)).buildIndex;
        }

        public static bool AddSceneToBuildSettings(SceneAsset scene)
        {
            if (EditorBuildSettings.scenes.Any(_ => _.path == AssetDatabase.GetAssetPath(scene)))
                return false;
            EditorBuildSettings.scenes = EditorBuildSettings.scenes.Add(
                new EditorBuildSettingsScene(AssetDatabase.GetAssetPath(scene), true));
            return true;
        }

        public static void RemoveSceneFromBuildSettings(SceneAsset scene)
        {
            EditorBuildSettings.scenes = EditorBuildSettings.scenes
                .Where(_ => _.path != AssetDatabase.GetAssetPath(scene))
                .ToArray();
        }

        public static SceneAsset GetAssetFromScene(UnityEngine.SceneManagement.Scene scene)
        {
            return AssetDatabase.LoadAssetAtPath<SceneAsset>(scene.path);
        }

#endif

        #endregion
    }
}
