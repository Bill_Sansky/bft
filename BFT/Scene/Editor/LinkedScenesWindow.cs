﻿using System.Collections.Generic;
using System.Linq;
using BFT;
using Sirenix.OdinInspector;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

/// <summary>
///     SceneViewWindow class.
/// </summary>
public class LinkedScenesWindow : OdinEditorWindow
{
    [TabGroup("Editor"), InlineEditor(Expanded = true)]
    public EditorLinkedSceneManager EditorManager;

    [TabGroup("Manager"), InlineEditor(Expanded = true)]
    public LinkedSceneManager Manager;

    [TabGroup("Scenes")]
    [HideLabel, ListDrawerSettings(HideAddButton = true, IsReadOnly = true, NumberOfItemsPerPage = 25)]
    public List<SceneStruct> Scenes;

    /// <summary>
    ///     Initialize window state.
    /// </summary>
    [MenuItem("BFT/Scene Management/Scene Management Window")]
    internal static void Init()
    {
        LinkedScenesWindow win = GetWindow<LinkedScenesWindow>();
        win.Show();
        win.EditorManager = EditorLinkedSceneManager.Instance;
        win.Manager = LinkedSceneManager.Instance;
    }

    [OnInspectorGUI]
    public void CheckIfBound()
    {
        if (Scenes == null)
        {
            Scenes = new List<SceneStruct>(EditorLinkedSceneManager.Instance.LinkedSceneIDsBySceneAsset.Count);

            foreach (var key in EditorLinkedSceneManager.Instance.LinkedSceneIDsBySceneAsset.Keys)
            {
                Scenes.Add(new SceneStruct() {SceneAsset = key});
            }
        }

        if (!Manager)
        {
            Manager = LinkedSceneManager.Instance;
        }

        if (!EditorManager)
            EditorManager = EditorLinkedSceneManager.Instance;
        if (Scenes.Any(_ => _.SceneAsset == null))
            Scenes = Scenes.Where(_ => _.SceneAsset != null).ToList();
    }

    public struct SceneStruct
    {
        [HideInInspector] public SceneAsset SceneAsset;

        [HorizontalGroup("Scene Loading"), HideLabel]
        public string SceneName
        {
            get
            {
                if (!EditorLinkedSceneManager.IsRegistered || SceneAsset == null)
                    return "";
                return EditorLinkedSceneManager.Instance.LinkedSceneIDsBySceneAsset[SceneAsset] + ": " +
                       SceneAsset.name;
            }
        }

        [HorizontalGroup("Scene Loading"), Button(ButtonSizes.Medium)]
        public void AddScene()
        {
            EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(SceneAsset), OpenSceneMode.Additive);
        }

        [HorizontalGroup("Scene Loading"), Button(ButtonSizes.Medium)]
        public void OpenScene()
        {
            EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(SceneAsset), OpenSceneMode.Single);
        }

        [HorizontalGroup("Scene Loading"), Button(ButtonSizes.Medium)]
        public void PingScene()
        {
            Selection.activeObject = SceneAsset;
            EditorGUIUtility.PingObject(Selection.activeObject);
        }
    }
}