﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

#if UNITY_EDITOR

#endif

namespace BFT
{
    [ExecuteAlways]
    public class LinkedSceneComponent : MonoBehaviour
    {
        [BoxGroup("Activation"), ShowIf("UseActivation")]
        public bool ActivateHolderOnEnable = true;

        [BoxGroup("Attachment"), ShowIf("UseTransformAttachment"),
         Tooltip("If true, the object holder will copy the specified transform on " +
                 "activation every frame"), FormerlySerializedAs("attachAlways")]
        public bool AttachAlways = false;

        private AdditiveLinkedSceneLoader attachedLoader;
        private UnityEngine.Transform attachmentPoint;

        [BoxGroup("Reload"), ShowIf("UseActivation")]
        public bool CacheSceneReload;

        [BoxGroup("Attachment"), ShowIf("UseTransformAttachment")]
        public bool CopyAttachmentRotation = true;

        public bool LogDebug;

        private bool reloading = false;

        [BoxGroup("Reload"), ShowInInspector, ReadOnly]
        private LinkedSceneComponent reloadScene;

        private bool reloadSceneLoading = false;
        [FoldoutGroup("Scene Data", false)] public LinkedSceneData SceneData;

        [BoxGroup("Activation"), OnValueChanged("CheckIfSelf"), ShowIf("UseActivation")]
        public List<GameObject> SceneHolders = new List<GameObject>();

        [BoxGroup("Activation"), OnValueChanged("CheckAttachment")]
        public bool UseActivation = true;

        [BoxGroup("Attachment"), ShowIf("UseActivation")]
        public bool UseTransformAttachment;

        public bool CanBeActivated => UseActivation && SceneHolders.Count > 0;

        public bool IsAttachAlways => UseTransformAttachment && AttachAlways;

        public bool IsReloadScene => CacheSceneReload && !reloadScene && !reloadSceneLoading;

        protected void CheckAttachment()
        {
            if (!UseActivation)
                UseTransformAttachment = false;
        }

        public void CheckIfSelf()
        {
            if (SceneHolders.Contains(gameObject))
            {
                UnityEngine.Debug.LogError("Error: setting the scene holder as self " +
                               "is not allowed", this);
                SceneHolders = null;
            }
        }

        public virtual void Awake()
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlayingOrWillChangePlaymode)
            {
                EditorLinkedSceneManager.Instance.OpenedLinkedScene.Add(this);
                return;
            }
#endif

            if (CanBeActivated)
            {
                foreach (GameObject holder in SceneHolders)
                {
                    if (holder.activeInHierarchy)
                    {
                        UnityEngine.Debug.LogWarningFormat(this, "The Scene Holder{0}  was active on awake:" +
                                                     " this ,may cause activation order issues!", holder);
                        holder.SetActive(false);
                    }
                }
            }

            attachedLoader = LinkedSceneManager.Instance.PullLoaderForID(SceneData.LinkedSceneID);
            if (attachedLoader)
                attachedLoader.NotifyLinkedSceneLoaded(this);
            else if (LogDebug)
            {
                UnityEngine.Debug.LogFormat(this, "Linked Scene {0} did not find any loader trying to load it", gameObject.scene.name);
            }

            LinkedSceneComponent reloadBase =
                LinkedSceneManager.Instance.PullSceneWaitingForCachedReload(SceneData.LinkedSceneID);

            if (reloadBase)
            {
                reloadBase.reloadScene = this;
                reloadBase.reloadSceneLoading = false;
            }
        }

        private void LoadCachedReload()
        {
            LinkedSceneManager.Instance.AddLinkedSceneWaitingForReload(this);
            SceneData.LoadLinkedScene();
            reloadSceneLoading = true;
        }

        void OnEnable()
        {
            if (ActivateHolderOnEnable && CanBeActivated)
            {
                foreach (GameObject holder in SceneHolders)
                {
                    holder.SetActive(true);
                }
            }
        }

        void OnDestroy()
        {
            if (attachedLoader)
            {
                if (reloading)
                {
                    attachedLoader.NotifyLinkedSceneReloaded(CacheSceneReload ? reloadScene : null, this);
                }
                else
                {
                    attachedLoader.NotifySceneUnloaded(this);
                }
            }

            if (!reloading && reloadScene)
                reloadScene.UnLoad();

#if UNITY_EDITOR
            if (!EditorApplication.isPlayingOrWillChangePlaymode)
            {
                EditorLinkedSceneManager.Instance.OpenedLinkedScene.Remove(this);
                return;
            }
#endif
        }

        [FoldoutGroup("Utils"), Button(ButtonSizes.Medium)]
        public void UnLoad()
        {
            if (gameObject == null)
                return;
            if (CanBeActivated)
            {
                foreach (var holder in SceneHolders)
                {
                    if (holder)
                    {
                        holder.SetActive(false);
                    }
                }
            }

            SceneData.UnloadScene(gameObject.scene);
        }


        [FoldoutGroup("Utils"), Button(ButtonSizes.Medium)]
        public virtual void Reload()
        {
            if (IsReloadScene)
            {
                return;
            }

            reloading = true;

            if (reloadScene)
                reloadScene.NotifyReload(this);
            else
            {
                SceneData.LoadLinkedScene();
                reloadSceneLoading = true;
            }

            UnLoad();
        }

        public virtual void NotifyReload(LinkedSceneComponent mainScene)
        {
            if (!IsReloadScene)
                return;

            attachedLoader = mainScene.attachedLoader;
            attachmentPoint = mainScene.attachmentPoint;

            OnEnable();

            if (mainScene.IsAttachAlways)
                Activate(attachmentPoint);
        }

        public virtual void Activate(UnityEngine.Transform toAttachTo)
        {
            if (!CanBeActivated)
                return;

            if (IsReloadScene)
            {
                LoadCachedReload();
            }

            foreach (var o in SceneHolders)
            {
                if (toAttachTo && UseTransformAttachment)
                {
                    o.transform.position = toAttachTo.position;
                    if (CopyAttachmentRotation)
                        o.transform.rotation = toAttachTo.rotation;
                    if (IsAttachAlways)
                    {
                        this.attachmentPoint = toAttachTo;
                    }
                }

                if (o)
                    o.SetActive(true);
            }

            StartCoroutine(ActivateSceneAfterSomeTime());
        }

        public void Deactivate()
        {
            foreach (var o in SceneHolders)
            {
                if (o)
                    o.SetActive(false);
            }
        }

        private IEnumerator ActivateSceneAfterSomeTime()
        {
            yield return null;
            SceneManager.SetActiveScene(gameObject.scene);
        }

#if UNITY_EDITOR

        private void Reset()
        {
            if (SceneData == null || SceneData.EditorSceneReference != SceneUtil.GetAssetFromScene(gameObject.scene))
            {
                SceneData = new LinkedSceneData(SceneUtil.GetAssetFromScene(gameObject.scene));
            }
        }

        private void OnValidate()
        {
            if (!Application.isPlaying && !EditorLinkedSceneManager.Instance.OpenedLinkedScene.Contains(this))
            {
                EditorLinkedSceneManager.Instance.OpenedLinkedScene.Add(this);
            }
        }

        [OnInspectorGUI]
        protected void UpdateSceneData()
        {
            if (Application.isPlaying)
                return;

            if (SceneData.EditorSceneReference == SceneUtil.GetAssetFromScene(gameObject.scene))
                return;
            SceneData = new LinkedSceneData(gameObject.scene);

#if UNITY_EDITOR
            EditorUtility.SetDirty(EditorLinkedSceneManager.Instance);
            EditorUtility.SetDirty(LinkedSceneManager.Instance);
#endif
        }

        [InitializeOnLoadMethod]
        static void PlayModeChangeCallBackPreparation()
        {
            EditorApplication.playModeStateChanged += PlayModeChangeCallBack;
        }

        private static void PlayModeChangeCallBack(PlayModeStateChange obj)
        {
            if (obj == PlayModeStateChange.ExitingEditMode)
            {
                foreach (var linkedScene in EditorLinkedSceneManager.Instance.OpenedLinkedScene)
                {
                    if (linkedScene && linkedScene.CanBeActivated)
                    {
                        foreach (var holder in linkedScene.SceneHolders)
                        {
                            holder.SetActive(false);
                        }
                    }
                }
            }
            else if (obj == PlayModeStateChange.EnteredEditMode)
            {
                foreach (var linkedScene in EditorLinkedSceneManager.Instance.OpenedLinkedScene)
                {
                    if (linkedScene && linkedScene.CanBeActivated)
                    {
                        foreach (var holder in linkedScene.SceneHolders)
                        {
                            holder.SetActive(true);
                        }
                    }
                }
            }
        }

        public virtual void OnDrawGizmos()
        {
            if (gameObject.activeInHierarchy && UseActivation
                                             && !LinkedScenesEditorStatusChecks.IsSaving &&
                                             !LinkedScenesEditorStatusChecks.IsBuilding && !Application.isPlaying)
            {
                foreach (var holder in SceneHolders)
                {
                    if (holder)
                        holder.SetActive(true);
                }
            }
        }

#endif
    }
}
