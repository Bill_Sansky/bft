﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    public interface IShape
    {
        Vector3[] GetDirections(int amount);
        IEnumerable<Vector3[]> GetDebugShapeLines(float radius);
    }
}
