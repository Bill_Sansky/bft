﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    [CreateAssetMenu(fileName = "Straight Shape", menuName = "SW GO/Projectiles/Shape/Straight", order = 0)]
    public class Straight : ShapeScriptableObject
    {
        public override Vector3[] GetDirections(int amount)
        {
            var forward = Vector3.forward;
            var directions = new Vector3[amount];
            for (int i = 0; i < amount; i++)
                directions[i] = forward;
            return directions;
        }

        public override IEnumerable<Vector3[]> GetDebugShapeLines(float radius)
        {
            return GizmosExt.GetStraightLine(radius);
        }
    }
}
