﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    [CreateAssetMenu(fileName = "Cone Shape", menuName = "SW GO/Projectiles/Shape/Cone", order = 1)]
    public class Cone : ShapeScriptableObject
    {
        [SerializeField] private float angle;

        [SerializeField] private float maxDown = 1;

        [SerializeField] private float maxUp = 1;

        public float Angle => angle;

        public override Vector3[] GetDirections(int amount)
        {
            return DynamicCone.GetDirections(angle, amount, maxUp, maxDown);
        }

        public override IEnumerable<Vector3[]> GetDebugShapeLines(float radius)
        {
            return DynamicCone.GetDebugShapeLines(angle, radius);
        }
    }
}
