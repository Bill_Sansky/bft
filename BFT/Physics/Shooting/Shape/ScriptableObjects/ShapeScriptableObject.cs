﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    public abstract class ShapeScriptableObject : ScriptableObject, IShape
    {
        public abstract Vector3[] GetDirections(int amount);
        public abstract IEnumerable<Vector3[]> GetDebugShapeLines(float radius);
    }
}
