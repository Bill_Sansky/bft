﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    class DynamicCone : IShape
    {
        [SerializeField, MaxValue(360)] private float angle = 45;

        Vector3[] IShape.GetDirections(int amount)
        {
            return GetDirections(angle, amount, int.MaxValue, int.MinValue);
        }

        public IEnumerable<Vector3[]> GetDebugShapeLines(float radius)
        {
            return GetDebugShapeLines(angle, radius);
        }

        //Uniform spread
        //https://answers.unity.com/questions/467742/how-can-i-create-raycast-bullet-innaccuracy-as-a-c.html
        public static Vector3[] GetDirections(float angle, int amount, float maxHeight, float minHeight)
        {
            var directions = new Vector3[amount];

            float radradius = angle * Mathf.PI / 360;
            for (int i = 0; i < amount; i++)
            {
                //(sqrt(1 - z^2) * cosϕ, sqrt(1 - z^2) * sinϕ, z)
                var z = Random.Range(Mathf.Cos(radradius), 1);
                var t = Random.Range(0, Mathf.PI * 2);
                directions[i] = new Vector3(Mathf.Sqrt(1 - z * z) * Mathf.Cos(t),
                    Mathf.Clamp(Mathf.Sqrt(1 - z * z) * Mathf.Sin(t), minHeight, maxHeight), z);
            }

            return directions;
        }

        public static IEnumerable<Vector3[]> GetDebugShapeLines(float angle, float radius)
        {
            return GizmosExt.GetConeLines(angle, radius);
        }
    }
}
