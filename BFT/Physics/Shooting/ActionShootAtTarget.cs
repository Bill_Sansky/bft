﻿using UnityEngine;

namespace BFT
{
    public class ActionShootAtTarget : ActionShoot
    {
        [SerializeField] private UnityEngine.Transform root;

        [SerializeField] private UnityEngine.Transform target;

        protected override Vector3 RootPosition
        {
            get
            {
                if (root != null)
                    return root.transform.position;
                return transform.position;
            }
        }

        protected override Quaternion RootRotation
        {
            get
            {
                if (target != null && root != null)
                    return Quaternion.LookRotation(target.position - RootPosition);
                return Quaternion.identity;
            }
        }
    }
}
