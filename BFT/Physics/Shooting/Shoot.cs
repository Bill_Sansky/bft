﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public abstract class Shoot : SerializedScriptableObject
    {
        [SerializeField, Required] private List<Emitter> emitters = new List<Emitter>();

        public List<Emitter> Emitters => emitters;

        public virtual void Tick(Vector3 rootPosition, Quaternion rootRotation, float elapsed, float deltaTime)
        {
            for (var i = 0; i < emitters.Count; i++)
            {
                EmitShoot(emitters[i].Tick(rootPosition, rootRotation, elapsed, deltaTime));
            }
        }

        protected abstract void EmitShoot(Emitter.EmitData[] emitDatas);

        public void DebugDraw(Vector3 rootPosition, Quaternion rootRotation, float radius)
        {
            for (var i = 0; i < emitters.Count; i++)
                emitters[i].DebugDraw(rootPosition, rootRotation, radius);
        }

        public List<ShapeScriptableObject> GetShapes()
        {
            List<ShapeScriptableObject> shapes = new List<ShapeScriptableObject>();
            foreach (var emitter in emitters)
            {
                shapes.Add(emitter.ScriptableObjectShape);
            }

            return shapes;
        }

        public void SpawnTest(Vector3 rootPosition, Vector3 rootRotation)
        {
            foreach (var emitter in emitters)
            {
                emitter.SpawnTest(rootPosition, rootRotation);
            }
        }

        public void ClearTest()
        {
            foreach (var emitter in emitters)
            {
                emitter.ClearTest();
            }
        }
    }
}
