﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class ActionShoot : MonoBehaviour
    {
        private float elapsed;

        [SerializeField, FoldoutGroup("Debugging")]
        private float radius = 3;

        [SerializeField] private List<Shoot> shoots;

        [SerializeField, FoldoutGroup("Debugging")]
        private bool showDebugDraw;

        public List<Shoot> Shoots => shoots;

        protected virtual Vector3 RootPosition => transform.position;

        protected virtual Quaternion RootRotation => transform.rotation;

        public void BeginShoot()
        {
            elapsed = 0;

            enabled = true;
            foreach (var shoot in shoots)
            {
                shoot.Tick(RootPosition, RootRotation, elapsed, float.Epsilon);
            }
        }

        public void EndShoot()
        {
            enabled = false;
        }

        private void Update()
        {
            var deltaTime = UnityEngine.Time.deltaTime;
            elapsed += UnityEngine.Time.deltaTime;
            foreach (var shoot in shoots)
            {
                shoot.Tick(RootPosition, RootRotation, elapsed, deltaTime);
            }
        }

        private void OnDrawGizmos()
        {
            if (!showDebugDraw)
                return;
            foreach (var shoot in shoots)
            {
                shoot.DebugDraw(RootPosition, RootRotation, radius);
            }
        }

#if UNITY_EDITOR
        [Button("Spawn Test")]
        void SpawnTest()
        {
            foreach (var shoot in shoots)
            {
                shoot.SpawnTest(RootPosition, RootRotation.eulerAngles);
            }

            UnityEditor.SceneView.RepaintAll();
        }

        [Button("Clear Test")]
        void ClearTest()
        {
            foreach (var shoot in shoots)
            {
                shoot.ClearTest();
            }

            UnityEditor.SceneView.RepaintAll();
        }
#endif
    }
}
