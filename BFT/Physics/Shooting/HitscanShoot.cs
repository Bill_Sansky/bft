﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;


//[CreateAssetMenu(fileName = "Hitscan Shoot", menuName = "SW GO/Projectiles/Hitscan Shoot", order = 0)]
namespace BFT
{
    public class HitscanShoot : Shoot
    {
        [SerializeField, PropertyOrder(-4)] private float distance;

        [SerializeField, PropertyOrder(-5)] private LayerMask layer;

        [SerializeField, PropertyOrder(-3)] private bool piercing;

        //[SerializeField, PropertyOrder(-2)]
        //  private SWGOHitDamage hitReaction;

        protected override void EmitShoot(Emitter.EmitData[] emitDatas)
        {
            for (int i = 0; i < emitDatas.Length; i++)
            {
                if (piercing)
                {
                    var hits = UnityEngine.Physics.RaycastAll(emitDatas[i].Position, emitDatas[i].Direction * Vector3.forward, distance,
                        layer);
                    for (int j = 0; j < hits.Length; j++)
                    {
                        var hit = hits[i];
                        Affect(hit);
                    }
                }
                else
                {
                    RaycastHit hit;
                    var a = UnityEngine.Physics.Raycast(emitDatas[i].Position, emitDatas[i].Direction * Vector3.forward, out hit,
                        distance, layer);
                    if (a)
                    {
                        Affect(hit);
                    }
                }
            }
        }

        private void Affect(RaycastHit hit)
        {
            throw new NotImplementedException();
            /*var movementController = hit.transform.GetComponent<RigidBodyMovementController>();
        if (movementController != null)
        {
            hitReaction.Affect(movementController);
        }*/
        }
    }
}
