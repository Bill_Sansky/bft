﻿using UnityEngine;

namespace BFT
{
    class OverTime : Emitter
    {
        [SerializeField] private int rateOverTime;

        public override Vector3[] TickEmission(float elapsed, float deltaTime)
        {
            var lastFrameAmount = (int) ((elapsed - deltaTime) * rateOverTime);
            var currentFrameAmount = (int) (elapsed * rateOverTime);
            var diffrence = currentFrameAmount - lastFrameAmount;
            return Shape.GetDirections(diffrence);
        }
    }
}
