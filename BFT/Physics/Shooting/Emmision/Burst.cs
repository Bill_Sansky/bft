﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class Burst : Emitter
    {
        [SerializeField] private List<BurstData> bursts = new List<BurstData>();

        public override Vector3[] TickEmission(float elapsed, float deltaTime)
        {
            var count = 0;

            /// Burst at 0 time
            if (Math.Abs(elapsed) <= float.Epsilon)
            {
                for (int i = 0; i < bursts.Count; i++)
                {
                    if (bursts[i].Time <= float.Epsilon)
                    {
                        count += bursts[i].Count;
                    }
                }

                return Shape.GetDirections(count);
            }

            //Subsequent shoots
            var previousTime = elapsed - deltaTime;
            for (int i = 0; i < bursts.Count; i++)
            {
                if (bursts[i].Time > previousTime && bursts[i].Time <= elapsed)
                {
                    count += bursts[i].Count;
                }
            }

            return Shape.GetDirections(count);
        }
    }

    [Serializable]
    struct BurstData
    {
        [SerializeField, MinValue(0)] public float Time { get; private set; }

        [SerializeField, MinValue(0)] public int Count { get; private set; }
    }
}
