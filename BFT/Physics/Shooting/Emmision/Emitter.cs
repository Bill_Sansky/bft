﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public abstract class Emitter
    {
        [SerializeField, FoldoutGroup("Transformation")]
        private Vector3 position;

        [SerializeField, FoldoutGroup("Transformation")]
        private Vector3 rotation;

        public Vector3 Position => position;

        protected IShape Shape => dynamicShape ?? scriptableObjectShape;
        public abstract Vector3[] TickEmission(float elapsed, float deltaTime);

        public EmitData[] Tick(Vector3 rootPosition, Quaternion rootRotation, float elapsed, float deltaTime)
        {
            var directions = TickEmission(elapsed, deltaTime);
            var emitData = new EmitData[directions.Length];

            for (var i = 0; i < directions.Length; i++)
            {
                var rot = directions[i];

                rot = directions[i].RotatePointAroundPivot(Vector3.zero, rotation);

                var tmpRotation = rootRotation * Quaternion.LookRotation(rot);
                var tmpPosition =
                    (rootPosition + position).RotatePointAroundPivot(rootPosition, rootRotation.eulerAngles + rot);

                emitData[i] = new EmitData(tmpPosition, tmpRotation);
            }

            return emitData;
        }


        public struct EmitData
        {
            public EmitData(Vector3 position, Quaternion direction) : this()
            {
                Position = position;
                Direction = direction;
            }

            public Vector3 Position { get; private set; }
            public Quaternion Direction { get; private set; }
        }
#pragma warning disable 414
        [SerializeField, HideIf("IsUsingDynamicObjectShape"), FoldoutGroup("Shape")]
        private ShapeScriptableObject scriptableObjectShape;

        public ShapeScriptableObject ScriptableObjectShape => scriptableObjectShape;
        [SerializeField, HideIf("IsUsingScriptableObjectShape"), FoldoutGroup("Shape")]
        private IShape dynamicShape;
#pragma warning restore 414


        #region Editor

        public virtual void DebugDraw(Vector3 rootPosition, Quaternion rootRotation, float radius)
        {
            if (Shape == null)
                return;

            var linesToDraw = Shape.GetDebugShapeLines(radius);
            var tmpRotation = rootRotation.eulerAngles + rotation;
            GizmosExt.Draw(linesToDraw, (rootPosition + position).RotatePointAroundPivot(rootPosition, tmpRotation),
                tmpRotation);

#if UNITY_EDITOR
            //DebugDraw
            if (cached != null)
            {
                foreach (var direction in cached)
                {
                    Gizmos.color = Color.red;
                    Gizmos.DrawLine((rootPosition + position).RotatePointAroundPivot(rootPosition, tmpRotation), direction);
                    Gizmos.color = Color.white;
                }
            }
#endif
        }

        private bool IsUsingDynamicObjectShape => dynamicShape != null;
        private bool IsUsingScriptableObjectShape => scriptableObjectShape != null;

        #endregion

        #region Debug

        private Vector3[] cached = new Vector3[0];

        public void SpawnTest(Vector3 rootPosition, Vector3 rootRotation)
        {
            cached = Shape.GetDirections(500);
            for (var i = 0; i < cached.Length; i++)
            {
                cached[i] *= 3;
            }

            cached = ApplyTransformations(rootPosition, rootRotation, cached);
        }

        protected Vector3[] ApplyTransformations(Vector3 rootPosition, Vector3 rootRotation, Vector3[] directions)
        {
            if (Shape == null)
                return null;

            if (Shape != null)
            {
                //Rotation
                directions.ApplyRotation(rotation + rootRotation);

                //Position
                directions.ApplyTranslation(
                    (rootPosition + position).RotatePointAroundPivot(rootPosition, rootRotation + rotation));
            }

            return directions;
        }

        public void ClearTest()
        {
            cached = null;
        }

        #endregion
    }
}
