﻿using Sirenix.OdinInspector;

namespace BFT
{
    public abstract class ChainNode : SerializedMonoBehaviour
    {
        public int NodeOrder = 0;

        public virtual void AttachToPreviousNode(IChain chain, ChainNode node)
        {
            NodeOrder = node.NodeOrder + 1;
        }

        public abstract void UpdateNode(IChain chain);
    }
}
