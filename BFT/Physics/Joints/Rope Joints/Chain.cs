﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEditor;

#if UNITY_EDITOR

#endif

namespace BFT
{
    public class Chain : SerializedMonoBehaviour, IChain
    {
        //TODO move into a joint Node


        public List<ChainNode> Nodes = new List<ChainNode>();

        [AssetsOnly] public ChainNode RopeNodeAsset;

        public int NodeAmount => Nodes.Count;

        [Button(ButtonSizes.Medium)]
        public void AddRopeNode()
        {
#if UNITY_EDITOR
            ChainNode obj = (ChainNode) PrefabUtility.InstantiatePrefab(RopeNodeAsset);
#else
        ChainNode obj = Instantiate(RopeNodeAsset);
#endif
            obj.transform.SetParent(transform);


            if (Nodes.Count > 0)
            {
                obj.AttachToPreviousNode(this, Nodes.Last());
            }
            else
            {
                obj.AttachToPreviousNode(this, null);
            }

            Nodes.Add(obj);
        }

        public void FixedUpdate()
        {
            foreach (var node in Nodes)
            {
                node.UpdateNode(this);
            }
        }

        [Button(ButtonSizes.Medium)]
        public void ReAttachAllNodes()
        {
            ChainNode previousNode = null;
            foreach (ChainNode node in Nodes)
            {
                if (previousNode)
                {
                    node.AttachToPreviousNode(this, previousNode);
                }

                previousNode = node;
            }
        }
    }
}
