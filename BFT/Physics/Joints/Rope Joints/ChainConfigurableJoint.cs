﻿
using UnityEngine;

#if UNITY_EDITOR
using Sirenix.OdinInspector.Editor;
#endif

namespace BFT
{
    public class ChainConfigurableJoint : ChainNode
    {
        public int IterationAmount = 50;
        public Rigidbody JointBody;
        public float JointDistance;
        public ConfigurableJoint JointToPrevious;

        public void Reset()
        {
            JointBody = GetComponent<Rigidbody>();
        }

        public override void UpdateNode(IChain chain)
        {
            //nothing to do for now
        }

        public void Awale()
        {
            JointBody.solverIterations = IterationAmount;
        }

        public override void AttachToPreviousNode(IChain chain, ChainNode node)
        {
            if (!node)
                return;

            base.AttachToPreviousNode(chain, node);

            ChainConfigurableJoint joint = node as ChainConfigurableJoint;

            if (joint)
            {
                JointToPrevious.connectedBody = joint.JointBody;
#if UNITY_EDITOR
                OdinPrefabUtility.UpdatePrefabInstancePropertyModifications(joint, false);
#endif

                transform.localPosition = joint.transform.localPosition + JointDistance * Vector3.forward;
            }
            else
            {
                transform.localPosition = Vector3.zero;
            }
#if UNITY_EDITOR
            OdinPrefabUtility.UpdatePrefabInstancePropertyModifications(this, false);
#endif
        }
    }
}
