﻿using UnityEngine;
#if UNITY_EDITOR
using Sirenix.OdinInspector.Editor;

#endif

namespace BFT
{
    public class ChainJoint : ChainNode
    {
        public Rigidbody JointBody;

        public float JointDistance;

        public float MaxDistance;
        public float MaxPullOfNextJoint;

        public float MaxPullToCenterForce;

        public ChainJoint NextJoint;
        public ChainJoint PreviousJoint;


        public void Reset()
        {
            JointBody = GetComponent<Rigidbody>();
        }

        public override void UpdateNode(IChain chain)
        {
            if (NextJoint)
                SpringJoint(true);
            if (PreviousJoint)
                SpringJoint(false);
        }

        private void TensionPull(IChain chain)
        {
            //   float totalMass = chain.NodeAmount * JointBody.mass;
            float forwardPull = JointBody.mass * UnityEngine.Physics.gravity.magnitude *
                                (chain.NodeAmount - NodeOrder - 1);
            float backwardsPull = JointBody.mass * UnityEngine.Physics.gravity.magnitude * NodeOrder;

            if (NextJoint)
            {
                Vector3 dir = JointBody.position.DistanceFromVector(NextJoint.JointBody.position).normalized;
                NextJoint.JointBody.AddForce(forwardPull * dir);
            }

            if (PreviousJoint)
            {
                Vector3 dir = JointBody.position.DistanceFromVector(PreviousJoint.JointBody.position).normalized;
                PreviousJoint.JointBody.AddForce(backwardsPull * dir);
            }
        }

        private void MoveNeighborJoint(bool next = true)
        {
            ChainJoint neighbor = (next ? NextJoint : PreviousJoint);
            Vector3 dir = neighbor.JointBody.position - JointBody.position;

            Vector3 wantedPosition = JointBody.position + dir.normalized * JointDistance;

            neighbor.JointBody.AddForce(PhysicsTools.GetPushForceNeeded(neighbor.JointBody, wantedPosition,
                9999, 9999, 9999, MaxPullOfNextJoint));
        }

        private void SpringJoint(bool next = true)
        {
            ChainJoint neighbor = (next ? NextJoint : PreviousJoint);
            Vector3 dir = neighbor.JointBody.position - JointBody.position;

            neighbor.JointBody.AddForce(MaxPullToCenterForce * -dir);
        }

        private void ForceToCenter()
        {
            Vector3 midPoint = (NextJoint.JointBody.position + PreviousJoint.JointBody.position) / 2;

            JointBody.AddForce(PhysicsTools.GetPushForceNeeded(JointBody, midPoint, 9999, 9999, 9999,
                MaxPullToCenterForce));
        }

        public override void AttachToPreviousNode(IChain chain, ChainNode node)
        {
            if (!node)
                return;

            base.AttachToPreviousNode(chain, node);

            ChainJoint joint = node as ChainJoint;
            if (joint)
            {
                joint.NextJoint = this;
#if UNITY_EDITOR
                OdinPrefabUtility.UpdatePrefabInstancePropertyModifications(joint, false);
#endif
                PreviousJoint = joint;
                transform.localPosition = joint.transform.localPosition + JointDistance * Vector3.forward;
            }
            else
            {
                transform.localPosition = Vector3.zero;
            }
#if UNITY_EDITOR
            OdinPrefabUtility.UpdatePrefabInstancePropertyModifications(this, false);
#endif
        }
    }
}