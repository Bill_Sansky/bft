﻿namespace BFT
{
    public interface IChain
    {
        int NodeAmount { get; }
    }
}
