using UnityEngine;

namespace BFT
{
    public class HingeJointValueSetter : MonoBehaviour
    {
        public HingeJoint HingeJoint;

        public void SetBounciness(float value)
        {
            JointLimits limits = HingeJoint.limits;
            limits.bounciness = value;
            HingeJoint.limits = limits;
        }
    }
}
