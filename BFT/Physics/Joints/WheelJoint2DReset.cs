﻿using UnityEngine;

namespace BFT
{
    public class WheelJoint2DReset : MonoBehaviour
    {
        public bool ResetOnEnable = true;
        public bool SwitchAutoConfigure = false;
        public WheelJoint2D WheelJoint;

        void OnDisable()
        {
            WheelJoint.enabled = false;
            if (SwitchAutoConfigure)
                WheelJoint.autoConfigureConnectedAnchor = false;
        }

        void OnEnable()
        {
            if (ResetOnEnable)
            {
                EnableWheelJoint();
            }
        }

        public void EnableWheelJoint()
        {
            WheelJoint.enabled = true;
            if (SwitchAutoConfigure)
                WheelJoint.autoConfigureConnectedAnchor = true;
        }
    }
}
