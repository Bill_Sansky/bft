﻿using UnityEngine;

namespace BFT
{
    public class WheelJoint2DAnchorPositionUpdater : MonoBehaviour
    {
        public UnityEngine.Transform Reference;
        public WheelJoint2D WheelJoint;


        void Reset()
        {
            WheelJoint = GetComponent<WheelJoint2D>();
        }


        public void UpdateAnchorPosition()
        {
            WheelJoint.anchor = WheelJoint.transform.InverseTransformPoint(Reference.position);
            // WEEEEE <3 
        }
    }
}
