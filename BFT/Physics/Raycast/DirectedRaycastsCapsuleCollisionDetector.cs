﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class DirectedRaycastsCapsuleCollisionDetector : AbstractDirectedRaycastsCollisionDetector
    {
        public ICapsuleProperties Capsule;

        [FoldoutGroup("Cast")] [SerializeField]
        private CastType Cast;

        [FoldoutGroup("Debug")] public bool DebugView;

        [FoldoutGroup("Cast")] public float HeightScaleOffset = 0.05f;

        /// <summary>
        ///     Position of capsule bottom
        /// </summary>
        Vector3 Bottom => transform.position + Capsule.Center - Capsule.Height / 2 * Vector3.up;

        /// <summary>
        ///     Position of center sphere of capsule
        /// </summary>
        Vector3 CapsuleBottom => Bottom + Vector3.up * HeightScaleOffset * Capsule.Height + Capsule.Radius * Vector3.up;

        /// <summary>
        ///     Position of center sphere of capsule
        /// </summary>
        Vector3 CapsuleTop => Bottom + Vector3.up * Capsule.Height - Vector3.up * Capsule.Radius -
                              Vector3.up * HeightScaleOffset * Capsule.Height;

        protected override Vector3 CalculateOriginPoint()
        {
            return transform.position + Capsule.Center;
        }

        protected override void MakeRaycasts()
        {
            switch (Cast)
            {
                case CastType.RayCast:
                    RayCast();
                    break;
                case CastType.CapsuleCast:
                    CapsuleCast();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        ///     Three raycasts (center, left, right)
        /// </summary>
        private void RayCast()
        {
            CollisionRaycast(OriginPoint + Capsule.Radius * Direction);
            if (CollisionDetected)
                return;
            var directionTmp = Quaternion.AngleAxis(-90, Vector3.up) * Direction;
            CollisionRaycast(OriginPoint + Capsule.Radius * directionTmp);
            if (CollisionDetected)
                return;
            directionTmp = Quaternion.AngleAxis(90, Vector3.up) * Direction;
            CollisionRaycast(OriginPoint + Capsule.Radius * directionTmp);
        }

        private void CapsuleCast()
        {
            RaycastHit hit;
            var gotHit = UnityEngine.Physics.CapsuleCast(CapsuleBottom, CapsuleTop, Capsule.Radius, Direction, out hit, Distance, Mask);

            if (gotHit)
                collisionDetected = true;
        }

        enum CastType
        {
            RayCast,
            CapsuleCast
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (!DebugView)
                return;

            switch (Cast)
            {
                case CastType.RayCast:
                    DrawRayDebug();
                    break;
                case CastType.CapsuleCast:
                    DrawCapsuleDebug();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void DrawRayDebug()
        {
            Gizmos.DrawLine(OriginPoint + Capsule.Radius * Direction,
                OriginPoint + Capsule.Radius * Direction + Direction * Distance);
            var directionTmp = Quaternion.AngleAxis(-90, Vector3.up) * Direction;
            Gizmos.DrawLine(OriginPoint + Capsule.Radius * directionTmp,
                OriginPoint + Capsule.Radius * directionTmp + Direction * Distance);
            directionTmp = Quaternion.AngleAxis(90, Vector3.up) * Direction;
            Gizmos.DrawLine(OriginPoint + Capsule.Radius * directionTmp,
                OriginPoint + Capsule.Radius * directionTmp + Direction * Distance);
        }

        private void DrawCapsuleDebug()
        {
            var botEnd = CapsuleBottom + Direction * Distance;
            var topEnd = CapsuleTop + Direction * Distance;
            var spheresAmount = (int) Vector3.Magnitude(CapsuleBottom - botEnd);
            for (int i = 0; i <= spheresAmount; i++)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawSphere(Vector3.Lerp(CapsuleTop, topEnd, i / (float) spheresAmount), Capsule.Radius);
                Gizmos.color = Color.black;
                Gizmos.DrawSphere(Vector3.Lerp(CapsuleBottom, botEnd, i / (float) spheresAmount), Capsule.Radius);
            }
        }
#endif
    }
}
