﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class TopRaycast : UpdateTypeSwitchable, IValue<bool>
    {
        public bool CheckSurfaceAngle = false;
        public LayerMask Mask;

        [ShowIf("CheckSurfaceAngle")] public float MaxAngle;

        public Vector3 PositionOffset;
        public float RaycastHeight = 25;

        public bool Value { private set; get; }

        public override void UpdateMethod()
        {
            Value = true;

            Vector3 currentPos = transform.position;
            Vector3 raycastPos = currentPos;
            raycastPos.y = RaycastHeight;
            Vector3 dir = currentPos - raycastPos;
            RaycastHit hit;
            bool gotHit = UnityEngine.Physics.Raycast(raycastPos, dir, out hit, 9999, Mask);

            if (gotHit)
            {
                if (CheckSurfaceAngle)
                {
                    float angle = Vector3.Angle(Vector3.up, hit.normal);
                    if (angle > MaxAngle)
                        Value = false;
                }

                transform.position = hit.point + PositionOffset;
            }
        }
    }
}
