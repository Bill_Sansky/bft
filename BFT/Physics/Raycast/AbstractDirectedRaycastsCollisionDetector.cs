﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public abstract class AbstractDirectedRaycastsCollisionDetector : MonoBehaviour,
        IValue<bool>
    {
        protected bool collisionDetected = false;

        private Vector3 direction;
        private float distance;

        [MinValue(0)] public float Frequency = 0.025f;

        public LayerMask Mask;
        private Vector3 originPoint;

        [SerializeField] private bool preserveSourceHeight = true;

        public UnityEngine.Transform Target;
        protected float Distance => distance;
        protected Vector3 Direction => direction;
        protected bool CollisionDetected => collisionDetected;
        protected Vector3 OriginPoint => originPoint;

        public virtual bool Value => collisionDetected;

        protected abstract Vector3 CalculateOriginPoint();
        protected abstract void MakeRaycasts();

        private void OnEnable()
        {
            StartCoroutine(CheckForCollisions());
        }

        private IEnumerator CheckForCollisions()
        {
            while (true)
            {
                originPoint = CalculateOriginPoint();
                Vector3 targetPosition = Target.position;
                if (preserveSourceHeight)
                    targetPosition.y = OriginPoint.y;
                direction = targetPosition - OriginPoint;
                distance = direction.magnitude;
                direction = direction.normalized;
                collisionDetected = false;
                MakeRaycasts();
                yield return new WaitForSecondsRealtime(Frequency);
            }
        }

        protected void CollisionRaycast(Vector3 point)
        {
            RaycastHit hit;
            bool gotHit = UnityEngine.Physics.Raycast(point, direction, out hit, distance, Mask);

            if (gotHit)
            {
                collisionDetected = true;
            }
        }
    }
}
