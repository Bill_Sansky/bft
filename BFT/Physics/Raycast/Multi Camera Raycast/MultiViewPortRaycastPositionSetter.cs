﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class MultiViewPortRaycastPositionSetter : MonoBehaviour
    {
        [BoxGroup("Raycast Origin")] public RectTransform PositionToRaycast;
        [BoxGroup("Raycast Options")] public bool AlignToNormal;

        [BoxGroup("Raycast Options"), ShowIf("AlignToNormal")]
        public DirectionAxis Axis;

        [BoxGroup("Cameras")]
        public List<CameraForRectTransform> CamerasPerViewports = new List<CameraForRectTransform>();

        [Serializable]
        public class CameraForRectTransform
        {
            public RectTransform Transform;
            public Camera Camera;
        }
        
        [BoxGroup("Cameras")] [Tooltip("The camera that will be chosen for the raycast in case no rect overlap")]
        public CameraValue DefaultCamera;

        [BoxGroup("Raycast Options")] public bool IgnoreTriggers;

        private RectTransform lastViewPort;

        [BoxGroup("Tools")] public bool LogDebug = false;

        [BoxGroup("Raycast Options")] public LayerMask Mask;

        [BoxGroup("Raycast Options")] public float MinMovementDistanceForRaycast = 0.001f;

        [BoxGroup("Events")] public UnityEvent OnRayCastViewPortChanged;


        private Vector2 previousRectPosition;

        [BoxGroup("Referencing"), ShowIf("SyncGlobalReference")]
        public MultiViewPortRaycastVariableAsset Reference;

        [BoxGroup("Referencing")] public bool SyncGlobalReference = false;

        [BoxGroup("Raycast Options")] public float UpdateFrequency = 0;

        public Vector3 HitNormal { get; private set; }
        public Vector3 RaycastAntiDirection { get; private set; }

        public Vector3 RaycastOrigin { get; private set; }


        [ShowInInspector, ReadOnly, BoxGroup("Status")]
        public GameObject HitObject { get; private set; }

        public void Awake()
        {
            if (SyncGlobalReference)
            {
                Reference.Value = this;
            }
        }

        public void OnEnable()
        {
            StartCoroutine(CheckRayCast());
        }

        public void OnDisable()
        {
            StopAllCoroutines();
        }

        private IEnumerator CheckRayCast()
        {
            //making sure that the first raycast will occur by setting
            //the previous position to an arbitrary far distance from the canvas
            previousRectPosition = new Vector2(-5000, -5000);

            while (true)
            {
                if ((previousRectPosition - (Vector2) PositionToRaycast.position).sqrMagnitude
                    < MinMovementDistanceForRaycast)
                {
                    yield return null;
                }

                previousRectPosition = PositionToRaycast.position;

                bool found = false;
                foreach (var cameraPairs in CamerasPerViewports)
                {
                    if (RectTransformUtility
                        .RectangleContainsScreenPoint(cameraPairs.Transform, PositionToRaycast.position))
                    {
                        if (LogDebug)
                        {
                            UnityEngine.Debug.Log("Sub camera found! raycasting with it", this);
                        }
                        //then the rect was found, raycast using the corresponding camera
                        //and the adjusted position according to the parent rect                

                        Camera rectCam = cameraPairs.Camera;

                        int width = rectCam.activeTexture ? rectCam.activeTexture.width : rectCam.pixelWidth;
                        int height = rectCam.activeTexture ? rectCam.activeTexture.height : rectCam.pixelHeight;

                        Vector2 transformedPosition =
                            cameraPairs.Transform.InverseTransformPoint(PositionToRaycast.position);
                        Vector2 fakedPosition = new Vector2(
                            transformedPosition.x / cameraPairs.Transform.rect.width * width + (float) width / 2,
                            transformedPosition.y / cameraPairs.Transform.rect.height * height + (float) height / 2);

                        var ray = rectCam.ScreenPointToRay(fakedPosition);


                        found = Raycast(ray);

                        if (lastViewPort != cameraPairs.Transform)
                        {
                            lastViewPort = cameraPairs.Transform;
                            OnRayCastViewPortChanged.Invoke();
                        }

                        break;
                    }
                }

                if (!found)
                {
                    var ray = DefaultCamera.Value.ScreenPointToRay(PositionToRaycast.position);

                    Raycast(ray);

                    if (lastViewPort != null)
                    {
                        lastViewPort = null;
                        OnRayCastViewPortChanged.Invoke();
                    }
                }
            }
        }

        public void AttachViewPortCameraPair(Camera cam, RectTransform viewPort)
        {
           Debug.Assert(CamerasPerViewports.All(_ => _.Transform != viewPort),"Trying to attach a view port that is already present",this);
         

            CamerasPerViewports.Add(new CameraForRectTransform(){Transform = viewPort,Camera = cam});
        }

        public void DetachViewPortCameraPair(RectTransform viewPort)
        {
            Debug.Assert(CamerasPerViewports.Any(_ => _.Transform == viewPort),"Trying to detach a view port that is not present",this);
           
            CamerasPerViewports.RemoveAll(_=>_.Transform==viewPort);
        }

        protected virtual bool Raycast(Ray ray)
        {
            bool gotHit;

            RaycastHit hit;
            gotHit = UnityEngine.Physics.Raycast(ray, out hit, 9999, Mask,
                IgnoreTriggers ? QueryTriggerInteraction.Ignore : QueryTriggerInteraction.Collide);
            RaycastAntiDirection = -ray.direction;
            if (gotHit)
            {
                transform.AlignTransformToCollision(hit.point,
                    hit.normal, AlignToNormal, Axis);
                HitNormal = hit.normal;
                HitObject = hit.collider.gameObject;
                RaycastOrigin = ray.origin;
                return true;
            }

            return false;
        }
    }
}
