﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class ViewPortCameraPair : MonoBehaviour
    {
        public bool AutoAttachAndDetach;
        public Camera Camera;
        public RectTransform CameraViewPort;

        public MultiViewPortRaycastPositionSetterValue RayCasterReference;

        public void AttachToRayCaster()
        {
            RayCasterReference.Value.AttachViewPortCameraPair(Camera, CameraViewPort);
        }

        public void DetachFromRayCaster()
        {
            RayCasterReference.Value.DetachViewPortCameraPair(CameraViewPort);
        }

        public void OnEnable()
        {
            if (AutoAttachAndDetach)
                StartCoroutine(AttachWhenReady());
        }

        public void OnDisable()
        {
            if (AutoAttachAndDetach)
                DetachFromRayCaster();
        }

        public IEnumerator AttachWhenReady()
        {
            while (!RayCasterReference.Value)
            {
                yield return null;
            }

            RayCasterReference.Value.AttachViewPortCameraPair(Camera, CameraViewPort);
        }
    }
}
