﻿using System;

namespace BFT
{
    [Serializable]
    public class MultiViewPortRaycastPositionSetterValue : GenericValue<MultiViewPortRaycastPositionSetter>
    {
    }
}