﻿using System;
using UnityEngine.Events;

namespace BFT
{
    [Serializable]
    public class MultiViewPortRaycastEvent : UnityEvent<MultiViewPortRaycastPositionSetter>
    {
    }
}
