﻿using UnityEngine;

namespace BFT
{
    public abstract class AbstractForcedRotationAction : MonoBehaviour
    {
        public TransformAxisFlipper Inverter;

        protected virtual void Awake()
        {
            Inverter.OnFlipStarted.AddClean(PrepareForFlipping);
            Inverter.OnFlipEnded.AddClean(ResumeAfterFlipping);
        }

        protected abstract void ResumeAfterFlipping();

        protected abstract void PrepareForFlipping();

        protected virtual void Destroy()
        {
            if (Inverter)
            {
                Inverter.OnFlipStarted.RemoveListener(PrepareForFlipping);
                Inverter.OnFlipEnded .RemoveListener(ResumeAfterFlipping);
            }
        }
    }
}
