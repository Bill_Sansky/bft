﻿using UnityEngine;

namespace BFT
{
    public class CustomWheelJointRotationInverter : AbstractForcedRotationAction
    {
        private ConfigurableJoint joint;
        private ConfigurableJointMotion previousXAngleMotion;
        private ConfigurableJointMotion previousYAngleMotion;
        private ConfigurableJointMotion previousZAngleMotion;


        private RigidbodyConstraints priorConstaints;

        public Rigidbody WheelBody;


        protected override void Awake()
        {
            base.Awake();

            if (!joint)
                joint = GetComponent<ConfigurableJoint>();
        }

        protected override void ResumeAfterFlipping()
        {
            ResumeNormalJoint();
        }

        protected override void PrepareForFlipping()
        {
            PrepareJointForFlipping();
        }


        private void PrepareJointForFlipping()
        {
            priorConstaints = WheelBody.constraints;

            WheelBody.constraints = RigidbodyConstraints.None;

            previousXAngleMotion = joint.angularXMotion;
            joint.angularXMotion = ConfigurableJointMotion.Locked;

            previousYAngleMotion = joint.angularYMotion;
            joint.angularYMotion = ConfigurableJointMotion.Locked;

            previousZAngleMotion = joint.angularZMotion;
            joint.angularZMotion = ConfigurableJointMotion.Locked;
        }

        private void ResumeNormalJoint()
        {
            WheelBody.constraints = priorConstaints;

            joint.angularXMotion = previousXAngleMotion;

            joint.angularYMotion = previousYAngleMotion;

            joint.angularZMotion = previousZAngleMotion;
        }
    }
}
