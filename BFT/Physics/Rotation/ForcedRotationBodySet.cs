﻿using UnityEngine;

namespace BFT
{
    public class ForcedRotationBodySet : AbstractForcedRotationAction
    {
        private Rigidbody body;

        private RigidbodyConstraints priorConstraints;

        protected override void Awake()
        {
            base.Awake();
            body = GetComponent<Rigidbody>();
        }

        protected override void ResumeAfterFlipping()
        {
            body.constraints = priorConstraints;
        }

        protected override void PrepareForFlipping()
        {
            priorConstraints = body.constraints;
            body.constraints = RigidbodyConstraints.None;
        }
    }
}
