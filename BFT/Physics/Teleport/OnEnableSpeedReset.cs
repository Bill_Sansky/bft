﻿using UnityEngine;

namespace BFT
{
    public class OnEnableSpeedReset : MonoBehaviour
    {
        public Rigidbody2D Body;
        public Rigidbody2D ReferenceBody;

        void Reset()
        {
            Body = GetComponent<Rigidbody2D>();
        }

        void OnEnable()
        {
            Body.isKinematic = false;
            Body.velocity = ReferenceBody.velocity;
        }

        void OnDisable()
        {
            Body.isKinematic = true;
        }
    }
}
