using System;
using UnityEditor;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class Box2DTeleportConstrainer : MonoBehaviour
    {
        public delegate void TeleportAction(BoxConstrainEnum constrain);

        public float BottomDistance;
        private BoxConstrainEnum currentConstrain;

        public float LeftDistance;
        public float RightDistance;
        private bool shouldTeleport;

        public AbstractTeleporter Teleporter;

        public float TeleportOffset = 0;
        private Vector3 teleportOffsetVector;

        private Vector3 teleportPosition;
        public float TopDistance;

        public float Length => RightDistance - LeftDistance;
        public float Height => TopDistance - BottomDistance;

        public Vector3 BottomLeft => transform.position + new Vector3(LeftDistance, BottomDistance, 0);

        public event TeleportAction OnTeleportOccured;

        void Reset()
        {
            if (Teleporter)
                Teleporter.SetTeleportPosition(transform.position);
        }

        void FixedUpdate()
        {
            teleportOffsetVector = Vector3.zero;

            teleportPosition = transform.InverseTransformPoint(Teleporter.GetObjectToTeleportCurrentPosition());
            currentConstrain = BoxConstrainEnum.NONE;

            shouldTeleport = false;

            if (teleportPosition.x > RightDistance)
            {
                shouldTeleport = true;
                teleportPosition.x -= Length;
                currentConstrain = BoxConstrainEnum.RIGHT;
                teleportOffsetVector.x += TeleportOffset;
            }
            else if (teleportPosition.x < LeftDistance)
            {
                shouldTeleport = true;
                teleportPosition.x += Length;
                currentConstrain = BoxConstrainEnum.LEFT;
                teleportOffsetVector.x -= TeleportOffset;
            }

            if (teleportPosition.y > TopDistance)
            {
                shouldTeleport = true;
                teleportPosition.y -= Height;
                teleportOffsetVector.y += TeleportOffset;


                if (currentConstrain == BoxConstrainEnum.RIGHT)
                {
                    currentConstrain = BoxConstrainEnum.TOP_RIGHT;
                }
                else if (currentConstrain == BoxConstrainEnum.LEFT)
                {
                    currentConstrain = BoxConstrainEnum.TOP_LEFT;
                }
                else
                {
                    currentConstrain = BoxConstrainEnum.TOP;
                }
            }
            else if (teleportPosition.y < BottomDistance)
            {
                shouldTeleport = true;
                teleportPosition.y += Height;
                teleportOffsetVector.y -= TeleportOffset;

                if (currentConstrain == BoxConstrainEnum.RIGHT)
                {
                    currentConstrain = BoxConstrainEnum.BOTTOM_RIGHT;
                }
                else if (currentConstrain == BoxConstrainEnum.LEFT)
                {
                    currentConstrain = BoxConstrainEnum.BOTTOM_LEFT;
                }
                else
                {
                    currentConstrain = BoxConstrainEnum.BOTTOM;
                }
            }

            if (shouldTeleport)
            {
                teleportPosition = transform.TransformPoint(teleportPosition);
                Teleporter.SetTeleportPosition(teleportPosition + teleportOffsetVector);
                Teleporter.Teleport();
#if UNITY_EDITOR
                GUIUtility.hotControl = 0;
#endif
                if (OnTeleportOccured != null) OnTeleportOccured(currentConstrain);
            }
        }

#if UNITY_EDITOR

        public bool TeleportInEditMode = false;
        public bool DebugConstrains = true;
        public bool DebugNames = true;

        public void OnDrawGizmos()
        {
            if (!DebugConstrains)
                return;
            Gizmos.matrix = transform.localToWorldMatrix;
            Handles.matrix = transform.localToWorldMatrix;

            //top line
            Gizmos.color = Color.green.DarkerBrighter(0.1f);
            Gizmos.DrawLine(new Vector3(LeftDistance, TopDistance, 0), new Vector3(RightDistance, TopDistance, 0));
            if (DebugNames)
                HandleExt.Text(new Vector3(0, TopDistance, 0), "Top Distance");

            //bottom line
            Gizmos.color = Color.green;
            Gizmos.DrawLine(new Vector3(LeftDistance, BottomDistance, 0),
                new Vector3(RightDistance, BottomDistance, 0));
            if (DebugNames)
                HandleExt.Text(new Vector3(0, BottomDistance, 0), "Bottom Distance");

            //right Line
            Gizmos.color = Color.red.DarkerBrighter(0.1f);
            Gizmos.DrawLine(new Vector3(RightDistance, BottomDistance, 0), new Vector3(RightDistance, TopDistance, 0));
            if (DebugNames)
                HandleExt.Text(new Vector3(RightDistance, 0, 0), "Right Distance");

            //left Line
            Gizmos.color = Color.red;
            Gizmos.DrawLine(new Vector3(LeftDistance, BottomDistance, 0), new Vector3(LeftDistance, TopDistance, 0));
            if (DebugNames)
                HandleExt.Text(new Vector3(LeftDistance, 0, 0), "Left Distance");

            Gizmos.matrix = Matrix4x4.identity;
            Handles.matrix = Matrix4x4.identity;
        }

        void Update()
        {
            if (Application.isPlaying || !TeleportInEditMode)
                return;

            FixedUpdate();
        }

#endif
    }
}