﻿using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

#endif

namespace BFT
{
    public class TransformTeleporter : AbstractTeleporter
    {
        public Vector3 TeleportDestination;
        public UnityEngine.Transform ToTeleport;

        public override void Teleport()
        {
            ToTeleport.position = TeleportDestination;

#if UNITY_EDITOR
            if (Selection.activeGameObject == ToTeleport.gameObject)
                UnityEditor.Tools.current = Tool.Move;
#endif
        }

        public override void SetTeleportPosition(Vector3 position)
        {
            TeleportDestination = position;
        }

        public override Vector3 GetObjectToTeleportCurrentPosition()
        {
            return ToTeleport.position;
        }
    }
}
