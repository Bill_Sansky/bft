﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public abstract class AbstractTeleporter : MonoBehaviour
    {
        public abstract void Teleport();
        public abstract void SetTeleportPosition(Vector3 position);
        public abstract Vector3 GetObjectToTeleportCurrentPosition();
    }
}
