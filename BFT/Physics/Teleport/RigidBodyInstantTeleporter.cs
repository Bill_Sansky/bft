﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
#endif

namespace BFT
{
    public class RigidBodyInstantTeleporter : AbstractTeleporter
    {
        public List<Rigidbody> AttachedByJoints;

        public bool MotionKept = true;

        private Vector3 teleportDestination;

        public Rigidbody ToTeleport;

        public override void Teleport()
        {
            Teleport(ToTeleport, TeleportPosition());

#if UNITY_EDITOR
            // if (Selection.activeGameObject == toTeleport.gameObject)
            UnityEditor.Tools.current = Tool.Move;
#endif
        }

        public override void SetTeleportPosition(Vector3 position)
        {
            teleportDestination = position;
        }

        public override Vector3 GetObjectToTeleportCurrentPosition()
        {
            return ToTeleport.position;
        }

        public void AddAttachedByJoint(Rigidbody body)
        {
            if (AttachedByJoints.Contains(body))
                return;

            AttachedByJoints.Add(body);
        }


        public void Teleport(Rigidbody body, Vector3 position)
        {
            PhysicsTools.TeleportRigidBody(body, position, MotionKept, AttachedByJoints);
        }

        public void Teleport(Vector3 position)
        {
            PhysicsTools.TeleportRigidBody(ToTeleport, position, MotionKept, AttachedByJoints);
        }

        protected virtual Vector3 TeleportPosition()
        {
            return teleportDestination;
        }
    }
}
