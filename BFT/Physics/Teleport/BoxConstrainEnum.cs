﻿#if UNITY_EDITOR
#endif

namespace BFT
{
    public enum BoxConstrainEnum
    {
        NONE,
        TOP,
        BOTTOM,
        RIGHT,
        LEFT,
        TOP_RIGHT,
        TOP_LEFT,
        BOTTOM_RIGHT,
        BOTTOM_LEFT,
    }
}
