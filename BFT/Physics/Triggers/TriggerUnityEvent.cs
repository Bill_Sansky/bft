﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class TriggerUnityEvent : MonoBehaviour, ICallBackTriggerOnDisable
    {
        [BoxGroup("Utils")] public bool DebugLog = false;

        [BoxGroup("Options")] public bool IgnoreTriggers = true;

        [BoxGroup("Options")] public LayerMask LayerMask = ~0;

        [BoxGroup("Options")]
        [Tooltip(
            "If true, the event will return the gameobject attached to the rigidbody of the collider instead of the gameobject of the collider")]
        public bool GameObjectFromRigidbody = true;

        [BoxGroup("Events")] public GameObjectEvent OnTriggerZoneEntered;

        [BoxGroup("Events")] public GameObjectEvent OnTriggerZoneExited;

        public void OnTriggerEnter(Collider col)
        {
            if (col.isTrigger && IgnoreTriggers)
            {
                if (DebugLog)
                    UnityEngine.Debug.LogFormat(this,
                        "Trigger {0} did NOT trigger on collider {1} because it ignores triggers", name, col.name);
                return;
            }

            if (LayerMask == -1 || LayerMask.IsInLayerMask(col.gameObject))
            {
                GameObject go;
                if (GameObjectFromRigidbody)
                {
                    go = col.attachedRigidbody ? col.attachedRigidbody.gameObject : null;
                }
                else
                {
                    go = col.gameObject;
                }

                    OnTriggerZoneEntered.Invoke(go);

                OnTriggerEnterEvent?.Invoke(col);
                if (DebugLog)
                    UnityEngine.Debug.LogFormat(this, "Trigger {0} triggered on collider {1}", name, col.name);
                return;
            }

            if (DebugLog)
                UnityEngine.Debug.LogFormat(this, "Trigger {0} did NOT trigger on collider {1} because of layer masks",
                    name, col.name);
        }

        public void OnTriggerExit(Collider col)
        {
            if (col.isTrigger && IgnoreTriggers)
                return;


            if (LayerMask == -1 || LayerMask.IsInLayerMask(col.gameObject))
            {
                GameObject go;
                if (GameObjectFromRigidbody)
                {
                    go = col.attachedRigidbody ? col.attachedRigidbody.gameObject : null;
                }
                else
                {
                    go = col.gameObject;
                }

                OnTriggerZoneExited.Invoke(go);
                if (DebugLog)
                    UnityEngine.Debug.LogFormat(this, "Trigger {0} exited on collider {1}", name, col.name);
                OnTriggerExitEvent?.Invoke(col);
                return;
            }

            if (DebugLog)
                UnityEngine.Debug.LogFormat(this, "Trigger {0} did NOT exit on collider {1} because of layer masks",
                    name, col.name);
        }

        public event Action<Collider> OnTriggerEnterEvent;
        public event Action<Collider> OnTriggerExitEvent;
    }
}