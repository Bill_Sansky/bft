using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    /// <summary>
    ///     Unity does not call OnTriggerExit when colliders are disabled: this script allows triggers to receive that event
    /// </summary>
    public class TriggerOnDisableCallBack : MonoBehaviour
    {
        public Dictionary<GameObject, OnDisableEvent> EventsPerCollider = new Dictionary<GameObject, OnDisableEvent>();

        [OnValueChanged("CheckIfTrigger")] public MonoBehaviour TriggerReference;

        private ICallBackTriggerOnDisable DisabledCallBack => (ICallBackTriggerOnDisable) TriggerReference;

        private void Awake()
        {
            DisabledCallBack.OnTriggerEnterEvent -= RegisterNewCollider;
            DisabledCallBack.OnTriggerExitEvent -= UnRegisterNewCollider;
            DisabledCallBack.OnTriggerEnterEvent += RegisterNewCollider;
            DisabledCallBack.OnTriggerExitEvent += UnRegisterNewCollider;
        }


        private void CheckIfTrigger()
        {
            if (!TriggerReference)
                return;

            if (!(TriggerReference is ICallBackTriggerOnDisable))
            {
                foreach (var comp in TriggerReference.gameObject.GetComponents<MonoBehaviour>())
                {
                    if (comp is ICallBackTriggerOnDisable)
                    {
                        TriggerReference = comp;
                        return;
                    }
                }

                UnityEngine.Debug.LogWarning(
                    $"Your script must implement the {typeof(ICallBackTriggerOnDisable)} interface", this);
                TriggerReference = null;
            }
        }

        public void RegisterNewCollider(Collider collider)
        {
            if (EventsPerCollider.ContainsKey(collider.gameObject))
                return;

            OnDisableEvent evt;
            evt = collider.GetComponent<OnDisableEvent>();
            if (!evt)
                evt = collider.gameObject.AddComponent<OnDisableEvent>();
            evt.OnDisabled.AddListener(CallbackTriggerExit);
            EventsPerCollider.Add(collider.gameObject, evt);
        }

        private void CallbackTriggerExit(GameObject go)
        {
            RemoveCallback(go);
            DisabledCallBack.OnTriggerExit(go.GetComponent<Collider>());
        }

        private void RemoveCallback(GameObject go)
        {
            var callback = EventsPerCollider[go];
            EventsPerCollider.Remove(go);
            callback.OnDisabled.RemoveListener(CallbackTriggerExit);
          
        }

        private void UnRegisterNewCollider(Collider collider)
        {
            if (!EventsPerCollider.ContainsKey(collider.gameObject))
                return;
            RemoveCallback(collider.gameObject);
        }
    }
}