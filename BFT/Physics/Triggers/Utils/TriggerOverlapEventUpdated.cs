﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

namespace BFT
{
    class TriggerOverlapEventUpdated : TriggerUnityEvent
    {
        private float elapsed;
        private List<Collider> overlapped = new List<Collider>();

        [SerializeField] [FormerlySerializedAs("RealtimeUpdateIntervalMiliseconds")]
        private float realtimeUpdateIntervalMiliseconds;

        private Collider triggerColldier;

        protected void Start()
        {
            triggerColldier = GetComponent<Collider>();
            ResetCollider();
        }

        void OnValidate()
        {
            triggerColldier = GetComponent<Collider>();
            ResetCollider();
        }

        void Update()
        {
            elapsed += UnityEngine.Time.unscaledDeltaTime;
            if (elapsed >= realtimeUpdateIntervalMiliseconds / 1000)
            {
                Check();
                elapsed = 0;
            }
        }

        private void Check()
        {
            Collider[] currentlyOverlapped;
            GetOverlapped(out currentlyOverlapped);
            currentlyOverlapped = currentlyOverlapped.Except(new[] {triggerColldier}).ToArray();

            var newOverlapped = currentlyOverlapped.Except(overlapped);

            foreach (var coll in newOverlapped)
                OnTriggerEnter(coll);

            var oldOverlapped = overlapped.Except(currentlyOverlapped).ToArray();

            foreach (var coll in oldOverlapped)
                OnTriggerExit(coll);

            overlapped = currentlyOverlapped.ToList();
        }

        private void GetOverlapped(out Collider[] overlapped)
        {
            var boxCollider = triggerColldier as BoxCollider;
            if (boxCollider != null)
            {
                overlapped = UnityEngine.Physics.OverlapBox(boxCollider.transform.position,
                    new Vector3(boxCollider.transform.lossyScale.x / 2, boxCollider.transform.lossyScale.y / 2,
                        boxCollider.transform.lossyScale.z / 2),
                    boxCollider.transform.rotation, LayerMask);
                return;
            }

            var sphereCollider = triggerColldier as SphereCollider;
            if (sphereCollider != null)
            {
                var maxScale = new[]
                {
                    sphereCollider.transform.lossyScale.x, sphereCollider.transform.lossyScale.y,
                    sphereCollider.transform.lossyScale.z
                }.Max();
                overlapped = UnityEngine.Physics.OverlapSphere(sphereCollider.transform.position, sphereCollider.radius * maxScale,
                    LayerMask);
                return;
            }

            var capsuleCollider = triggerColldier as CapsuleCollider;
            if (capsuleCollider != null)
            {
                Vector3 p0, p1;
                p0 = p1 = capsuleCollider.transform.position;
                float heightDiff = capsuleCollider.height / 2f - capsuleCollider.radius;
                p0.y += heightDiff;
                p1.y -= heightDiff;
                overlapped = UnityEngine.Physics.OverlapCapsule(p0, p1, capsuleCollider.radius, LayerMask);
                return;
            }

            throw new NotSupportedException();
        }

        private void ResetCollider()
        {
            var boxCollider = triggerColldier as BoxCollider;
            if (boxCollider != null)
            {
                boxCollider.center = Vector3.zero;
                boxCollider.size = Vector3.one;
            }

            var sphereCollider = triggerColldier as SphereCollider;
            if (sphereCollider != null)
            {
                sphereCollider.center = Vector3.zero;
            }

            var capsuleCollider = triggerColldier as CapsuleCollider;
            if (capsuleCollider != null)
            {
                capsuleCollider.center = Vector3.zero;
            }
        }
    }
}
