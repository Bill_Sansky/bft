﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    class TriggerActivator : MonoBehaviour
    {
        [BoxGroup("Activation")] [SerializeField]
        public bool ActivateOnEnter;

        [BoxGroup("Activation")] public GameObject ObjectToActivate;

        public void OnTriggerEnter(Collider col)
        {
            ObjectToActivate.SetActive(ActivateOnEnter);
        }

        public void OnTriggerExit(Collider cold)
        {
            ObjectToActivate.SetActive(!ActivateOnEnter);
        }
    }
}
