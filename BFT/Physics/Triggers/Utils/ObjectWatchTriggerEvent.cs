﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    /// <summary>
    ///     Checks if a specific object enter or exited a trigger and calls specific events
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class ObjectWatchTriggerEvent : MonoBehaviour
    {
        public delegate void ObjectExit(GameObject gobject, ObjectWatchTriggerEvent watch);

        [BoxGroup("Events")] public UnityEvent OnObjectEntered;

        [BoxGroup("Events")] public UnityEvent OnObjectExited;

        [InfoBox("Checks if a specific object enter or exited a trigger and calls specific events")] [BoxGroup("Watch")]
        public GameObject ToWatch;

        public event ObjectExit ObjectLeavingEvent;
        public event ObjectExit ObjectEnteringEvent;

        public void ChangeGameObject(GameObject gObject)
        {
            ToWatch = gObject;
        }

        void OnTriggerEnter(Collider col)
        {
            if (col.gameObject == ToWatch)
            {
                if (ObjectEnteringEvent != null) ObjectEnteringEvent(ToWatch, this);
                OnObjectEntered.Invoke();
            }
        }

        void OnTriggerExit(Collider col)
        {
            if (col.gameObject == ToWatch)
            {
                if (ObjectLeavingEvent != null) ObjectLeavingEvent(ToWatch, this);
                OnObjectExited.Invoke();
            }
        }
    }
}
