﻿using UnityEngine;

namespace BFT
{
    public class TriggerDestroyer : MonoBehaviour
    {
        public bool DestroyOnEnter;

        public void OnTriggerEnter(Collider col)
        {
            if (DestroyOnEnter)
            {
                Destroy(col.gameObject);
            }
        }

        public void OnTriggerExit(Collider col)
        {
            if (!DestroyOnEnter)
            {
                Destroy(col.gameObject);
            }
        }
    }
}
