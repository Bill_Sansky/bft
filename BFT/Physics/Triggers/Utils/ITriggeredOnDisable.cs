using System;
using UnityEngine;

namespace BFT
{
    public interface ICallBackTriggerOnDisable
    {
        void OnTriggerExit(Collider other);
        event Action<Collider> OnTriggerEnterEvent;
        event Action<Collider> OnTriggerExitEvent;
    }
}
