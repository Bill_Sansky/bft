﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    /// <summary>
    ///     A detection trigger that directly links the detected colliders
    ///     to a dictionary for a faster access and a referencing of objects
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ReferencedDetectionTrigger<T,T1> : AbstractDetectionTrigger<T> where T : Component where T1:GenericVariable<T>
    {
        // [BoxGroup("References")] public ColliderDictionary<T> ColliderDictionary;

        [ShowIf("UseDetectedObjectReference")] [BoxGroup("References")]
        public T1 DetectedObjectReference;

        [BoxGroup("Options")] public bool IgnoreTriggers = true;

        [FoldoutGroup("Utils")] public bool LogDebug;
        [BoxGroup("References")] public bool UseDetectedObjectReference = true;

        public override T LastDetectedObject { get; protected set; }
        public override bool IsTriggered { get; protected set; }

        public void OnTriggerEnter(Collider other)
        {
            if (IgnoreTriggers && other.isTrigger)
                return;

            /*   Debug.Assert(ColliderDictionary.ObjectsByCollider.ContainsKey(other),
               "The colldier is not present in the dictionary but was detected by the trigger: this is not allowed",this);*/


            //  ColliderDictionary.ObjectsByCollider.TryGetValue(other, out var detected);

            T detected = other.GetComponent<T>();

            if (detected != null)
            {
                if (LastDetectedObject != null && LogDebug)
                {
                    UnityEngine.Debug.LogWarning("An object was already detected, " +
                                                 "but the trigger found another object and replaced the detected object with the new one",
                        this);
                }

                LastDetectedObject = detected;
                IsTriggered = true;
                if (UseDetectedObjectReference)
                {
                    DetectedObjectReference.Value = LastDetectedObject;
                }

                InvokeTriggerEntered();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (IgnoreTriggers && other.isTrigger)
                return;


            var detected = other.GetComponent<T>();


            if (detected && detected.Equals(LastDetectedObject))
            {
                InvokeTriggerExited();
                //the detected object is changed after the call to allow for modification before the reference is lost

                LastDetectedObject = default(T);
                IsTriggered = false;
                if (UseDetectedObjectReference)
                {
                    DetectedObjectReference.Value = LastDetectedObject;
                }
            }
            else if (LogDebug)
            {
                UnityEngine.Debug.LogWarning("An object exited the trigger, " +
                                             "but itr wasn't the detected object, somehow", this);
            }
        }
    }
}