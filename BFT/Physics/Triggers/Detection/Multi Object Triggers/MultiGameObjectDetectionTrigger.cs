﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    public class MultiGameObjectDetectionTrigger : MultiDetectionTrigger<GameObject>
    {
        public override List<GameObject> LastDetectedObject
        {
            get => CurrentObjectDetected;
            protected set
            {
                //do nothing
            }
        }
    }
}
