﻿using UnityEngine;

namespace BFT
{
    public class SelectableDetectionTrigger : ReferencedDetectionTrigger<UnityEngine.UI.Selectable, SelectableVariable>
    {
        public bool UseOnTriggerStay = true;

        public void OnTriggerStay(Collider other)
        {
            if (!LastDetectedObject)
                OnTriggerEnter(other);
        }
    }
}