﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    /// <summary>
    ///     An object tha6t can be influenced by a trigger influence
    /// </summary>
    public class InfluenceableObject : MonoBehaviour
    {
        public Collider ColliderForDetection;

        public List<EnumAsset> InfluenceableLayers;

        private void OnEnable()
        {
            TriggerInfluence.InfluenceableObjectsByCollider.Add(ColliderForDetection, this);
        }

        private void OnDisable()
        {
            TriggerInfluence.InfluenceableObjectsByCollider.Remove(ColliderForDetection);
        }
    }
}
