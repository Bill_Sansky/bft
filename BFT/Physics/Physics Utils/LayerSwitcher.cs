﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class LayerSwitcher : MonoBehaviour
    {
        [BoxGroup("Tools")] public bool DebugLog;

        [BoxGroup("Options"), ShowIf("SwitchChildren")]
        public bool IgnoreChildrenOnDifferentLayer = true;

        [BoxGroup("Options")] [ValueDropdown("LayerDropDown")]
        public int InitialLayer;

        [BoxGroup("Options")] [ValueDropdown("LayerDropDown")]
        public int Layer;

        [BoxGroup("Link")] public GameObject ObjectToSwitch;

        private int previousLayer;

        [BoxGroup("Options")] public bool RevertSwitchOnDisable;

        [BoxGroup("Options")] public bool SwitchChildren = true;

        [BoxGroup("Options")] public bool SwitchOnEnable;

        void Reset()
        {
            InitialLayer = gameObject.layer;
            ObjectToSwitch = gameObject;
        }

#if UNITY_EDITOR
        private ValueDropdownList<int> LayerDropDown()
        {
            ValueDropdownList<int> dropdown = new ValueDropdownList<int>();
            for (int i = 0; i <= 31; i++)
            {
                var layerN = LayerMask.LayerToName(i);
                if (layerN.Length > 0)
                    dropdown.Add(layerN, i);
            }

            return dropdown;
        }
#endif

        public void SwitchLayer(GameObject obj)
        {
            ObjectToSwitch = obj;
            SwitchLayer();
        }

        public void RevertSwitch(GameObject obj)
        {
            ObjectToSwitch = obj;
            RevertSwitch();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public void SwitchLayer()
        {
            previousLayer = ObjectToSwitch.layer;

            ObjectToSwitch.layer = Layer;
            if (SwitchChildren)
            {
                foreach (var tr in ObjectToSwitch.GetComponentsInChildren<UnityEngine.Transform>())
                {
                    if (!IgnoreChildrenOnDifferentLayer || tr.gameObject.layer == previousLayer)
                        tr.gameObject.layer = Layer;
                }
            }

            if (DebugLog)
                UnityEngine.Debug.LogFormat(this, "Switched layer {0} to layer {1} on object {2}",
                    LayerMask.LayerToName(previousLayer), LayerMask.LayerToName(Layer), name);
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public void RevertSwitch()
        {
            ObjectToSwitch.layer = InitialLayer;

            if (SwitchChildren)
            {
                foreach (var tr in ObjectToSwitch.GetComponentsInChildren<UnityEngine.Transform>())
                {
                    if (!IgnoreChildrenOnDifferentLayer || tr.gameObject.layer == Layer)
                        tr.gameObject.layer = InitialLayer;
                }
            }

            if (DebugLog)
                UnityEngine.Debug.LogFormat(this, "Reverted switch layer {0} to layer {1} on object {2}",
                    LayerMask.LayerToName(Layer), LayerMask.LayerToName(InitialLayer), name);
        }

        public void ToggleSwitch()
        {
            if (ObjectToSwitch.layer == InitialLayer)
                SwitchLayer();
            else
            {
                RevertSwitch();
            }
        }

        private void Awake()
        {
            previousLayer = ObjectToSwitch.layer;
        }

        public void OnEnable()
        {
            if (SwitchOnEnable)
                SwitchLayer();
        }

        public void OnDisable()
        {
            if (RevertSwitchOnDisable)
                RevertSwitch();
        }
    }
}
