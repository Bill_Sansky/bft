﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class ForceApplier : MonoBehaviour
    {
        [BoxGroup("Init")] public bool ApplyForceFrequently;

        [BoxGroup("Init")] public bool ApplyForceOnEnable;


        [BoxGroup("References")] public Rigidbody Body;

        [BoxGroup("References")]
        public Vector3Value Force;

        public float ForceApplyingDuration = 0;

        [BoxGroup("Frequency"), ShowIf("ApplyForceFrequently")]
        public float maxApplyFrequency = 1;

        [BoxGroup("Frequency"), ShowIf("ApplyForceFrequently")]
        public float minApplyFrequency = 1;


        void Reset()
        {
            Body = GetComponent<Rigidbody>();
        }

     

        public void OnEnable()
        {
            if (ApplyForceOnEnable)
                ApplyForce();
            if (ApplyForceFrequently)
                StartToApplyForceOverTime();
        }

        private Coroutine StartToApplyForceOverTime()
        {
            return StartCoroutine(ApplyForceOverTime());
        }

        public void ApplyForce()
        {
            Body.AddForce(Force.Value);
        }

        public void ApplyForceWithDelta()
        {
            Body.AddForce(Force.Value * UnityEngine.Time.deltaTime);
        }

        public IEnumerator ApplyForceOverTime()
        {
            while (true)
            {
                if (ForceApplyingDuration > 0)
                {
                    float currentDuration = 0;
                    while (currentDuration <= ForceApplyingDuration)
                    {
                        currentDuration += UnityEngine.Time.deltaTime;
                        ApplyForceWithDelta();

                        yield return null;
                    }
                }
                else
                {
                    ApplyForce();
                }

                yield return new WaitForSeconds(Random.Range(minApplyFrequency, maxApplyFrequency));
            }
        }
    }
}
