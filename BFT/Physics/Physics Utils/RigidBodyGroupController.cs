﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class RigidBodyGroupController : MonoBehaviour
    {
        [BoxGroup("Options")] public bool ReparentRigidBodies = true;

        [BoxGroup("Status")] public List<Rigidbody> RigidBodies = new List<Rigidbody>();

        public void AddRigidbody(Collider col)
        {
            AddRigidbody(col.attachedRigidbody);
        }

        public void RemoveRigidBody(Collider col)
        {
            RemoveRigidBody(col.attachedRigidbody);
            if (ReparentRigidBodies)
                col.attachedRigidbody.transform.SetParent(null);
        }

        public void AddRigidbody(Rigidbody body)
        {
            if (RigidBodies.Contains(body))
                return;
            RigidBodies.Add(body);
            if (ReparentRigidBodies)
                body.transform.SetParent(transform);
        }

        public void RemoveRigidBody(Rigidbody body)
        {
            RigidBodies.Remove(body);
        }

        public void SetAllBodiesKinematic()
        {
            foreach (var rigidBody in RigidBodies)
            {
                rigidBody.isKinematic = true;
            }
        }

        public void SetAllBodiesDynamic()
        {
            foreach (var rigidBody in RigidBodies)
            {
                rigidBody.isKinematic = false;
            }
        }
    }
}
