﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class RigidBodyKinematicChangeOnTrigger : MonoBehaviour
    {
        public Rigidbody Body;

        public Collider[] CollidersToIgnore;

        private List<Collider> detectedCollisions = new List<Collider>();
        public bool DisableKinematicAfterSomeTime = true;

        [ShowIf("DisableKinematicAfterSomeTime")]
        public float DisableTime = 0.5f;

        public bool SetDynamicOnDisable = true;
        public bool SetKinematicOnCollision = true;
        private WaitForSeconds waitTime;

        void Reset()
        {
            Body = GetComponent<Rigidbody>();
        }

        void Awake()
        {
            waitTime = new WaitForSeconds(DisableTime);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (CollidersToIgnore.Contains(other))
                return;
            detectedCollisions.Add(other);
            Body.isKinematic = SetKinematicOnCollision;
        }

        private void OnTriggerExit(Collider other)
        {
            detectedCollisions.Remove(other);
            if (detectedCollisions.Count == 0)
            {
                if (DisableKinematicAfterSomeTime)
                {
                    StartCoroutine(DisableKinematicAfterTime());
                }
                else
                    Body.isKinematic = !SetKinematicOnCollision;
            }
        }


        private IEnumerator DisableKinematicAfterTime()
        {
            yield return waitTime;
            if (detectedCollisions.Count == 0)
                Body.isKinematic = !SetKinematicOnCollision;
        }


        private void OnDisable()
        {
            if (SetDynamicOnDisable)
                Body.isKinematic = false;
        }
    }
}
