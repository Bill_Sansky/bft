using UnityEngine;

namespace BFT
{
    [RequireComponent(typeof(Rigidbody))]
    public class RigidbodySleepThreshold : MonoBehaviour
    {
        public bool SetThresholdOnEnable;
        public float SleepThreshold;

        private void OnEnable()
        {
            if (SetThresholdOnEnable)
                SetSleepThreshold();
        }

        public void SetSleepThreshold()
        {
            GetComponent<Rigidbody>().sleepThreshold = SleepThreshold;
        }
    }
}
