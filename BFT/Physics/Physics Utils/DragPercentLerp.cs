﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class DragPercentLerp : MonoBehaviour
    {
        public PercentValue AngularDragPercent;
        public Rigidbody Body;

        public PercentValue DragPercent;
        public float MaxAngularDrag;
        public float MaxDrag;

        public float MinAngularDrag;

        public float MinDrag;

        void FixedUpdate()
        {
            if (!Body)
                return;

            Body.drag = Mathf.Lerp(MinDrag, MaxDrag, DragPercent.Value);
            Body.angularDrag = Mathf.Lerp(MinAngularDrag, MaxAngularDrag, AngularDragPercent.Value);
        }
    }
}
