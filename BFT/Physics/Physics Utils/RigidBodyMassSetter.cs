using System;
using UnityEngine;

namespace BFT
{
    public class RigidBodyMassSetter : MonoBehaviour,IValue<float>
    {
        public Rigidbody Body;

        public FloatValue MassValue;

        public bool SetMassOnEnable = true;

        private void Reset()
        {
            Body = GetComponent<Rigidbody>();
        }

        public void OnEnable()
        {
            if (SetMassOnEnable)
                SetMass();
        }

        public void SetMass()
        {
            Body.mass = MassValue.Value;
        }

        public float Value => Body.mass;
    }
}