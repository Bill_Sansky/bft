﻿using UnityEngine;

namespace BFT
{
    public class RigidBodyVelocityOverrider : MonoBehaviour
    {
        public Rigidbody Body;
        public bool CountDelta;
        public Vector3Value Speed;

        public bool OverrideSpeedOnFixedUpdate;

        // Update is called once per frame
        void FixedUpdate()
        {
            if (OverrideSpeedOnFixedUpdate)
                OverrideSpeed();
        }

        public void OverrideSpeed()
        {
            if (CountDelta)
            {
                Body.velocity = Speed.Value / UnityEngine.Time.fixedDeltaTime;
            }
            else
                Body.velocity = Speed.Value;
        }
    }
}