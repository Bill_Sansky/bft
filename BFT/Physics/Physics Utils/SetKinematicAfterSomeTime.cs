﻿using System.Collections;
using UnityEngine;

namespace BFT
{
    public class SetKinematicAfterSomeTime : MonoBehaviour
    {
        public Rigidbody Body;
        public float Time = 1;

        public void SetKinematicAfterTime()
        {
            StartCoroutine(KinematiAfterTime());
        }

        private IEnumerator KinematiAfterTime()
        {
            yield return new WaitForSeconds(Time);
            Body.isKinematic = true;
        }
    }
}
