﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [CreateAssetMenu(menuName = "BFT/Profiles/Interpolation Profile", fileName = "Interpolation Profile")]
    public class InterpolationProfile : ScriptableObject
    {
        [LabelText("$LeftInValueLabel")] public float LeftInValue = 0;

        [LabelText("$LeftOutValueLabel")] public float LeftOutValue = 0;

        public AnimationCurve Profile = AnimationCurve.EaseInOut(0, 0, 1, 1);

        [LabelText("$RightInValueLabel")] public float RightInValue = 10;

        [LabelText("$RightOutValueLabel")] public float RightOutValue = 10;

        protected virtual string LeftInValueLabel => "LeftInValue";
        protected virtual string RightInValueLabel => "RightInValue";
        protected virtual string LeftOutValueLabel => "LeftOutValue";
        protected virtual string RightOutValueLabel => "RightOutValue";

        public float Evaluate(float valueIn)
        {
            return MathExt.EvaluateCurve(LeftOutValue, RightOutValue,
                Profile, valueIn, LeftInValue, RightInValue);
        }
    }
}
