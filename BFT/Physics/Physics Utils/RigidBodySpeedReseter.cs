﻿using System.Collections;
using UnityEngine;

namespace BFT
{
    public class RigidBodySpeedReseter : MonoBehaviour
    {
        public Rigidbody Body;

        private Vector3 capturedSpeed;
        private Vector3 lastPosition;

        void Reset()
        {
            Body = GetComponent<Rigidbody>();
        }

        public void StartToRecordSpeed()
        {
            StartCoroutine(RecordSpeed());
        }

        private IEnumerator RecordSpeed()
        {
            lastPosition = Body.transform.position;
            while (true)
            {
                capturedSpeed = (Body.transform.position - lastPosition) / UnityEngine.Time.deltaTime;
                lastPosition = Body.transform.position;
                yield return new WaitForEndOfFrame();
            }
        }

        public void RestoreSpeed()
        {
            RestoreSpeed(true);
        }

        public void RestoreSpeed(bool stopCapturing)
        {
            Body.velocity = capturedSpeed;
            if (stopCapturing)
                StopAllCoroutines();
        }
    }
}
