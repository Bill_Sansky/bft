﻿using UnityEngine;

namespace BFT
{
    public class TransformBodyVelocityToPositionCopy : UpdateTypeSwitchable
    {
        public Rigidbody Body;
        public bool LocalPosition = true;


        // Update is called once per frame
        public override void UpdateMethod()
        {
            if (LocalPosition)
            {
                transform.localPosition = Body.velocity;
            }
            else
            {
                transform.position = Body.velocity;
            }
        }
    }
}
