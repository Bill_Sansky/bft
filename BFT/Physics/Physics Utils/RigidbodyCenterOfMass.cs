﻿using System;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
#endif

namespace BFT
{
    public class RigidbodyCenterOfMass : MonoBehaviour
    {
        public Rigidbody Body;

        public Vector3 CenterOfMassPosition;

        [ShowInInspector] private bool editionMode;

        public bool ShowDebug;

        void Reset()
        {
            Body = GetComponent<Rigidbody>();
            CenterOfMassPosition = Body.centerOfMass;
        }

#if UNITY_EDITOR
        public void OnDrawGizmosSelected()
        {
            if (ShowDebug)
            {
                Gizmos.DrawWireSphere(Body.worldCenterOfMass, 0.1f);
                Handles.Label(Body.worldCenterOfMass, "Center Of Mass");
            }

            if (editionMode)
            {
                Handles.matrix = Body.transform.localToWorldMatrix;
                CenterOfMassPosition = Handles.DoPositionHandle(CenterOfMassPosition, Quaternion.identity);
                Handles.matrix = Matrix4x4.identity;
            }
        }
#endif
    }
}