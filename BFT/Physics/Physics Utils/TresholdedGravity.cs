﻿using UnityEngine;

namespace BFT
{
    public class TresholdedGravity : MonoBehaviour
    {
        public Rigidbody Body;
        public float TresholdHeight;


        // Update is called once per frame
        void FixedUpdate()
        {
            if (transform.position.y > TresholdHeight)
                Body.AddForce(UnityEngine.Physics.gravity);
        }
    }
}
