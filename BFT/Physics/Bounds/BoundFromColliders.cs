﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class BoundFromColliders : MonoBehaviour, IValue<Vector3>, IValue<UnityEngine.Bounds>
    {
        public float BoundCalculationMultiplier = 1;

        public bool CalculateOnAwake = false;
        public List<Collider> Colliders;
        public UnityEngine.Bounds CollidersBounds;
        [ShowIf("CalculateOnAwake")] public bool GetCollidersOnAwake = false;
        private UnityEngine.Bounds value;

        [ShowInInspector] public float MaxBoundDimension => CollidersBounds.size.MaxComponent();

        public Vector3 BoundDimension => CollidersBounds.extents;

        UnityEngine.Bounds IValue<UnityEngine.Bounds>.Value => CollidersBounds;

        public Vector3 Value => BoundDimension;

        void Awake()
        {
            if (CalculateOnAwake)
            {
                CalculateBounds();
            }
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void GetCollidersAndCalculateBound()
        {
            UnityEngine.Transform reference = transform.parent ? transform.parent : transform;
            Colliders = reference.GetComponentsInChildren<Collider>().ToList();
            CalculateBounds();
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void CalculateBounds()
        {
            CollidersBounds = new UnityEngine.Bounds(MathExt.GravityCenter(Colliders.Convert(_ => _.transform.position))
                , Vector3.zero);
            foreach (var col in Colliders)
            {
                CollidersBounds.Encapsulate(col.bounds);
            }

            CollidersBounds = new UnityEngine.Bounds(transform.InverseTransformPoint(CollidersBounds.center),
                transform.InverseTransformVector(CollidersBounds.size * BoundCalculationMultiplier));
        }

        /// <summary>
        ///     This method is only here to deal with the fact that odin stopped to serialize for nested prefabs
        /// </summary>
        /// <param name="variable"></param>
        public void AssignMaxBoundToVariable(FloatVariable variable)
        {
            variable.Value = MaxBoundDimension;
        }

        public void AssignBoundToVariable(Vector3VariableAsset variable)
        {
            variable.Value = BoundDimension;
        }

        public void AssignFirstColliderTransformToVariable(TransformVariableAsset variable)
        {
            UnityEngine.Debug.Assert(Colliders.Count > 0, "No Collider was defined but you are trying to sue the first one", this);
            variable.Value = Colliders[0].transform;
        }

        public void OnDrawGizmosSelected()
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = Color.green;
            Gizmos.DrawWireCube(CollidersBounds.center, CollidersBounds.size);
        }
    }
}
