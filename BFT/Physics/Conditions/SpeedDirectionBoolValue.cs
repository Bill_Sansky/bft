﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class SpeedDirectionBoolValue : MonoBehaviour, IValue<bool>
    {
        public AxisBFT Axis;

        public Rigidbody Body;

        [ShowInInspector, ReadOnly, LabelText("Current State")]
        private bool currentlyPositiveDirection;

        public bool DefaultValue = true;

        private bool stateIsInit;
        public bool TrueOnPositive = true;
        public float VelocityMagnitudeThresholdForChange;

        public bool Value
        {
            get
            {
                float dot = Vector3.Dot(MathExt.Axis(Axis), Body.velocity);

                if (Mathf.Abs(dot) > VelocityMagnitudeThresholdForChange)
                {
                    if (stateIsInit)
                    {
                        currentlyPositiveDirection = (TrueOnPositive) ? dot > 0 : dot < 0;
                        stateIsInit = false;
                    }
                    else
                    {
                        currentlyPositiveDirection = !currentlyPositiveDirection;
                    }
                }

                return currentlyPositiveDirection;
            }
        }

        void Awake()
        {
            stateIsInit = true;
            currentlyPositiveDirection = DefaultValue;
        }
    }
}
