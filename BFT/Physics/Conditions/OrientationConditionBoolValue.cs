﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class OrientationConditionBoolValue : MonoBehaviour, IValue<bool>
    {
        public AxisBFT Axis;
        public Rigidbody Body;

        [ShowInInspector, ReadOnly, LabelText("Current State")]
        private bool currentlyPositiveDirection;

        public bool DefaultValue = true;
        public bool TrueOnPositive = true;
        public float VelocityMagnitudeThresholdForChange;

        public bool Value
        {
            get
            {
                float dot = Vector3.Dot(MathExt.Axis(Axis), Body.transform.right);

                if (Mathf.Abs(dot) > VelocityMagnitudeThresholdForChange)
                {
                    currentlyPositiveDirection = (TrueOnPositive) ? dot > 0 : dot < 0;
                }

                return currentlyPositiveDirection;
            }
        }

        void Awake()
        {
            currentlyPositiveDirection = DefaultValue;
        }
    }
}
