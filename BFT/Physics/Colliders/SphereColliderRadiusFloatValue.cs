using System;
using UnityEngine;

namespace BFT
{
    public class SphereColliderRadiusFloatValue : MonoBehaviour, IValue<float>
    {
        public SphereCollider SphereCollider;

        public float Value => SphereCollider.radius;

        private void Reset()
        {
            SphereCollider = GetComponent<SphereCollider>();
        }
    }
}