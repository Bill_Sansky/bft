using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class SphereColliderRadiusSetter : MonoBehaviour
    {
        public SphereCollider SphereCollider;
        public FloatValue SphereRadius;

        public bool SetRadiusOnEnable;

        public void OnEnable()
        {
            if (SetRadiusOnEnable)
                SetRadius();
        }

        [Button(ButtonSizes.Medium)]
        public void SetRadius()
        {
            SphereCollider.radius = SphereRadius.Value;
        }
    }
}