﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public abstract class ReferencedColliderTrigger<T> : SerializedMonoBehaviour
    {
        public ColliderDictionary<T> Dictionary;

        public bool IgnoreLastDetected;

        private Collider lastDetectedCollider;

        public UnityEvent<T> OnTriggerEnterEvent;
        public UnityEvent<T> OnTriggerExitEvent;

        private void OnTriggerEnter(Collider other)
        {
            if (IgnoreLastDetected && other == lastDetectedCollider)
                return;

            if (Dictionary.ObjectsByCollider.ContainsKey(other))
            {
                lastDetectedCollider = other;
                OnTriggerEnterEvent.Invoke(Dictionary[other]);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (Dictionary.ObjectsByCollider.ContainsKey(other))
            {
                OnTriggerExitEvent.Invoke(Dictionary[other]);

                if (lastDetectedCollider == other)
                    lastDetectedCollider = null;
            }
        }
    }
}