using UnityEngine;

namespace BFT
{
    public class BoxColliderDimensionCopier : MonoBehaviour
    {
        public bool CopyOnEnable;
        public BoxCollider ToCopy;
        public BoxCollider ToPasteOn;

        public void Reset()
        {
            ToPasteOn = GetComponent<BoxCollider>();
        }

        public void OnEnable()
        {
            if (CopyOnEnable)
                Copy();
        }


        public void Copy()
        {
            ToPasteOn.center = ToCopy.center;
            ToPasteOn.size = ToCopy.size;
        }

        public void OnDrawGizmos()
        {
            if (ToCopy && ToPasteOn)
                Copy();
        }
    }
}
