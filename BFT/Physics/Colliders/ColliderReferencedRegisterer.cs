﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public abstract class ColliderReferencedRegisterer<T> : SerializedMonoBehaviour
    {
        [InfoBox(
            "An object that can automatically add a collider referenced object to a container in order to register it")]
        public IContainer<T> DataContainer;

        public ColliderDictionary<T> Dictionary;

        public bool LogDebug;

        public void AddData(Collider collider)
        {
            if (!Dictionary.ObjectsByCollider.ContainsKey(collider))
            {
                if (LogDebug)
                {
                    UnityEngine.Debug.LogWarningFormat(this, "The collider {0} was not part of the dictionary, ignored", collider.name);
                    UnityEngine.Debug.LogWarningFormat(collider, "Collider Ping");
                }

                return;
            }

            if (LogDebug)
            {
                UnityEngine.Debug.Log("Adding Collider " + collider.name, this);
            }

            DataContainer.Add(Dictionary[collider]);
        }

        public void Remove(Collider collider)
        {
            if (!Dictionary.ObjectsByCollider.ContainsKey(collider))
            {
                if (LogDebug)
                {
                    UnityEngine.Debug.LogWarningFormat(this, "The collider {0} was not part of the dictionary, ignored", collider.name);
                    UnityEngine.Debug.LogWarningFormat(collider, "Collider Ping");
                }

                return;
            }

            if (LogDebug)
            {
                UnityEngine.Debug.Log("Removing Collider " + collider.name, this);
            }

            DataContainer.Remove(Dictionary[collider]);
        }
    }
}
