﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    /// <summary>
    ///     An object that is referenced by a collider to be found faster
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ColliderReference<T> : SerializedMonoBehaviour
    {
        public Collider Collider;
        public T Data;

        [InfoBox(
            "A component that automatically registers an object linked to a collider to be easily recognized by triggers")]
        public ColliderDictionary<T> Dictionary;

        public virtual void Awake()
        {
            Dictionary.RegisterObject(Collider, Data);
        }

        void OnDestroy()
        {
            Dictionary.UnRegisterObject(Collider);
        }
    }
}
