﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public abstract class ColliderDictionary<T> : SerializedScriptableObject
    {
        public Dictionary<Collider, T> ObjectsByCollider = new Dictionary<Collider, T>();

        public T this[Collider index]
        {
            get
            {
                if (!ObjectsByCollider.ContainsKey(index))
                {
                    UnityEngine.Debug.LogErrorFormat(this,
                        "the collider {0} is not present in the dicitonary: make sure it was properly added", index.name);
                    UnityEngine.Debug.LogError("Collider ping", index);
                    return default(T);
                }

                return ObjectsByCollider[index];
            }
        }

        public void RegisterObject(Collider col, T obj)
        {
            if (!ObjectsByCollider.ContainsKey(col))
                ObjectsByCollider.Add(col, obj);
        }

        public void UnRegisterObject(Collider col)
        {
            ObjectsByCollider.Remove(col);
        }
    }
}
