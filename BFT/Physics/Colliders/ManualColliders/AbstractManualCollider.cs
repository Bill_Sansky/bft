﻿using UnityEngine;

namespace BFT
{
    public abstract class AbstractManualCollider : MonoBehaviour
    {
        public delegate void OnCollisionDelegate(Collider collider);

        public enum ColliderType
        {
            Box,
            Sphere,
            Capsule
        }

        public bool AutoUpdate = true;

        public LayerMask Mask;
        public abstract ColliderType GetColliderType { get; }
        public UnityEngine.Transform ColliderTransform { private set; get; }
        public event OnCollisionDelegate OnCollision;

        protected void ManualOnCollision(Collider collider)
        {
            OnCollisionFunction(collider);
            if (OnCollision != null)
            {
                OnCollision.Invoke(collider);
            }
        }

        private void Awake()
        {
            ColliderTransform = transform;
            Initialize();
        }

        private void Update()
        {
            if (!AutoUpdate)
                return;
            SearchForCollision();
        }

        protected virtual void Initialize()
        {
        }

        protected virtual void OnCollisionFunction(Collider collider)
        {
        }

        public abstract void SearchForCollision();
    }
}
