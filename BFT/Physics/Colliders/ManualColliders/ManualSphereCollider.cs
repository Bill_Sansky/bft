﻿using UnityEngine;

namespace BFT
{
    public class ManualSphereCollider : AbstractManualCollider
    {
        public Vector3 Center;
        public float Radius = 0.1f;

        public override ColliderType GetColliderType => ColliderType.Sphere;

        public override void SearchForCollision()
        {
            foreach (var c in UnityEngine.Physics.OverlapSphere(
                ColliderTransform.position + (Quaternion.LookRotation(ColliderTransform.forward) * Center), Radius, Mask))
            {
                ManualOnCollision(c);
            }
        }
    }
}
