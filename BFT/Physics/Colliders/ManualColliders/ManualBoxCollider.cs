﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class ManualBoxCollider : AbstractManualCollider, IBoxProperties
    {
        [SerializeField, ShowInInspector] private Vector3 center, halfSize;

        public override ColliderType GetColliderType => ColliderType.Box;

        public override void SearchForCollision()
        {
            Vector3 position = ColliderTransform.position + (Quaternion.LookRotation(ColliderTransform.forward) * Center);

            foreach (var c in UnityEngine.Physics.OverlapBox(position, halfSize, ColliderTransform.rotation, Mask))
            {
                ManualOnCollision(c);
            }
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            Vector3 position = ColliderTransform.position + (Quaternion.LookRotation(ColliderTransform.forward) * Center);

            Matrix4x4 cubeTransform = Matrix4x4.TRS(position, ColliderTransform.rotation, HalfSize * 2f);
            Matrix4x4 oldGizmosMatrix = Gizmos.matrix;

            Gizmos.matrix *= cubeTransform;
            Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
            Gizmos.matrix = oldGizmosMatrix;
        }
#endif

        #region IBox 

        public Vector3 Center
        {
            get => center;
            set => center = value;
        }

        public Vector3 HalfSize
        {
            get => halfSize;
            set => halfSize = value;
        }

        #endregion
    }
}
