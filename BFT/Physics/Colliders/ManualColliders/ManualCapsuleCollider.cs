﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class ManualCapsuleCollider : AbstractManualCollider, ICapsuleProperties
    {
        [SerializeField, ShowInInspector] private Vector3 center;

        [SerializeField, ShowInInspector] private float height = 1;

        [SerializeField, ShowInInspector] private float radius = 0.1f;

        public override ColliderType GetColliderType => ColliderType.Capsule;

        public override void SearchForCollision()
        {
            Vector3 p0, p1;
            p0 = p1 = ColliderTransform.position + (Quaternion.LookRotation(ColliderTransform.forward) * Center);
            float heightDiff = Height / 2f - Radius;
            p0.y += heightDiff;
            p1.y -= heightDiff;

            foreach (var c in UnityEngine.Physics.OverlapCapsule(p0, p1, Radius, Mask))
            {
                ManualOnCollision(c);
            }
        }

#if UNITY_EDITOR
        private void OnDrawGizmosSelected()
        {
            if (ColliderTransform != null)
            {
                UnityEditor.Handles.color = new Color(0, 1, 0, 0.5f);
                GizmosExt.DrawCapsule(
                    ColliderTransform.position + (Quaternion.LookRotation(ColliderTransform.forward) * Center), Height,
                    Radius, ColliderTransform.rotation * Quaternion.AngleAxis(90, Vector3.right));
            }
        }
#endif

        #region ICapsule

        public float Height
        {
            get => height;
            set => height = value;
        }

        public float Radius
        {
            get => radius;
            set => radius = value;
        }

        public Vector3 Center
        {
            get => center;
            set => center = value;
        }

        #endregion
    }
}
