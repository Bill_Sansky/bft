﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(Rigidbody))]
    [AddComponentMenu("Physics/Tools/Informed Rigidbody")]
    public class InformedRigidBody : SerializedMonoBehaviour
    {
        [ShowInInspector, SerializeField] private Rigidbody body;


        [Title("Debug")] [FoldoutGroup("Debug")]
        public bool DebugGizmos;

        [ShowIf("DebugGizmos")] [FoldoutGroup("Debug")]
        public Color OrientationDebugForwardColor = Color.blue;

        [ShowIf("DebugGizmos")] [FoldoutGroup("Debug")]
        public Color OrientationDebugUpColor = Color.green;

        [ShowIf("DebugGizmos")] [FoldoutGroup("Debug")]
        public float OrientationScale = 1f;

        [ShowIf("DebugGizmos")] [FoldoutGroup("Debug")]
        public Color VelocityColor = Color.cyan;

        [ShowIf("DebugGizmos")] [FoldoutGroup("Debug")]
        public float VelocityDebugThreshold = 1f;

        [ShowIf("DebugGizmos")] [FoldoutGroup("Debug")]
        public float VelocityScale = 0.01f;

        public Rigidbody Body
        {
            get
            {
                if (!body)
                {
                    body = GetComponent<Rigidbody>();
                }

                return body;
            }
        }

        public Vector3 Velocity => body.velocity;

        public Vector3 Position => body.position;

        void Reset()
        {
            body = GetComponent<Rigidbody>();
        }


        public virtual void Awake()
        {
            if (!body)
                body = GetComponent<Rigidbody>();
        }

#if UNITY_EDITOR

        public virtual void OnDrawGizmos()
        {
            if (!DebugGizmos)
                return;


            if (!body)
            {
                body = GetComponent<Rigidbody>();
                if (!body)
                    return;
            }


            if (OrientationScale > 0)
            {
                Gizmos.color = OrientationDebugUpColor;
                Gizmos.matrix = body.transform.localToWorldMatrix;
                Gizmos.DrawLine(Vector3.zero, body.transform.up * OrientationScale);
                Gizmos.color = OrientationDebugForwardColor;
                Gizmos.DrawLine(Vector3.up, Vector3.up + body.transform.forward * OrientationScale);
            }

            if (VelocityScale > 0 && body.velocity.magnitude > VelocityDebugThreshold)
            {
                Gizmos.matrix = Matrix4x4.identity;
                Gizmos.color = VelocityColor;
                Gizmos.DrawLine(body.position, body.position + body.velocity * VelocityScale);
                HandleExt.Text(body.position + body.velocity * VelocityScale, "Velocity: " + body.velocity);
            }
        }
#endif
    }
}
