using Sirenix.OdinInspector;

namespace BFT
{
    public class SavedObjectVariableAsset : SerializedScriptableObject, ISavedObject
    {
        [OnValueChanged("CheckIfSaveable")] public UnityEngine.Object SavedObject;

        public bool UseSaveCache = true;
        [ShowInInspector,ReadOnly,BoxGroup("Status")]
        private object cacheSave;

        public object Save()
        {
            if (UseSaveCache && cacheSave != null)
            {
                var toReturn = cacheSave;
                cacheSave = null;
                return toReturn;
            }

            return (((ISavedObject) SavedObject)).Save();
        }

        public void Load(object saveFile)
        {
            if (UseSaveCache && !SavedObject)
            {
                cacheSave = saveFile;
                return;
            }

            ((ISavedObject) SavedObject).Load(saveFile);
        }

        private void CheckIfSaveable()
        {
            if (SavedObject is ISavedObject)
                return;
            UnityEngine.Debug.LogWarning("You are trying to link an object that is not a saved Object!", this);
            SavedObject = null;
        }

        public void AssignSavedObject(UnityEngine.Object save)
        {
            UnityEngine.Debug.Assert(save is ISavedObject, "The oject pasted is not a saved object and will generate and exception",
                this);

            SavedObject = save;

            if (UseSaveCache && cacheSave != null)
            {
                ((ISavedObject) SavedObject).Load(cacheSave);
                cacheSave = null;
            }
        }

        public void CacheSave(object save)
        {
            UnityEngine.Debug.Assert(UseSaveCache,
                "you are trying to save a cache file but you are not using the cache mode: this file will be ignored",
                this);

            cacheSave = save;
        }
    }
}
