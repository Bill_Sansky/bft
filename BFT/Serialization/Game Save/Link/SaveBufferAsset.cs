using System;
using Sirenix.OdinInspector;
using Sirenix.Serialization;

namespace BFT
{
    /// <summary>
    ///     caches a save in memory so it can be used and loaded later, and destroyed right after;
    /// </summary>
    public class SaveBufferAsset : SerializedScriptableObject, ISavedObject
    {
        [NonSerialized, OdinSerialize] public object CachedSave;

        public bool DestroyCachedSaveAfterSave = true;

        [ReadOnly] public bool ReadyToBeLoaded { get; private set; }

        public object Save()
        {
            object cache = CachedSave;
            if (DestroyCachedSaveAfterSave)
                CachedSave = null;
            return cache;
        }

        public void Load(object saveFile)
        {
            CachedSave = saveFile;
            ReadyToBeLoaded = true;
        }

        public bool IsReadyToBeLoaded()
        {
            return ReadyToBeLoaded;
        }

        public void UpdateCachedSave(object newCache)
        {
            CachedSave = newCache;
        }

        public object FetchCachedSave()
        {
            object cache = CachedSave;
            CachedSave = null;
            ReadyToBeLoaded = false;
            return cache;
        }

        public object PeakCachedSave()
        {
            return CachedSave;
        }
    }
}
