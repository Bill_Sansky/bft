﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace BFT
{
    public static class SavedObjectUtils
    {
        public static Dictionary<string, object> GetSavedData(object obj)
        {
            Type type = obj.GetType();

            FieldInfo[] infos = type.GetFields(BindingFlags.GetField
                                               | BindingFlags.GetProperty
                                               | BindingFlags.NonPublic
                                               | BindingFlags.Public
                                               | BindingFlags.Instance)
                .Where(_ => _.GetCustomAttributes(typeof(SavedValueAttribute), true).Length > 0)
                .ToArray();
            Dictionary<string, object> serializedObjects = new Dictionary<string, object>(infos.Length);

            foreach (var memberInfo in infos)
            {
                object memberObject = memberInfo.GetValue(obj);
                object outObject = memberObject;
                var asSavable = memberObject as ISavedObject;
                if (asSavable != null)
                {
                    //then get the save data from that thing instead of serializing the whole object
                    outObject = asSavable.Save();
                }

                serializedObjects.Add(memberInfo.Name, outObject);
            }

            return serializedObjects;
        }

        public static void LoadSaveData(object obj, Dictionary<string, object> data)
        {
            Type type = obj.GetType();

            FieldInfo[] infos = type.GetFields(BindingFlags.GetField
                                               | BindingFlags.GetProperty
                                               | BindingFlags.NonPublic
                                               | BindingFlags.Public
                                               | BindingFlags.Instance)
                .Where(_ => _.GetCustomAttributes(typeof(SavedValueAttribute), true).Length > 0)
                .ToArray();

            foreach (var memberInfo in infos)
            {
                object memberObject = memberInfo.GetValue(obj);

                var asSavable = memberObject as ISavedObject;
                if (memberObject != null && asSavable != null)
                {
                    asSavable.Load(data[memberInfo.Name]);
                }
                else
                {
                    memberInfo.SetValue(obj, data[memberInfo.Name]);
                }
            }
        }

        public static Dictionary<T1, List<object>> SaveSavedObjectListDictionary<T, T1>(Dictionary<T1, List<T>> listdic)
            where T : ISavedObject
        {
            return listdic.ConvertValues<T1, object, T>(_ => _.Save());
        }

        public static Dictionary<T1, List<T>> LoadSavedObjectListDictionary<T1, T>(object listDicObject)
            where T : ISavedObject, new()
        {
            var listDicSave = (Dictionary<T1, List<object>>) listDicObject;
            return listDicSave.ConvertValues(NewTWithSave);

            T NewTWithSave(object tSave)
            {
                var t = new T();
                t.Load(tSave);
                return t;
            }
        }

        public static object SaveSavedObjectDictionary<T>(Dictionary<int, T> dic) where T : ISavedObject
        {
            return dic.ConvertValues(_ => _.Save());
        }

        public static void LoadSavedObjectDictionary<T>(Dictionary<int, T> dic, object save) where T : ISavedObject
        {
            if (save == null)
            {
                foreach (var key in dic.Keys)
                {
                    dic[key].Load(null);
                }

                return;
            }

            var saveDic = (Dictionary<int, object>) save;
            foreach (var key in dic.Keys)
            {
                dic[key].Load(saveDic[key]);
            }
        }

        /// 
        public static SerializableDictionaryBase<T1, List<object>> SaveSavedObjectListDictionaryBase<T, T1>(
            SerializableDictionaryBase<T1, List<T>> listdic)
            where T : ISavedObject
        {
            return listdic.ConvertValues<T1, object, T>(_ => _.Save());
        }

        public static SerializableDictionaryBase<T1, List<T>> LoadSavedObjectListDictionaryBase<T1, T>(
            object listDicObject)
            where T : ISavedObject, new()
        {
            var listDicSave = (SerializableDictionaryBase<T1, List<object>>) listDicObject;
            return listDicSave.ConvertValues(NewTWithSave);

            T NewTWithSave(object tSave)
            {
                var t = new T();
                t.Load(tSave);
                return t;
            }
        }

        public static object SaveSavedObjectDictionaryBase<T>(SerializableDictionaryBase<int, T> dic)
            where T : ISavedObject
        {
            return dic.ConvertValues(_ => _.Save());
        }

        public static void LoadSavedObjectDictionaryBase<T>(SerializableDictionaryBase<int, T> dic, object save)
            where T : ISavedObject
        {
            if (save == null)
            {
                foreach (var key in dic.Keys)
                {
                    dic[key].Load(null);
                }

                return;
            }

            var saveDic = (SerializableDictionaryBase<int, object>) save;
            foreach (var key in dic.Keys)
            {
                dic[key].Load(saveDic[key]);
            }
        }

        public static object SaveSavedObjectList<T>(List<T> list) where T : ISavedObject
        {
            return list.Convert(_ => _.Save());
        }

        public static List<T> NewSavedObjectListFromSave<T>(object save) where T : ISavedObject, new()
        {
            List<object> saveList = (List<object>) save;
            return saveList.Convert(NewTWithSave).ToList();

            T NewTWithSave(object tSave)
            {
                var t = new T();
                t.Load(tSave);
                return t;
            }
        }

        public static object Save<T>(this List<T> list) where T : ISavedObject
        {
            return SaveSavedObjectList(list);
        }

        public static void Load<T>(this List<T> list, object save) where T : ISavedObject, new()
        {
            if (save == null)
            {
                foreach (var savedObject in list)
                {
                    savedObject.Load(null);
                }

                return;
            }

            var saveList = (List<object>) save;
            for (var i = 0; i < list.Count; i++)
            {
                var savedObject = list[i];
                savedObject.Load(saveList[i]);
            }
        }


        /// does not take care of null (default) save files
        public static void LoadParameters(object saveFile, params BFTAction<object>[] parametersLoadAction)
        {
            UnityEngine.Debug.Assert(saveFile != null,
                "The save file cannot be null: you must handle your default loading before calling this function");

            var array = ((object[]) saveFile);

            UnityEngine.Debug.Assert(array.Length == parametersLoadAction.Length,
                "Theere are not a similar amount of parameters and save objects, the loading will generate an exception");

            for (var index = 0; index < array.Length; index++)
            {
                var save = array[index];
                var param = parametersLoadAction[index];
                param.Invoke(save);
            }
        }

        public static object SaveParameters(params Func<object>[] parametersSaveAction)
        {
            return parametersSaveAction.Convert(_ => _.Invoke()).ToArray();
        }
    }
}