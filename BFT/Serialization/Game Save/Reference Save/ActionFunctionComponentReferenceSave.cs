using System;
using System.Diagnostics;
using Plugins.BFT.BFT.Referencing.Component.Delegates;
using Sirenix.OdinInspector;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace BFT
{
    [Serializable]
    public class ActionFunctionComponentReferenceSave : ISavedObject
    {

        [InfoBox(
            "The type of the getter and of the setter aren't the same: this will cause runtime errors when trying to use the save!",
            InfoMessageType.Error, "IsGoodType")]
        public ComponentFunction ReferenceGetter;

        public ComponentAction ReferenceSetter;

        [AssetSelector] public GameObjectSavedReferenceDictionary ReferenceDictionary;

        private bool IsGoodType => ReferenceGetter.Target && ReferenceSetter.Target &&
                                   ReferenceGetter.Target.GetType() != ReferenceSetter.Target.GetType();


        [Button(ButtonSizes.Medium)]
        private void FindSetterFromGetter()
        {
            ReferenceSetter.GenerateActionSetterFromFunctionGetter(ReferenceGetter);
        }

        public object Save()
        {
            var comp = ReferenceGetter.Invoke();
            if (!comp)
                return (-1, -1);
            var saveRef = comp.GetComponent<GameObjectSavedReference>();
            UnityEngine.Debug.Assert(saveRef,
                "The component reference you are trying to save does not have a game object reference save component on its game object, so it cannot be saved",
                comp);
            int compID = saveRef.GetComponentID(comp);


            return (saveRef.ID, compID);
        }

        private int componentIDToLoad;

        public void Load(object saveFile)
        {
            if (saveFile == null)
            {
                ReferenceSetter.Invoke(default);
                return;
            }

            (int, int) save = ((int, int)) saveFile;
            componentIDToLoad = save.Item2;

            if (componentIDToLoad == -1)
                return;

            if (ReferenceDictionary.GetLoadedReference(save.Item1, out var refFound, SetReferenceDelayed))
            {
                SetReferenceDelayed(refFound);
            }
        }

        private void SetReferenceDelayed(GameObjectSavedReference obj)
        {
            ReferenceSetter.Invoke(obj.GetComponent(componentIDToLoad));
            componentIDToLoad = -1;
        }
    }
}