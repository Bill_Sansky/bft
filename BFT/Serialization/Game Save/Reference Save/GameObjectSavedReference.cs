using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace BFT
{
    public class GameObjectSavedReference : MonoBehaviour, ISavedObject
    {
        [ShowInInspector,ReadOnly,BoxGroup("Status")]
        private int id = -1;
        
        public GameObjectSavedReferenceDictionary dictionary;

        public int ID => id;

        [SerializeField] private List<Component> ReferencedComponents = new List<Component>();

        public void Awake()
        {
            dictionary.AddReferenceSaveOnFirstAvailableID(this);
        }

        [Button(ButtonSizes.Medium)]
        public void ReferenceAllGameObjectComponents()
        {
            foreach (var component in GetComponents<Component>())
            {
                ReferencedComponents.Add(component);
            }
        }

        public int GetComponentID(Component comp)
        {
            Debug.Assert(ReferencedComponents.Contains(comp), $"The component {comp} is not registered!", this);
            return ReferencedComponents.IndexOf(comp);
        }

        public Component GetComponent(int componentID)
        {
            return ReferencedComponents[componentID];
        }

        public void Unbind()
        {
            id = -1;
        }
        
        public void UpdateID(int newID)
        {
            //TODO add assertion there to make sure this came from the dictionary
            Debug.Assert(
                dictionary.ContainsID(newID) && dictionary.GetAnyReferenceSave(newID) == this,
                "you are trying to update an id, but the dictionary did not get updated first: this is forbidden",
                this);
            id = newID;
        }


        public object Save()
        {
            return id;
        }

        public void Load(object saveFile)
        {
            if (id != -1)
                dictionary.RemoveReference(this);

            if (saveFile == null)
                id = -1;
            else
            {
                id = (int) saveFile;
            }

            if (id == -1)
            {
                dictionary.AddReferenceSaveOnFirstAvailableID(this);
            }
            else
            {
                dictionary.LoadReferenceSaveOnID(id, this);
            }
        }
    }
}