using System;
using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    public class ComponentReferencesSaver : MonoBehaviour, ISavedObject
    {
        public List<ActionFunctionComponentReferenceSave> References = new List<ActionFunctionComponentReferenceSave>();

        public object Save()
        {
            return References.Save();
        }

        public void Load(object saveFile)
        {
            References.Load(saveFile);
        }
    }
}