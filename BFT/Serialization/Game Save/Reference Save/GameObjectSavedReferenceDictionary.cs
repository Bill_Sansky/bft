using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class GameObjectSavedReferenceDictionary : ScriptableObject
    {
        [ShowInInspector, BoxGroup("Status"), NonSerialized]
        private Dictionary<int, GameObjectSavedReference> currentReferences =
            new Dictionary<int, GameObjectSavedReference>();

        private readonly HashSet<int> loadedIds = new HashSet<int>();

        public bool ContainsID(int id)
        {
            return currentReferences.ContainsKey(id);
        }

        [NonSerialized] private Dictionary<int, List<Action<GameObjectSavedReference>>> referenceRequests =
            new Dictionary<int, List<Action<GameObjectSavedReference>>>();

        public void Awake()
        {
            currentReferences.Clear();
            referenceRequests.Clear();
        }

        public void OnEnable()
        {
            currentReferences.RemoveValueWhere(_ => !_);
        }

        public void OnDisable()
        {
            currentReferences.Clear();
            referenceRequests.Clear();
        }

        public bool GetLoadedReference(int id, out GameObjectSavedReference referenceFound,
            Action<GameObjectSavedReference> actionOnReferenceLoaded)
        {
            if (!loadedIds.Contains(id))
            {
                if (!referenceRequests.ContainsKey(id))
                {
                    referenceRequests.Add(id, new List<Action<GameObjectSavedReference>>());
                }

                referenceRequests[id].Add(actionOnReferenceLoaded);
                referenceFound = null;
                return false;
            }

            referenceFound = currentReferences[id];
            return true;
        }


        /// <summary>
        /// Warning, this method is not suited for getting loaded references, since there is a chance that the object at that id is not available yet, or pointing to the wrrong object
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public GameObjectSavedReference GetAnyReferenceSave(int id)
        {
            Debug.Assert(currentReferences.ContainsKey(id), "The id was not found in the dictionary", this);
            return currentReferences[id];
        }

        public void AddReferenceSaveOnFirstAvailableID(GameObjectSavedReference save)
        {
            /*  Debug.Assert(currentReferences.Count == 0 || !currentReferences.ContainsValue(save),
                  $"The save is already present under a different id " /*({currentReferences.First(_ => _.Value == save).Key})
                  this);*/
            int newID = currentReferences.AddOnFirstAvailableID(save);
            save.UpdateID(newID);
        }

        public void LoadReferenceSaveOnID(int id, GameObjectSavedReference save)
        {
            if (currentReferences.ContainsKey(save.ID) && currentReferences[save.ID] == save)
            {
                currentReferences.Remove(save.ID);
            }

            if (currentReferences.ContainsKey(id))
            {
                AddReferenceSaveOnFirstAvailableID(currentReferences[id]);
            }

            currentReferences.AddOrReplace(id, save);
            save.UpdateID(id);
            loadedIds.Add(id);

            if (referenceRequests.ContainsKey(id))
            {
                foreach (var action in referenceRequests[id])
                {
                    action(currentReferences[id]);
                }

                referenceRequests.Remove(id);
            }
        }

        public void RemoveReference(GameObjectSavedReference gameObjectSavedReference)
        {
            currentReferences.Remove(gameObjectSavedReference.ID);
            loadedIds.Remove(gameObjectSavedReference.ID);

            gameObjectSavedReference.Unbind();
        }
    }
}