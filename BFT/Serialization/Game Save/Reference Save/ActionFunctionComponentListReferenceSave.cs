using System;
using System.Collections;
using System.Collections.Generic;
using Plugins.BFT.BFT.Referencing.Component.Delegates;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class ActionFunctionComponentListReferenceSave : ISavedObject
    {
        [InfoBox(
            "The type of the getter and of the setter aren't the same: this will cause runtime errors when trying to use the save!",
            InfoMessageType.Error, "IsGoodType")]
        public ComponentListFunction ReferenceGetter;

        public ComponentListAction ReferenceSetter;

        [AssetSelector] public GameObjectSavedReferenceDictionary ReferenceDictionary;

        private bool IsGoodType => ReferenceGetter.Target && ReferenceSetter.Target &&
                                   ReferenceGetter.Target.GetType() != ReferenceSetter.Target.GetType();


        [Button(ButtonSizes.Medium)]
        private void FindSetterFromGetter()
        {
            ReferenceSetter.GenerateActionSetterFromFunctionGetter(ReferenceGetter);
        }

        public object Save()
        {
            var comp = ReferenceGetter.Invoke();
            if (comp.Count == 0)
                return new (int, int)[0];

            var savearray = new (int, int)[comp.Count];
            for (var index = 0; index < comp.Count; index++)
            {
                var component = comp[index];
                var saveRef = component.GetComponent<GameObjectSavedReference>();
                UnityEngine.Debug.Assert(saveRef,
                    "The component reference you are trying to save does not have a game object reference save component on its game object, so it cannot be saved",
                    component);
                int compID = saveRef.GetComponentID(component);


                savearray[index] = (saveRef.ID, compID);
            }

            return savearray;
        }


        public void Load(object saveFile)
        {
            if (saveFile == null)
            {
                ReferenceSetter.Invoke(default);
                return;
            }

            save = ((int, int)[]) saveFile;
            components = new List<Component>(save.Length);
            lastIndex = 0;
            SetNextReference();
        }
        
        private int componentIDToLoad;
        private int lastIndex;
        private List<Component> components;
        private (int, int)[] save;

        private void SetNextReference()
        {
            if (lastIndex >= save.Length)
            {
                ReferenceSetter.Invoke(components);
                components = null;
                save = null;
                lastIndex = -1;
                return;
            }

            (int, int) saveTuple = save[lastIndex];
            if (ReferenceDictionary.GetLoadedReference(saveTuple.Item1, out var refFound, SetReferenceDelayed))
            {
                SetReferenceDelayed(refFound);
            }
        }

        private void SetReferenceDelayed(GameObjectSavedReference obj)
        {
            components.Add(obj.GetComponent(componentIDToLoad));
            componentIDToLoad = -1;
            lastIndex++;
            SetNextReference();
        }
    }
}