using UnityEngine;

namespace BFT
{
    public class GameObjectActiveStatusSave : MonoBehaviour, ISavedObject
    {
        public GameObjectValue GameObjectValue;

        public object Save()
        {
            return GameObjectValue.Value.activeSelf;
        }

        public void Load(object saveFile)
        {
            if (saveFile == null)
                return;
            
            GameObjectValue.Value.SetActive((bool) saveFile);
        }
    }
}