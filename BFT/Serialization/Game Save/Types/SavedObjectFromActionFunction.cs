using System;
using UnityEngine.Serialization;

namespace BFT
{
    [Serializable]
    public class SavedObjectFromActionFunction : ISavedObject
    {
        public ObjectAction LoadAction;
        public ObjectFunction SaveFunction;
        
        public object Save()
        {
            return SaveFunction.Invoke();
        }

        public void Load(object saveFile)
        {
            LoadAction.Invoke(saveFile);
        }
    }
}