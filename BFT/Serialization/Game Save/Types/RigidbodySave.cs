using UnityEngine;

namespace BFT
{
    public class RigidbodySave : MonoBehaviour, ISavedObject
    {
        public Rigidbody Rigidbody;

        public object Save()
        {
            return new object[]
            {
                Rigidbody.mass, Rigidbody.drag, Rigidbody.position, Rigidbody.rotation, Rigidbody.velocity,
                Rigidbody.angularDrag, Rigidbody.angularVelocity, Rigidbody.isKinematic
            };
        }

        public void Load(object saveFile)
        {
            if (saveFile == null)
                return;

            var objs = (object[]) saveFile;

            Rigidbody.mass = (float) objs[0];
            Rigidbody.drag = (float) objs[1];
            Rigidbody.position = (Vector3) objs[2];
            Rigidbody.rotation = (Quaternion) objs[3];
            Rigidbody.velocity = (Vector3) objs[4];
            Rigidbody.angularDrag = (float) objs[5];
            Rigidbody.angularVelocity = (Vector3) objs[6];
            Rigidbody.isKinematic = (bool) objs[7];
        }
    }
}