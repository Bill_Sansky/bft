﻿using UnityEngine;

namespace BFT
{
    public class TransformSave : MonoBehaviour, ISavedObject
    {
        public Transform transformToSave;

        public bool LocalTransform;

        public void Reset()
        {
            transformToSave = transform;
        }

        public object Save()
        {
            var state = new TransformSaveState() {Local = LocalTransform};
            state.RegisterState(transformToSave);
            return state;
        }

        public void Load(object saveFile)
        {
            TransformSaveState state = saveFile as TransformSaveState;
            if (state != null)
            {
                state.RestoreState(transformToSave);
            }
        }
    }
}