﻿#if UNITY_EDITOR
#endif
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using BFT.Generics;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;
using Debug = UnityEngine.Debug;

namespace BFT
{
    public class SaveToFileSaver : MonoBehaviour
    {
        [SerializeField, ReadOnly, FoldoutGroup("Status")]
        private BackgroundFileAccessThread backupFileAccess;

        [BoxGroup("Save")] public SavedInterfaceObjectReferenceReference CurrentSaveFile;


        [SerializeField, ReadOnly, FoldoutGroup("Status")]
        private BackgroundFileAccessThread fileAccess;

        [ReadOnly, FoldoutGroup("Status")] private bool isBusy;

        [BoxGroup("Load")] public bool LoadOnEnable;

        [BoxGroup("File")] [FilePath] public string LocalSaveDirectory = "Save/";

        [BoxGroup("Load")] public bool SafeLoading = true;
        [BoxGroup("File")] public string SaveExtension = ".save";

        [BoxGroup("File")] [SerializeField, ShowInInspector]
        private string saveFileName = "Save";

        [BoxGroup("Backup")] public bool BackupSave = true;

        [BoxGroup("Backup")] public string BackupSaveFileSuffix = "_backup";

        [BoxGroup("Backup")] [ShowIf("BackupSave")]
        public int TimeBetweenBackups = 5 * 60;

        public string SaveFileName
        {
            get => saveFileName;
            set
            {
                saveFileName = value;
                RefreshPaths();
            }
        }

        public bool IsBusy => isBusy;

        public string CurrentLocalSavePath => LocalSaveDirectory + SaveFileName + SaveExtension;

        public string CurrentBackupSavePath => CurrentLocalSavePath + BackupSaveFileSuffix;

        public bool IsSaveFileExisting => File.Exists(CurrentLocalSavePath);

        public UnityEvent OnSavingEnded;

        public UnityEvent OnSavingStarted;

        public UnityEvent OnLoadingStarted;

        [FormerlySerializedAs("OnSaveFileLoaded")]
        public UnityEvent OnLoadingEnded;

        public void Awake()
        {
            InitSaveManager();
        }

        public void OnEnable()
        {
            if (LoadOnEnable)
                Load();
        }

        private void RefreshPaths()
        {
            if (fileAccess != null)
            {
                fileAccess.Path = CurrentLocalSavePath;
            }

            if (backupFileAccess != null)
            {
                backupFileAccess.Path = CurrentBackupSavePath;
            }
        }

        private void InitSaveManager()
        {
            if (BackupSave)
                StartCoroutine(BackUpSaves());

            fileAccess = new BackgroundFileAccessThread(CurrentLocalSavePath);
            backupFileAccess = new BackgroundFileAccessThread(CurrentBackupSavePath);

            isBusy = false;
        }

        private IEnumerator BackUpSaves()
        {
            while (true)
            {
                yield return new WaitForSeconds(TimeBetweenBackups);
                while (!fileAccess.IsDone)
                    yield return new WaitForSeconds(1);
                SaveBackup();
            }
        }

        public void Save(bool threaded)
        {
#if UNITY_EDITOR
            if (EditorSave()) return;
#endif

            StartCoroutine(PerformTaskWhenNotBusy(fileAccess, false, threaded));
        }

        [Button(ButtonSizes.Medium)]
        public void Save()
        {
            Save(true);
        }

        public void SaveBackup(bool threaded)
        {
            {
#if UNITY_EDITOR
                if (EditorSaveBackup()) return;
#endif

                StartCoroutine(PerformTaskWhenNotBusy(backupFileAccess, false, threaded));
            }
        }

        [Button(ButtonSizes.Medium)]
        public void SaveBackup()
        {
            SaveBackup(true);
        }

        public void Load(bool threaded)
        {
#if UNITY_EDITOR
            if (EditorLoad()) return;
#endif

            StartCoroutine(PerformTaskWhenNotBusy(fileAccess, true, threaded));
            StartCoroutine(CheckLoad());
        }

        [Button(ButtonSizes.Medium)]
        public void Load()
        {
            Load(true);
        }

        private IEnumerator CheckLoad()
        {
            while (!fileAccess.IsDone)
            {
                yield return new WaitForSeconds(.2f);
            }

            if (SafeLoading)
            {
                try
                {
                    CurrentSaveFile.Value.Load(fileAccess.FecthAndDestroyDataContent());
                }
                catch (Exception e)
                {
                    Debug.LogWarning(
                        "The save file could not be read, most likely due to save file structure incompatibility");
                    
                    Debug.LogException(e);
                    
                    Debug.LogError(
                        "The default values will be loaded instead");
                    
                    CurrentSaveFile.Value.Load(null);
                }
            }
            else
            {
                CurrentSaveFile.Value.Load(fileAccess.FecthAndDestroyDataContent());
            }

            OnLoadingEnded.Invoke();
        }

        private IEnumerator PerformTaskWhenNotBusy(BackgroundFileAccessThread thread, bool load = false,
            bool threaded = true)
        {
            if (isBusy)
                yield break;

            isBusy = true;

            while (!thread.IsDone)
            {
                yield return new WaitForSeconds(.2f);
            }

            if (load)
            {
                OnLoadingStarted.Invoke();
                if (threaded)
                {
                    thread.Load();
                }
                else
                {
                    thread.NonThreadedLoad();
                }
            }
            else
            {
                object save = CurrentSaveFile.Value.Save();
                OnSavingStarted.Invoke();

                if (threaded)
                {
                    thread.Save(save);
                }
                else
                {
                    thread.DataContent = save;
                    thread.NonThreadedSave();
                }
            }

            while (!thread.IsDone)
            {
                yield return new WaitForSeconds(.2f);
            }

            isBusy = false;
            OnSavingEnded.Invoke();
        }

        #region EDITOR

#if UNITY_EDITOR

        [BoxGroup("Save Directory"), MultiLineProperty]
        public string FullSaveDirectory => Application.persistentDataPath + LocalSaveDirectory;

        [ShowInInspector, BoxGroup("Save Directory")]
        public bool IsDirectoryCreated => Directory.Exists(FullSaveDirectory);

        [BoxGroup("Save Directory"), Button(ButtonSizes.Medium)]
        public void OpenSaveDirectory()
        {
            if (IsDirectoryCreated)
                Process.Start(@Application.persistentDataPath + LocalSaveDirectory);
            else
            {
                Process.Start(@Application.persistentDataPath);
            }
        }

        private bool EditorSave()
        {
            if (!Application.isPlaying)
            {
                InitSaveManager();

                fileAccess.DataContent = CurrentSaveFile.Value.Save();
                fileAccess.NonThreadedSave();

                return true;
            }

            return false;
        }

        private bool EditorSaveBackup()
        {
            if (!Application.isPlaying)
            {
                InitSaveManager();
                backupFileAccess.DataContent = CurrentSaveFile.Value.Save();
                backupFileAccess.NonThreadedSave();

                return true;
            }

            return false;
        }

        private bool EditorLoad()
        {
            if (!Application.isPlaying)
            {
                InitSaveManager();
                fileAccess.NonThreadedLoad();

                CurrentSaveFile.Value.Load(fileAccess.DataContent);
                return true;
            }

            return false;
        }
#endif

        #endregion
    }
}