﻿#if UNITY_EDITOR
#endif
using System.Collections.Generic;
using System.Linq;
using BFT.Generics;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class SavedObjectListComponent : MonoBehaviour, ISavedObject
    {
        public List<SavedInterfaceObjectReferenceReference> SavableObjects = new List<SavedInterfaceObjectReferenceReference>();

        public UnityEvent OnBeforeSave;
        public UnityEvent OnAfterSave;

        public UnityEvent OnBeforeLoad;
        public UnityEvent OnAfterLoad;

        public object Save()
        {
            OnBeforeSave.Invoke();
            List<object> saveFile = new List<object>(SavableObjects.Count);
            foreach (var o in SavableObjects)
            {
                saveFile.Add(o.Value.Save());
            }

            OnAfterSave.Invoke();

            return saveFile;
        }

        public void Load(object saveFile)
        {
            OnBeforeLoad.Invoke();
            if (saveFile == null)
            {
                SavableObjects.ForEach(_ => _.Value.Load(null));
                OnAfterLoad.Invoke();
                return;
            }

            List<object> saveList = (List<object>) saveFile;

            UnityEngine.Debug.Assert(saveList.Count == SavableObjects.Count,
                $"The save object contains a different amount of elements ({saveList.Count}) than the list of objects to save ({SavableObjects.Count})!",
                this);

            for (int i = 0; i < saveList.Count; i++)
            {
                SavableObjects[i].Value.Load(saveList[i]);
            }

            OnAfterLoad.Invoke();
        }

        [FoldoutGroup("Tools"), Button(ButtonSizes.Medium)]
        public void FindSavableInSiblings()
        {
            IEnumerable<Component> comps;
            if (!transform.parent)
            {
                List<Component> components = new List<Component>();
                foreach (var rootGameObject in gameObject.scene.GetRootGameObjects())
                {
                    components.AddRange(rootGameObject.GetComponentsInChildren<Component>().Where(_ => _ != this));
                }

                comps = components;
            }
            else
            {
                comps = transform.parent.GetComponentsInChildren<Component>();
            }

            foreach (var component in comps)
            {
                if (component != this && component is ISavedObject sob &&
                    SavableObjects.All(_ => _.Value as Component != component))
                {
                    SavableObjects.Add(new SavedInterfaceObjectReferenceReference() {Reference = component});
                }
            }
        }

#if UNITY_EDITOR
        [FoldoutGroup("Tools"), ValueDropdown("Tags"), ShowInInspector]
        private string tagToLookFor;

        private ValueDropdownList<string> Tags => TagUtils.TagValueDropDown();

        [FoldoutGroup("Tools"), Button(ButtonSizes.Medium)]
        public void FindSavableInSiblingsWithTag()
        {
            IEnumerable<Component> comps;
            if (!transform.parent)
            {
                List<Component> components = new List<Component>();
                foreach (var rootGameObject in gameObject.scene.GetRootGameObjects())
                {
                    components.AddRange(rootGameObject.GetComponentsInChildren<Component>().Where(_ => _ != this));
                }

                comps = components;
            }
            else
            {
                comps = transform.parent.GetComponentsInChildren<Component>();
            }

            foreach (var component in comps)
            {
                if (component != this && component.gameObject.CompareTag(tagToLookFor) &&
                    component is ISavedObject sob &&
                    SavableObjects.All(_ => _.Value as Component != component))
                {
                    SavableObjects.Add(new SavedInterfaceObjectReferenceReference() {Reference = component});
                }
            }
        }
#endif
    }
}