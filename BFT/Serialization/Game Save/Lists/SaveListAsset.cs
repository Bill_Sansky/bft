using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;

namespace BFT
{
    public class SaveListAsset : SerializedScriptableObject, ISavedObject
    {
        public bool AddStringManifestToSave = true;
        public List<ISavedObject> SavableObjects = new List<ISavedObject>();

        public object Save()
        {
            List<object> saveFile = new List<object>(SavableObjects.Count);
            foreach (ISavedObject o in SavableObjects)
            {
                var asObj = (UnityEngine.Object) o;
                var save = AddStringManifestToSave && asObj ? (asObj.name, o.Save()) : o.Save();
                saveFile.Add(save);
            }

            return saveFile;
        }

        public void Load(object saveFile)
        {
            UnityEngine.Debug.Assert(SavableObjects.All(_ => _ != null),
                "Some of teh elements of the list are null, which will generate an exception", this);

            if (saveFile == null)
            {
                SavableObjects.ForEach(_ => _.Load(null));
                return;
            }

            List<object> saveList = (List<object>) saveFile;

            UnityEngine.Debug.Assert(saveList.Count == SavableObjects.Count,
                "The save object contains a different amount of elements than the list of objects to save!", this);

            for (int i = 0; i < saveList.Count; i++)
            {
                if (AddStringManifestToSave)
                {
                    var tuple = ((string, object)) saveList[i];
                    SavableObjects[i].Load(tuple.Item2);
                }
                else
                {
                    SavableObjects[i].Load(saveList[i]);
                }
            }
        }
    }
}
