﻿using UnityEngine.Events;

namespace BFT
{
    public interface ISavedObject
    {
        object Save();
        void Load(object saveFile);
    }
}
