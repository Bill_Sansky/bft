﻿namespace BFT
{
    public class SavableObjectDictionaryAsset : DictionaryAsset<ISavedObject, SavableObjectEntry>
    {
        public override SavableObjectEntry CreateNewDataHolder(object data)
        {
            return new SavableObjectEntry();
        }
    }
}
