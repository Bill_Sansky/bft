using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT.Generics
{
    public class SavedObjectFromActionFunctionComponent : MonoBehaviour, ISavedObject
    {
        public SavedObjectFromActionFunction SavedObject;

        public object Save()
        {
            return SavedObject.Save();
        }

        public void Load(object saveFile)
        {
            SavedObject.Load(saveFile);
        }
    }
}