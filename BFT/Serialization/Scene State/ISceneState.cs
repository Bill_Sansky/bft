﻿namespace BFT
{
    public interface ISaveState<in T>
    {
        void RestoreState(T reference);
        void RegisterState(T reference);
    }
}
