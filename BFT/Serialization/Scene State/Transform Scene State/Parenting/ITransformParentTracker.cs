﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    public interface ITransformParentTracker
    {
        IList<Component> TrackedObjects { get; }

        IList<UnityEngine.Transform> TrackedParents { get; }
    }
}
