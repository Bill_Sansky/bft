﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class ParentTransformSaveState : ISaveState<ITransformParentTracker>
    {
        public int[] ParentObjectsID;
        public TransformSaveState[] TransformState;

        public void RestoreState(ITransformParentTracker reference)
        {
            int i = 0;
            foreach (var o in reference.TrackedObjects)
            {
                RestoreState(i, reference.TrackedParents, o);
                i++;
            }
        }

        public void RegisterState(ITransformParentTracker reference)
        {
            RegisterState(reference.TrackedParents, reference.TrackedObjects);
        }

        public void RestoreState(int id, IEnumerable<UnityEngine.Transform> parents, Component component)
        {
            if (ParentObjectsID[id] != -1)
            {
                component.transform.SetParent(parents.ElementAt(ParentObjectsID[id]));
            }

            TransformState[id].RestoreState(component.transform);
        }

        public void RegisterState<T>(IList<UnityEngine.Transform> parent, IList<T> components) where T : Component
        {
            ParentObjectsID = new int[components.Count];
            TransformState = new TransformSaveState[components.Count];

            int i = 0;
            foreach (var o in components)
            {
                ParentObjectsID[i] = parent.IndexOf(o.transform.parent);
                TransformState[i] = new TransformSaveState();
                TransformState[i].RegisterState(o.transform);
                i++;
            }
        }
    }
}
