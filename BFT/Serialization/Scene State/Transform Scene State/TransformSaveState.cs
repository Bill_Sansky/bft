﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class TransformSaveState : ISaveState<UnityEngine.Transform>
    {
        public bool Local = false;
        [BoxGroup("Transform Values")] public Vector3 LocalScale = Vector3.one;

        [BoxGroup("Transform Values")] public Vector3 Position = Vector3.zero;
        [BoxGroup("Transform Values")] public Quaternion Rotation = Quaternion.identity;

        public TransformSaveState()
        {
            Local = true;
        }

        public TransformSaveState(UnityEngine.Transform transform, bool local = true)
        {
            Local = local;
            RegisterState(transform);
        }

        public TransformSaveState(TransformSaveState transformState)
        {
            Local = transformState.Local;
            RegisterState(transformState);
        }

        [Button(ButtonSizes.Medium)]
        public void RestoreState(UnityEngine.Transform reference)
        {
            if (!Local)
            {
                reference.position = Position;
                reference.rotation = Rotation;
                reference.localScale = LocalScale;
            }
            else
            {
                reference.localPosition = Position;
                reference.localRotation = Rotation;
                reference.localScale = LocalScale;
            }
        }

        [Button(ButtonSizes.Medium)]
        public void RegisterState(UnityEngine.Transform reference)
        {
            if (!Local)
            {
                Position = reference.position;
                Rotation = reference.rotation;
                LocalScale = reference.localScale;
            }
            else
            {
                Position = reference.localPosition;
                Rotation = reference.localRotation;
                LocalScale = reference.localScale;
            }
        }


        public void RegisterState(TransformSaveState reference)
        {
            Local = reference.Local;
            Position = reference.Position;
            Rotation = reference.Rotation;
            LocalScale = reference.LocalScale;
        }
    }
}
