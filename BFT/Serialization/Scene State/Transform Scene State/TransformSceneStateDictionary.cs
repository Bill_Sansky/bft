﻿namespace BFT
{
    public class TransformSceneStateDictionary :
        DictionaryAsset<TransformSaveState, TransformSceneStateDictionaryEntry>
    {
        public override TransformSceneStateDictionaryEntry CreateNewDataHolder(object data)
        {
            return new TransformSceneStateDictionaryEntry();
        }
    }
}
