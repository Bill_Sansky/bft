using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace BFT
{
    public class TransformSceneStateAsset : SerializedScriptableObject
    {
        [FormerlySerializedAs("TransformSceneState")]
        public TransformSaveState TransformSaveState;

        public Vector3 Position => TransformSaveState.Position;
        public Quaternion Rotation => TransformSaveState.Rotation;
        public Vector3 Scale => TransformSaveState.LocalScale;

        public void StoreTransform(UnityEngine.Transform transform)
        {
            TransformSaveState.RegisterState(transform);
        }
    }
}
