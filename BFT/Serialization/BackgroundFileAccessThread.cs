using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using Sirenix.Serialization;
using UnityEngine;
using ThreadPriority = System.Threading.ThreadPriority;

namespace BFT
{
    public class BackgroundFileAccessThread
    {
        public bool BinaryReadWrite = true;

        private object dataContent;

        private Thread fileAccessThread = null;
        private bool isDone = true;

        private string path;

        public bool UseOdin;

        public BackgroundFileAccessThread()
        {
            isDone = true;
        }

        public BackgroundFileAccessThread(string localPath)
        {
            Path = localPath;
            isDone = true;
        }

        public string Path
        {
            get => path;
            set => path = Application.persistentDataPath + value;
        }

        public bool IsDone
        {
            get => isDone;
            set => isDone = value;
        }

        public object DataContent
        {
            get => dataContent;
            set => dataContent = value;
        }

        public object FecthAndDestroyDataContent()
        {
            var obj = DataContent;
            DataContent = null;
            return obj;
        }

        public void Save(object obj)
        {
            if (!isDone)
                return;

            isDone = false;
            DataContent = obj;
            fileAccessThread = new Thread(NonThreadedSave) {Priority = ThreadPriority.Lowest, IsBackground = true};
            fileAccessThread.Start();
        }

        public void Load()
        {
            if (!isDone)
                return;

            isDone = false;

            fileAccessThread = new Thread(NonThreadedLoad) {Priority = ThreadPriority.Lowest, IsBackground = true};
            fileAccessThread.Start();
        }

        public void NonThreadedLoad()
        {
            if (BinaryReadWrite)
            {
                if (!UseOdin)
                {
                    if (!File.Exists(Path))
                    {
                        IsDone = true;
                        fileAccessThread?.Abort();
                        return;
                    }

                    FileStream file = File.Open(Path, FileMode.Open);
                    BinaryFormatter bf = new BinaryFormatter();
                    dataContent = bf.Deserialize(file);
                    file.Close();
                }
                else
                {
                    if (!File.Exists(Path))
                    {
                        IsDone = true;
                        fileAccessThread?.Abort();
                        return;
                    }

                    byte[] bytes = File.ReadAllBytes(Path);
                    dataContent = SerializationUtility.DeserializeValue<object>(bytes, DataFormat.Binary);
                }
            }
            else
            {
                dataContent = File.ReadAllText(Path);
            }

            IsDone = true;
            if (fileAccessThread != null)
                fileAccessThread.Abort();
        }


        public void NonThreadedSave()
        {
            string dPath = System.IO.Path.GetDirectoryName(Path);

            if (dPath != null && !Directory.Exists(dPath))
            {
                Directory.CreateDirectory(dPath);
            }

            if (BinaryReadWrite)
            {
                if (UseOdin)
                {
                    byte[] bytes = SerializationUtility.SerializeValue(DataContent, DataFormat.Binary);
                    File.WriteAllBytes(Path, bytes);
                }
                else
                {
                    BinaryFormatter bf = new BinaryFormatter();
                    FileStream file = File.Create(Path);
                    bf.Serialize(file, DataContent);
                    file.Close();
                }
            }
            else
            {
                File.WriteAllText(Path, DataContent.ToString());
            }

            DataContent = null;
            IsDone = true;

            if (fileAccessThread != null)
                fileAccessThread.Abort();
        }
    }
}