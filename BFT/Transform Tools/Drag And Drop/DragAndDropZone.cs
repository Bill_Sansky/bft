﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
#endif

namespace BFT
{
    /// <summary>
    ///     A single drop Position Zone
    /// </summary>
    public class DragAndDropZone : MonoBehaviour, IValue<DragAndDropZone>, ISavedObject
    {
        [FoldoutGroup("Tools"), OnValueChanged("ToggleDragInEditor")]
        public bool DragInEditor;

        [BoxGroup("Logic")] public AbstractDragReleaseLogic DragLogic;
        [BoxGroup("Logic")] public AbstractDragReleaseLogic DropLogic;

        [BoxGroup("Logic")] public bool DragOnEnable = true;
        [BoxGroup("Logic")] [ShowIf("IsDrag")] public bool AutoReleaseOnDragDone;


        [BoxGroup("Lock")] public BoolValue ExternalDragLock;
        [BoxGroup("Lock")] public BoolValue ExternalDropLock;

        [BoxGroup("Logic"), ShowIf("DragOnEnable")]
        public bool InstantDragOnEnable = true;

        private bool isInterrupted;

        [BoxGroup("Lock")] public bool LockDraggingWhenDropping = true;


        [BoxGroup("Lock")] public bool LockedOnTrue = true;

        [FoldoutGroup("Status"), SerializeField]
        private DraggableObject objectToDrag;

        [FoldoutGroup("Status"), SerializeField]
        private DraggableObject objectToRelease;

        [FoldoutGroup("Events"), PreviouslySerializedAs("OnDragStarted")]
        public GameObjectEvent OnDragStarting;

        [FoldoutGroup("Events"), PreviouslySerializedAs("OnDragStarted")]
        public GameObjectEvent OnBeforeDragStarting;

        public GameObjectEvent OnDragTargetReached;
        [FoldoutGroup("Events")] public UnityEvent OnDropDone;

        [FoldoutGroup("Events")] public GameObjectEvent OnDropStarted;

        [FoldoutGroup("Events")] public UnityEvent OnInterrupted;
        [FoldoutGroup("Events")] public UnityEvent OnResumed;

        private bool targetReachedNotified;

        [FoldoutGroup("Status")] public bool IsDragging => ObjectToDrag;
        [FoldoutGroup("Status")] public bool IsDropping => objectToRelease;

        public DraggableObject ObjectToDrag => objectToDrag;
        public DraggableObject ObjectToDrop => objectToRelease;

        public bool IsDrag => DragLogic;

        public bool IsDrop => DropLogic;

        private bool IsDragLocked => (LockDraggingWhenDropping && IsDropping) ||
                                     LockedOnTrue
            ? ExternalDragLock?.Value ?? false
            : !ExternalDragLock?.Value ?? false;

        private bool IsDropLocked =>
            LockedOnTrue ? ExternalDropLock?.Value ?? false : !ExternalDropLock?.Value ?? false;

        [FoldoutGroup("Status")]
        [ShowInInspector]
        public bool CanDrag => enabled && !IsDragging && !IsDragLocked;

        [FoldoutGroup("Status")]
        [ShowInInspector]
        public bool CanDrop => enabled && IsDragging && !IsDropLocked;

        public object Save()
        {
            throw new System.NotImplementedException();
        }

        public void Load(object saveFile)
        {
            throw new System.NotImplementedException();
        }

        public DragAndDropZone Value => this;

        void OnEnable()
        {
            if (ObjectToDrag && DragOnEnable)
            {
                if (ObjectToDrag.CurrentDraggingZone == null || ObjectToDrag.CurrentDraggingZone == this)
                {
                    if (InstantDragOnEnable)
                    {
                        InstantDrag(ObjectToDrag);
                    }
                    else
                    {
                        ObjectToDrag.RequestDrag(this);
                    }
                }
                else
                {
                    UnityEngine.Debug.LogWarningFormat(this,
                        "The object {1} attached to the zone {0} cannot be dragged on enable" +
                        "as its current dragging zone is {1}", name, ObjectToDrag.name,
                        ObjectToDrag.CurrentDraggingZone.name);
                }
            }
        }

        public void SetInterrupted(bool interuption)
        {
            isInterrupted = interuption;
            if (isInterrupted)
            {
                OnInterrupted.Invoke();
            }
            else
            {
                OnResumed.Invoke();
            }
        }

        [BoxGroup("Logic")]
        [Button(ButtonSizes.Medium), HideIf("IsDrag")]
        public void CreateDefaultDragLogic()
        {
            GameObject go = new GameObject("Drag Logic");
            go.transform.SetParent(transform);

            DragLogic = go.AddComponent<SpeedProfileDragLogic>();
            var speedProfileDragLogic = DragLogic as SpeedProfileDragLogic;
            if (speedProfileDragLogic != null)
                speedProfileDragLogic.DropPosition = go.transform;
        }

        [BoxGroup("Logic")]
        [Button(ButtonSizes.Medium), HideIf("IsDrop")]
        public void CreateDefaultDropLogic()
        {
            GameObject go = new GameObject("Drop Logic");
            go.transform.SetParent(transform);

            DropLogic = go.AddComponent<SpeedProfileDragLogic>();

            var speedProfileDragLogic = DropLogic as SpeedProfileDragLogic;
            if (speedProfileDragLogic != null)
                speedProfileDragLogic.DropPosition = go.transform;
        }


        [FoldoutGroup("Tools", false, 99999), Button(ButtonSizes.Medium)]
        public bool Drop()
        {
            if (!ObjectToDrag || !CanDrop)
                return false;

            return Drop(ObjectToDrag);
        }

        [FoldoutGroup("Tools", false, 99999), Button(ButtonSizes.Medium)]
        public bool InstantDrop()
        {
            if (!ObjectToDrag || !CanDrop)
                return false;

            return InstantDrop(ObjectToDrag);
        }

        public void InstantDropNoReturn()
        {
            InstantDrop();
        }

        public bool InstantDrop(DraggableObject drag)
        {
            if (!CanDrop)
                return false;

            objectToRelease = drag;
            objectToDrag = null;

            if (IsDrop)
                DropLogic.InstantDragLogic(this, objectToRelease);

            objectToRelease.NotifyDropDone(this, true);
            objectToRelease = null;

            OnDropDone.Invoke();

#if UNITY_EDITOR
            /*   if (!Application.isPlaying && PrefabUtility.GetPrefabType(this) == PrefabType.PrefabInstance)
               OdinPrefabUtility.UpdatePrefabInstancePropertyModifications(this, false);*/
#endif
            return true;
        }

        public void DropCurrentObject()
        {
            Drop(ObjectToDrag);
        }

        public bool Drop(DraggableObject drag)
        {
            if (!CanDrop)
                return false;

            objectToRelease = drag;
            objectToDrag = null;

            if (IsDrag)
                DragLogic.NotifyDragEnd(this, objectToRelease);

            if (IsDrop)
                DropLogic.NotifyDragStart(this, objectToRelease);

            OnDropStarted.Invoke(objectToRelease.gameObject);

#if UNITY_EDITOR
            /* if (!Application.isPlaying && PrefabUtility.GetPrefabType(this) == PrefabType.PrefabInstance)
             OdinPrefabUtility.UpdatePrefabInstancePropertyModifications(this, false);*/
#endif
            return true;
        }

        [FoldoutGroup("Tools", false, 99999), Button(ButtonSizes.Medium)]
        public void EditorInstantDrag()
        {
            if (ObjectToDrag)
                DragLogic.InstantDragLogic(this, ObjectToDrag);
        }

        public void InstantDragOrDropIfNull(Component toDrag)
        {
            if (!toDrag)
            {
                if (objectToDrag || objectToRelease)
                    InstantDrop();
            }
            else
            {
                InstantDragComponent(toDrag);
            }
        }

        public void InstantDragComponent(Component toDrag)
        {
            Debug.Assert(toDrag is DraggableObject, $"The component you pasted ({toDrag}) is not a draggable object",
                this);
            InstantDrag((DraggableObject) toDrag);
        }

        public void InstantDrag(DraggableObject toDrag)
        {
            Debug.Assert(toDrag, "The object you want to drag is null!", this);
            if (CanDrag || toDrag == objectToDrag)
            {
                OnBeforeDragStarting.Invoke(toDrag.gameObject);
                if (toDrag.CurrentDraggingZone != this)
                {
                    //then first the object must drop the previous zone, and confirm we can drag it
                    toDrag.RequestInstantDrag(this);
                    return;
                }

                //then it was confirmed: it can be attached and dragged
                objectToDrag = toDrag;
                if (objectToRelease == toDrag)
                    objectToRelease = null;

                OnDragStarting.Invoke(objectToDrag.gameObject);

                if (DragLogic)
                    DragLogic.InstantDragLogic(this, ObjectToDrag);

#if UNITY_EDITOR

                // PrefabUtility.SetPropertyModifications
                /*   if (!Application.isPlaying && PrefabUtility.GetPrefabType(this) == PrefabType.PrefabInstance)
                   OdinPrefabUtility.UpdatePrefabInstancePropertyModifications(this, false);*/
#endif
                return;
            }

            return;
        }

        public void Drag(GameObject obj)
        {
            var draggable = obj.GetComponent<DraggableObject>();

            UnityEngine.Debug.Assert(draggable,
                "The object you want to drag must have a draggable object component on it in order to be dragged", obj);

            Drag(draggable);
        }

        public bool Drag(DraggableObject toDrag)
        {
            if (CanDrag)
            {
                OnBeforeDragStarting.Invoke(toDrag.gameObject);
                if (toDrag.CurrentDraggingZone != this)
                {
                    //then first the object must drop the previous zone, and confirm we can drag it
                    return toDrag.RequestDrag(this);
                }

                //then it was confirmed: it can be attached and dragged
                objectToDrag = toDrag;
                objectToDrag.IsDragTargetReached = false;

                if (objectToRelease == toDrag)
                    objectToRelease = null;

                if (IsDrag)
                    DragLogic.NotifyDragStart(this, objectToDrag);

                OnDragStarting.Invoke(objectToDrag.gameObject);
#if UNITY_EDITOR
                /*   if (!Application.isPlaying && PrefabUtility.GetPrefabType(this) == PrefabType.PrefabInstance)
                   OdinPrefabUtility.UpdatePrefabInstancePropertyModifications(this, false);*/
#endif
                return true;
            }

            return false;
        }

        protected virtual void FixedUpdate()
        {
            DragObjectsToDrag();
            DropObjectsToRelease();
        }

        protected virtual void DragObjectsToDrag()
        {
            if (!objectToDrag || !DragLogic)
                return;

            if (DragLogic.IsDragLogicDone(this, objectToDrag))
            {
                if (!targetReachedNotified)
                {
                    targetReachedNotified = true;
                    objectToDrag.IsDragTargetReached = true;
                    OnDragTargetReached.Invoke(objectToDrag.gameObject);
                }

                if (AutoReleaseOnDragDone && CanDrop)
                    Drop(objectToDrag);
            }
            else
            {
                DragLogic.DragLogic(this, objectToDrag);
                targetReachedNotified = false;
            }
        }

        protected virtual void DropObjectsToRelease()
        {
            if (!objectToRelease)
                return;

            if (DropLogic)
                DropLogic.DragLogic(this, objectToRelease);
            if (!DropLogic || DropLogic.IsDragLogicDone(this, objectToRelease))
            {
                if (DropLogic)
                    DropLogic.NotifyDragEnd(this, objectToRelease);
                objectToRelease.NotifyDropDone(this);
                objectToRelease = null;
                OnDropDone.Invoke();
            }
        }

        #region Tools

        [ShowInInspector, FoldoutGroup("Tools")]
        private DraggableObject objectToAdd;

#if UNITY_EDITOR
        [FoldoutGroup("Tools"), Button(ButtonSizes.Medium)]
        public void AddObjectToDrag()
        {
            if (!objectToAdd)
                UnityEngine.Debug.Log("Select an object to drag first");
            Drag(objectToAdd);
        }

#endif

        #endregion

#if UNITY_EDITOR

        public void ToggleDragInEditor()
        {
            EditorApplication.update -= DragAndDropEditor;
            if (DragInEditor)
                EditorApplication.update += DragAndDropEditor;
        }


        [OnInspectorGUI]
        public void OnDrawGizmos()
        {
#if UNITY_EDITOR
            if (EditorApplication.isPlayingOrWillChangePlaymode)
                return;

            ToggleDragInEditor();
#endif
        }

        public void DragAndDropEditor()
        {
            if (!Application.isPlaying)
            {
                FixedUpdate();
            }
        }
#endif
    }
}