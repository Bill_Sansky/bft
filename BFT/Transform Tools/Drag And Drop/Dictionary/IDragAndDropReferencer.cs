﻿using System.Collections.Generic;

namespace BFT
{
    public interface IDragAndDropReferencer
    {
        IEnumerable<int> DragAndDropZoneIDs { get; }
        IEnumerable<int> DraggableIDs { get; }

        int DragAndDropCount { get; }
        int DraggableCount { get; }

        /// <summary>
        ///     if true, the Ids of the drag and drop zones and draggable objects will start from 0 until the count
        /// </summary>
        bool IsContinuousIds { get; }

        DraggableObject GetDraggableObject(int id);
        DragAndDropZone GetDragAndDropZone(int id);
        int IndexOf(DraggableObject obj);
    }
}
