﻿namespace BFT
{
    public class DragAndDropZoneDictionary : DictionaryAsset<DragAndDropZone, DragAndDropZoneEntry>
    {
        public override DragAndDropZoneEntry CreateNewDataHolder(object data)
        {
            return new DragAndDropZoneEntry();
        }
    }
}
