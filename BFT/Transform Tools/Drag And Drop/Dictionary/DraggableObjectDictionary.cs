﻿namespace BFT
{
    public class DraggableObjectDictionary : DictionaryAsset<DraggableObject, DraggableObjectEntry>
    {
        public override DraggableObjectEntry CreateNewDataHolder(object data)
        {
            return new DraggableObjectEntry();
        }
    }
}
