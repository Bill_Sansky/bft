﻿using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace BFT
{
    public class DragAndDropZoneWatcher : SerializedMonoBehaviour
    {
        [BoxGroup("References")] public DraggableObject DraggableObject;

        [BoxGroup("References")] public IValue<DragAndDropZone> DragZoneToWatch;

        private bool isDragZone = false;


        [BoxGroup("Tools")] public bool LogDebug;

        [BoxGroup("Events")] public UnityEvent OnDragZoneDragEnded;

        [BoxGroup("Events")] public UnityEvent OnDragZoneDragStarted;

        private void OnEnable()
        {
            DraggableObject.OnDraggingStarted.AddListener(CheckIfDragZone);
            DraggableObject.OnDraggingEnded.AddListener(CheckIfDragZoneStop);
        }

        private void OnDisable()
        {
            DraggableObject.OnDraggingStarted.RemoveListener(CheckIfDragZone);
            DraggableObject.OnDraggingEnded.RemoveListener(CheckIfDragZoneStop);
        }


        private void CheckIfDragZoneStop()
        {
            if (LogDebug)
                UnityEngine.Debug.LogFormat(this, "Checking if dragged by the good drag zone stopped on {0}", name);

            if (isDragZone)
            {
                if (LogDebug)
                    UnityEngine.Debug.LogFormat(this, "Drag did stop on {0}", name);

                isDragZone = false;
                NotifyDragZoneDragStopped();
                OnDragZoneDragEnded.Invoke();
            }
        }


        private void CheckIfDragZone()
        {
            if (LogDebug)
                UnityEngine.Debug.LogFormat(this, "Checking if dragged by the good drag zone on {0}", name);

            if (DraggableObject.CurrentDraggingZone == DragZoneToWatch.Value)
            {
                if (LogDebug)
                    UnityEngine.Debug.LogFormat(this, "Dragged by the good zone on {0}", name);

                isDragZone = true;
                NotifyDragZoneDragging();
                OnDragZoneDragStarted.Invoke();
            }
        }


        protected virtual void NotifyDragZoneDragging()
        {
        }


        protected virtual void NotifyDragZoneDragStopped()
        {
        }
    }
}
