﻿using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    /// <summary>
    ///     Compares the drag and drop zone that started to drag a draggable object
    ///     to a referenced one and fires an event when it is the same
    /// </summary>
    public class DragAndDropZoneComparison : MonoBehaviour
    {
        public bool CheckOnEnable = true;
        public DragAndDropZoneVariableAsset DragAndDropReference;
        public DraggableObject DraggableObjectToCheck;

        public UnityEvent OnMatched;
        public UnityEvent OnNotMatched;

        public void OnEnable()
        {
            if (CheckOnEnable)
                CheckDragAndDropZoneMatch();
        }

        public void CheckDragAndDropZoneMatch()
        {
            Debug.Assert(DraggableObjectToCheck,"The draggable Object was not defined",this);
            Debug.Assert(DragAndDropReference,"The drag and drop reference was not defined",this);
            
            if (DraggableObjectToCheck.CurrentDraggingZone == DragAndDropReference.Value)
                OnMatched.Invoke();
            else
            {
                OnNotMatched.Invoke();
            }
        }
    }
}
