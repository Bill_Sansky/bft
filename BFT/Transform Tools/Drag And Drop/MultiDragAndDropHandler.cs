﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class MultiDragAndDropHandler : MonoBehaviour
    {
        [BoxGroup("Handlers")] public List<DragAndDropHandler> Handlers = new List<DragAndDropHandler>();

        void Reset()
        {
            Handlers = new List<DragAndDropHandler>(FindObjectsOfType<DragAndDropHandler>());
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void FindAllHandlers()
        {
            Handlers.RemoveNulls();
            DragAndDropHandler[] handlers = FindObjectsOfType<DragAndDropHandler>();
            foreach (var handler in handlers)
            {
                if (!Handlers.Contains(handler))
                {
                    Handlers.Add(handler);
                }
            }
        }

        public void OnEnable()
        {
            if (Handlers.Count == 0)
                FindAllHandlers();
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void Drag()
        {
            foreach (var dropHandler in Handlers)
            {
                dropHandler.Drag();

                if (dropHandler.ObjectHandled)
                    return;
            }
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void Drop()
        {
            foreach (var dropHandler in Handlers)
            {
                dropHandler.Drop();
            }
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void SwitchDragDrop()
        {
            foreach (var dropHandler in Handlers)
            {
                dropHandler.ToggleDragDrop();
            }
        }
    }
}
