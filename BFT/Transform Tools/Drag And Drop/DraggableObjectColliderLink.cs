using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    public class DraggableObjectColliderLink : MonoBehaviour
    {
        public Collider ColliderLink;
        public DraggableObject DraggableObject;
    }
}
