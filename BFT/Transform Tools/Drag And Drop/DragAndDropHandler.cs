﻿using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    /// <summary>
    ///     manages the transfer of one dragged object onto different drop zones automatically
    /// </summary>
    public class DragAndDropHandler : MonoBehaviour
    {
        [BoxGroup("Option")] public bool AllowDropOutsideOfZone;


        [BoxGroup("Drag Zone")] public DragAndDropZoneObjectTrigger DragZoneTrigger;
        [BoxGroup("Drop Zones")] public List<DragAndDropZoneObjectTrigger> DropZonesTriggers;
        [BoxGroup("Option")] public bool ExcludeDragZoneDetectedObjectsOnDrop = true;

        [FoldoutGroup("Status"), ShowInInspector]
        private DraggableObject objectHandled;

        [BoxGroup("Events")] public UnityEvent OnDragEnded;

        [BoxGroup("Events")] public UnityEvent OnDragStarted;

        [BoxGroup("Events")] public UnityEvent OnDragZoneObjectRegistered;
        [BoxGroup("Events")] public UnityEvent OnDragZoneObjectUnRegister;

        [FoldoutGroup("Status"), ShowInInspector]
        private List<DragAndDropZoneObjectTrigger> triggersThatWantToDrag
            = new List<DragAndDropZoneObjectTrigger>();


        public bool WaitForDropWhenAvailable = true;

        private bool waitingForDrop = false;

        public List<EnumAsset> ConnectionLayers
        {
            get
            {
                List<EnumAsset> ConnectionLayers = new List<EnumAsset>();

                foreach (var trigger in DropZonesTriggers)
                {
                    ConnectionLayers.AddRange(
                        trigger.ConnectionLayers.Where(_ => !ConnectionLayers.Contains(_)));
                }

                return ConnectionLayers;
            }
        }

        public DraggableObject ObjectHandled => objectHandled;

        public void OnEnable()
        {
            foreach (DragAndDropZoneObjectTrigger dropZone in DropZonesTriggers)
            {
                RegisterDropZoneTriggerEvents(dropZone);
            }
        }

        private void RegisterDropZoneTriggerEvents(DragAndDropZoneObjectTrigger dropZone)
        {
            dropZone.OnObjectRegisteredInZone += CheckLastTriggeredZone;
            dropZone.OnObjectUnregisteredFromZone += FreeLastTriggerZone;
        }

        public void OnDisable()
        {
            foreach (DragAndDropZoneObjectTrigger dropZone in DropZonesTriggers)
            {
                dropZone.OnObjectRegisteredInZone -= CheckLastTriggeredZone;
                dropZone.OnObjectUnregisteredFromZone -= FreeLastTriggerZone;
            }
        }

        public void AddDropZoneTrigger(DragAndDropZoneObjectTrigger trigger)
        {
            if (DropZonesTriggers.Contains(trigger))
                return;

            DropZonesTriggers.Add(trigger);
            RegisterDropZoneTriggerEvents(trigger);
        }


        private void FreeLastTriggerZone(DragAndDropZoneObjectTrigger zone, DraggableObject toFree)
        {
            if (toFree == objectHandled &&
                triggersThatWantToDrag.Contains(zone))
            {
                triggersThatWantToDrag.Remove(zone);
            }
        }

        private void CheckLastTriggeredZone(DragAndDropZoneObjectTrigger zone, DraggableObject trans)
        {
            if (trans == objectHandled &&
                !triggersThatWantToDrag.Contains(zone))
            {
                triggersThatWantToDrag.Add(zone);
            }
        }

        public bool DropAndUnRegister()
        {
            DraggableObject drag = DragZoneTrigger.DragAndDropZone.ObjectToDrag;


            if (!enabled || !objectHandled || drag != objectHandled
                || !DragZoneTrigger.DragAndDropZone.CanDrop)
            {
                if (WaitForDropWhenAvailable)
                {
                    waitingForDrop = true;
                }

                return false;
            }

            foreach (DragAndDropZoneObjectTrigger trigger in triggersThatWantToDrag)
            {
                if (trigger.DragAndDropZone && trigger.DragAndDropZone.Drag(drag))
                {
                    PostDrop();
                    OnDragEnded.Invoke();
                    return true;
                }
            }

            if (AllowDropOutsideOfZone)
            {
                DragZoneTrigger.DragAndDropZone.Drop(drag);
                PostDrop();
                OnDragEnded.Invoke();

                return true;
            }

            return false;
        }

        private void PostDrop()
        {
            if (ExcludeDragZoneDetectedObjectsOnDrop)
            {
                DragZoneTrigger.DetectedObject.Clear();
            }

            objectHandled = null;
            triggersThatWantToDrag.Clear();

            OnDragZoneObjectUnRegister.Invoke();
        }

        public void Drop()
        {
            DropAndUnRegister();
        }

        public void Drag()
        {
            DragAndRegister();
        }

        public bool DragAndRegister()
        {
            waitingForDrop = false;
            if (!gameObject.activeInHierarchy || !DragZoneTrigger.DragAndDropZone.CanDrag)
                return false;

            foreach (DraggableObject draggableObject in DragZoneTrigger.DetectedObject)
            {
                if (!draggableObject.gameObject.activeInHierarchy)
                    continue;

                if (DragZoneTrigger.DragAndDropZone.Drag(draggableObject))
                {
                    OnDragZoneObjectRegistered.Invoke();
                    objectHandled = draggableObject;
                    if (WaitForDropWhenAvailable)
                        DragZoneTrigger.DragAndDropZone.OnDragStarting.AddListener(CheckIfWaitingForDrop);
                    OnDragStarted.Invoke();
                    return true;
                }
            }

            return false;
        }

        private void CheckIfWaitingForDrop(GameObject arg0)
        {
            DragZoneTrigger.DragAndDropZone.OnDragStarting.RemoveListener(CheckIfWaitingForDrop);
            if (waitingForDrop)
                Drop();
        }

        public void ToggleDragDrop()
        {
            if (!enabled)
                return;

            if (DragZoneTrigger.DragAndDropZone.IsDragging)
                DropAndUnRegister();
            else
            {
                DragAndRegister();
            }
        }

        public void OnDrawGizmosSelected()
        {
            DropZonesTriggers.RemoveAll(x => x == null);
        }

        #region Editor Utils

#if UNITY_EDITOR

        [FoldoutGroup("Tools"), Button(ButtonSizes.Medium)]
        public void CreateNewDragDropZone()
        {
            GameObject go = new GameObject("Drag And Drop Zone");
            go.transform.SetParent(transform);
            DragAndDropZone zone = go.AddComponent<DragAndDropZone>();
            DragAndDropZoneObjectTrigger trigger = go.AddComponent<DragAndDropZoneObjectTrigger>();
            SphereCollider col = go.AddComponent<SphereCollider>();

            trigger.DragAndDropZone = zone;
            col.isTrigger = true;
        }

        [FoldoutGroup("Tools"), Button(ButtonSizes.Medium)]
        public void AddAllDropZonesInSiblings()
        {
            UnityEngine.Transform parent = gameObject.transform.parent;
            if (!parent)
            {
                UnityEngine.Debug.LogWarning("Right now drop zones cannot be " +
                                 "found if the handler has no parent object");
                return;
            }

            DragAndDropZoneObjectTrigger[] triggers =
                parent.gameObject.GetComponentsInChildren<DragAndDropZoneObjectTrigger>();

            foreach (var trigger in triggers)
            {
                if (DropZonesTriggers.Contains(trigger))
                    continue;
                DropZonesTriggers.Add(trigger);
            }
        }

#endif

        #endregion
    }
}
