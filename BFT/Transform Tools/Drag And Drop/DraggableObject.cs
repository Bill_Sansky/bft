﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
#endif

namespace BFT
{
    /// <summary>
    ///     A single drag and drop zone at a time
    /// </summary>
    [ExecuteInEditMode]
    public class DraggableObject : MonoBehaviour
    {

        private bool checkTargetReached = false;

        [BoxGroup("Layers")] public List<EnumAsset> ConnectionLayers = new List<EnumAsset>();

        [BoxGroup("Status")] [SerializeField] [OnValueChanged("EditorAutoDrag")]
        private DragAndDropZone currentDraggingZone;

        [BoxGroup("Tools")] public bool DebugLog;

        [BoxGroup("Dragging Parameters")] public bool DropOnDisable = false;

        [FoldoutGroup("Events")] public bool IgnoreDraggingEventOnEnable = false;

        [BoxGroup("Status")] private bool isDragTargetReached;

        [FoldoutGroup("Events")] public UnityEvent OnBeforeDraggingEnded;
        [FoldoutGroup("Events")] public UnityEvent OnBeforeDraggingStarted;
        [FoldoutGroup("Events")] public UnityEvent OnDraggingEnded;


        [FoldoutGroup("Events")] public UnityEvent OnDraggingStarted;

        [FoldoutGroup("Events")] public UnityEvent OnDraggingTargetReached;

        [FoldoutGroup("Events")] public GameObjectEvent OnEnteredDragAndDropTrigger;

        [FoldoutGroup("Events")] public GameObjectEvent OnExitedDragAndDropTrigger;

        [BoxGroup("Dragging Parameters")] public bool PreventPositionChange;
        [BoxGroup("Dragging Parameters")] public bool PreventRotationChange;
        [BoxGroup("Dragging Parameters")] public bool PreventScaleChange = true;

        [BoxGroup("Dragging Parameters")] public Rigidbody RigidBody;

        [BoxGroup("Dragging Parameters")] public bool SetKinematicOnDrop = false;

        [BoxGroup("Utils")] public bool UseEditorAutoDrag = false;

        [BoxGroup("Status")] [SerializeField] private DragAndDropZone waitingForADrag;

        [BoxGroup("Status"), ReadOnly] public Vector3 Velocity { get; protected set; }

        public DragAndDropZone CurrentDraggingZone
        {
            get => currentDraggingZone;
            protected set => currentDraggingZone = value;
        }

        public DragAndDropZone WaitingForADrag
        {
            get => waitingForADrag;
            set => waitingForADrag = value;
        }

        public bool IsDragTargetReached
        {
            get => isDragTargetReached;
            set
            {
                if (!isDragTargetReached && value)
                {
                    isDragTargetReached = true;
                    OnDraggingTargetReached.Invoke();
                    CurrentDraggingZone.OnDragTargetReached.Invoke(gameObject);
                    return;
                }

                isDragTargetReached = value;
            }
        }

        public bool CanBeDragged => !CurrentDraggingZone || CurrentDraggingZone.CanDrop;

        void Reset()
        {
            RigidBody = GetComponent<Rigidbody>();
            if (RigidBody)
                SetKinematicOnDrop = RigidBody.isKinematic;
        }

        private void EditorAutoDrag()
        {
            if (UseEditorAutoDrag && currentDraggingZone)
                currentDraggingZone.InstantDrag(this);
        }

        void Awake()
        {
            CurrentDraggingZone = null;
        }

        void OnEnable()
        {
            if (CurrentDraggingZone && !IgnoreDraggingEventOnEnable)
                OnDraggingStarted.Invoke();
        }

        void OnDisable()
        {
            if (CurrentDraggingZone && DropOnDisable)
                CurrentDraggingZone.InstantDrop();
        }

        public void NotifyTriggerDetected(GameObject triggerObject)
        {
            OnEnteredDragAndDropTrigger.Invoke(triggerObject);
        }

        public void NotifyTriggerExited(GameObject triggerObject)
        {
            OnExitedDragAndDropTrigger.Invoke(triggerObject);
        }

        public virtual bool RequestInstantDrag(DragAndDropZone zone)
        {
            UnityEngine.Debug.AssertFormat(zone.CanDrag || zone.ObjectToDrag == this,
                this, "The zone {0} set to request instant drag {1} is not currently able to drag",
                zone.name, name);

            if (CurrentDraggingZone)
            {
                if (CurrentDraggingZone != zone)
                {
                    if (CurrentDraggingZone.InstantDrop(this))
                    {
                        ForceStartDrag(zone, true);
                        return true;
                    }

                    return false;
                }

                return false;
            }

            ForceStartDrag(zone, true);
            return true;
        }

        public virtual bool RequestDrag(DragAndDropZone zone)
        {
            if (!enabled)
                return false;

            UnityEngine.Debug.AssertFormat(zone.CanDrag || zone.ObjectToDrag == this,
                this, "The zone {0} set to request drag {1} is not currently able to drag",
                zone.name, name);

            if (CurrentDraggingZone)
            {
                if (CurrentDraggingZone != zone)
                {
                    if (CurrentDraggingZone.Drop(this) || CurrentDraggingZone.ObjectToDrop == this)
                    {
                        waitingForADrag = zone;

                        //not sure if it's a good idea to automatically do that, but for now it's more convenient
                        CurrentDraggingZone.SetInterrupted(false);
                        return true;
                    }

                    return false;
                }

                return false;
            }

            ForceStartDrag(zone);
            return true;
        }

        private void ForceStartDrag(DragAndDropZone zone, bool instant = false)
        {
            if (!enabled)
                return;

            OnBeforeDraggingStarted.Invoke();

            CurrentDraggingZone = zone;

            if (!instant)
                CurrentDraggingZone.Drag(this);
            else
            {
                CurrentDraggingZone.InstantDrag(this);
            }

            isDragTargetReached = false;

            OnDraggingStarted.Invoke();

#if UNITY_EDITOR
            /*    if (!Application.isPlaying && PrefabUtility.GetPrefabType(this) == PrefabType.PrefabInstance)
                PrefabUtility.RecordPrefabInstancePropertyModifications(this);*/
#endif

            if (DebugLog)
                UnityEngine.Debug.LogFormat(this, "Drag Started on {0} with zone {1}", name, zone.name);
        }

        public void NotifyDropDone(DragAndDropZone zone, bool instant = false)
        {
            if (zone != CurrentDraggingZone)
                return;

            ForceDrop();

            if (WaitingForADrag)
            {
                DragAndDropZone newZone = WaitingForADrag;
                waitingForADrag = null;
                ForceStartDrag(newZone, instant);
            }
#if UNITY_EDITOR
            /*  if (!Application.isPlaying && PrefabUtility.GetPrefabType(this) == PrefabType.PrefabInstance)
              PrefabUtility.RecordPrefabInstancePropertyModifications(this);*/
#endif
        }

        public void ForceDrop()
        {
            if (!CurrentDraggingZone)
                return;

            OnBeforeDraggingEnded.Invoke();

            CurrentDraggingZone.InstantDrop(this);
            CurrentDraggingZone = null;

            if (SetKinematicOnDrop)
                RigidBody.isKinematic = true;
            else if (gameObject.activeInHierarchy)
            {
                this.CallAfterOneFrame(RigidBody.WakeUp);
            }

            OnDraggingEnded.Invoke();

            if (DebugLog)
                UnityEngine.Debug.LogFormat(this, "Drag Ended on {0}", name);
        }

        public void HandleDragRequest(Vector3 wantedPosition, Quaternion wantedRotation, Vector3 wantedScale)
        {
            Velocity = wantedPosition - transform.position;

            if (!PreventPositionChange)
            {
                transform.position = wantedPosition;
            }

            if (!PreventRotationChange)
            {
                transform.rotation = wantedRotation;
            }

            if (!PreventScaleChange)
            {
                transform.localScale = wantedScale;
            }

            if (checkTargetReached)
            {
                if (isDragTargetReached)
                    OnDraggingTargetReached.Invoke();
                checkTargetReached = false;
            }
            else if (!isDragTargetReached)
            {
                checkTargetReached = true;
            }
        }

        public void HandleInstantDragRequest(Vector3 wantedPosition, Quaternion wantedRotation, Vector3 wantedScale)
        {
            if (!PreventPositionChange)
            {
                transform.position = wantedPosition;
            }

            if (!PreventRotationChange)
            {
                transform.rotation = wantedRotation;
            }

            if (!PreventScaleChange)
            {
                transform.localScale = wantedScale;
            }

            isDragTargetReached = true;
            checkTargetReached = false;
            OnDraggingTargetReached.Invoke();
        }

        void OnDestroy()
        {
            if (CurrentDraggingZone && CurrentDraggingZone.gameObject.activeInHierarchy)
                CurrentDraggingZone.Drop(this);
        }
    }
}