﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [RequireComponent(typeof(Collider))]
    public class DragAndDropZoneObjectTrigger : MonoBehaviour
    {
        public delegate void ZoneAction(DragAndDropZoneObjectTrigger zone, DraggableObject registered);

        [BoxGroup("Options")] public bool AutoDragOnTriggerEnter;
        [BoxGroup("Options")] public bool AutoDropOnTriggerExit;

        [BoxGroup("Options")] public bool AutoRegisterToGlobalHandler = false;
        [BoxGroup("Layer")] public List<EnumAsset> ConnectionLayers = new List<EnumAsset>();

        [FoldoutGroup("Tools")] public bool DebugLog;

        [BoxGroup("Status", order: 9999), ReadOnly]
        public List<DraggableObject> DetectedObject = new List<DraggableObject>();

        [BoxGroup("Zone")] public DragAndDropZone DragAndDropZone;

        [BoxGroup("Options"), ShowIf("AutoRegisterToGlobalHandler")]
        public DragAndDropHandlerVariableAsset GlobalHandler;

        [ShowInInspector, FoldoutGroup("Tools")]
        private DragAndDropHandler handlerToLink;

        public GameObjectEvent OnObjectRegistered;
        public GameObjectEvent OnObjectUnregistered;

        [Tooltip(
            "if true, the trigger will not detect draggable object, but only the object dragged by the main drag zone")]
        [BoxGroup("Options"), ShowIf("AutoRegisterToGlobalHandler")]
        public bool RespondOnlyToMainDragZone;

        public void OnTriggerExit(Collider other)
        {
            DraggableObject drag;

            if (RespondOnlyToMainDragZone)
            {
                if (other.gameObject == GlobalHandler.Value.DragZoneTrigger.gameObject)
                {
                    //unregister all the objects, no matter what the dragging zone is carrying: 
                    //if it would have dropped the object before, we need to undetect it anyway
                    for (int i = DetectedObject.Count - 1; i >= 0; i--)
                    {
                        UnRegisterDraggable(DetectedObject[i], AutoDropOnTriggerExit);
                    }
                }
            }
            else
            {
                drag = other.GetComponent<DraggableObject>();

                if (!drag)
                {
                    if (DebugLog)
                    {
                        Debug.LogWarning(
                            $"An Object ({other}) was detected but it does not contains a draggable object ",
                            this);
                        Debug.LogWarning("Ping the other object", other);
                    }

                    return;
                }

                if (DetectedObject.Contains(drag) && IsConnectionLayerCompatible(drag))
                {
                    UnRegisterDraggable(drag, AutoDropOnTriggerExit);
                }
            }
        }


        public event ZoneAction OnObjectRegisteredInZone;
        public event ZoneAction OnObjectUnregisteredFromZone;

        void Reset()
        {
            DragAndDropZone = GetComponent<DragAndDropZone>();
        }

        void Awake()
        {
#if UNITY_EDITOR
            if (!GetComponent<Collider>().isTrigger)
            {
                UnityEngine.Debug.LogWarning("The associated Collider with the drop zone'"
                                             + name + "' is not a trigger: please correct it", this);
                GetComponent<Collider>().isTrigger = true;
            }

            DetectedObject.Clear();
#endif
        }

        void OnEnable()
        {
            if (AutoRegisterToGlobalHandler)
                StartCoroutine(CoroutineUtils.ActionWhenValueReady(() => GlobalHandler.Value,
                    () => GlobalHandler.Value.AddDropZoneTrigger(this)));
        }

        private bool IsConnectionLayerCompatible(DraggableObject obj)
        {
            return ConnectionLayers.Count == 0 || ConnectionLayers.ContainsAny(obj.ConnectionLayers);
        }

        public void OnTriggerEnter(Collider other)
        {
            if (DebugLog)
                UnityEngine.Debug.LogFormat(this, "Object {0} entered Drag And Drop Trigger {1}", other.name, name);

            if (RespondOnlyToMainDragZone)
            {
                if (other.gameObject == GlobalHandler.Value.DragZoneTrigger.gameObject)
                {
                    if (DebugLog)
                        UnityEngine.Debug.LogFormat(this, "Main Drag Trigger Recognized on Enter");
                    DraggableObject mainDragged = GlobalHandler.Value.DragZoneTrigger.DragAndDropZone.ObjectToDrag;
                    if (mainDragged && IsConnectionLayerCompatible(mainDragged))
                    {
                        if (DebugLog)
                            UnityEngine.Debug.LogFormat(this, "Main Drag Trigger object Registering");
                        RegisterDraggable(mainDragged, AutoDragOnTriggerEnter);
                    }

                    if (DebugLog)
                        UnityEngine.Debug.LogFormat(this, "Main Drag Trigger object Not Registered");
                }
            }
            else
            {
                DraggableObject drag = other.GetComponent<DraggableObject>();
                if (!drag)
                {
                    if (DebugLog)
                    {
                        Debug.LogWarning(
                            $"An Object ({other}) was detected but it does not contains a draggable object ",
                            this);
                        Debug.LogWarning("Ping the other object", other);
                    }

                    return;
                }

                if (IsConnectionLayerCompatible(drag))
                {
                    RegisterDraggable(drag, AutoDragOnTriggerEnter);
                }
            }
        }

        public void UnRegisterDraggable(DraggableObject drag, bool stopToDrag = true)
        {
            if (DetectedObject.Contains(drag))
            {
                if (DebugLog)
                    UnityEngine.Debug.LogFormat(this,
                        "Object {0} was unregistered from detected object on {1}", drag.name, name);

                DetectedObject.Remove(drag);

                OnObjectUnregistered.Invoke(drag.gameObject);

                if (OnObjectUnregisteredFromZone != null) OnObjectUnregisteredFromZone(this, drag);
            }

            if (stopToDrag)
                DragAndDropZone.Drop(drag);
        }

        public void RegisterDraggable(DraggableObject obj, bool forceDrag = false)
        {
            if (!DetectedObject.Contains(obj))
            {
                if (DebugLog)
                    UnityEngine.Debug.LogFormat(this,
                        "Object {0} was registered as detected object on {1}", obj.name, name);

                DetectedObject.Add(obj);

                OnObjectRegistered.Invoke(obj.gameObject);
                if (OnObjectRegisteredInZone != null) OnObjectRegisteredInZone(this, obj);
            }

            if (forceDrag)
            {
                if (DebugLog)
                    UnityEngine.Debug.LogFormat(this, "Object {0} Force dragged by zone {1}", obj.name,
                        DragAndDropZone.name);

                DragAndDropZone.Drag(obj);
            }
        }

        [Button(ButtonSizes.Medium), FoldoutGroup("Tools")]
        private void LinkHandler()
        {
            if (!handlerToLink)
                return;
            handlerToLink.DropZonesTriggers.Add(this);
        }
    }
}