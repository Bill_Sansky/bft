using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    public class DragAndDropZoneTriggerStayHelper : MonoBehaviour
    {
        private List<Collider> colliders = new List<Collider>();
        public DragAndDropZoneObjectTrigger Trigger;

        public void Reset()
        {
            Trigger = GetComponent<DragAndDropZoneObjectTrigger>();
        }

        public void OnTriggerStay(Collider other)
        {
            if (colliders.Contains(other))
                return;
            colliders.Add(other);
            Trigger.OnTriggerEnter(other);
        }

        public void OnTriggerExit(Collider other)
        {
            colliders.Remove(other);
        }
    }
}
