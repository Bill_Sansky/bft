﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class DynamicSpeedDragLogic : AbstractDragReleaseLogic
    {
        [BoxGroup("Attenuation Options")] public AnimationCurve AttenuationProfile;

        [BoxGroup("Attenuation Options")] public float DistanceForAttenuationCurve;

        [BoxGroup("Dragging Options")] public bool DragPosition = true;

        [BoxGroup("Dragging Options")] public bool DragRotation = false;

        [BoxGroup("Position")] public UnityEngine.Transform DropPosition;

        [BoxGroup("Clamping Options")] public float MaxForce = float.PositiveInfinity;

        [BoxGroup("Clamping Options")] public float MaxVelocity = float.PositiveInfinity;

        [BoxGroup("Clamping Options")] public float MaxVelocityDifferenceDown = float.PositiveInfinity;

        [BoxGroup("Clamping Options")] public float MaxVelocityDifferenceUp = float.PositiveInfinity;

        [SerializeField] [BoxGroup("Dragging Options"), ShowIf("DragPosition")]
        private float targetReachedPositionThreshold;

        [SerializeField] [BoxGroup("Dragging Options"), ShowIf("DragRotation")]
        private double targetReachedRotationThreshold;

        public double TargetReachedRotationThreshold
        {
            get => targetReachedRotationThreshold;
            set => targetReachedRotationThreshold = value;
        }


        public float TargetReachedPositionThreshold
        {
            get => targetReachedPositionThreshold;
            set => targetReachedPositionThreshold = value;
        }


        void Reset()
        {
            DropPosition = transform;
        }

        public override void DragLogic(DragAndDropZone zone, DraggableObject draggable)
        {
            if (DragPosition && !draggable.PreventPositionChange)
                draggable.RigidBody.AddForce(
                    PhysicsTools.GetPushForceNeeded(draggable.RigidBody, DropPosition.position,
                        MaxVelocity, MaxVelocityDifferenceDown, MaxVelocityDifferenceUp, MaxForce));

            if (DragRotation && !draggable.PreventRotationChange)
            {
                PhysicsTools.TargetLookAt(draggable.RigidBody,
                    DropPosition.position - draggable.RigidBody.position.normalized, DropPosition.up, 9999);
            }
        }

        public override void InstantDragLogic(DragAndDropZone zone, DraggableObject draggable)
        {
            draggable.HandleInstantDragRequest((!DragPosition) ? draggable.transform.position : DropPosition.position,
                (!DragRotation) ? draggable.transform.rotation : DropPosition.rotation, draggable.transform.localScale);
        }

        public override bool IsDragLogicDone(DragAndDropZone zone, DraggableObject obj)
        {
            if (!obj)
                return true;

            if (!obj.PreventPositionChange && DragPosition)
            {
                if (!(obj.transform.position.IsDistanceUnder(DropPosition.position,
                    TargetReachedPositionThreshold)))
                    return false;
            }

            if (!obj.PreventRotationChange && DragRotation)
            {
                if (!(Quaternion.Angle(obj.transform.rotation,
                          DropPosition.rotation) < TargetReachedRotationThreshold))
                    return false;
            }

            return true;
        }


        public override void NotifyDragStart(DragAndDropZone zone, DraggableObject draggable)
        {
            draggable.RigidBody.isKinematic = false;
        }

        public override void NotifyDragEnd(DragAndDropZone zone, DraggableObject draggable)
        {
        }
    }
}
