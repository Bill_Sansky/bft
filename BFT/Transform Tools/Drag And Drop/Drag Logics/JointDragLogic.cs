﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class JointDragLogic : AbstractDragReleaseLogic
    {
        [SerializeField] [BoxGroup("Drag Options"), ShowIf("overrideAngularDrag")]
        private FloatValue angularDragOverrideValue;

        [BoxGroup("Drag Options"), ShowIf("teleportBodyToJointOnStart")] [SerializeField]
        private bool dragDoneOnObjectSleeping = false;

        [SerializeField] [BoxGroup("Drag Options"), ShowIf("overrideAngularDrag")]
        private FloatValue dragOverrideValue;

        [BoxGroup("Joint")] public Joint Joint;

        [BoxGroup("Drag Options")] [SerializeField]
        private bool overrideAngularDrag = true;

        [BoxGroup("Drag Options")] [SerializeField]
        private bool overrideDrag = true;

        private float previousAngularDrag;
        private float previousDrag;

        [BoxGroup("Drag Options")] [SerializeField]
        public Vector3Value JointPivotOffset;
        
        [BoxGroup("Drag Options")] [SerializeField]
        private bool teleportBodyToJointOnStart = true;

        //TODO try with the center of the bound? 
        [BoxGroup("Drag Options"), ShowIf("teleportBodyToJointOnStart")] [SerializeField]
        private bool teleportBoundCenter = true;

        public override void DragLogic(DragAndDropZone zone, DraggableObject draggable)
        {
        }

        public override void InstantDragLogic(DragAndDropZone zone, DraggableObject draggable)
        {
        }

        public override bool IsDragLogicDone(DragAndDropZone zone, DraggableObject obj)
        {
            return dragDoneOnObjectSleeping && obj.RigidBody.IsSleeping();
        }

        public override void NotifyDragStart(DragAndDropZone zone, DraggableObject draggable)
        {
            if (teleportBodyToJointOnStart)
            {
                Vector3 boundOffset = Vector3.zero;
                if (teleportBoundCenter)
                {
                    boundOffset = draggable.RigidBody.worldCenterOfMass - draggable.RigidBody.position;
                }

                draggable.RigidBody.position = transform.position + boundOffset;
            }

            draggable.RigidBody.isKinematic = false;
            draggable.RigidBody.WakeUp();
            Joint.connectedBody = draggable.RigidBody;

            Joint.connectedAnchor = JointPivotOffset.Value;
            if (overrideDrag)
            {
                previousDrag = draggable.RigidBody.drag;
                draggable.RigidBody.drag = dragOverrideValue.Value;
            }

            if (overrideAngularDrag)
            {
                previousAngularDrag = draggable.RigidBody.angularDrag;
                draggable.RigidBody.angularDrag = angularDragOverrideValue.Value;
            }

            //TODO configure joint depending on degree of freedom declared in the draggable object
        }

        public override void NotifyDragEnd(DragAndDropZone zone, DraggableObject draggable)
        {
            Joint.connectedBody = null;
            draggable.RigidBody.WakeUp();

            if (overrideDrag)
            {
                draggable.RigidBody.drag = previousDrag;
            }

            if (overrideAngularDrag)
            {
                draggable.RigidBody.angularDrag = previousAngularDrag;
            }
        }
    }
}
