﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    /// <summary>
    ///     a free way to do whatever is needed for a certain duration
    /// </summary>
    public class FreeDragDurationLogic : AbstractDragReleaseLogic
    {
        [BoxGroup("Options")] public bool CopyTransform;

        [BoxGroup("Status"), ShowInInspector] private float currentTime;
        [BoxGroup("Options")] public float Duration;

        [BoxGroup("Tools")] public bool LogDebug;
        [BoxGroup("Events")] public UnityEvent OnDragLogicEndNotified;

        [BoxGroup("Events")] public UnityEvent OnDragLogicStart;
        [BoxGroup("Events")] public UnityEvent OnDragLogicStopped;
        [BoxGroup("Events")] public UnityEvent OnPostInstantDrag;
        [BoxGroup("Events")] public UnityEvent OnPreInstantDrag;

        [BoxGroup("Options")] public bool SetDynamicOnDrop;

        [BoxGroup("Options")] public bool SetKinematicOnDrag;

        [BoxGroup("Options")] [ShowIf("CopyTransform")]
        public UnityEngine.Transform TransformToCopy;

        public void NotifyEnd()
        {
            if (LogDebug)
            {
                UnityEngine.Debug.Log("End of free logic notified", this);
            }

            currentTime = Duration;
            OnDragLogicEndNotified.Invoke();
        }

        public override void DragLogic(DragAndDropZone zone, DraggableObject draggable)
        {
            if (currentTime >= 0)
                currentTime += UnityEngine.Time.deltaTime;

            if (CopyTransform)
                draggable.transform.Copy(TransformToCopy, true, true, true);
        }

        public override void InstantDragLogic(DragAndDropZone zone, DraggableObject draggable)
        {
            OnPreInstantDrag.Invoke();

            if (SetKinematicOnDrag)
                draggable.RigidBody.isKinematic = true;

            currentTime = Duration;
            if (CopyTransform)
                draggable.transform.Copy(TransformToCopy, true, true, true);
            OnPostInstantDrag.Invoke();
        }

        public override bool IsDragLogicDone(DragAndDropZone zone, DraggableObject obj)
        {
            return currentTime < 0 || (Duration > 0 && Mathf.Epsilon >= Duration - currentTime);
        }

        public override void NotifyDragStart(DragAndDropZone zone, DraggableObject draggable)
        {
            if (LogDebug)
            {
                UnityEngine.Debug.Log("Free Drag Logic Started", this);
            }

            currentTime = 0;

            if (SetKinematicOnDrag)
                draggable.RigidBody.isKinematic = true;

            OnDragLogicStart.Invoke();
        }

        public override void NotifyDragEnd(DragAndDropZone zone, DraggableObject draggable)
        {
            if (LogDebug)
            {
                UnityEngine.Debug.Log("Free Drag Logic Ended", this);
            }

            if (SetDynamicOnDrop)
                draggable.RigidBody.isKinematic = false;
            OnDragLogicStopped.Invoke();
        }
    }
}
