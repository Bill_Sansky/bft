using UnityEditor;
using UnityEngine;

namespace BFT
{
    public class ReparentingDragLogic : AbstractDragReleaseLogic
    {
        public UnityEngine.Transform ParentTransform;

        public bool ReleaseParentOnDrop = true;
        public bool WorldPositionStay = false;

        public override void DragLogic(DragAndDropZone zone, DraggableObject draggable)
        {
            draggable.transform.SetParent(ParentTransform);
        }

        public override void InstantDragLogic(DragAndDropZone zone, DraggableObject draggable)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                Undo.SetTransformParent(draggable.transform, ParentTransform, "parenting");
                return;
            }
#endif
            draggable.transform.SetParent(ParentTransform, WorldPositionStay);
        }

        public override bool IsDragLogicDone(DragAndDropZone zone, DraggableObject obj)
        {
            return obj.transform.parent == ParentTransform;
        }

        public override void NotifyDragStart(DragAndDropZone zone, DraggableObject draggable)
        {
            draggable.transform.SetParent(ParentTransform);
        }

        public override void NotifyDragEnd(DragAndDropZone zone, DraggableObject draggable)
        {
            if (ReleaseParentOnDrop)
                draggable.transform.SetParent(null);
        }
    }
}
