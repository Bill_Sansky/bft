﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [RequireComponent(typeof(LayerSwitcher))]
    public class LayerSwitchingDragLogic : AbstractDragReleaseLogic
    {
        [BoxGroup("Layer Switcher")] public LayerSwitcher Switcher;


        void Reset()
        {
            Switcher = GetComponent<LayerSwitcher>();
        }


        public override void DragLogic(DragAndDropZone zone, DraggableObject draggable)
        {
        }

        public override void InstantDragLogic(DragAndDropZone zone, DraggableObject draggable)
        {
        }

        public override bool IsDragLogicDone(DragAndDropZone zone, DraggableObject obj)
        {
            return true;
        }

        public override void NotifyDragStart(DragAndDropZone zone, DraggableObject draggable)
        {
            Switcher.SwitchLayer(draggable.gameObject);
        }

        public override void NotifyDragEnd(DragAndDropZone zone, DraggableObject draggable)
        {
            Switcher.RevertSwitch(draggable.gameObject);
        }
    }
}
