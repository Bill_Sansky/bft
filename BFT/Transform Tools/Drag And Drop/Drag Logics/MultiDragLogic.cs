﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine.Events;

namespace BFT
{
    public enum MultiDragMode
    {
        LINEAR,
        PARALLEL
    }

    public class MultiDragLogic : AbstractDragReleaseLogic
    {
        [ShowInInspector, BoxGroup("Status"), ReadOnly]
        private int currentLinearID = -1;

        [BoxGroup("Logics")] public List<AbstractDragReleaseLogic> DragLogics;

        [BoxGroup("Options")] public MultiDragMode Mode = MultiDragMode.LINEAR;
        [BoxGroup("Events")] public UnityEvent OnDragEnd;
        [BoxGroup("Events")] public UnityEvent OnDragStart;

        [BoxGroup("Events")] public UnityEvent OnInstantDrag;
        [BoxGroup("Options")] public bool SetDraggableDynamicOnDrop;

        [BoxGroup("Options")] public bool SetDraggableKinematicOnDrag;

        public override void DragLogic(DragAndDropZone zone, DraggableObject draggable)
        {
            if (Mode == MultiDragMode.LINEAR)
            {
                if (currentLinearID == -1)
                    return;

                if (IsCurrentLinearDragDone(zone, draggable))
                {
                    if (currentLinearID < DragLogics.Count)
                    {
                        DragLogics[currentLinearID].NotifyDragEnd(zone, draggable);
                        currentLinearID++;
                        if (currentLinearID >= DragLogics.Count)
                        {
                            currentLinearID = -1;
                            draggable.IsDragTargetReached = true;
                            return;
                        }

                        DragLogics[currentLinearID].NotifyDragStart(zone, draggable);
                    }
                }
                else
                    DragLogics[currentLinearID].DragLogic(zone, draggable);
            }
            else
            {
                bool reached = true;
                foreach (var dragLogic in DragLogics)
                {
                    dragLogic.DragLogic(zone, draggable);
                    reached = reached && dragLogic.IsDragLogicDone(zone, draggable);
                }

                if (reached)
                {
                    draggable.IsDragTargetReached = true;
                }
            }
        }

        private bool IsCurrentLinearDragDone(DragAndDropZone zone, DraggableObject draggable)
        {
            return DragLogics[currentLinearID].IsDragLogicDone(zone, draggable);
        }

        public override void InstantDragLogic(DragAndDropZone zone, DraggableObject draggable)
        {
            if (SetDraggableKinematicOnDrag)
                draggable.RigidBody.isKinematic = true;

            //no difference here since in the linear case they will still execute in the correct order
            foreach (var dragLogic in DragLogics)
            {
                dragLogic.InstantDragLogic(zone, draggable);
            }

            OnInstantDrag.Invoke();
        }

        public override bool IsDragLogicDone(DragAndDropZone zone, DraggableObject obj)
        {
            if (Mode == MultiDragMode.LINEAR)
            {
                return currentLinearID == -1;
            }

            foreach (var logic in DragLogics)
            {
                if (!logic.IsDragLogicDone(zone, obj))
                    return false;
            }

            return true;
        }

        public override void NotifyDragStart(DragAndDropZone zone, DraggableObject draggable)
        {
            if (SetDraggableKinematicOnDrag)
                draggable.RigidBody.isKinematic = true;

            if (Mode == MultiDragMode.LINEAR)
            {
                currentLinearID = 0;
                DragLogics[0].NotifyDragStart(zone, draggable);
            }
            else
            {
                foreach (var dragLogic in DragLogics)
                {
                    dragLogic.NotifyDragStart(zone, draggable);
                }
            }

            OnDragStart.Invoke();
        }

        public override void NotifyDragEnd(DragAndDropZone zone, DraggableObject draggable)
        {
            if (SetDraggableDynamicOnDrop)
                draggable.RigidBody.isKinematic = false;

            if (Mode == MultiDragMode.LINEAR)
            {
                if (currentLinearID != -1)
                    DragLogics[currentLinearID].NotifyDragEnd(zone, draggable);
            }
            else
            {
                foreach (var dragLogic in DragLogics)
                {
                    dragLogic.NotifyDragEnd(zone, draggable);
                }
            }

            OnDragEnd.Invoke();
        }
    }
}
