﻿using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

#endif
namespace BFT
{
    public class SpeedProfileDragLogic : AbstractDragReleaseLogic
    {
        [BoxGroup("Position")] public bool DragPosition = true;

        [BoxGroup("Rotation")] public bool DragRotation;

        [BoxGroup("Scale")] public bool DragScale;

        private float dragTime;

        [BoxGroup("Position")] public UnityEngine.Transform DropPosition;

        [ShowIf("DragRotation")] [BoxGroup("Rotation")]
        public float MinRotationSpeed = 1;

        [ShowIf("DragScale")] [BoxGroup("Scale")]
        public float MinScaleSpeed = 1;

        [ShowIf("DragRotation")] [BoxGroup("Rotation")]
        public FloatValue RotationSpeedMultiplier;

        [ShowIf("DragScale")] [BoxGroup("Scale")]
        public float ScaleSpeedMultiplier = 1;

        [BoxGroup("Options")] public bool SetDynamicOnDragEnd;

        [BoxGroup("Options")] public bool SetKinematicOnDragStart;
        [BoxGroup("Speed"), InlineEditor] public SpeedProfile SpeedProfile;

        [BoxGroup("Position")] [ShowIf("DragPosition")]
        public float TargetReachedPositionThreshold = .1f;

        [BoxGroup("Rotation")] [ShowIf("DragRotation")]
        public float TargetReachedRotationThreshold = .01f;

        [BoxGroup("Scale")] [ShowIf("DragScale")]
        public float TargetReachedScaleThreshold = .01f;

        public bool IsSpeedProfile => SpeedProfile != null;

        public Vector3 FinalPosition => DropPosition.position;

        public void Awake()
        {
            UnityEngine.Debug.AssertFormat(SpeedProfile, "The speed profile wasn't specified! the zone wont be able to drag", this);
        }

        public override void DragLogic(DragAndDropZone zone, DraggableObject t)
        {
            if (!t)
                return;

            dragTime += UnityEngine.Time.deltaTime;

            Vector3 speed = SpeedProfile.ComputeMovementSpeed(t.transform, DropPosition, t.Velocity);

            Vector3 newPosition = DragPosition ? t.transform.position + speed : t.transform.position;

            var rotation = DragRotationLogic(t, speed);
            Vector3 scale = (!DragScale)
                ? t.transform.localScale
                : Vector3.MoveTowards(t.transform.localScale,
                    DropPosition.localScale, Mathf.Max(speed.magnitude * ScaleSpeedMultiplier, MinScaleSpeed));
            t.HandleDragRequest(newPosition, rotation, scale);
        }

        private Quaternion DragRotationLogic(DraggableObject t, Vector3 speed)
        {
            var rotation = (!DragRotation)
                ? t.transform.rotation
                : Quaternion.RotateTowards(t.transform.rotation, DropPosition.rotation,
                    Mathf.Max(speed.magnitude * RotationSpeedMultiplier.Value, MinRotationSpeed));

            return rotation;
        }

        public override void InstantDragLogic(DragAndDropZone zone, DraggableObject draggable)
        {
            if (!draggable || !DropPosition)
                return;

            if (SetKinematicOnDragStart)
                draggable.RigidBody.isKinematic = true;
            
            draggable.HandleInstantDragRequest((!DragPosition) ? draggable.transform.position : DropPosition.position,
                (!DragRotation) ? draggable.transform.rotation : DropPosition.rotation,
                (!DragScale) ? draggable.transform.localScale : DropPosition.localScale);
        }

        public override bool IsDragLogicDone(DragAndDropZone zone, DraggableObject obj)
        {
            if (!obj)
                return true;

            if (!obj.PreventPositionChange && DragPosition)
            {
                if (!(obj.transform.position.IsDistanceUnder(DropPosition.position,
                    TargetReachedPositionThreshold)))
                    return false;
            }

            if (!obj.PreventRotationChange && DragRotation)
            {
                if (!(Quaternion.Angle(obj.transform.rotation,
                          DropPosition.rotation) < TargetReachedRotationThreshold))
                    return false;
            }

            if (!obj.PreventScaleChange && DragScale)
            {
                if (!obj.transform.localScale.IsDistanceUnder(DropPosition.localScale,
                    TargetReachedScaleThreshold))
                    return false;
            }

            return true;
        }

        public override void NotifyDragStart(DragAndDropZone zone, DraggableObject draggable)
        {
            if (SetKinematicOnDragStart)
                draggable.RigidBody.isKinematic = true;
            dragTime = 0;
        }

        public override void NotifyDragEnd(DragAndDropZone zone, DraggableObject draggable)
        {
            if (SetDynamicOnDragEnd)
                draggable.RigidBody.isKinematic = false;

            draggable.HandleInstantDragRequest((!DragPosition) ? draggable.transform.position : DropPosition.position,
                (!DragRotation) ? draggable.transform.rotation : DropPosition.rotation,
                (!DragScale) ? draggable.transform.localScale : DropPosition.localScale);
        }

#if UNITY_EDITOR
        [FoldoutGroup("Tools"), HideIf("IsSpeedProfile"), Button(ButtonSizes.Medium)]
        public void CreateSpeedProfile()
        {
            SpeedProfile = ScriptableObject.CreateInstance<SpeedProfile>();
        }

        [FoldoutGroup("Tools"), HideIf("IsSpeedProfile"), Button(ButtonSizes.Medium)]
        public void CreateSpeedProfileAsAsset()
        {
            string path = EditorUtility.SaveFilePanelInProject("Save Speed Profile", "Speed Profile", "asset",
                "Save profile", "/Content/");

            AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<SpeedProfile>(), path);
            SpeedProfile = AssetDatabase.LoadAssetAtPath<SpeedProfile>(path);
        }
#endif


#if UNITY_EDITOR

        private bool play;
        private Vector3 demoCurrentPosition;
        private Vector3 demoCurrentSpeed;

        [FoldoutGroup("Tools"), ShowIf("IsSpeedProfile"), Button(ButtonSizes.Medium)]
        public void PlayPreview(UnityEngine.Transform origin)
        {
            play = true;
            demoCurrentPosition = origin.position;
            demoCurrentSpeed = Vector3.zero;
        }


        public void OnDrawGizmosSelected()
        {
            if (play)
            {
                demoCurrentSpeed =
                    SpeedProfile.ComputeMovementSpeed(demoCurrentPosition, DropPosition.position, demoCurrentSpeed);
                demoCurrentPosition = DragPosition ? demoCurrentPosition + demoCurrentSpeed : demoCurrentPosition;
                Gizmos.DrawWireSphere(demoCurrentPosition, .5f);
                if ((demoCurrentPosition - DropPosition.position).magnitude <= TargetReachedPositionThreshold)
                {
                    play = false;
                }

                SceneView.RepaintAll();
            }
        }

#endif
    }
}
