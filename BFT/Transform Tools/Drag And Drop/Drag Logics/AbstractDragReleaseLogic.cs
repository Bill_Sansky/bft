﻿using UnityEngine;

namespace BFT
{
    public abstract class AbstractDragReleaseLogic : MonoBehaviour, ISavedObject
    {
        public virtual object Save()
        {
            return null;
        }

        public virtual void Load(object saveFile)
        {
        }

        public abstract void DragLogic(DragAndDropZone zone, DraggableObject draggable);
        public abstract void InstantDragLogic(DragAndDropZone zone, DraggableObject draggable);

        public abstract bool IsDragLogicDone(DragAndDropZone zone, DraggableObject obj);

        public abstract void NotifyDragStart(DragAndDropZone zone, DraggableObject draggable);
        public abstract void NotifyDragEnd(DragAndDropZone zone, DraggableObject draggable);
    }
}
