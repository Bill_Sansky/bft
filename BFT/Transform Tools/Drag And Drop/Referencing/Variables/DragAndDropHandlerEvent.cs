﻿using System;
using UnityEngine.Events;

namespace BFT
{
    [Serializable]
    public class DragAndDropHandlerEvent : UnityEvent<DragAndDropHandler>
    {
    }
}
