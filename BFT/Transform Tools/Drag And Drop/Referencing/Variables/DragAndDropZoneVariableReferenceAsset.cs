﻿using Sirenix.OdinInspector;

namespace BFT
{
    public class DragAndDropZoneVariableReferenceAsset : SerializedScriptableObject, IValue<DragAndDropZone>
    {
        public IValue<DragAndDropZone> ValueReference;
        public DragAndDropZone Value => ValueReference.Value;
    }
}
