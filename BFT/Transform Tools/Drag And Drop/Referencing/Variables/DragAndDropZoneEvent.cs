﻿using System;
using UnityEngine.Events;

namespace BFT
{
    [Serializable]
    public class DragAndDropZoneEvent : UnityEvent<DragAndDropZone>
    {
    }
}
