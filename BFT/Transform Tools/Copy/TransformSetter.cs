﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public enum ECopyType
    {
        LOCAL,
        GLOBAL
    }

    public class TransformSetter : MonoBehaviour
    {
        [SerializeField] private bool CopyOnEnable = true;

        public bool CopyRegularly;

        [ShowIf("CopyRegularly")] public float CopyTimeInterval = 0;

        [TitleGroup("Position")] public bool CopyPosition = true;

        [TitleGroup("Rotation")] public bool CopyRotation;

        [TitleGroup("Scale")] public bool CopyScale;

        [TitleGroup("Position"), ShowIf("CopyPosition")]
        public ECopyType PositionCopyType;

        [TitleGroup("Rotation"), ShowIf("CopyRotation")]
        public ECopyType RotationCopyType;

        public TransformValue ToCopy;

        public void OnEnable()
        {
            if (CopyOnEnable)
                Copy();
            if (CopyRegularly)
                StartCoroutine(CopyCoroutine());
        }


        private IEnumerator CopyCoroutine()
        {
            while (CopyRegularly)
            {
                Copy();
                yield return new WaitForSeconds(CopyTimeInterval);
            }
        }

        public void OnDrawGizmos()
        {
#if UNITY_EDITOR
            if (Application.isPlaying)
                return;
#endif

            if (ToCopy.Value)
                Copy();
        }

        public virtual void Copy()
        {
            CopyTransform(ToCopy.Value);
        }

        public void StartAutoCopy()
        {
            CopyOnEnable = true;
            StartCoroutine(CopyCoroutine());
        }

        public void StopAutoCopy()
        {
            CopyOnEnable = false;
        }

        protected void CopyTransform(UnityEngine.Transform copyTransform)
        {
            if (CopyPosition)
            {
                if (PositionCopyType == ECopyType.GLOBAL)
                    transform.position = copyTransform.position;
                else
                {
                    transform.localPosition = copyTransform.localPosition;
                }
            }

            if (CopyRotation)
            {
                if (RotationCopyType == ECopyType.GLOBAL)
                    transform.rotation = copyTransform.rotation;
                else
                {
                    transform.localRotation = copyTransform.localRotation;
                }
            }

            if (CopyScale)
                transform.localScale = copyTransform.localScale;
        }
    }
}