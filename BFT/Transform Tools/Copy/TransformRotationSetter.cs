using System;
using UnityEngine;

namespace BFT
{
    public class TransformRotationSetter : MonoBehaviour
    {
        public Vector3Value AnglesToSet;

        public bool SetOnEnable;
        public bool SetEveryFrame;

        public UnityEngine.Transform ToSet;

        public void SetAngles()
        {
            ToSet.rotation = Quaternion.Euler(AnglesToSet.Value);
        }

        public void OnEnable()
        {
            if (SetOnEnable)
            {
                SetAngles();
            }

            if (SetEveryFrame)
                this.CallEveryFrame(SetAngles);
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }
    }
}