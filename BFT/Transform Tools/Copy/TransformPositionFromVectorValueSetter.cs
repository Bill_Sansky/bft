﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class TransformPositionFromVectorValueSetter : MonoBehaviour
    {
        public bool CopyOnEnable;

        public bool CopyRegularly;

        [ShowIf("CopyRegularly")] public float CopyTimeInterval = 0;

        [BoxGroup("Options")] public ECopyType CopyType;
        [BoxGroup("To Copy")] public Vector3Value VectorValue;

        public void OnEnable()
        {
            if (CopyOnEnable)
            {
                CopyValue();
                if (CopyRegularly)
                    StartToCopy();
            }
        }

        private void StartToCopy()
        {
            this.CallRegularly(CopyValue, CopyTimeInterval);
        }

        public void CopyValue()
        {
            if (VectorValue == null)
                return;
            CopyExternalPosition(VectorValue.Value);
        }

        public void CopyExternalPosition(Vector3 toCopyPosition)
        {
            if (CopyType == ECopyType.GLOBAL)
                transform.position = toCopyPosition;
            else
            {
                transform.localPosition = toCopyPosition;
            }
        }

        public void OnDrawGizmos()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying && CopyOnEnable && VectorValue != null && enabled)
                CopyValue();
#endif
        }
    }
}
