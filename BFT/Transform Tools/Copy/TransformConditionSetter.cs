﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{

    public class TransformConditionSetter : TransformSetter
    {
        [BoxGroup("Condition")] public BoolValue CopyCondition;

        [BoxGroup("Condition")] public bool CopyOnTrue;

        public override void Copy()
        {
            if ((CopyCondition.Value && CopyOnTrue)
                || (!CopyCondition.Value && CopyOnTrue))
            {
                base.Copy();
            }
        }
    }
}
