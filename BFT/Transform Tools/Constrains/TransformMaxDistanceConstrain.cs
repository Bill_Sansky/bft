﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class TransformMaxDistanceConstrain : AbstractTransformConstraint, IValue<bool>
    {
        [BoxGroup("Constraints")] public Transform toConstrain;
        
        [BoxGroup("Constraints")] public FloatValue MaxDistance;

        [BoxGroup("Constraints")] public TransformValue Reference;

        [ShowInInspector] [BoxGroup("Utils")]
        public bool ConstrainInEditMode;

        [BoxGroup("Utils")] public bool LogDebug;
        [BoxGroup("Status"), ReadOnly,ShowInInspector]
        private bool isConstrained;
        
        public bool Value => !isConstrained;

        void Reset()
        {
            toConstrain = transform;
        }
        
        public override void OnEnable()
        {
            base.OnEnable();
#if UNITY_EDITOR
            if (!Reference.Value || MaxDistance == null)
            {
                UnityEngine.Debug.LogWarningFormat(this,
                    "The reference transform or the distance are missing on {0}, disabling the component",
                    gameObject.name);
                enabled = false;
                return;
            }
#endif
        }

        [BoxGroup("Status"), ReadOnly,ShowInInspector] private float currentDistance;

        public override void Constrain()
        {
#if UNITY_EDITOR
            if (!Reference.Value || MaxDistance == null)
            {
                UnityEngine.Debug.LogWarningFormat(this,
                    "The reference transform or the distance are missing on {0}, disabling the component",
                    gameObject.name);
                enabled = false;
                return;
            }
#endif

            isConstrained = false;
            Vector3 distance = (toConstrain.position - Reference.position);
            currentDistance = distance.magnitude;
            if (distance.magnitude > MaxDistance.Value)
            {
                isConstrained = true;

                if (LogDebug)
                {
                    UnityEngine.Debug.LogFormat(this, "Transform {0} Constrained", name);
                }

                distance = Vector3.ClampMagnitude(distance, MaxDistance.Value);
                toConstrain.position = Reference.position + distance;
            }
        }

        [BoxGroup("Utils")]
        [Button(ButtonSizes.Medium)]
        public void SetConstrainToCurrentDistance()
        {
            if (!Reference.Value)
            {
                return;
            }

            float distance = (toConstrain.position - Reference.position).magnitude;

            MaxDistance.LocalValue = distance;
        }

#if UNITY_EDITOR

        [BoxGroup("Utils")] public bool ShowGizmos;

        public void OnDrawGizmosSelected()
        {
            if (!enabled || !ShowGizmos || !Reference.Value || MaxDistance == null || !ConstrainInEditMode)
            {
                return;
            }

            Gizmos.color = Color.cyan.Alphaed(.2f);
            Gizmos.DrawSphere(Reference.position, MaxDistance.Value);
            HandleExt.Text(toConstrain.position + Vector3.forward * (MaxDistance.Value + 1), "Clamping Distance", true);

            if (!Application.isPlaying)
            {
                Constrain();
            }
        }

        public void OnDrawGizmos()
        {
            if (enabled && !Application.isPlaying && ConstrainInEditMode)
            {
                Constrain();
            }
        }
#endif
    }
}


/*
public class TransformChainMaxDistanceConstrain : AbstractTransformConstraint, IGenericValue<bool>
{
    [BoxGroup("Constraints")]
    public List<Transform> ChainTransformsBefore;
    public List<Transform> ChainTransformsAfter;

    [BoxGroup("Constraints")]
    public IGenericValue<float> MaxDistanceBetweenChainElements;

    [BoxGroup("Status"), ShowInInspector, ReadOnly]
    private bool isConstrained;

    [BoxGroup("Utils")]
    public bool ConstrainInEditMode;

    [BoxGroup("Utils")]
    public bool LogDebug;

    public override void OnEnable()
    {
        base.OnEnable();
#if UNITY_EDITOR
        if (!ChainTransformsBefore || MaxDistanceBetweenChainElements == null)
        {
            Debug.LogWarningFormat(this, "The reference transform or the distance are missing on {0}, disabling the component", gameObject.name);
            enabled = false;
            return;
        }
#endif

    }

    public override void Constrain()
    {
#if UNITY_EDITOR
        if (!ChainTransformsBefore || MaxDistanceBetweenChainElements == null)
        {
            Debug.LogWarningFormat(this, "The reference transform or the distance are missing on {0}, disabling the component", gameObject.name);
            enabled = false;
            return;
        }
#endif

        isConstrained = false;
        Vector3 distance = (transform.position - ChainTransformsBefore.position);
        if (distance.magnitude > MaxDistanceBetweenChainElements.Value)
        {
            isConstrained = true;

            if (LogDebug)
                Debug.LogFormat(this, "Transform {0} Constrained", name);

            distance = Vector3.ClampMagnitude(distance, MaxDistanceBetweenChainElements.Value);
            transform.position = ChainTransformsBefore.position + distance;
        }
    }

    public bool Value => !isConstrained;

#if UNITY_EDITOR

    [BoxGroup("Utils")]
    public bool ShowGizmos;

    public void OnDrawGizmosSelected()
    {

        if (!enabled || !ShowGizmos || !ChainTransformsBefore || MaxDistanceBetweenChainElements == null || !ConstrainInEditMode)
        {
            return;
        }

        Gizmos.color = Color.cyan.Alphaed(.2f);
        Gizmos.DrawSphere(ChainTransformsBefore.position, MaxDistanceBetweenChainElements.Value);
        HandleExt.Text(transform.position + Vector3.forward * (MaxDistanceBetweenChainElements.Value + 1), "Clamping Distance", true);

        if (!Application.isPlaying)
        {
            Constrain();
        }
    }

    public void OnDrawGizmos()
    {
        if (enabled && !Application.isPlaying && ConstrainInEditMode)
        {
            Constrain();
        }
    }
#endif

}*/