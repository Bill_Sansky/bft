using System;
using UnityEditor;
using UnityEngine;

namespace BFT
{
    [ExecuteAlways]
    public class BoxTeleportConstrain : MonoBehaviour
    {
        public Transform BoxCenter;
        public float XSize;
        public float YSize;
        public float ZSize;

        public void SetCenter(Transform center)
        {
            BoxCenter = center;
        }
        public void LateUpdate()
        {
            if (!BoxCenter)
                return;

            if (transform.position.x > BoxCenter.position.x + XSize)
                transform.position =
                    transform.position.WithSetX(BoxCenter.position.x - XSize +
                                                ((transform.position.x - BoxCenter.position.x + XSize) % XSize));
            else if (transform.position.x < BoxCenter.position.x - XSize)
                transform.position =
                    transform.position.WithSetX(BoxCenter.position.x + XSize +
                                                ((transform.position.x - (BoxCenter.position.x - XSize)) % XSize));
            
            if (transform.position.y > BoxCenter.position.y + YSize)
                transform.position =
                    transform.position.WithSetY(BoxCenter.position.y - YSize +
                                                ((transform.position.y - BoxCenter.position.y + YSize) % YSize));
            else if (transform.position.y < BoxCenter.position.y - YSize)
                transform.position =
                    transform.position.WithSetY(BoxCenter.position.y + YSize +
                                                ((transform.position.y - (BoxCenter.position.y - YSize)) % YSize));
            
            if (transform.position.z > BoxCenter.position.z + ZSize)
                transform.position =
                    transform.position.WithSetZ(BoxCenter.position.z - ZSize +
                                                ((transform.position.z - BoxCenter.position.z + ZSize) % ZSize));
            else if (transform.position.z < BoxCenter.position.z - ZSize)
                transform.position =
                    transform.position.WithSetZ(BoxCenter.position.z + ZSize +
                                                ((transform.position.z - (BoxCenter.position.z - ZSize)) % ZSize));
            
        }

        public void OnDrawGizmosSelected()
        {
            if (!BoxCenter)
                return;

            Gizmos.color = Color.red;

            Gizmos.DrawLine(BoxCenter.position + XSize * Vector3.right - Vector3.up * 100,
                BoxCenter.position + XSize * Vector3.right + Vector3.up * 100);
            Gizmos.DrawLine(BoxCenter.position - XSize * Vector3.right - Vector3.up * 100,
                BoxCenter.position - XSize * Vector3.right + Vector3.up * 100);
            
            Gizmos.color = Color.green;
            
            Gizmos.DrawLine(BoxCenter.position + YSize * Vector3.up - Vector3.right * 100,
                BoxCenter.position + YSize * Vector3.up + Vector3.right * 100);
            Gizmos.DrawLine(BoxCenter.position - YSize * Vector3.up - Vector3.right * 100,
                BoxCenter.position - YSize * Vector3.up + Vector3.right * 100);
            
            Gizmos.color = Color.blue;
            
            Gizmos.DrawLine(BoxCenter.position + ZSize * Vector3.forward - Vector3.right * 100,
                BoxCenter.position + ZSize * Vector3.forward + Vector3.right * 100);
            Gizmos.DrawLine(BoxCenter.position - ZSize * Vector3.forward - Vector3.right * 100,
                BoxCenter.position - ZSize * Vector3.forward + Vector3.right * 100);
        }
    }
}