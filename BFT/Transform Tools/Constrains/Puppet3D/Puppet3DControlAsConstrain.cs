﻿#if BFT_PUPPET3D
using Puppet3D;
using UnityEngine;

public class Puppet3DControlAsConstrain : AbstractTransformConstraint
{
    public GlobalControl Control;

    void Reset()
    {
        Control = GetComponent<GlobalControl>();
        if (Control)
            Control.ControlsEnabled = false;

        Order = 999;
    }

    public override void Constrain()
    {
        Control.Run();
    }
}
#endif