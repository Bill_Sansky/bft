
using BFT;
using UnityEngine;
#if BFT_CURVY
using System.Collections;
using FluffyUnderware.Curvy.Controllers;
using Sirenix.OdinInspector;

public class CurvyPositionFromVariable : MonoBehaviour
{
    public CurvyController Controller;
    public FloatValue PositionValue;

    public bool UpdateEveryFrame;

    public bool UpdateOnEnable;

    public void OnEnable()
    {
        if (UpdateOnEnable)
            UpdatePosition();

        if (UpdateEveryFrame)
            StartToUpdateEveryFrame();
    }

    public void StartToUpdateEveryFrame()
    {
        StartCoroutine(UpdatePositionEveryFrame());
    }

    public void StopToUpdateEveryFrame()
    {
        StopAllCoroutines();
    }

    private IEnumerator UpdatePositionEveryFrame()
    {
        while (true)
        {
            UpdatePosition();
            yield return null;
        }
    }

    private void Reset()
    {
        Controller = GetComponent<CurvyController>();
    }

    public void UpdatePosition()
    {
        Controller.Position = PositionValue.Value;
    }
}
#endif