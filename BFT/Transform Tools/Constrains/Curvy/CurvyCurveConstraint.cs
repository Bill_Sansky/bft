﻿
using BFT;
#if BFT_CURVY
using FluffyUnderware.Curvy;
using UnityEngine;
using Sirenix.OdinInspector;

public class CurvyCurveConstraint : MonoBehaviour
{
    public CurvySpline Curve;
    public TransformValue LookUpTransform;
    public bool AutoConstrain;

    void Reset()
    {
        LookUpTransform.LocalValue = transform;
    }

    public void LateUpdate()
    {
        if (AutoConstrain)
        {
            UpdateConstrainedPosition();
        }
    }

    [OnInspectorGUI]
    private void UpdateConstrainedPosition()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying && (!Curve || !LookUpTransform.Value))
        {
            return;
        }
#endif
        var lookupPos = Curve.transform.InverseTransformPoint(LookUpTransform.Value.position);
        float nearestTf = Curve.GetNearestPointTF(lookupPos);
        transform.position = Curve.transform.TransformPoint(Curve.Interpolate(nearestTf));
    }

}

#endif