﻿
using BFT;
#if BFT_CURVY
using FluffyUnderware.Curvy;
using FluffyUnderware.Curvy.Controllers;
using UnityEngine;

public class CurvyPositionTween : UpdateTypeSwitchable
{
    public CurvyController Controller;
    public CurvySpline Curve;
    public CurvyController ControllerToFollow;
    public AnimationCurve EasingCurve;
    public SpeedProfile SpeedProfile;

    private

        void Reset()
    {
        Controller = GetComponent<CurvyController>();
    }

    public override void UpdateMethod()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying && (!SpeedProfile || !Controller ||
                                       !Curve || !ControllerToFollow))
        {
            return;
        }
#endif
        float distance = ControllerToFollow.Position - Controller.Position;
        float wantedSpeedMag = MathExt.EvaluateCurve(SpeedProfile.SpeedAtMinDistance,
            SpeedProfile.SpeedAtMaxDistance, SpeedProfile.Profile,
           Mathf.Abs(distance),
            SpeedProfile.MinDistance, SpeedProfile.MaxDistance);
        Controller.Position += wantedSpeedMag * Mathf.Sign(distance)*Time.deltaTime;
    }
}

#endif