#if UNITY_EDITOR
#endif
using UnityEngine;

namespace BFT
{
    [ExecuteAlways]
    public class TransformPivotScale : MonoBehaviour
    {
        public TransformSaveState TargetInitState;

        public Vector3Value TargetScale;
        public Transform TransformToScale;

        public void OnEnable()
        {
            InitTarget();
        }

        public void InitTarget()
        {
            TargetInitState.RegisterState(TransformToScale.transform);
        }


        public void Scale()
        {
            var prevParent = TransformToScale.parent;
            //   ScaleAround(TransformToScale, transform, transform.localScale);

            transform.localScale = Vector3.one;
            TargetInitState.RestoreState(TransformToScale);
            TransformToScale.SetParent(transform, true);
            transform.localScale = TargetScale.Value;
            TransformToScale.SetParent(prevParent, true);

            //  previousState.RestoreState(transform);
        }

        public static void ScaleAround(UnityEngine.Transform target, UnityEngine.Transform pivot, Vector3 scale)
        {
            UnityEngine.Transform pivotParent = pivot.parent;
            Vector3 pivotPos = pivot.position;
            pivot.parent = target;
            target.localScale = scale;
            target.position += pivotPos - pivot.position;
            pivot.parent = pivotParent;
        }

        public void Update()
        {
            if (TransformToScale)
                Scale();
        }

        public void OnDrawGizmosSelected()
        {
            /*  Attachment.matrix = transform.localToWorldMatrix;
          Attachment.TransformHandle(ref Modification.Position, ref Modification.Rotation, ref Modification.LocalScale);*/
        }
    }
}
