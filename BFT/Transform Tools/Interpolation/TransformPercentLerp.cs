﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class TransformPercentLerp : SerializedMonoBehaviour
    {
        public PercentValue Percent;
        public TransformValue LerpedTransform;

        public TransformValue TransformA;
        public TransformValue TransformB;

        void Reset()
        {
            LerpedTransform.LocalValue = transform;
        }

        public void Update()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                if (Percent != null && TransformA.Value && TransformB.Value)
                    LerpedTransform.Value.LerpPositionRotation(TransformA.Value, TransformB.Value, DebugRange);
                return;
            }
#endif
            LerpedTransform.Value.LerpPositionRotation(TransformA.Value, TransformB.Value, Percent.Value);
        }


        [Button]
        public void MoveToTransformA()
        {
            LerpedTransform.Value.LerpPositionRotation(TransformA.Value, TransformB.Value, 0);
#if UNITY_EDITOR
            DebugRange = 0;
#endif
        }

        [Button]
        public void MoveToTransformB()
        {
            LerpedTransform.Value.LerpPositionRotation(TransformA.Value, TransformB.Value, 1);
#if UNITY_EDITOR
            DebugRange = 1;
#endif
        }

        [Button]
        public void CopyTransformIntoA()
        {
            TransformA.position = LerpedTransform.Value.position;
            TransformA.rotation = LerpedTransform.Value.rotation;
        }

        [Button]
        public void CopyTransformIntoB()
        {
            TransformB.position = LerpedTransform.Value.position;
            TransformB.rotation = LerpedTransform.Value.rotation;
        }

#if UNITY_EDITOR

        public bool DebugInEditor;

        [Range(0, 1)] public float DebugRange;

#endif
    }
}