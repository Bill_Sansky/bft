using System.Collections;
using UnityEngine;

namespace BFT
{
    public class TransformStateLerp : MonoBehaviour
    {
        public bool CalculateOnEnable = false;

        public bool LerpPosition = true;
        public bool LerpRotation = true;
        public bool LerpScale = true;

        public PercentValue Percent;

        public bool StartLerpOnEnable = false;

        public TransformSceneStateAsset StartTransformState;
        public TransformSceneStateAsset EndTransformState;

        public TransformValue TransformToLerp;

        public void OnEnable()
        {
            if (CalculateOnEnable)
                CalculateTransform();
            if (StartLerpOnEnable)
                StartLerping();
        }

        public void CalculateTransform()
        {
            if (LerpPosition)
            {
                TransformToLerp.position =
                    Vector3.Lerp(StartTransformState.Position, EndTransformState.Position, Percent.Value);
            }

            if (LerpRotation)
            {
                TransformToLerp.rotation =
                    Quaternion.Lerp(StartTransformState.Rotation, EndTransformState.Rotation, Percent.Value);
            }

            if (LerpScale)
            {
                TransformToLerp.localScale =
                    Vector3.Lerp(StartTransformState.Scale, EndTransformState.Scale, Percent.Value);
            }
        }

        public void StartLerping()
        {
            StartCoroutine(LerpOverTime());
        }

        public void StopLerping()
        {
            StopAllCoroutines();
        }

        private IEnumerator LerpOverTime()
        {
            while (true)
            {
                CalculateTransform();
                yield return null;
            }
        }
    }
}
