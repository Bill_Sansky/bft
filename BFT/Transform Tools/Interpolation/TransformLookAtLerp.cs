﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class TransformLookAtLerp : UpdateTypeSwitchable
    {
        [BoxGroup("Lerp Target")] public Transform Target;

        [BoxGroup("Lerp Target")] public bool FaceAway;

        [BoxGroup("Lerp Options")] public bool ClampYAngle;

        [BoxGroup("Lerp Options")] public FloatValue Speed;

        [BoxGroup("Lerp Options"), ShowIf("ClampYAngle")]
        public float MaxYAngle;

        [BoxGroup("Lerp Options")] public bool FilterRotationAxes;

        [ShowIf("FilterRotationAxes")] [BoxGroup("Lerp Options")]
        public Vector3 LookAtAxisFilter = Vector3.one;

        [BoxGroup("Look At")] public TransformValue ToLookAt;

        [BoxGroup("Lerp Options")] [SerializeField]
        private bool useUnscaledDeltaTime;

        [BoxGroup("Initialization")] [SerializeField]
        public bool ResetOrientationOnDisable = true;

        [BoxGroup("Initialization")] [SerializeField]
        public bool InstantLookAtOnEnable = true;
        
        [BoxGroup("Status"), ShowInInspector] private Quaternion startLocalRotation;

        [BoxGroup("Status"), ShowInInspector] private bool stopping;

        public UnityEvent OnInitialStateRestored;

        void Reset()
        {
            Target = transform;
        }

        public void Awake()
        {
            startLocalRotation = Target.localRotation;
        }

        public void OnDisable()
        {
            stopping = false;
            if (ResetOrientationOnDisable)
                Target.localRotation = startLocalRotation;
        }

        public void OnEnable()
        {
            if(InstantLookAtOnEnable)
                InstantLookAt();
            
            stopping = false;
            StopAllCoroutines();
        }

        public void Start()
        {
            if (FilterRotationAxes)
                Target.rotation.eulerAngles.Mult(LookAtAxisFilter);
        }

        public void LerpBackToInitialAndDisable()
        {
            if (stopping)
                return;

            stopping = true;
            this.CallEveryFrame(CheckIfBackToStart);
        }

        public void StartToLerp()
        {
            stopping = false;
            StopAllCoroutines();
            enabled = true;
        }

        private void CheckIfBackToStart()
        {
            if (Target.rotation == startLocalRotation)
            {
                enabled = false;
                OnInitialStateRestored.Invoke();
                StopAllCoroutines();
            }
        }

        // Update is called once per frame
        public override void UpdateMethod()
        {
            if (ToLookAt != null)
            {
                Quaternion old;
                Quaternion newRot;

                if (stopping)
                {
                    var localRotation = Target.localRotation;
                    old = localRotation;
                    localRotation = startLocalRotation;
                    Target.localRotation = localRotation;
                    newRot = localRotation;
                }
                else
                {
                    old = Target.rotation;
                    InstantLookAt();
                    newRot = Target.rotation;
                }

                if (!stopping && FilterRotationAxes)
                {
                    if (ClampYAngle)
                    {
                        float y = Target.eulerAngles.y;
                        if (y > 180)
                            y = -(360 - y);
                        y = Mathf.Clamp(y, -MaxYAngle, MaxYAngle);
                        var eulerAngles = Target.eulerAngles;
                        Target.rotation = Quaternion.Euler(eulerAngles.x * LookAtAxisFilter.x,
                            y * LookAtAxisFilter.y,
                            eulerAngles.z * LookAtAxisFilter.z);
                    }

                    newRot = Quaternion.Euler(Target.rotation.eulerAngles.Mult(LookAtAxisFilter));
                }

                Quaternion lerped = Quaternion.Lerp(old, newRot,
                    (useUnscaledDeltaTime ? UnityEngine.Time.unscaledDeltaTime : UnityEngine.Time.deltaTime) *
                    Speed.Value);

                if (stopping)
                {
                    Target.localRotation = lerped;
                }
                else
                {
                    Target.rotation = lerped;
                }
            }
        }

        private void InstantLookAt()
        {
            if (!FaceAway)
                Target.LookAt(ToLookAt.Value);
            else
            {
                var position = Target.position;
                Target.LookAt(position - (ToLookAt.position - position), Vector3.up);
            }
        }
    }
}