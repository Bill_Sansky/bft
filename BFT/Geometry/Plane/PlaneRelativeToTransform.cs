﻿using UnityEngine;

namespace BFT
{
    public class PlaneRelativeToTransform : MonoBehaviour, IPlaneGiver
    {
        [SerializeField] private Vector3 inNormal;

        private UnityEngine.Plane plane;

        [SerializeField] private UnityEngine.Transform relativeTransform;

        public UnityEngine.Plane GetPlane => plane;

        private void Awake()
        {
            plane = new UnityEngine.Plane(inNormal, relativeTransform.position);
        }

        private void Update()
        {
            if (relativeTransform.hasChanged)
            {
                plane = new UnityEngine.Plane(inNormal, relativeTransform.position);
            }
        }
    }
}
