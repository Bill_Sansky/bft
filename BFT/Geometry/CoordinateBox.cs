﻿using System;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class CoordinateBox
    {
        public Vector3 center;
        public Vector3 extends;

        public CoordinateBox(Vector3 center, Vector3 extends)
        {
            this.center = center;
            this.extends = extends;
        }

        public Vector3 TopRightFront => center + extends / 2;

        public Vector3 TopLeftFront => center + extends.Mult(-1, 1, 1) / 2;

        public Vector3 BottomRightFront => center + extends.Mult(1, -1, 1) / 2;

        public Vector3 BottomLeftFront => center + extends.Mult(-1, -1, 1) / 2;

        public Vector3 TopRightBack => center + extends.Mult(1, 1, -1) / 2;

        public Vector3 TopLeftBack => center + extends.Mult(-1, 1, -1) / 2;

        public Vector3 BottomRightBack => center + extends.Mult(1, -1, -1) / 2;

        public Vector3 BottomLeftBack => center + extends.Mult(-1, -1, -1) / 2;

        public void MoveCenter(Vector3 newCenter)
        {
            center = newCenter;
        }
    }
}
