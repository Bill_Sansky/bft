﻿using UnityEngine;

namespace BFT
{
    /// <summary>
    ///     Gathers points of interest thanks to a trigger attached to it, and communicates it to a barycenter calculator to
    ///     updte it real time
    /// </summary>
    [RequireComponent(typeof(Collider))]
    public class PointOfInterestGatherer : MonoBehaviour
    {
        public delegate void OnPointOfInterest(SphereCollider poi);

        public Barycenter BarycenterToUpdate;

        public event OnPointOfInterest PointOfInterestIn;
        public event OnPointOfInterest PointOfInterestOut;

        public void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.CompareTag(tag))
            {
                if (PointOfInterestIn != null)
                    PointOfInterestIn(other as SphereCollider);
                PointOfInterest point = other.GetComponent<PointOfInterest>();
                BarycenterToUpdate.AddTransform(other.transform, point.Weight);
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (other.gameObject.CompareTag(tag))
            {
                if (PointOfInterestOut != null) PointOfInterestOut(other as SphereCollider);
                BarycenterToUpdate.RemoveTransform(other.transform);
            }
        }
    }
}
