﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    [AddComponentMenu("Math/Barycenter")]
    public class Barycenter : MonoBehaviour
    {
        [Tooltip("The speed at which the transform will move to the actual barycenter")]
        public float Speed;

        /// <summary>
        /// TODO change the dictionary type
        /// </summary>
        public Dictionary<UnityEngine.Transform, float> Transforms = new Dictionary<UnityEngine.Transform, float>();

        public void AddTransform(UnityEngine.Transform t, float weight)
        {
            if (!Transforms.ContainsKey(t))
            {
                Transforms.Add(t, weight);
            }
            else
            {
                Transforms[t] += weight;
            }
        }


        public void ModifyWeight(UnityEngine.Transform t, float weight)
        {
            if (!Transforms.ContainsKey(t))
            {
                Transforms.Add(t, weight);
            }
            else
            {
                Transforms[t] = weight;
            }
        }

        public void RemoveTransform(UnityEngine.Transform t)
        {
            Transforms.Remove(t);
        }


        public bool ContainsTransform(UnityEngine.Transform trans)
        {
            return Transforms.ContainsKey(trans);
        }

        public void Update()
        {
            Vector4[] coordinateAndWeigth = new Vector4[Transforms.Count];

            int i = 0;
            foreach (var transWeight in Transforms)
            {
                Vector3 key = transWeight.Key.position;
                coordinateAndWeigth[i] = key.Vector4(transWeight.Value);
                i++;
            }

#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                transform.position = Vector3.Lerp(transform.position, MathExt.Barycenter(coordinateAndWeigth), Speed);
                return;
            }
#endif
            transform.position = Vector3.Lerp(transform.position, MathExt.Barycenter(coordinateAndWeigth),
                UnityEngine.Time.deltaTime * Speed);
        }

#if DEBUG

        public bool ShowDebug = true;
        public float DebugRadius = 1;

        public void OnDrawGizmosSelected()
        {
            if (!ShowDebug)
                return;

            Gizmos.color = Color.blue.Alphaed(.5f);
            Gizmos.DrawSphere(transform.position, DebugRadius);
            Gizmos.color = Color.yellow.Alphaed(.5f);

            foreach (UnityEngine.Transform trans in Transforms.Keys)
            {
                Gizmos.DrawSphere(trans.position, DebugRadius);
            }
        }


#endif
    }
}
