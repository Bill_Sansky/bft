﻿using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Math/Point Of Interest")]
    public class PointOfInterest : MonoBehaviour
    {
        public float Weight;
    }
}
