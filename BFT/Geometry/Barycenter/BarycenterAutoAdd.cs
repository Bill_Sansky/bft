﻿using System.Collections;
using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Math/Barycenter Auto Add")]
    public class BarycenterAutoAdd : MonoBehaviour
    {
        Barycenter pos;
        public string TagToLookFor;
        public UnityEngine.Transform[] Transforms;
        public float[] Weights;

        void Start()
        {
            pos = GetComponent<Barycenter>();
            for (int i = 0; i < Transforms.Length; i++)
            {
                pos.AddTransform(Transforms[i], Weights[i]);
            }

            if (TagToLookFor != "")
                StartCoroutine(AddWeigth());
        }

        IEnumerator AddWeigth()
        {
            while (true)
            {
                GameObject[] objs = GameObject.FindGameObjectsWithTag(TagToLookFor);
                foreach (GameObject obj in objs)
                {
                    pos.AddTransform(obj.transform, 1);
                }

                yield return new WaitForSeconds(.300f);
            }
        }
    }
}
