﻿using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Math/Curves/Dynamic Segment Path")]
    public class DynamicSegmentPath : SegmentPath
    {
        public override void InsertPointBefore(int index, Vector3 point)
        {
            SimpleSegmentPoint newPoint = new SimpleSegmentPoint(point, this);
            InsertSegmentBefore(newPoint, Segments[index]);
        }

        public override void AddPoints(SegmentPointCreator curve)
        {
            /*
        SegmentPoint[] temp = curve.ComputeSegmentPoints(this);
        temp[0].lastIndex = points.Count-1;
        points.AddRange(temp);*/
        }

        public override void AddPoint(Vector3 point)
        {
            SimpleSegmentPoint newPoint = new SimpleSegmentPoint(point, this);
            InsertSegmentAfter(newPoint, Last);
        }

        public override void InsertPointAfter(Vector3 point, SegmentPoint after)
        {
            throw new System.NotImplementedException();
        }

        public override void ResetPath(Vector3 newOrigin)
        {
            Segments.Clear();
            nextIdToGive = 0;
            firstIndex = 0;
            AddPoint(newOrigin);
        }
    }
}
