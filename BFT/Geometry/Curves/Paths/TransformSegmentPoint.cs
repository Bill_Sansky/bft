﻿using System;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class TransformSegmentPoint : SegmentPoint
    {
        public UnityEngine.Transform point;

        public TransformSegmentPoint(UnityEngine.Transform trans, SegmentPath path) : base(path)
        {
            point = trans;
        }

        public override Vector3 StartLocal
        {
            get => point.position;
            set => point.position = value;
        }
    }
}
