using System;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public abstract class SegmentPoint
    {
        public int index;
        public int lastIndex = -1;
        public int nextIndex = -1;

        [SerializeField] private SegmentPath path;

        public SegmentPoint(SegmentPath path)
        {
            this.path = path;
        }

        public Vector3 StartTrans => path.transform.TransformPoint(StartLocal);

        public SegmentPoint Next => path.Segments[nextIndex];

        public SegmentPoint Last => path.Segments[lastIndex];

        public Vector3 NextDirection => (Next.StartLocal - StartLocal).normalized;

        public Vector3 LastDirection => (Last.StartLocal - StartLocal).normalized;

        //keep commented for further usages

        /*
    public Vector3 NextDirectionTrans
    {
        get { return path.transform.TransformDirection(NextDirectionLocal); }
    }

    public Vector3 LastDirectionTrans
    {
        get { return path.transform.TransformDirection(LastDirectionLocal); }
    }
    */

        public bool HasNext => nextIndex != -1;

        public bool HasLast => lastIndex != -1;
        public abstract Vector3 StartLocal { get; set; }

        public int Index
        {
            get => index;
            set => index = value;
        }

        //keep commented for further usages

        /*
    public float LengthTrans()
    {
        return path.transform.TransformVector((Next.StartLocal - StartLocal)).magnitude;
    }*/

        public float Length()
        {
            return (Next.StartLocal - StartLocal).magnitude;
        }

        public float LengthSqrt()
        {
            return (Next.StartLocal - StartLocal).sqrMagnitude;
        }


        public Vector3 ProjectOn(Vector3 toProject)
        {
            return toProject.ProjectOnSegment(StartLocal, Next.StartLocal);
        }

        public Vector3 NextPosition(Vector3 currentLocal, float linearLocalSpeed, ref SegmentPoint newSegmentAtPosition)
        {
            if (!HasNext)
                return StartLocal;

            if (linearLocalSpeed < 0)
                return LastPosition(currentLocal, linearLocalSpeed, ref newSegmentAtPosition);

            float left = (Next.StartLocal - StartLocal - Vector3.Project(currentLocal - StartLocal, NextDirection))
                .magnitude;

            if (left > linearLocalSpeed)
            {
                return currentLocal + linearLocalSpeed * NextDirection;
            }

            if (Next == this)
                return currentLocal;

            newSegmentAtPosition = Next;
            return Next.NextPosition(Next.StartLocal, linearLocalSpeed - left, ref newSegmentAtPosition);
        }

        //keep commented for further usages

        /*
    public Vector3 NextPositionTrans(Vector3 current, float linearSpeed,
        ref SegmentPoint newSegmentAtPosition)
    {
        return
            path.transform.TransformPoint(NextPositionLocal(path.transform.InverseTransformPoint(current),
                path.transform.InverseTransformVector(linearSpeed * Vector3.one).magnitude, ref newSegmentAtPosition));
    }
    */
        public Vector3 LastPosition(Vector3 currentLocal, float linearSpeed, ref SegmentPoint newSegmentAtPosition)
        {
            if (linearSpeed > 0)
                return NextPosition(currentLocal, linearSpeed, ref newSegmentAtPosition);

            float left = (Last.StartLocal - StartLocal - Vector3.Project(currentLocal - StartLocal, LastDirection))
                .magnitude;

            if (left > Mathf.Abs(linearSpeed))
            {
                return currentLocal + linearSpeed * LastDirection;
            }

            if (left <= 0)
                return currentLocal;

            newSegmentAtPosition = Last;
            return Last.LastPosition(Last.StartLocal, linearSpeed - left, ref newSegmentAtPosition);
        }

        //keep commented for further usages

        /*
    public Vector3 LastPositionTrans(Vector3 current, float linearSpeed,
        ref SegmentPoint newSegmentAtPosition)
    {
        return
            path.transform.TransformPoint(LastPositionLocal(path.transform.InverseTransformPoint(current),
                path.transform.InverseTransformVector(linearSpeed * Vector3.one).magnitude, ref newSegmentAtPosition));
    }*/

        /// <summary>
        ///     The local position is the one relative to the transform of the path
        /// </summary>
        /// <param name="currentLocal"></param>
        /// <param name="velocity"></param>
        /// <param name="seg"></param>
        /// <returns></returns>
        public Vector3 NextPositionInDirection(Vector3 currentLocal, Vector3 velocity,
            ref SegmentPoint seg)
        {
            return (Mathf.Abs(Vector3.Angle(velocity.normalized, NextDirection)) > 90)
                ? LastPosition(currentLocal, velocity.magnitude, ref seg)
                : NextPosition(currentLocal, velocity.magnitude, ref seg);
        }

        //keep commented for further usages

        public Vector3 NextPositionInDirection(SegmentPath path, Vector3 current, Vector3 velocity,
            ref SegmentPoint newSegmentAtPosition)
        {
            return
                NextPositionInDirection(path.transform.InverseTransformPoint(current),
                    path.transform.InverseTransformVector(velocity), ref newSegmentAtPosition);
        }
    }
}
