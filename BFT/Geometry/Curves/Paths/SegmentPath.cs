using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace BFT
{
    public abstract class SegmentPath : MonoBehaviour
    {
        protected int firstIndex;
        protected int lastIndex;

        protected int nextIdToGive = 0;

        [SerializeField] private List<SegmentPoint> savingPoints;

        //TODO get rid of local transmormation, and of any access from the point directly

        public SegmentPoint First => Segments[firstIndex];
        public SegmentPoint Last => Segments[lastIndex];

        public virtual Vector3 StartPoint => transform.TransformPoint(First.StartLocal);
        public virtual Vector3 EndPoint => transform.TransformPoint(Last.StartLocal);

        public Vector3 StartLocVector => First.StartLocal;
        public Vector3 EndLocVector => First.StartLocal;

        public float Count => Segments.Count;

        public float Length
        {
            get
            {
                float length = 0;
                foreach (KeyValuePair<int, SegmentPoint> segmentPoint in Segments)
                {
                    length += segmentPoint.Value.Length();
                }

                return length;
            }
        }

        public Dictionary<int, SegmentPoint> Segments { get; set; }

        public virtual void Awake()
        {
            int size;
            size = (savingPoints == null) ? 10 : savingPoints.Count;

            Segments = new Dictionary<int, SegmentPoint>(size);
            firstIndex = 0;

            if (savingPoints != null)
            {
                foreach (SegmentPoint segmentPoint in savingPoints)
                {
                    Segments.Add(nextIdToGive, segmentPoint);
                    nextIdToGive++;
                }

                lastIndex = nextIdToGive - 1;
            }
        }

        public void SaveAllPoints()
        {
            savingPoints = new List<SegmentPoint>();
            if ((Segments != null && Segments.Count == 0) && First == null)
            {
                SegmentPoint current = First;
                savingPoints.Add(current);
                while (current.HasNext)
                {
                    savingPoints.Add(current.Next);
                    current = current.Next;
                }
            }
        }

        void OnDestroy()
        {
            SaveAllPoints();
        }

        public bool IsEmpty()
        {
            return Segments == null || !Segments.Any();
        }

        public IEnumerable<Vector3> Points()
        {
            SegmentPoint current = First;
            yield return transform.TransformPoint(current.StartLocal);
            while (current.HasNext)
            {
                current = current.Next;
                yield return transform.TransformPoint(current.StartLocal);
            }
        }

        public abstract void InsertPointBefore(int index, Vector3 point);
        public abstract void AddPoints(SegmentPointCreator curve);
        public abstract void AddPoint(Vector3 point);
        public abstract void InsertPointAfter(Vector3 point, SegmentPoint after);
        public abstract void ResetPath(Vector3 newOrigin);

        public void InsertSegmentBefore(SegmentPoint toInsert, SegmentPoint after)
        {
            toInsert.index = nextIdToGive;

            if (Segments.Count > 0 && after == First)
                firstIndex = nextIdToGive;

            nextIdToGive++;

            toInsert.nextIndex = after.index;
            if (after.HasLast)
            {
                after.Last.nextIndex = toInsert.index;
                toInsert.lastIndex = after.lastIndex;
            }

            after.lastIndex = toInsert.index;

            Segments.Add(toInsert.index, toInsert);
        }

        public void InsertSegmentAfter(SegmentPoint toInsert, SegmentPoint before)
        {
            toInsert.index = nextIdToGive;

            if (Segments.Count > 0 && before == Last)
                lastIndex = nextIdToGive;

            nextIdToGive++;
            if (before != null)
            {
                toInsert.lastIndex = before.index;
                if (before.HasNext)
                {
                    before.Next.lastIndex = toInsert.index;
                    toInsert.nextIndex = before.nextIndex;
                }

                before.nextIndex = toInsert.index;
            }

            Segments.Add(toInsert.index, toInsert);
        }

        public Vector3 ProjectionTrans(Vector3 toPut, out SegmentPoint projectedOn, Projection proj = Projection.CLOSEST,
            Vector3 heading = new Vector3())
        {
            float minCount = float.MaxValue;
            int index = 0;
            //first find the closest segment start point
            foreach (KeyValuePair<int, SegmentPoint> segmentPoint in Segments)
            {
                if ((toPut - segmentPoint.Value.StartTrans).magnitude < minCount)
                {
                    minCount = (toPut - segmentPoint.Value.StartTrans).magnitude;
                    index = segmentPoint.Key;
                }
            }

            projectedOn = Segments[index];

            Vector3 returnV = Vector3.Project((toPut - projectedOn.StartTrans), projectedOn.NextDirection);

            if (Vector3.Angle(returnV, projectedOn.NextDirection) > 90)
                returnV = projectedOn.StartTrans;
            else
                returnV += projectedOn.StartTrans;
            return returnV;
        }

        public Vector3 GetPointAtPercent(float p)
        {
            float distance = Count * p;
            SegmentPoint point = First;
            return First.NextPosition(StartPoint, distance, ref point);
        }

        private SegmentPoint ClosestPointTo(Vector3 vec)
        {
            float minCount = float.MaxValue;
            int index = 0;
            //first find the closest segment start point
            foreach (KeyValuePair<int, SegmentPoint> segmentPoint in Segments)
            {
                if ((vec - segmentPoint.Value.StartTrans).magnitude < minCount)
                {
                    minCount = (vec - segmentPoint.Value.StartTrans).magnitude;

                    index = segmentPoint.Key;
                }
            }

            return Segments[index];
        }

        private SegmentPoint FirstClosestPoint(Vector3 vec)
        {
            float minCountI = float.MaxValue;

            SegmentPoint current = First;
            SegmentPoint toReturn = current;
            while (current.HasNext)
            {
                if ((vec - current.StartTrans).magnitude < minCountI)
                {
                    minCountI = (vec - current.StartTrans).magnitude;
                    toReturn = current;
                }
                else
                {
                    break;
                }

                current = current.Next;
            }

            return toReturn;
        }

        private SegmentPoint LastClosestPoint(Vector3 vec)
        {
            float minCountI = float.MaxValue;

            SegmentPoint current = Last;
            SegmentPoint toReturn = current;
            while (current.HasLast)
            {
                if ((vec - current.StartTrans).magnitude < minCountI)
                {
                    minCountI = (vec - current.StartTrans).magnitude;
                    toReturn = current;
                }
                else
                {
                    break;
                }

                current = current.Last;
            }

            return toReturn;
        }

        private SegmentPoint ClosestPointFromTwoSides(Vector3 vec)
        {
            float minCountI = float.MaxValue;
            float minCountJ = float.MaxValue;
            SegmentPoint toReturnI;
            SegmentPoint toReturnJ;
            //first find the closest segment start point

            SegmentPoint current = First;
            toReturnI = current;
            while (current.HasNext)
            {
                if ((vec - current.StartTrans).magnitude < minCountI)
                {
                    minCountI = (vec - current.StartTrans).magnitude;
                    toReturnI = current;
                }
                else
                {
                    break;
                }

                current = current.Next;
            }

            current = Last;
            toReturnJ = current;
            while (current.HasLast)
            {
                if ((vec - current.StartTrans).magnitude < minCountI)
                {
                    minCountJ = (vec - current.StartTrans).magnitude;
                    toReturnJ = current;
                }
                else
                {
                    break;
                }

                current = current.Last;
            }


            return (minCountI > minCountJ) ? toReturnJ : toReturnI;
        }

        public SegmentPoint ClosestSegment(Vector3 vec, Projection proj = Projection.CLOSEST)
        {
            switch (proj)
            {
                case Projection.CLOSEST_TWO_SIDES:
                    return ClosestPointFromTwoSides(vec);
                case Projection.FROM_END:
                    return LastClosestPoint(vec);
                case Projection.FROM_START:
                    return FirstClosestPoint(vec);
                default:
                    return ClosestPointTo(vec);
            }
        }

        public Vector3 ProjectionTrans(Vector3 toPut)
        {
            SegmentPoint point;
            return ProjectionTrans(toPut, out point);
        }


#if UNITY_EDITOR
        public Color debugColor = Color.red;
        public bool debugAllTheTime;

        public void OnDrawGizmosSelected()
        {
            if (Segments != null)
            {
                Gizmos.color = debugColor;
                HandleExt.Text(StartPoint, "Start", true);
                HandleExt.Text(EndPoint, "End", true);

                foreach (SegmentPoint seg in Segments.Values)
                {
                    if (seg.HasNext)
                        Gizmos.DrawLine(transform.TransformPoint(seg.StartLocal),
                            transform.TransformPoint(seg.Next.StartLocal));
                }
            }
        }

        public void OnDrawGizmos()
        {
            if (debugAllTheTime)
                OnDrawGizmosSelected();
        }

#endif
    }
}
