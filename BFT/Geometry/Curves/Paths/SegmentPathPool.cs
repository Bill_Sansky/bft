﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Math/Curves/Segment Path Pool")]
    public class SegmentPathPool : MonoBehaviour
    {
        private List<SegmentPath> availablePath;

        public SegmentPath pathModel;
        public int poolSize;
        private List<SegmentPath> usedPath;

        void Start()
        {
            availablePath = new List<SegmentPath>(poolSize);
            usedPath = new List<SegmentPath>(poolSize);
            for (int i = 0; i < poolSize; i++)
            {
                SegmentPath path = Instantiate(pathModel);
                path.gameObject.SetActive(false);
                path.gameObject.transform.parent = transform;
                availablePath.Add(path);
            }
        }

        public SegmentPath GetPathInstance(Vector3 origin)
        {
            if (availablePath.IsEmpty())
                ReleaseFirst();
            SegmentPath path = availablePath[0];
            path.ResetPath(origin);
            availablePath.Remove(path);
            usedPath.Add(path);
            path.gameObject.SetActive(true);
            return path;
        }

        public void ReleaseFirst()
        {
            Release(usedPath[0]);
        }

        public void Release(SegmentPath path)
        {
            availablePath.Add(path);
            usedPath.Remove(path);
            path.gameObject.SetActive(false);
        }
    }
}
