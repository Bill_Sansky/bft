﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Math/Curves/Transform Segment Path")]
    public class TransformSegmentPath : SegmentPath
    {
        public List<UnityEngine.Transform> OnPath;

        public override void Awake()
        {
            base.Awake();
            OnPath = new List<UnityEngine.Transform>(transform.childCount);
            for (int i = 0; i < transform.childCount; i++)
            {
                AddPoint(transform.GetChild(i));
            }
        }

        public void ChangeFirstTransform(UnityEngine.Transform first)
        {
            OnPath[0] = first;
            TransformSegmentPoint point = (TransformSegmentPoint) First;
            point.point = first;
        }

        public void ChangeLastTransform(UnityEngine.Transform last)
        {
            OnPath[OnPath.Count - 1] = last;
            TransformSegmentPoint point = (TransformSegmentPoint) Last;
            point.point = last;
        }

        public override void InsertPointBefore(int index, Vector3 point)
        {
            throw new System.NotImplementedException();
        }

        public void InsertPointBefore(int index, UnityEngine.Transform trans)
        {
            TransformSegmentPoint point = new TransformSegmentPoint(trans, this);
            InsertSegmentBefore(point, Segments[index]);
            OnPath.Insert(index, trans);
        }

        public override void AddPoints(SegmentPointCreator curve)
        {
            throw new System.NotImplementedException();
        }

        public override void AddPoint(Vector3 point)
        {
            throw new System.NotImplementedException();
        }

        public void AddPoint(UnityEngine.Transform trans)
        {
            TransformSegmentPoint point = new TransformSegmentPoint(trans, this);
            OnPath.Add(trans);

            InsertSegmentAfter(point, (Segments != null && Segments.Count > 0) ? Last : null);
        }

        public override void InsertPointAfter(Vector3 point, SegmentPoint after)
        {
            throw new System.NotImplementedException();
        }

        public override void ResetPath(Vector3 newOrigin)
        {
            foreach (UnityEngine.Transform transform1 in OnPath)
            {
                transform1.position = newOrigin;
            }
        }
    }
}
