﻿using UnityEngine;

namespace BFT
{
    public abstract class SegmentPointCreator : MonoBehaviour
    {
        public Loop loopStyle;

        public abstract SegmentPoint[] ComputeSegmentPoints(SegmentPath forReference);
    }
}
