﻿using System;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode,
     Obsolete("The linear Segment Path is not functional anymore, and as a result this class shouldn't be used either")]
    public class LinearCurveProjector : MonoBehaviour
    {
        public bool debug = true;


        public float dSize = 1f;

        public LinearSegmentPath Path;
        public UnityEngine.Transform toProject;
#if UNITY_EDITOR
        public void OnDrawGizmos()
        {
            if (debug)
            {
                Gizmos.color = Color.blue.Alphaed(.4f);

                Gizmos.DrawCube(transform.position, Vector3.one * dSize);
                Gizmos.color = Color.cyan.Alphaed(.6f);
                Gizmos.DrawCube(toProject.position, Vector3.one * dSize);
            }
        }
#endif
        // Update is called once per frame
        void Update()
        {
            transform.position = Path.ProjectionTrans(toProject.position);
        }
    }
}
