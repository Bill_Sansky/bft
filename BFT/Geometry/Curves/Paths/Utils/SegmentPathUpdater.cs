﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Math/Curves/Segment Path Updater")]
    public class SegmentPathUpdater : MonoBehaviour
    {
        public float DistanceThresholdForUpdate;

        public SegmentPathPool Pool;
        public List<UnityEngine.Transform> TransformsToWatch;

        private Dictionary<UnityEngine.Transform, PathInfo> transformToPathDic;

        public float UpdateFrequency;

        void Start()
        {
            transformToPathDic =
                new Dictionary<UnityEngine.Transform, PathInfo>((TransformsToWatch != null) ? TransformsToWatch.Count : 10);
            if (TransformsToWatch != null)
                for (int i = 0; i < TransformsToWatch.Count; i++)
                {
                    transformToPathDic[TransformsToWatch[i]] = new PathInfo(UpdateFrequency,
                        Pool.GetPathInstance(TransformsToWatch[i].position), false);
                }
        }

        public SegmentPath GetPathFor(UnityEngine.Transform trans)
        {
            return transformToPathDic[trans].Path;
        }

        public void TrackNewTransform(UnityEngine.Transform trans)
        {
            transformToPathDic.Add(trans, new PathInfo(UpdateFrequency, Pool.GetPathInstance(trans.position), false));
        }

        public void StartUpdateFor(UnityEngine.Transform trans)
        {
            if (!transformToPathDic.ContainsKey(trans))
                TrackNewTransform(trans);

            //For now, clear on stop, but could become a parameter
            transformToPathDic[trans].Path.ResetPath(trans.position);
            transformToPathDic[trans].ShouldUpdate = true;

            StartCoroutine(UpdatePathPositions(trans, transformToPathDic[trans]));
        }

        public void StopUpdate(UnityEngine.Transform trans)
        {
            transformToPathDic[trans].ShouldUpdate = false;
        }

        private IEnumerator UpdatePathPositions(UnityEngine.Transform trans, PathInfo info)
        {
            while (info.ShouldUpdate)
            {
                if (Vector3.Distance(info.Path.EndPoint, trans.position) > DistanceThresholdForUpdate)
                    info.Path.AddPoint(trans.position);
                yield return new WaitForSeconds(info.UpdateFrequency);
            }
        }

        public void ForceUpdate(UnityEngine.Transform trans)
        {
            transformToPathDic[trans].Path.AddPoint(trans.position);
        }

        [Serializable]
        private class PathInfo
        {
            public readonly SegmentPath Path;
            public readonly float UpdateFrequency;
            public bool ShouldUpdate;

            public PathInfo(float updateFrequency, SegmentPath path, bool shouldUpdate)
            {
                this.UpdateFrequency = updateFrequency;
                this.Path = path;
                this.ShouldUpdate = shouldUpdate;
            }
        }
    }
}
