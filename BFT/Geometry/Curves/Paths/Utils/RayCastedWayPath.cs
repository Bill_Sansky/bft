﻿using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Math/Curves/Raycasted Way Path")]
    public class RayCastedWayPath : MonoBehaviour
    {
        public LayerMask CastMasks;
        private bool changeEndsCalledOnce;

        public float HeightOfRayCast;

        private Vector3 lastRecastPosition;

        public TransformSegmentPath Path;

        public float RecastDistanceThreshold;

        public UnityEngine.Transform ToGoFrom;
        public UnityEngine.Transform ToGoTo;

#if UNITY_EDITOR

        public void OnDrawGizmosSelected()
        {
            Path.OnDrawGizmosSelected();
            HandleExt.Text(ToGoFrom.position, "FROM", true);
            HandleExt.Text(ToGoFrom.position, "TO", true);
            Gizmos.DrawLine(ToGoTo.position, ToGoTo.position + HeightOfRayCast * Vector3.down);
            HandleExt.Text(ToGoTo.position + HeightOfRayCast * Vector3.down, "Raycast Length", true);
        }
#endif

        public void ChangeEnds(UnityEngine.Transform start, UnityEngine.Transform end)
        {
            ToGoFrom = start;
            ToGoTo = end;

            Path.ChangeFirstTransform(start);
            Path.ChangeLastTransform(end);

            if (!changeEndsCalledOnce)
                changeEndsCalledOnce = true;
        }

        // Update is called once per frame
        void Update()
        {
            if (!Path.IsEmpty() && ToGoFrom && ToGoTo &&
                (ToGoTo.position - lastRecastPosition).sqrMagnitude > RecastDistanceThreshold)
            {
                //make sure the ends are added properly (kinda sucks, but easy fix)
                if (!changeEndsCalledOnce)
                {
                    Path.InsertPointBefore(0, ToGoFrom);
                    Path.AddPoint(ToGoTo);
                    changeEndsCalledOnce = true;
                }

                //then raycast again

                lastRecastPosition = ToGoTo.position;

                //start raycast
                Vector3 dir = (ToGoTo.position - ToGoFrom.position).normalized;
                float distanceBetweenRaycasts = (ToGoFrom.position - ToGoTo.position).magnitude / (Path.Count + 1);
                Vector3 interpPosiion = ToGoFrom.position + distanceBetweenRaycasts * dir;
                UnityEngine.Transform previousTrtans;

                foreach (UnityEngine.Transform trans in Path.OnPath)
                {
                    previousTrtans = trans;
                    if (trans == ToGoFrom)
                    {
                        interpPosiion += distanceBetweenRaycasts * dir;
                        continue;
                    }

                    if (trans == ToGoTo)
                        break;

                    RaycastHit hit;
                    if (UnityEngine.Physics.Raycast(new Ray(interpPosiion + Vector3.up * HeightOfRayCast, Vector3.down), out hit, 9999,
                        CastMasks))
                    {
                        trans.position = (hit.point);
                    }
                    else
                    {
                        trans.position = previousTrtans.position;
                    }

                    interpPosiion += distanceBetweenRaycasts * dir;
                }

                /*   while (!(ToGoTo.position - interpPosiion).normalized.IsColinearlyOpposed(dir))
               {
                   //while the vector isnt after toGoTo
                   //raycast from up to down
                   RaycastHit hit;
                   if (Physics.Raycast(new Ray(interpPosiion + Vector3.up*heightOfRayCast, Vector3.down),out hit,9999,castMasks))
                   {
                       path.AddPoint(hit.point);
                   }
                   interpPosiion += distanceBetweenRaycasts*dir;
               }
               //then add ToGoTo
               path.AddPoint(ToGoTo.position);*/
            }
        }
    }
}
