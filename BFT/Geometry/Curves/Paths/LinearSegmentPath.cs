﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public enum Loop
    {
        NONE,
        WHOLE,
        LAST
    }

    public enum Projection
    {
        FROM_START,
        FROM_END,
        CLOSEST,
        CLOSEST_TWO_SIDES
    }

    [Obsolete("This class needs to be rewritten: it wont work as is")]
    public class LinearSegmentPath : SegmentPath
    {
#if UNITY_EDITOR
        [InfoBox("The class is deprecated and wont work!")]
#endif
        public override void InsertPointBefore(int index, Vector3 point)
        {
            throw new NotImplementedException();
        }

        public override void AddPoints(SegmentPointCreator curve)
        {
            //TODO fix according to the new curve
            throw new NotImplementedException();
            /* int segmentCount = 0;
         if (Segments == null)
         {
             Segments = curve.ComputeSegmentPoints(this);
             segmentCount = Segments.Count;
         }
         else
         {
             List<SegmentPoint> tempSegments = new List<SegmentPoint>(Segments.Count);
             tempSegments.AddRange(Segments);
             segmentCount = tempSegments.Count;
 
             tempSegments.AddRange(curve.ComputeSegmentPoints(this));
             Segments = tempSegments.ToArray();
         }
 
         for (int i = 0; i < Segments.Count; i++)
         {
             if (i != Segments.Count - 1 && i != 0)
             {
                 Segments[i].nextIndex = i + 1;
                 Segments[i].lastIndex = i - 1;
             }
             else if (i == Segments.Count - 1)
             {
                 Segments[i].lastIndex = i - 1;
                 switch (curve.loopStyle)
                 {
                     case Loop.NONE:
                         Segments[i].nextIndex = i;
                         Segments[i].lastIndex = i - 1;
                         break;
                     case Loop.WHOLE:
                         Segments[i].nextIndex = 0;
                         Segments[i].lastIndex = i - 1;
                         break;
                     case Loop.LAST:
                         Segments[i].nextIndex = segmentCount;
                         Segments[i].lastIndex = i - 1;
                         break;
                 }
             }
             else
             {
                 Segments[i].nextIndex = i + 1;
                 switch (curve.loopStyle)
                 {
                     case Loop.NONE:
                         Segments[i].lastIndex = i;
                         break;
                     case Loop.WHOLE:
                         Segments[i].lastIndex = Segments.Count - 1;
                         break;
                     case Loop.LAST:
                         Segments[i].lastIndex = segmentCount;
                         break;
                 }
             }
         }*/
        }

        public override void AddPoint(Vector3 point)
        {
            throw new NotImplementedException();
        }

        public override void InsertPointAfter(Vector3 point, SegmentPoint after)
        {
            //TODO fix
            throw new NotImplementedException();
            /* List<SegmentPoint> tempPoints = new List<SegmentPoint>(Segments);
        SegmentPoint newS = new SimpleSegmentPoint(transform.InverseTransformPoint(point), this);
         /*   newS.Last = after;
           newS.Next = after.Next;*/ //TODO pick the code from before
            /* tempPoints.Insert(tempPoints.IndexOf(newS.Last), newS);
         Segments = tempPoints.ToArray();*/
        }

        public override void ResetPath(Vector3 newOrigin)
        {
            throw new NotImplementedException();
        }
    }
}
