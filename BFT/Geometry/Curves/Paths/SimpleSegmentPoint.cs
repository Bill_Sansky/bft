﻿using System;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class SimpleSegmentPoint : SegmentPoint
    {
        public Vector3 startLocal;

        public SimpleSegmentPoint(Vector3 current, SegmentPath path) : base(path)
        {
            startLocal = current;
        }

        public override Vector3 StartLocal
        {
            get => startLocal;
            set => startLocal = value;
        }
    }
}
