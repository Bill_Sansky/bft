﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

#endif
namespace BFT
{
    /// <summary>
    ///     Do no change coords without recomputing length or big fuck up is to expect
    /// </summary>
    [ExecuteInEditMode]
    [AddComponentMenu("Math/Curves/Bezier Curve")]
    public class BezierCurve : SegmentPointCreator
    {
        private static int lengthCalculationPrecision = 10;

        [ReadOnly] public float Length;

        public UnityEngine.Transform LocalControlPoint1;
        public UnityEngine.Transform LocalControlPoint2;
        public UnityEngine.Transform LocalEnd;

        public UnityEngine.Transform LocalStart;

        public float PointSearchPrecision = 10;

        // Use this for initialization
        void Start()
        {
            ResolveControlPoints();
            ComputeLength();
        }


        void Update()
        {
            ResolveControlPoints();
        }

        private void ResolveControlPoints()
        {
            if (LocalControlPoint1 == null && LocalStart != null)
            {
                LocalControlPoint1 = LocalStart;
            }

            if (LocalControlPoint2 == null && LocalEnd != null)
            {
                LocalControlPoint1 = LocalEnd;
            }
        }

#if UNITY_EDITOR
        public void OnDrawGizmosSelected()
        {
            if (LocalControlPoint1 != null && LocalControlPoint2 != null && LocalStart != null && LocalEnd != null)
                Handles.DrawBezier(LocalStart.position
                    , LocalEnd.position, LocalControlPoint1.position
                    , LocalControlPoint2.position
                    , Color.red, Texture2D.whiteTexture, 3);

            if (LocalControlPoint1 != null && LocalControlPoint2 != null && LocalStart != null && LocalEnd != null)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawCube(LocalStart.position, new Vector3(5, 5, 5));
                Gizmos.color = Color.green;
                Gizmos.DrawCube(LocalEnd.position, new Vector3(5, 5, 5));

                Gizmos.color = Color.blue;
                Gizmos.DrawCube(LocalControlPoint1.position, new Vector3(5, 5, 5));
                Gizmos.DrawCube(LocalControlPoint2.position, new Vector3(5, 5, 5));
            }
        }

#endif

        public void ComputeLength()
        {
            //first compute linear distance to get the stepping
            float step = (float) lengthCalculationPrecision / (LocalEnd.position - LocalStart.position).magnitude;

            Vector3 point;
            Vector3 lastPoint = LocalStart.position;
            float i = 0;
            bool lastStep = false;

            Length = 0;

            while (i <= 1)
            {
                point = ComputePoint(i);
                Length += (point - lastPoint).magnitude;
                lastPoint = point;

                i += step;
                if (i > 1 && !lastStep)
                {
                    lastStep = true;
                    i = 1;
                }
            }
        }

        public override SegmentPoint[] ComputeSegmentPoints(SegmentPath reference)
        {
            if (Length == 0)
                ComputeLength();

            float step = PointSearchPrecision / Length;
            float t = 0;
            bool lastStep = false;

            List<SegmentPoint> tempSegments = new List<SegmentPoint>();

            while (t <= 1)
            {
                tempSegments.Add(new SimpleSegmentPoint(reference.transform.InverseTransformPoint(ComputePoint(t)),
                    reference));

                t += step;

                if (t > 1 && !lastStep)
                {
                    t = 1;
                    lastStep = true;
                }
            }

            return tempSegments.ToArray();
        }


        public Vector3 ComputePoint(float t)
        {
            //cubic bezier curve formula
            return (1 - t) * (1 - t) * (1 - t) * LocalStart.position +
                   3 * (1 - t) * (1 - t) * t * LocalControlPoint1.position +
                   3 * (1 - t) * t * t * LocalControlPoint2.position + t * t * t * LocalEnd.position;
        }

        /// <summary>
        ///     changes a coordinate base on a duration and a current time t
        /// </summary>
        /// <returns></returns>
        public Vector3 GetAdvanceBasedOnSpeed(float speedAlongLine, ref float t)
        {
            t += speedAlongLine / Length;
            t = Mathf.Clamp01(t);

            return ComputePoint(t);
        }

        public void TestLinearSpeed()
        {
            float step = 0.01f;
            float t = 0;
            bool lastStep = false;
            Vector3 previous = LocalStart.position;
            Vector3 current;
            while (t <= 1)
            {
                t += step;
                current = ComputePoint(t);
                UnityEngine.Debug.Log(current + " " + previous);
                if (t > 1 && !lastStep)
                {
                    t = 1;
                    lastStep = true;
                }
            }
        }
    }
}
