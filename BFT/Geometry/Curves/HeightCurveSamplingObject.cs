﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class HeightCurveSamplingObject : AbstractCurveSamplingObject
    {
        [SerializeField, ShowInInspector] private IVariable<AnimationCurve> curveVariable;

        public override bool AvailabilityCheck => curveVariable.Value != null &&
                                                  (curveVariable.Value != null || curveVariable.Value.length >= 2);

        public override Vector3 GetPosition(float currentSampleTime, Vector3 startWorldPosition, Vector3 endWorldPosition)
        {
            return AbstractCurveSampler.GetPointBasedOnHeightCurve(curveVariable.Value, currentSampleTime, GetDuration(),
                startWorldPosition, endWorldPosition);
        }

        protected override float RecalculateDuration()
        {
            return curveVariable.Value[curveVariable.Value.length - 1].time;
        }
    }
}
