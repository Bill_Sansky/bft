﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public abstract class AbstractCurveSamplingObject : SerializedMonoBehaviour
    {
        private float duration;
        public abstract bool AvailabilityCheck { get; }

        public float GetDuration(bool recalculate = false)
        {
            if (recalculate) duration = RecalculateDuration();
            return duration;
        }

        public abstract Vector3 GetPosition(float currentSampleTime, Vector3 startWorldPosition, Vector3 endWorldPosition);
        protected abstract float RecalculateDuration();
    }
}
