﻿using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class CurveRenderer : AbstractCurveSampler
    {
        private LineRenderer lineRenderer;

        private void OnDisable()
        {
            lineRenderer.positionCount = 0;
        }

        protected override void Awake()
        {
            lineRenderer = GetComponent<LineRenderer>();
        }

        protected override void OnNotAvailable()
        {
            base.OnNotAvailable();
            if (lineRenderer.positionCount > 0)
                lineRenderer.positionCount = 0;
        }

        protected override void OnSamplingStart(float duration, int sampling)
        {
            lineRenderer.positionCount = sampling;
        }

        protected override void OnSample(int sampleIndex, float currentSampleTime, float curveSampling)
        {
            lineRenderer.SetPosition(sampleIndex,
                samplingObject.GetPosition(currentSampleTime, StartPosition, EndPosition));
        }
    }
}
