﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public abstract class AbstractCurveSampler : SerializedMonoBehaviour
    {
        [SerializeField] private IValue<float> endCurveTime;

        public bool RefreshEveryUpdate = true;

        [MinValue(3), SerializeField, ShowInInspector]
        private int sampling;

        public AbstractCurveSamplingObject samplingObject;

        [SerializeField] private IValue<float> starCurveTime;

        [SerializeField, ShowInInspector] public UnityEngine.Transform start, end;

        public Vector3 StartPosition { private set; get; }
        public Vector3 EndPosition { private set; get; }

        protected virtual void Awake()
        {
        }

        protected void LateUpdate()
        {
            if (!RefreshEveryUpdate)
                return;

            CurveSampling();
        }

        public void CurveSampling()
        {
            if (sampling < 3 || start == null || end == null || samplingObject == null || !samplingObject.AvailabilityCheck)
            {
                OnNotAvailable();
                return;
            }

            samplingObject.GetDuration(true);
            float currentSampleTime = starCurveTime != null ? starCurveTime.Value : 0;
            float fullDuration = samplingObject.GetDuration();
            float realDuration = (endCurveTime != null ? endCurveTime.Value : fullDuration) - currentSampleTime;

            if (realDuration <= 0)
            {
                OnNotAvailable();
                return;
            }

            int realSampling = Mathf.Max(3, Mathf.CeilToInt((realDuration * sampling) / fullDuration));
            float curveSampling = realDuration / (realSampling - 1);

            StartPosition = start.transform.position;
            EndPosition = end.transform.position;

            OnSamplingStart(realDuration, realSampling);

            for (int i = 0; i < realSampling; i++)
            {
                OnSample(i, currentSampleTime, curveSampling);
                currentSampleTime += curveSampling;
            }

            OnSamplingEnd();
        }

        protected virtual void OnNotAvailable()
        {
        }

        protected virtual void OnSample(int sampleIndex, float currentSampleTime, float curveSampling)
        {
        }

        protected virtual void OnSamplingStart(float duration, int sampling)
        {
        }

        protected virtual void OnSamplingEnd()
        {
        }

        public static Vector3 GetPointBasedOnHeightCurve(AnimationCurve animationCurve, float currentSampleTime,
            float totalTime, Vector3 startPoint, Vector3 endPoint)
        {
            Vector2 way = new Vector2(endPoint.x, endPoint.z) - new Vector2(startPoint.x, startPoint.z);
            float wayPercent = currentSampleTime / totalTime;
            Vector3 samplePoint = startPoint;
            samplePoint.x += way.x * wayPercent;
            samplePoint.z += way.y * wayPercent;
            samplePoint.y += animationCurve.Evaluate(currentSampleTime);
            return samplePoint;
        }
    }
}
