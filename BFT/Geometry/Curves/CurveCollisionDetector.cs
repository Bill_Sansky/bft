﻿using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

#if UNITY_EDITOR
#endif

namespace BFT
{
    [ExecuteInEditMode]
    public class CurveCollisionDetector : AbstractCurveSampler, IValue<bool>
    {
        private bool? allowed, prevAllowed;
        public bool DebugGizmos = true;

        [SerializeField, ShowInInspector] private LayerMask mask;

        public UnityEvent OnCollisionDetected, OnCollisionUndetected;

        private Vector3[] samplingPoints;

        public bool IsTrue => allowed.HasValue && allowed.Value;

        public bool Value => IsTrue;

        protected override void OnNotAvailable()
        {
            allowed = true;
            samplingPoints = null;
        }

        protected override void OnSamplingStart(float duration, int sampling)
        {
            base.OnSamplingStart(duration, sampling);
            allowed = true;
            samplingPoints = new Vector3[sampling];
        }

        protected override void OnSample(int sampleIndex, float currentSampleTime, float curveSampling)
        {
            samplingPoints[sampleIndex] = samplingObject.GetPosition(currentSampleTime, StartPosition, EndPosition);

            if (IsTrue && sampleIndex > 0)
            {
                Vector3 direction = samplingPoints[sampleIndex] - samplingPoints[sampleIndex - 1];
                if (UnityEngine.Physics.Raycast(samplingPoints[sampleIndex - 1], direction, direction.magnitude, mask))
                {
                    allowed = false;
                }
            }
        }

        protected override void OnSamplingEnd()
        {
            if (prevAllowed != IsTrue)
            {
                if (IsTrue)
                {
                    OnCollisionUndetected.Invoke();
                }
                else
                {
                    OnCollisionDetected.Invoke();
                }
            }

            prevAllowed = IsTrue;
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            if (!Application.isPlaying)
            {
                LateUpdate();
            }

            if (DebugGizmos && samplingPoints != null)
            {
                Handles.color = IsTrue ? Color.green : Color.red;
                Handles.DrawPolyLine(samplingPoints);
            }
        }
#endif
    }
}
