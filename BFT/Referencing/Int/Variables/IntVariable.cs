﻿using System;

namespace BFT
{
    [Serializable]
    public class IntVariable : GenericVariable<int>, IValue<float>
    {
        public IntVariable(int value)
        {
            Value = value;
        }

        public IntVariable(bool useRef = false)
        {
            UseReference = useRef;
        }

        float IValue<float>.Value => Value;
    }
}
