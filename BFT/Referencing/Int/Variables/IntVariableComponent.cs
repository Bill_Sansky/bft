﻿namespace BFT
{
    public class IntVariableComponent : VariableComponent<int,IntVariable>, IValue<float>
    {
        float IValue<float>.Value => Value;

        public void Increment()
        {
            Value++;
        }

        public void Decrement()
        {
            Value--;
        }

        public void Add(int amount)
        {
            Value += amount;
        }
    }
}
