﻿using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR
#endif

namespace BFT
{
    [CreateAssetMenu(menuName = "Data/Variables/Int Variable", fileName = "Int Variable")]
    public class IntVariableAsset :
        VariableAsset<int>, IValue<float>, IValue<string>, ISavedObject
    {
        public object Save()
        {
            return Value;
        }

        public void Load(object saveFile)
        {
            if (saveFile == null)
            {
                Value = 0;
                return;
            }

            Value = (int) saveFile;
#if UNITY_EDITOR
            EditorUtility.SetDirty(this);
#endif
        }

        float IValue<float>.Value => Value;

        string IValue<string>.Value => Value.ToString();

        public void Increment()
        {
            Value++;
        }

        public void Decrement()
        {
            Value--;
        }

        public void Increment(int value)
        {
            Value += value;
        }

        public void Decrement(int value)
        {
            Value -= value;
        }

        public void IncrementFromIntValue(Object intValue)
        {
            Value += ((IValue<int>) intValue).Value;
        }

        public void DecrementFromIntValue(Object intValue)
        {
            Value -= ((IValue<int>) intValue).Value;
        }

        public void Add(int amount)
        {
            Value += amount;
        }
    }
}