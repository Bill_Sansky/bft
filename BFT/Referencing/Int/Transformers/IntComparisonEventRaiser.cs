﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class IntComparisonEventRaiser : MonoBehaviour
    {
        [BoxGroup("Init")] public bool CompareOnEnable;

        [BoxGroup("Values")] [SerializeField] private IntValue firstValue;
        [BoxGroup("Values")] [SerializeField] private IntValue secondValue;

        [SerializeField, BoxGroup("Events")] private UnityEvent OnEqual;

        [SerializeField, BoxGroup("Events")] private UnityEvent OnFirstBigger;

        [SerializeField, BoxGroup("Events")] private UnityEvent OnSecondBigger;

        void OnEnable()
        {
            if (CompareOnEnable)
                Compare();
        }


        [Button(ButtonSizes.Medium)]
        public void Compare()
        {
            if (firstValue.Value == secondValue.Value)
                OnEqual.Invoke();
            else if (firstValue.Value > secondValue.Value)
                OnFirstBigger.Invoke();
            else if (firstValue.Value < secondValue.Value)
                OnSecondBigger.Invoke();
        }
    }
}