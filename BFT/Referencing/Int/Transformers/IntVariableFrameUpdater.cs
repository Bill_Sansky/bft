using UnityEngine;

namespace BFT
{
    public class IntVariableFrameUpdater : MonoBehaviour
    {
        public IntVariable VariableToUpdate;
        public IntBFTFunction ValueToTake;

        public void Update()
        {
            VariableToUpdate.Value = ValueToTake.Value;
        }
    }
}