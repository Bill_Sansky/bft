using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class IntValueSelectorFromDictionaryID : MonoBehaviour, IValue<int>
    {
        public IDHolderReference DictionaryReference;

        [MinValue(0), MaxValue("@IDs.Count - 1")]
        public int IDIndex = 0;

        [ValueDropdown("idDD")] public List<int> IDs;

        private ValueDropdownList<int> idDD => DictionaryReference.Reference
            ? DictionaryReference.Value.ContentIDs
            : new ValueDropdownList<int>();

        public int Value => IDs[IDIndex];

        [ShowInInspector]
        [ValueDropdown("idDD")]
        private int CurrentValue => IDs.Count > 0 ? Value : -1;

        public void NextID()
        {
            IDIndex = IDs.LoopID(IDIndex + 1);
        }

        public void PreviousID()
        {
            IDIndex = IDs.LoopID(IDIndex + 1);
        }
    }
}