using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class IntValueFromDictionaryID : MonoBehaviour, IValue<int>
         {
             public IDHolderReference DictionaryReference;
     
             [ValueDropdown("idDD")] public int ID;
             private ValueDropdownList<int> idDD => DictionaryReference.Reference? DictionaryReference.Value.ContentIDs : new ValueDropdownList<int>();
             public int Value => ID;
         }
}