﻿using System;

namespace BFT
{
    [Serializable]
    public class IntValue : GenericValue<int>
    {
        public IntValue()
        {
        }

        public IntValue(int i)
        {
            UseReference = false;
            LocalValue = i;
        }
    }
}