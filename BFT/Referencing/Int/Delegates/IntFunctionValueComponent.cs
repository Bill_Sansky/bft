﻿namespace BFT
{
    public class IntFunctionValueComponent : FunctionValueComponent<int, IntBFTFunction>, IValue<string>
    {
        string IValue<string>.Value => Value.ToString();
    }
}