﻿using System;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class AnimationCurveVariable : GenericVariable<AnimationCurve>
    {
    }
}