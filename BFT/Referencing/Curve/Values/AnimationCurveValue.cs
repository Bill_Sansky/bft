using System;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class AnimationCurveValue : GenericValue<AnimationCurve>
    {
    }
}
