﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class GradientToColorComponent : MonoBehaviour, IValue<UnityEngine.Color>
    {
        public UnityEngine.Gradient Gradient;

        public PercentValue Percent;

        public UnityEngine.Color Value => Gradient.Evaluate(Percent.Value);
    }
}