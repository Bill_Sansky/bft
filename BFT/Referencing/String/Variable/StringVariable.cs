﻿using System;

namespace BFT
{
    [Serializable]
    public class StringVariable : GenericVariable<string>
    {
        public StringVariable(string value)
        {
            Value = value;
        }
    }
}
