﻿using System;

namespace BFT
{
    [Serializable]
    public class StringValue : GenericValue<string>
    {
        public StringValue()
        {
        }

        public StringValue(string noObjective)
        {
            LocalValue=noObjective;
        }
    }
}
