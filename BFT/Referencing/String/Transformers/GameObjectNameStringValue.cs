using UnityEngine;

namespace BFT
{
    public class GameObjectNameStringValue : MonoBehaviour, IValue<string>
    {
        public GameObject GameObject;
        public string Value => GameObject.name;
    }
}