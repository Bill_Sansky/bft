﻿using UnityEngine.EventSystems;

namespace BFT
{
    public class SelectableVariableComponent : VariableComponent<UnityEngine.UI.Selectable,SelectableVariable>
    {
      

        public void Select()
        {
            if (Value)
                Value.Select();
        }

        public void DeSelect()
        {
            if (Value && EventSystem.current.currentSelectedGameObject == Value.gameObject)
                EventSystem.current.SetSelectedGameObject(null);
        }
    }
}
