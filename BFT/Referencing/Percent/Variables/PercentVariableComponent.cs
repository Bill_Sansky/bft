﻿using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class PercentVariableComponent : FloatVariableComponent
    {
        public UnityEvent OnPercentReached100;
        
        public override float Value
        {
            get => Mathf.Clamp01(base.Value);
            set
            {
                value = Mathf.Clamp01(value);
                base.Value = value;
                if (value >= 1)
                    OnPercentReached100.Invoke();
            }
        }
    }
}
