using UnityEngine;

namespace BFT
{
    public class PercentAnimatedValue : MonoBehaviour, IValue<float>
    {
        [Range(0, 1)] public float Percent;

        public float Value => Percent;
    }
}