﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Referencing/Percent/Invert Percent")]
    public class InvertedPercentValue : MonoBehaviour, IValue<float>
    {
        public PercentValue PercentValue;

        [ShowInInspector, ReadOnly] public float Value => 1 - PercentValue.Value;
    }
}
