﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class FloatToPercentValue : MonoBehaviour, IValue<float>
    {
        public FloatValue FloatValue;
        public FloatValue MaxValue;

        public FloatValue MinValue;

        public float Value => MathExt.Percent(MinValue.Value, MaxValue.Value, FloatValue.Value);
    }
}
