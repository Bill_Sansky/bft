﻿using System;
using UnityEngine;

// ReSharper disable InconsistentNaming Unity name FTW

namespace BFT
{
    [Serializable]
    public class TransformValue : GenericValue<Transform>
    {
        public Vector3 position
        {
            get => Value.position;
            set => Value.position = value;
        }

        public Vector3 localPosition
        {
            get => Value.localPosition;
            set => Value.localPosition = value;
        }

        public Vector3 eulerAngles
        {
            get => Value.eulerAngles;
            set => Value.eulerAngles = value;
        }

        public Vector3 localEulerAngles
        {
            get => Value.localEulerAngles;
            set => Value.localEulerAngles = value;
        }

        public Vector3 right
        {
            get => Value.right;
            set => Value.right = value;
        }

        public Vector3 up
        {
            get => Value.up;
            set => Value.up = value;
        }

        public Vector3 forward
        {
            get => Value.forward;
            set => Value.forward = value;
        }

        public UnityEngine.Quaternion rotation
        {
            get => Value.rotation;
            set => Value.rotation = value;
        }

        public UnityEngine.Quaternion localRotation
        {
            get => Value.localRotation;
            set => Value.localRotation = value;
        }

        public Vector3 localScale
        {
            get => Value.localScale;
            set => Value.localScale = value;
        }

        public UnityEngine.Transform parent
        {
            get => Value.parent;
            set => Value.parent = value;
        }
    }
}
