﻿using UnityEngine;

namespace BFT
{
    public class AdvancedTransform : MonoBehaviour, IValue<Transform>, IValue<Vector3>, IValue<Quaternion>
    {
        Quaternion IValue<Quaternion>.Value => transform.rotation;
        Transform IValue<Transform>.Value => transform;

        Vector3 IValue<Vector3>.Value => transform.position;
    }
}
