﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class TransformValueComponent : ValueComponent<Transform, TransformValue>,
        IValue<Transform>, IValue<Vector3>, IValue<Quaternion>, IOrientation, IValue<IOrientation>
    {
        
        public Vector3 Up => Value.up;
        public Vector3 Right => Value.right;
        public Vector3 Forward => Value.forward;
        IOrientation IValue<IOrientation>.Value => this;
        Quaternion IValue<Quaternion>.Value => Value.rotation;
        Transform IValue<Transform>.Value => Value;

        Vector3 IValue<Vector3>.Value => Value.position;

        [Button(ButtonSizes.Medium)]
        public void Copy()
        {
            transform.Copy(Value);
        }
    }
}
