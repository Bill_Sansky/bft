using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class TransformSceneStateCopier : SerializedMonoBehaviour
    {
        [BoxGroup("Options")] public bool AutoUpdate;

        [BoxGroup("Options")] public bool CopyPosition = true;
        [BoxGroup("Options")] public bool CopyRotation;
        [BoxGroup("Options")] public bool CopyScale;

        [BoxGroup("Transforms")] public UnityEngine.Transform Target;

        [BoxGroup("Transforms")] public TransformSceneStateAsset TransformReference;

        [BoxGroup("Options"), ShowIf("AutoUpdate")]
        public float UpdateFrequency = 0;

        [BoxGroup("Options")] public bool UpdateOnEnable = true;

        void Reset()
        {
            if (!Target)
                Target = transform;
        }

        public void OnEnable()
        {
            if (UpdateOnEnable)
                CopyTransform();
        }

        public void CopyTransform()
        {
            UpdateTransform();
            if (AutoUpdate)
                StartCoroutine(AutoUpdateCoroutine());
        }

        public void OnDisable()
        {
            StopAllCoroutines();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        private void UpdateTransform()
        {
            if (TransformReference.TransformSaveState == null)
                return;
            TransformReference.TransformSaveState.RestoreState(Target);
        }

        private IEnumerator AutoUpdateCoroutine()
        {
            while (true)
            {
                UpdateTransform();
                yield return new WaitForSeconds(UpdateFrequency);
            }
        }
    }
}
