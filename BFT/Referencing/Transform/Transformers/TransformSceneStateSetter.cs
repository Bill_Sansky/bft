using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class TransformSceneStateSetter : SerializedMonoBehaviour
    {
        [BoxGroup("Options")] public bool AutoUpdate;

        [BoxGroup("Options")] public bool CopyPosition = true;
        [BoxGroup("Options")] public bool CopyRotation;
        [BoxGroup("Options")] public bool CopyScale;

        [BoxGroup("Transforms")] public UnityEngine.Transform Reference;

        [BoxGroup("Transforms")] public TransformSceneStateAsset TransformReference;

        [BoxGroup("Options"), ShowIf("AutoUpdate")]
        public float UpdateFrequency = 0;

        [BoxGroup("Options")] public bool UpdateOnEnable = true;

        void Reset()
        {
            if (!Reference)
                Reference = transform;
        }

        public void OnEnable()
        {
            if (UpdateOnEnable)
                SetTransformState();
        }

        public void SetTransformState()
        {
            UpdateTransformState();
            if (AutoUpdate)
                StartCoroutine(AutoUpdateCoroutine());
        }

        public void OnDisable()
        {
            StopAllCoroutines();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        private void UpdateTransformState()
        {
            TransformReference.TransformSaveState?.RegisterState(Reference);
        }

        private IEnumerator AutoUpdateCoroutine()
        {
            while (true)
            {
                UpdateTransformState();
                yield return new WaitForSeconds(UpdateFrequency);
            }
        }
    }
}
