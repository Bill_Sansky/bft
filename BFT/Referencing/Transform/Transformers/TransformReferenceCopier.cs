﻿using System;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace BFT
{
    /// <summary>
    ///     copies the values of the referenced transform onto the targeted transform
    /// </summary>
    public class TransformReferenceCopier : MonoBehaviour
    {
        [BoxGroup("Options")] public bool AutoUpdate;

        [BoxGroup("Options"), ShowIf("AutoUpdate")]
        public bool UpdateOnLateUpdate = true;

        [BoxGroup("Options"), ShowIf("@ (AutoUpdate && !UpdateOnLateUpdate)")]
        public float UpdateFrequency = 0;

        [BoxGroup("Options")] public bool CopyPosition = true;
        [BoxGroup("Options")] public bool CopyRotation;
        [BoxGroup("Options")] public bool CopyScale;

        [BoxGroup("Transforms")] public TransformValue TargetToCopyOn;
        [BoxGroup("Transforms")] public TransformValue TransformReference;

        [BoxGroup("Options")] public bool UpdateOnEnable = true;
        [BoxGroup("Options")] public bool ReverseCopyOnEnable = false;
        [BoxGroup("Options")] public bool UpdateOnDisable = true;


        void Reset()
        {
            if (!TargetToCopyOn.Value)
                TargetToCopyOn.LocalValue = transform;
        }

        public void OnEnable()
        {
            if (ReverseCopyOnEnable)
                TransformReference.Value.Copy(TargetToCopyOn.Value, CopyPosition, CopyRotation, CopyScale);
            if (UpdateOnEnable)
                CopyTransform();
        }

        public void CopyTransform()
        {
            UpdateTransform();
            if (AutoUpdate && !UpdateOnLateUpdate)
                StartCoroutine(AutoUpdateCoroutine());
        }

        public void ChangeTransformVariableToCopy(TransformVariableAsset toCopy)
        {
            TransformReference.Reference = toCopy;
        }

        public void OnDisable()
        {
            StopAllCoroutines();

            if (UpdateOnDisable)
                UpdateTransform();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        private void UpdateTransform()
        {
            if (!TransformReference.Value)
                return;
            TargetToCopyOn.Value.Copy(TransformReference.Value, CopyPosition, CopyRotation, CopyScale);
        }

        private IEnumerator AutoUpdateCoroutine()
        {
            while (true)
            {
                UpdateTransform();
                yield return new WaitForSeconds(UpdateFrequency);
            }
        }

        private void LateUpdate()
        {
            if (!AutoUpdate || !UpdateOnLateUpdate)
                return;

            UpdateTransform();
        }

        public void OnDrawGizmos()
        {
            if (!Application.isPlaying && AutoUpdate)
            {
                UpdateTransform();
            }
        }
    }
}