using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class BoolValueFromFloatSign : MonoBehaviour, IValue<bool>
    {
        public FloatValue FloatToSign = new FloatValue() {UseReference = true};
        public bool Value => FloatToSign.Value >= 0;
        
        [ShowInInspector,LabelText("Is Positive")]
        private bool EditorValue => !FloatToSign.Reference || Value;
        
    }
}