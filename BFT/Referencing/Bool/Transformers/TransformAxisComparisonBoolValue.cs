﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class TransformAxisComparisonBoolValue : MonoBehaviour, IValue<bool>
    {
        private Vector3 cachedPositionA, cachedPositionB;

        [SerializeField] private bool cachePositionAtTheEndOfFrame = false;

        public Vector3 Margin;

        [Required] public UnityEngine.Transform TransformA;

        [Required] public UnityEngine.Transform TransformB;

        public bool Value
        {
            get
            {
                Vector3 positionA = cachePositionAtTheEndOfFrame ? cachedPositionA : TransformA.position;
                Vector3 positionB = cachePositionAtTheEndOfFrame ? cachedPositionB : TransformB.position;
                positionB.x += GetMargin(positionA.x, positionB.x, Margin.x);
                positionB.y += GetMargin(positionA.y, positionB.y, Margin.y);
                positionB.z += GetMargin(positionA.z, positionB.z, Margin.z);

                return Vector3.Distance(positionA, positionB) <= Vector3.kEpsilon;
            }
        }

        private void OnEnable()
        {
            if (cachePositionAtTheEndOfFrame)
                StartCoroutine(PositionsCachingCoroutine());
        }

        private IEnumerator PositionsCachingCoroutine()
        {
            while (true)
            {
                yield return null;
                cachedPositionA = TransformA.position;
                cachedPositionB = TransformB.position;
            }
        }

        private float GetMargin(float positionA, float positionB, float maxMargin)
        {
            float diff = positionA - positionB;
            return Mathf.Min(maxMargin, Mathf.Abs(diff)) * Mathf.Sign(diff);
        }
    }
}
