﻿using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace BFT
{
    public class IsReferenceInListBoolValue<T,T1> : SerializedMonoBehaviour, IValue<bool> where T1:IValue<T>
    {
        public T1 Element;
        public IList<T> List;

        public bool Value => List.Contains(Element.Value);
    }
}
