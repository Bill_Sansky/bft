﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class GameObjectReferenceComparisonBoolValue : MonoBehaviour, IValue<bool>
    {
        [BoxGroup("Objects"), SerializeField] private GameObjectValue objectToCompare;

        [BoxGroup("Events")] public UnityEvent OnDifferentObject;

        [BoxGroup("Events")] public UnityEvent OnSameObject;

        [BoxGroup("Objects"), SerializeField] private GameObjectValue referenceObject;

        public bool Value => referenceObject.Value == objectToCompare.Value;
    }
}