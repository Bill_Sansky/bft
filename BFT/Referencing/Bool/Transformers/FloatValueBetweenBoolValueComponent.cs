﻿using UnityEngine;

namespace BFT
{
    public class FloatValueBetweenBoolValueComponent : MonoBehaviour, IValue<bool>
    {
        public FloatValue FloatValue;
        public FloatValue MinForTrue, MaxForTrue;

        public bool Value => MathExt.IsClampedInclusive(FloatValue.Value, MinForTrue.Value, MaxForTrue.Value);
    }
}