﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class AndConditionListValueComponent : MonoBehaviour, IValue<bool>
    {
        public List<BoolValue> Conditions;

        [ShowInInspector, ReadOnly]
        public bool Value
        {
            get
            {
#if UNITY_EDITOR
                if (!Application.isPlaying)
                {
                    if (Conditions == null)
                        return false;
                }
#endif

                foreach (var condition in Conditions)
                {
                    if (!condition.Value)
                        return false;
                }

                return true;
            }
        }
    }
}
