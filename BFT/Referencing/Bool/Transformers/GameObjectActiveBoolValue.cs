using UnityEngine;

namespace BFT
{
    public class GameObjectActiveBoolValue : MonoBehaviour, IValue<bool>
    {
        public GameObjectValue GameObject;
        
        public bool Value => GameObject.Value.activeInHierarchy;
    }
}