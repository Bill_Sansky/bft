﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{

    public class OrConditionListComponent : MonoBehaviour, IValue<bool>
    {
        public List<BoolValue> Conditions = new List<BoolValue>();

        [ShowInInspector, ReadOnly]
        public bool Value
        {
            get
            {
#if UNITY_EDITOR
                if (!Application.isPlaying && Conditions == null)
                    return false;
#endif

                foreach (var condition in Conditions)
                {
                    if (condition.Value)
                    {
                        return true;
                    }
                }

                return false;
            }
        }
    }
}
