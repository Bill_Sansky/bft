﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class ButtonBoolValue : MonoBehaviour, IValue<bool>, ISavedObject
    {
        [SavedValue] public bool AutoRelease;

        [SavedValue] public bool IsPressed;

        public UnityEvent OnInstantPressed;
        public UnityEvent OnInstantReleased;

        public UnityEvent OnPressed;
        public UnityEvent OnPressedRequested;
        public UnityEvent OnReleaseDone;
        public UnityEvent OnReleaseRequested;

        [SavedValue] public bool PressedFalseOnReleaseStart = true;

        public object Save()
        {
            return SavedObjectUtils.GetSavedData(this);
        }

        public void Load(object saveFile)
        {
            SavedObjectUtils.LoadSaveData(this, (Dictionary<string, object>) saveFile);
            if (IsPressed)
                OnInstantPressed.Invoke();
            else
            {
                OnInstantReleased.Invoke();
            }
        }

        public bool Value => IsPressed;

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public void NotifyReleaseDone()
        {
            if (!PressedFalseOnReleaseStart)
                IsPressed = false;
            OnReleaseDone.Invoke();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public void NotifyPressedDone()
        {
            IsPressed = true;
            OnPressed.Invoke();
            if (AutoRelease)
            {
                RequestRelease();
            }
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public void RequestPress()
        {
            if (!IsPressed)
                OnPressedRequested.Invoke();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public void RequestRelease()
        {
            if (PressedFalseOnReleaseStart)
                IsPressed = false;
            OnReleaseRequested.Invoke();
        }
    }
}
