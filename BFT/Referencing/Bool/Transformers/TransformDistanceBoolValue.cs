﻿using System;
using UnityEngine;

namespace BFT
{
    public enum ComparisonOperations
    {
        GREATER,
        GREATER_EQUAL,
        LOWER,
        LOWER_EQUAL,
        EQUAL
    }

    public class TransformDistanceBoolValue : MonoBehaviour, IValue<bool>
    {
        public ComparisonOperations Condition = ComparisonOperations.LOWER;
        public UnityEngine.Transform First;
        public UnityEngine.Transform Second;

        public float Threshold = 1;

        public float Distance => First.Distance(Second);

        public bool Value
        {
            get
            {
                switch (Condition)
                {
                    case ComparisonOperations.GREATER:
                        return Distance > Threshold;
                    case ComparisonOperations.GREATER_EQUAL:
                        return Distance >= Threshold;
                    case ComparisonOperations.LOWER:
                        return Distance < Threshold;
                    case ComparisonOperations.LOWER_EQUAL:
                        return Distance <= Threshold;
                    case ComparisonOperations.EQUAL:
                        return Mathf.Approximately(Distance, Threshold);
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
    }
}