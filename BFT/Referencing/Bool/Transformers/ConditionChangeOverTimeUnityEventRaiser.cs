using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class ConditionChangeOverTimeUnityEventRaiser : MonoBehaviour
    {
        [BoxGroup("Values")] public BoolValue Condition;
        [BoxGroup("Values")] public FloatValue Timer;

        [BoxGroup("Init")] public bool ResetCheckOnRecheck = true;
        [BoxGroup("Init")] public bool CheckOnEnable;

        private bool previousBoolValue;

        [BoxGroup("Events")] public UnityEvent OnConditionChanged;
        [BoxGroup("Events")] public UnityEvent OnConditionRemained;

        [BoxGroup("Events")] public UnityEvent OnConditionTrueThenFalse;
        [BoxGroup("Events")] public UnityEvent OnConditionFalseThenTrue;
        [BoxGroup("Events")] public UnityEvent OnConditionTrueThenTrue;
        [BoxGroup("Events")] public UnityEvent OnConditionFalseThenFalse;

        private void OnEnable()
        {
            if (CheckOnEnable)
            {
                StartCheckingCondition();
            }
        }

        public void StartCheckingCondition()
        {
            if (ResetCheckOnRecheck)
                StopAllCoroutines();

            previousBoolValue = Condition.Value;
            this.CallAfterSomeTime(Timer.Value, CheckValueAfterTime);
        }

        private void CheckValueAfterTime()
        {
            if (previousBoolValue)
            {
                if (Condition.Value)
                {
                    OnConditionRemained.Invoke();
                    OnConditionTrueThenTrue.Invoke();
                }
                else
                {
                    OnConditionChanged.Invoke();
                    OnConditionTrueThenFalse.Invoke();
                }
            }
            else
            {
                if (!Condition.Value)
                {
                    OnConditionRemained.Invoke();
                    OnConditionFalseThenFalse.Invoke();
                }
                else
                {
                    OnConditionChanged.Invoke();
                    OnConditionFalseThenTrue.Invoke();
                }
            }
        }
    }
}