using UnityEngine;

namespace BFT
{
    public class TransformComparisonBoolValue : MonoBehaviour, IValue<bool>
    {
        public TransformValue ValueA;

        public TransformValue ValueB;

        public bool Value => ValueA.Value == ValueB.Value;
    }
}