﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class BoolVariableListWatcherEvent : MonoBehaviour
    {
        [InfoBox("Watches over a list of bool variable, and emits and event when all of them are true")]
        public List<BoolVariableComponent> Variables;

        [Tooltip("Should the event be raised as soon as one value is true (as opposed to all of them)")]
        public bool ConditionMetOnOneTrueInsteadOfAll = false;

        public UnityEvent OnConditionMet;

        [ShowInInspector, BoxGroup("Status"), ReadOnly, DisableInEditorMode]
        private int trueCount;

        public void OnEnable()
        {
            trueCount = 0;
            foreach (var variable in Variables)
            {
                variable.EmitUnityEvent = true;
                variable.EmitEventEvenWhenValueIsSame = false;
                variable.OnValueTrue.AddListenerClean(AddOneTrue);
                variable.OnValueFalse.AddListenerClean(AddOneFalse);
                if (variable.Value)
                    trueCount++;
            }
        }

        private void AddOneFalse(bool arg0)
        {
            Debug.Assert(Variables.All(_ => !_.EmitEventEvenWhenValueIsSame),
                "The count for the condition will be false because not all the variables are emitting events only when the value is changed",
                this);
            trueCount--;
        }

        private void AddOneTrue(bool arg0)
        {
            Debug.Assert(Variables.All(_ => !_.EmitEventEvenWhenValueIsSame),
                "The count for the condition will be false because not all the variables are emitting events only when the value is changed",
                this);

            trueCount++;
            if (trueCount >= Variables.Count)
                OnConditionMet.Invoke();
        }

#if UNITY_EDITOR

        [ShowInInspector, BoxGroup("Tools")] [ValueDropdown("tags")]
        private string tagToLookFor;

        private ValueDropdownList<string> tags => TagUtils.TagValueDropDown();

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        private void AddVariableToWatchInScene()
        {
            Debug.Assert(!Application.isPlaying, "calling this method during play is going to mess with the count");
            var gos = GameObject.FindGameObjectsWithTag(tagToLookFor);
            foreach (var go in gos)
            {
                if (go.scene == gameObject.scene)
                {
                    var boo = go.GetComponent<BoolVariableComponent>();
                    if (boo && !Variables.Contains(boo))
                        Variables.Add(boo);
                }
            }
        }
#endif
    }
}