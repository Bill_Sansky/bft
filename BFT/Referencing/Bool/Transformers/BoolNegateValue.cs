using UnityEngine;

namespace BFT
{
    public class BoolNegateValue : MonoBehaviour, IValue<bool>
    {
        public BoolValue InputBool;

        public bool Value => InputBool.Value;
    }
}