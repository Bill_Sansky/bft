﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    [CreateAssetMenu(menuName = "BFT/Data/Variables/Bool Variable", fileName = "Bool Variable")]
    public class BoolVariableAsset : VariableAsset<bool>
    {
        [BoxGroup("Events")] public UnityEvent OnValueTrue;
        [BoxGroup("Events")] public UnityEvent OnValueFalse;

        public override bool Value
        {
            get => base.Value;
            set
            {
                base.Value = value;
                if (value)
                    OnValueTrue.Invoke();
                else
                {
                    OnValueFalse.Invoke();
                }
            }
        }
    }
}