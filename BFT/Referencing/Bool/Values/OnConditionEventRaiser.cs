﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class OnConditionEventRaiser : MonoBehaviour
    {
        [InfoBox("Can check for a boolean value before raising an event by calling 'CheckAndRaise'." +
                 "\nIn that case the event will be raised only if the bool value is true")]
        [BoxGroup("Reference")]
        public BoolValue Condition;

        [BoxGroup("Reference")] public IEvent Event;

        [BoxGroup("Utils"), Button(ButtonSizes.Medium)]
        public void CheckAndRaise()
        {
            if (Condition == null || Condition.Value)
                Event.RaiseEvent();
        }
    }
}
