using UnityEngine;

namespace BFT
{
    public class DistanceVectorValueComponent : MonoBehaviour,IValue<Vector3>
    {
        public Vector3Value FromVector;
        public Vector3Value ToVector;

        public bool NormalizeVector;

        public FloatValue MagnitudeMultiplier;

        public Vector3 Value
        {
            get
            {
                var dir = (ToVector.Value - FromVector.Value);
                if (NormalizeVector)
                    dir = dir.normalized;
                return dir * MagnitudeMultiplier.Value;
            }
        } 
    }
}