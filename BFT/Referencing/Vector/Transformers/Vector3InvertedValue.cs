using UnityEngine;

namespace BFT
{
    public class Vector3InvertedValue : MonoBehaviour, IValue<Vector3>
    {
        public Vector3Value InputValue= new Vector3Value(){UseReference = true};
        public Vector3 Value => -InputValue.Value;
    }
}