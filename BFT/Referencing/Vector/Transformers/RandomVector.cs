using UnityEngine;

namespace BFT
{
    public class RandomVector : MonoBehaviour, IValue<Vector3>
    {
      
        public FloatValue MinX;
        public FloatValue MaxX;
            
        public FloatValue MinY;
        public FloatValue MaxY;    
       
        public FloatValue MinZ;
        public FloatValue MaxZ;
  

        public Vector3 Value => new Vector3(Random.Range(MinX.Value,MaxX.Value),Random.Range(MinY.Value,MaxY.Value),Random.Range(MinZ.Value,MaxZ.Value));
    }
}