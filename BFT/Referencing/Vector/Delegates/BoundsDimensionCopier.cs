using UnityEngine;

namespace BFT
{
    public class BoundsDimensionCopier : MonoBehaviour
    {
        public BoundFromColliders Bounds;
        public Vector3Variable DimensionVariable;

        public void Copy()
        {
            DimensionVariable.Value = Bounds.BoundDimension;
        }
    }
}
