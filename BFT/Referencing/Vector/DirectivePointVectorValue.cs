﻿using UnityEngine;

namespace BFT
{
    public class DirectivePointVectorValue : MonoBehaviour, IValue<Vector3>
    {
        public Vector3Value Direction;

        public bool LocalDirection;

        public UnityEngine.Transform Reference;

        public Vector3 Value =>
            (LocalDirection) ? Reference.InverseTransformPoint(Direction.Value) : Reference.position + Direction.Value;

        public void OnDrawGizmosSelected()
        {
            Gizmos.DrawLine(Reference.position,
                Value);
        }
    }
}
