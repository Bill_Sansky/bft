﻿using UnityEngine;

namespace BFT
{
    public class Vector3VariableComponent : VariableComponent<Vector3, Vector3Variable>
    {
        public void SetValueFromTransformPosition(Transform toCopy)
        {
            Value = toCopy.position;
        }
    }
}