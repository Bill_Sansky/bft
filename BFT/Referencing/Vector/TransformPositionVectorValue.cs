﻿using UnityEngine;

namespace BFT
{
    public class TransformPositionVectorValue : MonoBehaviour, IValue<Vector3>
    {
        public bool UseLocalPosition;

        public Vector3 Value => (UseLocalPosition) ? transform.localPosition : transform.position;
    }
}
