﻿using System;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class GameObjectVariable : GenericVariable<GameObject>
    {
    }
}