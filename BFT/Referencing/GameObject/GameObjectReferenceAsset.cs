using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class GameObjectReferenceAsset : ScriptableObject, IValue<UnityEngine.GameObject>
    {
        public UnityEngine.GameObject Reference;
        public UnityEngine.GameObject Value => Reference;

        public UnityEvent OnReferenceRemoved;
        public UnityEvent OnReferenceChanged;

        public void SetReference(UnityEngine.GameObject go)
        {
            Reference = go;
            if (Reference)
                OnReferenceChanged.Invoke();
            else
            {
                OnReferenceRemoved.Invoke();
            }
        }

        public void RemoveReferenceIfAlreadyReferenced(GameObject toCheck)
        {
            if (toCheck == Reference)
                RemoveReference();
        }

        public void RemoveReference()
        {
            Reference = null;
            OnReferenceRemoved.Invoke();
        }

        public bool IsReferenceSet => Reference;
    }
}