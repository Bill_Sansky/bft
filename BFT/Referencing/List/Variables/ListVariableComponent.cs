﻿using System.Collections.Generic;

namespace BFT
{
    public class ListVariableComponent<T, T1> : VariableComponent<List<T>, T1> where T1 : GenericVariable<List<T>>
    {
    }
}