﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    /// <summary>
    ///     A component holding a value that can be changed, or a reference to one
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="T1"></typeparam>
    public abstract class VariableComponent<T, T1> : MonoBehaviour, IVariable<T> where T1 : GenericVariable<T>
    {
        [Tooltip("If true, the value will emit an event every time it is changed. " +
                 "Otherwise, no event will ever be fired." +
                 " Put false if the value changes every frame to avoid garbage collection")]
        [BoxGroup("Events", Order = 999999), SerializeField]
        private bool emitUnityEvent;

        [BoxGroup("Events"), ShowIf("emitUnityEvent"), SerializeField]
        private bool emitEventEvenWhenValueIsSame = true;


        [InfoBox("The Event will be called only when this component sets the value, " +
                 "but not when the value it references changes.", InfoMessageType.Info, "emitUnityEvent")]
        [BoxGroup("Events"), SerializeField, ShowIf("emitUnityEvent")]
        private UnityEvent onBeforeValueChanged;

        [BoxGroup("Events"), SerializeField, ShowIf("emitUnityEvent")]
        private UnityEvent onValueChanged;

        [HideReferenceObjectPicker] public T1 Variable;

        public void SetValue(T value)
        {
            Variable.Value = value;
        }

        public void SetDefaultValue()
        {
            Variable.Value = default;
        }

        public bool IsVariableReference => Variable.UseReference;

        private bool VariableConstantAndEmitEvent => IsVariableReference && emitUnityEvent;

        protected T SelfValue => Value;

        public virtual T Value
        {
            get => Variable.Value;

            set
            {
                Debug.Assert(!Variable.UseReference || Variable.Reference,
                    "The reference was not set, but you are trying to assign a value to it!", this);


                if (!Equals(value, Variable.Value) || emitEventEvenWhenValueIsSame)
                {
                    if (emitUnityEvent)
                        onBeforeValueChanged.Invoke();

                    Variable.Value = value;
                    if (emitUnityEvent)
                        EmitEvent();
                }
            }
        }

        public bool EmitUnityEvent
        {
            get => emitUnityEvent;
            set => emitUnityEvent = value;
        }

        public UnityEvent OnValueChanged => onValueChanged;

        public bool EmitEventEvenWhenValueIsSame
        {
            get => emitEventEvenWhenValueIsSame;
            set => emitEventEvenWhenValueIsSame = value;
        }
        
        
        protected virtual void EmitEvent()
        {
            onValueChanged.Invoke();
        }

        public void SendValueToVariableObject(Object obj)
        {
            Debug.Assert(obj is IVariable<T>,$"The object pasted is not a IVariable<{typeof(T1)}>",this);
            IVariable<T> varia = (IVariable<T>) obj;
            varia.Value = Value;
        }
        
        public void SetVariableFromValueObject(Object obj)
        {
            Debug.Assert(obj is IValue<T>,$"The object pasted is not a IValue<{typeof(T1)}>",this);
            IValue<T> varia = (IValue<T>) obj;
            Value = varia.Value;
        }
    }
}