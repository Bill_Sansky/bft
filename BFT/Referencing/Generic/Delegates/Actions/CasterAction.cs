﻿using System;

namespace BFT
{
    [Serializable]
    public class CasterAction<T, T1,T2> where T2: BFTAction<T1> where T1:T
    {
        
        public T2 CastAction;
   
        public void Invoke(object param)
        {
            CastAction.Invoke((T1)param);
        }
    }
}