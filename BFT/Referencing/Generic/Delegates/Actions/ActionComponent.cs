﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class ActionComponent : MonoBehaviour
    {
        public BFTAction bftAction;

        [Button(ButtonSizes.Medium)]
        public void Invoke()
        {
            Debug.Assert(bftAction.Act!=null,"The Action was not defined but you are trying to call it",this);
            bftAction.Act();
        }
    }
}
