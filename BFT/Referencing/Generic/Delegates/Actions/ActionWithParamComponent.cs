﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace BFT
{
    public abstract class ActionWithParamComponent<T, T1, T2> : MonoBehaviour
        where T1 : BFTAction<T> where T2 : IValue<T>
    {
        [BoxGroup("Action")] public T1 Action;
        [BoxGroup("Options")]
        public bool UseOwnValue = true;
        [BoxGroup("Options")] public bool ExecuteOnAwake = false;
        [BoxGroup("Options")] public bool ExecuteOnEnable = false;
        [BoxGroup("Options")] public bool ExecuteRegularly = false;

        [BoxGroup("Options"), ShowIf("ExecuteRegularly")]
        public float ExecutionTimeInterval = 0;

        [FormerlySerializedAs("Value")] [BoxGroup("Action")] [ShowIf("UseOwnValue")]
        public T2 ValueForAction;

        [FoldoutGroup("Tools")]
        [Button(ButtonSizes.Medium)]
        public void Invoke()
        {
            Debug.Assert(UseOwnValue,
                "You are invoking the action without specifying a parameter, but you are not using the 'Use Own Value' " +
                "option: this will call the action with a null or default value",
                this);
                
            Debug.Assert(Action.TargetAction!=null ,"The target of the action is undefined, pleased fix it",this);
            Debug.Assert(Action.ActionName!=null ,"The target of the action is undefined, pleased fix it",this);
            Debug.Assert(Action.TargetAction.GetMethodInfo(typeof(T))!=null ,"The target of the action is undefined, pleased fix it",this);
            
            Action.Act(ValueForAction.Value);
        }

        public void InvokeWithParameter(T parameter)
        {
            Action.Act(parameter);
        }

        public void InvokeWithObjectParameter(object param)
        {
            Debug.Assert(param is T,$"The object parameter ({param}) is not of the expected type {typeof(T)}",this);
            Action.Act((T) param);
        }

        public void Awake()
        {
            if (ExecuteOnAwake)
                Invoke();
        }

        public void OnEnable()
        {
            if (ExecuteOnEnable)
                Invoke();

            if (ExecuteRegularly)
            {
                this.CallRegularly(Invoke, ExecutionTimeInterval);
            }
        }
    }
}