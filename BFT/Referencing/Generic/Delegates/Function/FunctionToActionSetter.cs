using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class FunctionToActionSetter<T, T1, T2> : MonoBehaviour where T1 : BFTAction<T> where T2 : BFTFunction<T>
    {
        public T2 Getter;
        public T1 Setter;

        public bool SetValueOnEnable = false;

        public void OnEnable()
        {
            if (SetValueOnEnable)
                SetValue();
        }

        [Button(ButtonSizes.Medium)]
        public void SetValue()
        {
            Setter.Invoke(Getter.Invoke());
        }
    }
}