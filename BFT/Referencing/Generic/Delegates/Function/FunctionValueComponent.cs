﻿using System;
using UnityEngine;

namespace BFT
{
    public abstract class FunctionValueComponent<T, T1> : MonoBehaviour, IValue<T> where T1 : BFTFunction<T>
    {
        public T1 Function;

        public T Value
        {
            get
            {
#if UNITY_EDITOR
                try
                {
                    return Function.Value;
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                    Debug.LogError(
                        "The function produced an exception, make sure things are linked together or that your code works fine",
                        this);
                }

#endif
                return Function.Value;
            }
        }
    }
}