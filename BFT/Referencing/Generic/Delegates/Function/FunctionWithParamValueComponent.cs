﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public abstract class FunctionWithParamValueComponent<T, T2, T1, T3> : MonoBehaviour, IValue<T>
        where T1 : BFTFunction<T, T2> where T3 : GenericValue<T2>
    {
        public T1 Function;
        public T3 ParameterValue;

        [ShowInInspector, BoxGroup("Status")] public T Value => Function.Invoke(ParameterValue.Value);
    }
}