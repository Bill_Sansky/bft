﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT.Transformers
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    [InfoBox("Sets a variable from a function")]
    public class VariableSetterFromFunction<T, T1, T2> : MonoBehaviour
        where T1 : GenericVariable<T>, new() where T2 : BFTFunction<T>
    {
        [BoxGroup("Referencing")]
        public T1 Variable = new T1() {UseReference = true};
        [BoxGroup("Referencing")]
        public T2 Function;

        [BoxGroup("Options")]
        public bool SetValueOnEnable;
        [BoxGroup("Options")]
        public bool SetValueRegularly;
        [BoxGroup("Options")]
        [ShowIf("SetValueRegularlyOnEnable")]
        public float ValueSettingTimeInterval = 0;
        
        public void OnEnable()
        {
            if (SetValueOnEnable)
                SetValue();
            if(SetValueRegularly)
                this.CallRegularly(SetValue, ValueSettingTimeInterval);
        }

        public void OnDisable()
        {
            if(SetValueRegularly)
                StopAllCoroutines();
        }

        [BoxGroup("Tools")]
        [Button]
        public void SetValue()
        {
            Variable.Value = Function.Value;
        }
    }
}