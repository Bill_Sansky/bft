﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace BFT.Transformers
{
    public class VariableSetterFromValue<T, T1, T2> : MonoBehaviour
        where T1 : GenericVariable<T>, new() where T2 : GenericValue<T>
    {
        public T1 Variable = new T1() {UseReference = true};

        public T2 Value;

        public bool SetValueOnEnable;

        [FormerlySerializedAs("SetValueRegularly")] public bool SetValueRegularlyOnEnable;
        [ShowIf("SetValueRegularlyOnEnable")] public float ValueSettingTimeInterval = 0;

        public void OnEnable()
        {
            if (SetValueOnEnable)
                SetValue();
            if (SetValueRegularlyOnEnable)
                StartToSetValueRegularly();
        }

        public void StartToSetValueRegularly()
        {
            this.CallRegularly(SetValue, ValueSettingTimeInterval);
        }

        public void StopToSetValueRegularly()
        {
            StopAllCoroutines();
        }

        public void OnDisable()
        {
            if (SetValueRegularlyOnEnable)
                StopAllCoroutines();
        }

        [Button]
        public void SetValue()
        {
            Variable.Value = Value.Value;
        }
    }
}