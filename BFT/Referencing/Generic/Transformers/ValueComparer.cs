﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

namespace BFT.Transformers
{
    [InfoBox("Compares a value to a list of values. Whenever called," +
             " an event will be called wether the list contains the value or not")]
    public class ValueComparer<T, T1> : MonoBehaviour where T1 : GenericValue<T>
    {
        [BoxGroup("References")] public T1 ValueToCheck;
        [BoxGroup("References")] public List<T1> GoodValues;

        [FormerlySerializedAs("CheckOnEnable")] [BoxGroup("Options")] public bool CompareOnEnable = false;

        [BoxGroup("Events")] public UnityEvent OnBeforeCheck;
        [BoxGroup("Events")] public UnityEvent OnGoodValue;
        [BoxGroup("Events")] public UnityEvent OnBadValue;

        public void OnEnable()
        {
            if (CompareOnEnable)
                CompareValue();
        }

        public void CompareValue()
        {
            OnBeforeCheck.Invoke();
            foreach (var value in GoodValues)
            {
                //TODO handle Unity Object null check with a virtual method checking for "null"
                if (ValueToCheck.Value == null)
                {
                    if (value.Value == null)
                    {
                        OnGoodValue.Invoke();
                        return;
                    }
                }
                else if (ValueToCheck.Value.Equals(value.Value))
                {
                    OnGoodValue.Invoke();
                    return;
                }
            }

            OnBadValue.Invoke();
        }
    }
}