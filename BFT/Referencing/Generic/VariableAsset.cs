﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

namespace BFT
{
    public enum VariableChangePermission
    {
        FORBIDDEN,
        ALLOW,
        ALLOW_TEMPORARY
    }

    /// <summary>
    ///     An Asset Holding a value that can be changed or not depending on the authorization it gets
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    [Serializable]
    public class VariableAsset<T1> : SerializedScriptableObject, IVariable<T1>
    {
        [SerializeField, HideInInspector] private T1 cachedValue;

        [Tooltip("If true, the value will emit an event every time it is changed. " +
                 "Otherwise, no event will ever be fired." +
                 " Put false if the value changes every frame to avoid garbage collection")]
        [BoxGroup("Events"), SerializeField]
        private bool emitUnityEvent;

        [BoxGroup("Events"), ShowIf("emitUnityEvent"), SerializeField]
        private bool emitEventEvenWhenValueIsSame = true;

        [BoxGroup("Options"), SerializeField] private bool logDebug;

        [BoxGroup("Events"), SerializeField, ShowIf("emitUnityEvent")]
        public UnityEvent OnValueChanged;

        [BoxGroup("Variable", Order = -1), SerializeField, OnValueChanged("ChangeCachedValue"), HideLabel]
        private T1 value;

        [BoxGroup("Options"), SerializeField]
        private VariableChangePermission variableChangePermission = VariableChangePermission.ALLOW;

        public void SetValueToDefault()
        {
            Value = default;
        }

        public bool IsVariableChangeDiscrete
        {
            get => emitUnityEvent;
            set => emitUnityEvent = value;
        }

        public VariableChangePermission ChangePermission
        {
            get => variableChangePermission;
            set => variableChangePermission = value;
        }

        public virtual T1 Value
        {
            get => value;
            set
            {
                if (logDebug)
                {
                    UnityEngine.Debug.LogFormat(this, "Value change was requested on {0}", name);
                }

                if (ChangePermission == VariableChangePermission.FORBIDDEN)
                {
                    UnityEngine.Debug.LogError("BFT Error: you are trying to change the value of a variable " +
                                               "that was marked FORBIDDEN");
                    return;
                }

                if (!Equals(value, this.value) || emitEventEvenWhenValueIsSame)
                {
                    this.value = value;

#if UNITY_EDITOR
                    UnityEditor.EditorUtility.SetDirty(this);
#endif
                    if (emitUnityEvent)
                        InvokeValueChangedEvents();
                }
            }
        }

        protected virtual void ChangeCachedValue()
        {
            cachedValue = value;
        }

        public T1 GetValue()
        {
            return Value;
        }

        public void ResetValueIfValueEqual(T1 otherValue)
        {
            if (Value.Equals(otherValue))
            {
                Value = default;
            }
        }

        public void SetValueFromIValue(Object valueObject)
        {
            Debug.Assert(valueObject is IValue<T1>, $"The object pasted is not a IValue<{typeof(T1)}>", this);

            IValue<T1> va = (IValue<T1>) valueObject;
            Value = va.Value;
        }

        public void OnEnable()
        {
            if (ChangePermission == VariableChangePermission.ALLOW_TEMPORARY)
            {
                value = cachedValue;
            }
        }

        public void OnDisable()
        {
            if (ChangePermission == VariableChangePermission.ALLOW_TEMPORARY)
                value = cachedValue;
        }

        protected virtual void InvokeValueChangedEvents()
        {
            OnValueChanged.Invoke();
        }
    }
}