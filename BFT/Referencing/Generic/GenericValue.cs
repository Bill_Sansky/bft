﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace BFT
{
    /// <summary>
    ///     Can hold a reference to an object that implements the IValue iterface of the type specified, or a direct value of
    ///     the type specified
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    [InlineProperty]
    public class GenericValue<T> : IValue<T>
    {
        [HideLabel, CustomValueDrawer("EditorDrawRef")] [HorizontalGroup(Width = 20)]
        public bool UseReference = false;

        [SerializeField, HideInInspector] public T LocalValue;

        [FormerlySerializedAs("Variable")]
        [HorizontalGroup]
        [ShowIf("UseReference"), HideLabel, OnValueChanged("EditorCheckType")]
        [CustomContextMenu("Toggle Current Value", "ToggleCurrentValue")]
        public UnityEngine.Object Reference;

        public GenericValue()
        {
        }

        public GenericValue(T localValue)
        {
            this.LocalValue = localValue;
        }

        [HorizontalGroup]
        [HideIf("UseReference"), HideLabel, ShowInInspector,HideReferenceObjectPicker]
        protected virtual T LocalValueInspected
        {
            get => LocalValue;
            set => LocalValue = value;
        }

        private bool IsVariableDefined => UseReference && !Reference;

#if UNITY_EDITOR
        [NonSerialized] private bool showCurrent = false;

        private bool ShowCurrentRefValue => showCurrent && UseReference && Reference;

        private void ToggleCurrentValue()
        {
            showCurrent = !showCurrent;
        }
#endif

        [ShowInInspector]
        [ShowIf("ShowCurrentRefValue")]
        [LabelText("Current Value")]
        [InfoBox("$RefProblemMessage", InfoMessageType.Warning, GUIAlwaysEnabled = true,
            VisibleIf = "IsVariableDefined")]
        public virtual T Value
        {
            get
            {
                if (!UseReference)
                {
                    return LocalValue;
                }

                if (!Reference)
                    return default;

#if UNITY_EDITOR

                stackOver++;

                if (stackOver > 50)
                {
                    UnityEngine.Debug.LogError(
                        "The variable is linking to itself at some point: this is not allowed, the reference usage has been disabled",
                        Reference);
                    Reference = null;
                    UseReference = false;
                    stackOver = 0;
                    return default;
                }

                T value = ((IValue<T>) Reference).Value;

                stackOver = 0;

                return value;

#else
                return ((IValue<T>) Reference).Value;
#endif
            }
        }

#if UNITY_EDITOR
        protected virtual void EditorCheckType()
        {
            InterfaceUtils.GetObjectAfterTypeCheck<IValue<T>>(ref Reference);

            if (!Reference)
                return;

            var type = Reference.GetType();

            var fields = type.GetAllFields();

            foreach (var field in fields)
            {
                var value = field.GetValue(Reference);
                if (value == this)
                {
                    UnityEngine.Debug.LogWarning("Self referencing is not allowed!", Reference);
                    Reference = null;
                    return;
                }
            }

            if (!Reference)
                RefProblemMessage =
                    $"The Variable linked was of the wrong type, expecting a {typeof(T).Name} Value";
        }

#endif
        public void SetAndUseReference(UnityEngine.Object reference)
        {
            Reference = reference;
            UseReference = true;
        }

#if UNITY_EDITOR
        private ValueDropdownList<bool> ReferenceDropDown =>
            new ValueDropdownList<bool>()
            {
                {"Value", false},
                {"Reference", true}
            };

        public virtual bool EditorDrawRef(bool value, GUIContent label)
        {
            if (popupStyle == null)
            {
                popupStyle = new GUIStyle(GUI.skin.GetStyle("PaneOptions"))
                {
                    imagePosition = ImagePosition.ImageOnly, alignment = TextAnchor.MiddleCenter, fixedHeight = 20,
                    fixedWidth = 20,
                };
            }

            /*  if (label != null)
              EditorGUILayout.LabelField(label);*/
            bool useConstant = !value;

            int result = EditorGUILayout.Popup(useConstant ? 0 : 1, PopupOptions, popupStyle, GUILayout.MinWidth(10),
                GUILayout.MinHeight(15));

            return result != 0;
        }

        /// <summary> Cached style to use to draw the popup button. </summary>
        private static GUIStyle popupStyle;

        private static readonly string[] PopupOptions =
            {"Use Value", $"Use Reference (Type Value<{typeof(T).Name}>)"};

#endif

        public override bool Equals(object obj)
        {
            if (obj is IValue<T> otherval)
                return Value.Equals(otherval.Value);
            return base.Equals(obj);
        }

        protected bool Equals(GenericValue<T> other)
        {
            return Value.Equals(other.Value);
            ;
        }

        public override int GetHashCode()
        {
            var hashCode = EqualityComparer<T>.Default.GetHashCode(Value);
            return hashCode;
        }


#if UNITY_EDITOR

        private string RefProblemMessage = $"Please Set a Reference of type {typeof(T).Name}";
        private int stackOver = 0;
#endif
    }
}