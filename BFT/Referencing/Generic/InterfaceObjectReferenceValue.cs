using System;
using Sirenix.OdinInspector;

namespace BFT
{
    [Serializable]
    [InlineProperty]
    public class InterfaceObjectReferenceValue<T> : IValue<T> where T : class
    {
        [HideLabel,OnValueChanged("EditorCheckType")]
        public UnityEngine.Object Reference;

#if UNITY_EDITOR
        protected void EditorCheckType()
        {
            InterfaceUtils.GetObjectAfterTypeCheck<T>(ref Reference);

            /*var type = Reference.GetType();

            var fields = type.GetAllFields();

            foreach (var field in fields)
            {
                var value = field.GetValue(Reference);
                if (value == this)
                {
                    UnityEngine.Debug.LogWarning("Self referencing is not allowed!", Reference);
                    Reference = null;
                    return;
                }
            }*/
            
        }

#endif
        
        public T Value => Reference as T;
    }
}