﻿using System;

namespace BFT
{
    [Serializable]
    public class SpriteVariable : GenericVariable<UnityEngine.Sprite>
    {
        public SpriteVariable(UnityEngine.Sprite value)
        {
            Value = value;
        }
    }
}
