using System;
using UnityEngine.Events;

namespace BFT
{
    public class EventFunctionValueComponent : FunctionValueComponent<UnityEvent, EventBFTFunction>
    {
        public UnityEvent OnEventInvoked;

        public void Awake()
        {
            Value.AddListener(OnEventInvoked.Invoke);
        }
    }
}