using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BFT
{
    public class UIToggleHandler : MonoBehaviour
    {
        public Toggle Toggle;

        public UnityEvent OnToggleOn;
        public UnityEvent OnToggleOff;

        public bool DebugLog;
        
        public void Awake()
        {
            Toggle.onValueChanged.AddListenerClean(CheckStatus);
        }

        private void CheckStatus(bool arg0)
        {
            if (Toggle.isOn)
            {
                if (DebugLog)
                    Debug.Log("Toggle On");
                OnToggleOn.Invoke();
            }
            else
            {
                if (DebugLog)
                    Debug.Log("Toggle Off");
                
                OnToggleOff.Invoke();
            }
        }
    }
}