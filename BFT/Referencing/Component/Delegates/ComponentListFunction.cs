using System;
using System.Collections.Generic;
using BFT;

namespace Plugins.BFT.BFT.Referencing.Component.Delegates
{
    [Serializable]
    public class ComponentListFunction : BFTFunction<List<UnityEngine.Component>>
    {
    }
}