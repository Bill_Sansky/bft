using System;
using System.Collections.Generic;
using BFT;

namespace Plugins.BFT.BFT.Referencing.Component.Delegates
{
    [Serializable]
    public class ComponentListAction : BFTAction<List<UnityEngine.Component>>
    {
        
    }
}