using UnityEngine;

namespace BFT
{
    public class FloatVariableFrameUpdater : MonoBehaviour
    {
        public FloatVariable VariableToUpdate;
        public FloatBFTFunction ValueToTake;

        public void Update()
        {
            VariableToUpdate.Value = ValueToTake.Value;
        }
    }
}