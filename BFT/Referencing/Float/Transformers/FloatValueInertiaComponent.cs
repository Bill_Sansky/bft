﻿using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;

#if UNITY_EDITOR

#endif


namespace BFT
{
    public class FloatValueInertiaComponent : SerializedMonoBehaviour,
        IValue<float>
    {
        [BoxGroup("Value")] public bool UseExternalVariable = false;

        [BoxGroup("Value")] [ShowIf("UseExternalVariable")]
        public FloatVariableComponent Variable;

        [BoxGroup("Value")] [ShowInInspector, ReadOnly]
        private float currentValue;

        [FormerlySerializedAs("FloatGiver")] public FloatValue InputFloat;

        public FloatValue SpeedDown;
        public FloatValue SpeedUp;

        public float FloatValue
        {
            get => UseExternalVariable ? Variable.Value : currentValue;
            set
            {
                if (!UseExternalVariable)
                    currentValue = value;
                else
                {
                    Variable.Value = value;
                }
            }
        }

        public float Value => FloatValue;

        void Start()
        {
            FloatValue = InputFloat.Value;
        }

        void Update()
        {
            FloatValue = Mathf.Lerp(FloatValue, InputFloat.Value,
                (InputFloat.Value > FloatValue)
                    ? SpeedUp.Value * UnityEngine.Time.deltaTime
                    : SpeedDown.Value * UnityEngine.Time.deltaTime);
        }

        public void ForceTargetValue()
        {
            FloatValue = InputFloat.Value;
        }

        public void OnDrawGizmosSelected()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                FloatValue = Mathf.Lerp(FloatValue, InputFloat.Value,
                    (InputFloat.Value > FloatValue) ? SpeedUp.Value * 0.02f : SpeedDown.Value * 0.02f);
                EditorUtility.SetDirty(this);
                return;
            }
#endif
        }
    }
}