using UnityEngine;

namespace BFT
{
    public class FloatNegationValueComponent : MonoBehaviour, IValue<float>
    {
        public FloatValue InputValue;
        public float Value => -InputValue.Value;
    }
}