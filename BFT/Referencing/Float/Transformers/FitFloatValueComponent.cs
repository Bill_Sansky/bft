using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class FitFloatValueComponent : MonoBehaviour, IValue<float>
    {
        public FloatValue InputValue;
        public FloatValue MinInput;
        public FloatValue MaxInput;
        public FloatValue MinOuput;
        public FloatValue MaxOutput;


        public bool UseInputCurve;
        [ShowIf("UseInputCurve")] public AnimationCurveValue InputPercentCurve;

        private bool IsInputDefined => InputValue != null && InputPercentCurve.Value != null;

        [ShowInInspector, ShowIf("IsInputDefined"), LabelText("Value")]
        public float EditorValue => IsInputDefined ? Value : 0;

        public float Value => UseInputCurve
            ? MathExt.Fit(InputValue.Value, MinInput.Value, MaxInput.Value, MinOuput.Value, MaxOutput.Value,
                InputPercentCurve.Value)
            : MathExt.Fit(InputValue.Value, MinInput.Value, MaxInput.Value, MinOuput.Value, MaxOutput.Value);
    }
}