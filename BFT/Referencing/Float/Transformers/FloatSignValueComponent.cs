using UnityEngine;

namespace BFT
{
    public class FloatSignValueComponent : MonoBehaviour, IValue<float>
    {
        public FloatValue InputValue;
        public float Value => Mathf.Sign(InputValue.Value);
    }
}