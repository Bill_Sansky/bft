using UnityEngine;

namespace BFT
{
    public class FloatTimedLerp : MonoBehaviour, IValue<float>
    {
        public FloatVariable ValueToLerp = new FloatVariable(0) {UseReference = true};
        public FloatValue StartValue;
        public FloatValue EndValue;

        public AnimationCurve LerpingCurve;
        
        public FloatValue Duration;

        public bool SampleStartValueOnlyOnLerpStart = true;
        public bool SampleEndValueOnlyOnLerpStart = false;

        private float sampledStart;
        private float sampledEnd;

        public bool LerpOnEnable;

        public void StartLerp()
        {
            if (SampleEndValueOnlyOnLerpStart)
                sampledStart = StartValue.Value;
            if (SampleEndValueOnlyOnLerpStart)
                sampledEnd = EndValue.Value;
            this.CallForSomeTime(Duration.Value, LerpValue);
        }

        public void StopToLerp()
        {
            StopAllCoroutines();
        }
        
        private void LerpValue(float time)
        {
            float end = SampleEndValueOnlyOnLerpStart ? sampledEnd : EndValue.Value;
            float start = SampleStartValueOnlyOnLerpStart ? sampledStart : StartValue.Value;
            ValueToLerp.Value = Mathf.Lerp(start, end, time / Duration.Value);

        }

        public float Value => ValueToLerp.Value;
    }
}