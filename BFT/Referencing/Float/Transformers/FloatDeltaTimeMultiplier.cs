using UnityEngine;

namespace BFT
{
    public class FloatDeltaTimeMultiplier : MonoBehaviour, IValue<float>
    {
        public FloatValue InputValue;

        public float Value => InputValue.Value * Time.deltaTime;
    }
}