using UnityEngine;

namespace BFT
{
    public class FloatValueIncrementer : MonoBehaviour
    {
        public FloatVariable VariableToIncrement;
        public FloatValue Increment;
        public bool NegateIncrement;

        public bool IncrementOnEnable;

        public void OnEnable()
        {
            if (IncrementOnEnable)
                IncrementVariable();
        }

        public void IncrementVariable()
        {
            VariableToIncrement.Value += NegateIncrement ? -Increment.Value : Increment.Value;
        }
    }
}