using UnityEngine;

namespace BFT
{
    public class FloatPercentLerp : MonoBehaviour, IValue<float>
    {
        public FloatValue StartValue;
        public FloatValue EndValue;
        public AnimationCurve Profile;
        public PercentValue Percent;
        
        public float Value => Mathf.Lerp(StartValue.Value, EndValue.Value, Profile.Evaluate(Percent.Value));
    }
}