using System;
using UnityEngine;

namespace BFT
{
    public class FloatVariableClamper : MonoBehaviour
    {
        public FloatVariable VariableToClamp;
        public FloatValue MinValue;
        public FloatValue MaxValue;

        public bool ClampEveryFrame;

        public bool ClampOnEnable;

        public void Clamp()
        {
            VariableToClamp.Value = Mathf.Clamp(VariableToClamp.Value, MinValue.Value, MaxValue.Value);
        }

        private void OnEnable()
        {
            if (ClampOnEnable)
                Clamp();
            if (ClampEveryFrame)
            {
                this.CallEveryFrame(Clamp);
            }
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }
    }
}