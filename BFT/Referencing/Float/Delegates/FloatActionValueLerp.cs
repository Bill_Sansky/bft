﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class FloatActionValueLerp : MonoBehaviour
    {
        public FloatValue StartLerp;
        public FloatValue EndLerp;

        public PercentValue Percent;

        public AnimationCurve PercentCurve;

        public FloatAction Action;

        public bool LerpRegularlyOnEnable;
        [ShowIf("LerpRegularlyOnEnable")]
        public float LerpTimeInterval = 0;

        public void Lerp()
        {
            Action.Invoke(Mathf.Lerp(StartLerp.Value, EndLerp.Value, PercentCurve.Evaluate(Percent.Value)));
        }

        public void OnEnable()
        {
            if (LerpRegularlyOnEnable)
                StartLerpRegularly();
        }

        public void StartLerpRegularly()
        {
            this.CallRegularly(Lerp, LerpTimeInterval);
        }

        public void StopLerpRegularly()
        {
            StopAllCoroutines();
        }

        public void OnDrawGizmos()
        {
            if (Application.isPlaying)
                return;

            try
            {
                Lerp();
            }
            catch (Exception)
            {
            }
        }
    }
}