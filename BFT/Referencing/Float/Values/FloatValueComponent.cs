﻿namespace BFT
{
    public class FloatValueComponent : ValueComponent<float, FloatValue>, IValue<object>
    {
        object IValue<object>.Value => Value;
    }
}