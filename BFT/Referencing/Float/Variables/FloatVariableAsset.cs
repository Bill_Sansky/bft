﻿using UnityEngine;

namespace BFT
{
    [CreateAssetMenu(menuName = "BFT/Data/Variables/Float Variable", fileName = "Float Variable")]
    public class FloatVariableAsset :
        VariableAsset<float>
    {
        public float FloatValue => Value;

        public void SetValueFromComponent(FloatValueComponent component)
        {
            Value = component.Value;
        }

        public void AddValue(float toAdd)
        {
            Value += toAdd;
        }

        public void AddFromFloatValue(Object floatValue)
        {
            IValue<float> val = (IValue<float>) floatValue;

            Value += val.Value;
        }

        public void RemoveValue(float toRemove)
        {
            Value -= toRemove;
        }

        public void RemoveFromFloatValue(Object floatValue)
        {
            IValue<float> val = (IValue<float>) floatValue;

            Value -= val.Value;
        }
    }
}