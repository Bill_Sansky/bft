﻿using System.Globalization;

namespace BFT
{
    public class FloatVariableComponent : VariableComponent<float, FloatVariable>, IValue<string>
    {
        string IValue<string>.Value => Value.ToString(CultureInfo.InvariantCulture);
    }
}