﻿#if BFT_REWIRED
using Sirenix.OdinInspector;
using UnityEngine.Events;
using Rewired;
using UnityEngine;

namespace BFT
{
    public abstract class AxisAction : MonoBehaviour
    {
        private bool active = false;
        [BoxGroup("Activity")] public float activityThreshold = 0.01f;

        
        [BoxGroup("Activity")] public UnityEvent OnBecameActive;
        [BoxGroup("Activity")] public UnityEvent OnBecameInactive;

        [BoxGroup("Tools")] public bool DebugLog;
        
        public void CheckAxis(Player player, int axisID)
        {
            float value = player.GetAxis(axisID);

            if(DebugLog)
                Debug.Log($"{name} Axis Value : {value}");
            
            if (Mathf.Abs(value) > activityThreshold)
            {
                if (!active)
                {
                    active = true;
                    OnBecameActive.Invoke();
                }
            }
            else
            {
                if (active)
                {
                    active = false;
                    OnBecameInactive.Invoke();
                }
            }

            AxisValueAction(value);
        }

        public abstract void AxisValueAction(float value);
    }
}
#endif