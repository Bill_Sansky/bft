﻿using UnityEngine;
using UnityEngine.Serialization;
#if BFT_REWIRED
using Sirenix.OdinInspector;

namespace BFT
{
    public class AxisFloatValueIncrementAction : AxisAction
    {
        public FloatValue MaxDownSpeed;
        public FloatValue MaxUpSpeed;

        [FormerlySerializedAs("Percent")] public FloatVariable FloatVariable;

        public override void AxisValueAction(float value)
        {
            float speed = value < 0 ? MaxDownSpeed.Value : MaxUpSpeed.Value;
            FloatVariable.Value += speed * value * Time.deltaTime;
        }
    }
}

#endif