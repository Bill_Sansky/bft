﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class PhysicalSelectable : MonoBehaviour
    {
        public Collider Collider;

        [InfoBox("Registers an object through a collider to be selected based on triggers by a selector object")]
        [SerializeField, HideInInspector]
        private UnityEngine.UI.Selectable selectable;

        public SelectableColliderDictionary SelectableColliderDictionary;

        void Reset()
        {
            selectable = GetComponent<UnityEngine.UI.Selectable>();
        }

        void Awake()
        {
            if (!selectable)
                selectable = GetComponent<UnityEngine.UI.Selectable>();
        }

        void OnEnable()
        {
            SelectableColliderDictionary.RegisterObject(Collider, selectable);
        }

        private void OnDisable()
        {
            SelectableColliderDictionary.UnRegisterObject(Collider);
        }
    }
}
