using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BFT
{
    public class EventSystemWatcher : MonoBehaviour
    {
        public bool DebugLog;

        public bool ReselectOnSelectionConflict = true;

        [ShowIf("ReselectOnSelectionConflict")]
        public SelectableVariableComponent SelectableVariable;

        [ReadOnly] public GameObject SelectedObject;

        public void Update()
        {
            var go = EventSystem.current.currentSelectedGameObject;
            if (go != SelectedObject)
            {
                SelectedObject = go;
                if (DebugLog)
                    UnityEngine.Debug.Log($"New selection: {go?.name}", this);
                if (ReselectOnSelectionConflict && !SelectedObject && SelectableVariable.Value)
                {
                    SelectableVariable.Value.Select();
                }
            }
        }
    }
}
