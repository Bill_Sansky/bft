using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BFT
{
    public class AdvancedUISelectable : MonoBehaviour
    {
        [InfoBox("Allows you to call select and deselect through events and delegates")] [SerializeField]
        private UnityEngine.UI.Selectable selectable;

        void Reset()
        {
            selectable = GetComponent<UnityEngine.UI.Selectable>();
        }

        public void Select()
        {
            if (selectable)
                selectable.Select();
        }

        public void DeSelect()
        {
            if (selectable && EventSystem.current.currentSelectedGameObject == selectable.gameObject)
                EventSystem.current.SetSelectedGameObject(null);
        }
    }
}
