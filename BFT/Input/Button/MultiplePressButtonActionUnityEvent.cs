#if BFT_REWIRED

using System;
using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class MultiplePressButtonActionUnityEvent : ButtonActionUnityEvent
    {
        [BoxGroup("Multiple Press", Order = -1)]
        public bool CountOnButtonDown = true;

        [BoxGroup("Multiple Press", Order = -1)]
        public FloatValue MaxTimeBetweenInput = new FloatValue(0.3f);

        [BoxGroup("Multiple Press", Order = -1)]
        public IntValue InputAmountRequired = new IntValue(2);

        [BoxGroup("Status"), ShowInInspector, ReadOnly]
        private bool isCounting = false;

        [BoxGroup("Status"), ShowInInspector, ReadOnly]
        private int currentInputCount;

        [BoxGroup("Events")] public UnityEvent OnInputCountMatched, OnInputCountDidNotMatch;

        public void OnEnable()
        {
            isCounting = false;
            currentInputCount = 0;
        }

        public override void OnButtonDownAction()
        {
            base.OnButtonDownAction();
            if (CountOnButtonDown)
                CountLogic();
        }

        private void CountLogic()
        {
            if (!isCounting)
            {
                StartCoroutine(Count());
                currentInputCount++;
            }
            else
            {
                currentInputCount++;
                if (currentInputCount >= InputAmountRequired.Value)
                {
                    StopAllCoroutines();
                    isCounting = false;
                    currentInputCount = 0;
                    OnInputCountMatched.Invoke();
                }
            }
        }

        private IEnumerator Count()
        {
            isCounting = true;
            currentInputCount = 0;
            yield return new WaitForSeconds(MaxTimeBetweenInput.Value * InputAmountRequired.Value);
            isCounting = false;
            currentInputCount = 0;
            OnInputCountDidNotMatch.Invoke();
        }

        public override void OnButtonHeldAction()
        {
            base.OnButtonHeldAction();
        }

        public override void OnButtonUpAction()
        {
            base.OnButtonUpAction();
            if (!CountOnButtonDown)
                CountLogic();
        }
    }
}

#endif