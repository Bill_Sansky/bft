#if BFT_REWIRED
using System;
using System.Collections.Generic;

namespace BFT
{
    public class MultipleButtonActionLogic : ButtonAction
    {
        public List<ButtonAction> Actions;
        
        public override void OnButtonDownAction()
        {
            foreach (ButtonAction action in Actions)
            {
               action.OnButtonDownAction();
            }
        }

        public override void OnButtonHeldAction()
        {
            foreach (ButtonAction action in Actions)
            {
                action.OnButtonHeldAction();
            }
        }

        public override void OnButtonUpAction()
        {
            foreach (ButtonAction action in Actions)
            {
                action.OnButtonUpAction();
            }
        }
    }
}
#endif