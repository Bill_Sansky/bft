#if BFT_REWIRED

using UnityEngine;

namespace BFT
{
    public enum IncrementType
    {
        ON_DOWN,
        ON_UP,
        ON_HELD,
    }

    public class ButtonFloatIncrementAction : ButtonAction
    {
        public IncrementType IncrementType;

        public FloatVariable VariableToIncrement;

        public FloatValue IncrementValue;

        public bool UseDeltaTime;

        public override void OnButtonDownAction()
        {
            if (IncrementType == IncrementType.ON_DOWN)
                Increment();
        }

        private void Increment()
        {
            VariableToIncrement.Value += IncrementValue.Value * (UseDeltaTime ? Time.deltaTime : 1);
        }

        public override void OnButtonHeldAction()
        {
            if (IncrementType == IncrementType.ON_HELD)
                Increment();
        }

        public override void OnButtonUpAction()
        {
            if (IncrementType == IncrementType.ON_UP)
                Increment();
        }
    }
}

#endif