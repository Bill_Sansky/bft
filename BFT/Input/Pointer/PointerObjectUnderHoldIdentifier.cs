﻿using UnityEngine;

namespace BFT
{
    public class PointerObjectUnderHoldIdentifier : MonoBehaviour
    {
        public delegate void GameObjectChanger(GameObject objectChanged, GameObject newObject);

        public Camera Cam;

        public PointerHoldRegister HoldRegister;

        [SerializeField] private bool ignoreEventSystemObjects = false;

        public bool KeepObjectReferenceOnRelease;

        public LayerMask LayersToCheck;

        public GameObject UnderHold;

        public event GameObjectChanger OnUnderHoldChangedEvent;

        public void OnEnable()
        {
            HoldRegister.OnHoldStarted.AddListener(CheckUnderHold);
            HoldRegister.OnHoldEnded.AddListener(CheckRelease);
        }

        private void CheckRelease()
        {
            if (!KeepObjectReferenceOnRelease)
            {
                UnderHold = null;
                if (OnUnderHoldChangedEvent != null) OnUnderHoldChangedEvent(UnderHold, null);
            }
        }

        // Update is called once per frame
        void CheckUnderHold()
        {
            if (!ignoreEventSystemObjects && PointerUtils.CheckForEventSystemObject())
            {
                return;
            }

            Ray ray = Cam.ScreenPointToRay(UnityEngine.Input.mousePosition);
            RaycastHit hit;
            if (UnityEngine.Physics.Raycast(ray, out hit, 9999, LayersToCheck))
            {
                if (hit.collider.gameObject != UnderHold)
                    if (OnUnderHoldChangedEvent != null)
                        OnUnderHoldChangedEvent(UnderHold, hit.collider.gameObject);
                UnderHold = hit.collider.gameObject;
            }
            else
            {
                UnderHold = null;
                if (OnUnderHoldChangedEvent != null) OnUnderHoldChangedEvent(UnderHold, null);
            }
        }
    }
}
