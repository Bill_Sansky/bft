﻿using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class PointerHoldRegister : MonoBehaviour, IValue<bool>
    {
        private static bool hold;
        public UnityEvent OnHoldEnded;

        public UnityEvent OnHoldStarted;
        public UnityEvent OnHoldStatusChanged;

        private bool previousStatus;
        public UnityEvent BoolChangedEvent => OnHoldStatusChanged;

        public bool Value => IsHolding();

        public void Update()
        {
#if MOBILE_INPUT
        hold = Input.touchCount > 0;

#else

            if (UnityEngine.Input.GetMouseButtonDown(0))
            {
                hold = true;
            }
            else if (UnityEngine.Input.GetMouseButtonUp(0))
            {
                hold = false;
            }
#endif

            if (previousStatus != hold)
            {
                previousStatus = hold;
                if (hold)
                {
                    OnHoldStarted.Invoke();
                    OnHoldStatusChanged.Invoke();
                }
                else
                {
                    OnHoldEnded.Invoke();
                    OnHoldStatusChanged.Invoke();
                }
            }
        }

        public static bool IsHolding()
        {
            return hold;
        }
    }
}
