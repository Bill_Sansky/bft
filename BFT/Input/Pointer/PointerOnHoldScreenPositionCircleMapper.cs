﻿#if UNITY_EDITOR
#endif
using UnityEditor;
using UnityEngine;

namespace BFT
{
    public class PointerOnHoldScreenPositionCircleMapper : MonoBehaviour
    {
        public UnityEngine.Transform BaseTransform;

        private UnityEngine.Transform child;
        public float MaxWorldCircleRadius;

        private bool pressed;
        public Vector2 ScreenCenterOffset;

        // Use this for initialization
        void Start()
        {
            child = transform.GetChild(0);
        }

#if UNITY_EDITOR
        public void OnDrawGizmosSelected()
        {
            if (BaseTransform != null)
            {
                Handles.color = Color.blue.Alphaed(.5f);
                Handles.DrawSolidDisc(BaseTransform.position, BaseTransform.up, MaxWorldCircleRadius);
            }
        }
#endif
        // Update is called once per frame
        void Update()
        {
            if (UnityEngine.Input.GetMouseButton(0))
            {
                pressed = true;
            }
            else if (UnityEngine.Input.GetMouseButtonUp(0))
            {
                pressed = false;
            }

            if (pressed)
            {
                Vector2 percents = UnityEngine.Input.mousePosition.Mult(1 / (float) Screen.width, 1 / (float) Screen.height, 0);
                UnityEngine.Debug.Log(percents);
                child.transform.position = Vector3.forward * MaxWorldCircleRadius * percents.magnitude;
                transform.rotation = Quaternion.Euler(0, Vector2.Angle(Vector2.right, percents), 0);
            }
        }
    }
}
