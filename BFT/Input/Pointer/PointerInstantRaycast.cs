﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public enum RaycastOffsetDirection
    {
        RAY_DIRECTION,
        HIT_NORMAL,
    }

    public enum DirectionAxis
    {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        FORWARD,
        BACK,
        NONE,
    }

    public class PointerInstantRaycast : PointerRaycast, IValue<Transform>, IValue<Vector3>
    {
        [BoxGroup("Hit Options")] public bool AlignTransformToSurfaceHit;

        [BoxGroup("Hit Options"), ShowIf("AlignTransformToSurfaceHit")]
        public DirectionAxis AxisAlignment;

        [BoxGroup("Hit Options")] public bool KeepPositionOnNoHit;

        protected Vector3 lastHitPosition;

        [BoxGroup("Offset")] public RaycastOffsetDirection OffsetDirection;

        [SerializeField] [BoxGroup("Offset")] private FloatValue positionOffset;

        private Vector3 positionOnEnable;

        [HideIf("KeepPositionOnNoHit")] [BoxGroup("Hit Options")]
        public UnityEngine.Transform PositionOnNoHit;

        private Vector3 value;

        public float PositionOffset => positionOffset.Value;

        protected override void Init()
        {
            base.Init();
            positionOnEnable = transform.position;
        }

        protected override void OnRaycast(bool isHit, RaycastHit hit, Ray ray)
        {
            if (isHit)
            {
                lastHitPosition = transform.position;

                Vector3 offset = OffsetDirection == RaycastOffsetDirection.HIT_NORMAL
                    ? positionOffset.Value * hit.normal
                    : -positionOffset.Value * ray.direction;
                transform.AlignTransformToCollision(hit.point + offset,
                    hit.normal, AlignTransformToSurfaceHit, AxisAlignment);
            }
            else
            {
                if (KeepPositionOnNoHit)
                {
                    transform.position = lastHitPosition;
                }
                else if (PositionOnNoHit)
                {
                    transform.position = PositionOnNoHit.position;
                }
                else
                {
                    transform.position = positionOnEnable;
                }
            }
        }

        public Transform Value => transform;

        Vector3 IValue<Vector3>.Value => transform.position;
    }
}