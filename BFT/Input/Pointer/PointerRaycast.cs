﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public abstract class PointerRaycast : MonoBehaviour
    {
        [BoxGroup("Raycast")] public bool AutoAssignMainCamera = true;

        [BoxGroup("Raycast")] public Camera Cam;

        [SerializeField] [BoxGroup("Raycast")] private bool ignoreEventSystemObjects;

        [BoxGroup("Raycast")] public bool IgnoreTriggers = true;

        private Vector2 lastScreenMousePosition;

        [BoxGroup("Raycast")] public LayerMask Mask;

        [BoxGroup("Pointer Options")] public float MouseMovementThreshold = .1f;

        private void OnEnable()
        {
            if (!Cam)
                Cam = Camera.main;

            lastScreenMousePosition = UnityEngine.Input.mousePosition;

            Init();
            Update();
        }

        protected virtual void Init()
        {
        }

        protected void Update()
        {
            if (ignoreEventSystemObjects && PointerUtils.CheckForEventSystemObject())
                return;

            if ((!Mathf.Approximately(MouseMovementThreshold, 0)
                 && UnityEngine.Input.mousePosition.IsDistanceUnder(lastScreenMousePosition, MouseMovementThreshold)))
            {
                return;
            }

            lastScreenMousePosition = UnityEngine.Input.mousePosition;

            var ray = Cam.ScreenPointToRay(UnityEngine.Input.mousePosition);

            Raycast(ray);
        }

        protected virtual void Raycast(Ray ray)
        {
            bool gotHit;

            RaycastHit hit;
            gotHit = UnityEngine.Physics.Raycast(ray, out hit, 9999, Mask,
                IgnoreTriggers ? QueryTriggerInteraction.Ignore : QueryTriggerInteraction.Collide);
            OnRaycast(gotHit, hit, ray);
        }


        protected abstract void OnRaycast(bool gotHit, RaycastHit hit, Ray ray);
    }
}
