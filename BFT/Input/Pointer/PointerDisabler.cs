﻿using UnityEngine;

namespace BFT
{
    public class PointerDisabler : MonoBehaviour
    {
        public bool ResetPositionOnHide, ResetPositionOnShow;
        private bool wasCursorEnabled = false;

        private void OnEnable()
        {
            wasCursorEnabled = Cursor.visible;

            if (Cursor.visible)
            {
                if (ResetPositionOnHide)
                {
                    Cursor.lockState = CursorLockMode.Locked;
                    Cursor.lockState = CursorLockMode.None;
                }

                Cursor.visible = false;
            }
        }

        private void OnDisable()
        {
            if (!wasCursorEnabled)
                return;

            if (Cursor.visible && ResetPositionOnShow)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.lockState = CursorLockMode.None;
            }

            Cursor.visible = true;
        }
    }
}
