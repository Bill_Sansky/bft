﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    public class PointerActivityCondition : MonoBehaviour, IValue<bool>
    {
        public float DistanceThreshold;

        [ShowInInspector, ReadOnly] private bool isPointerActive;

        public UnityEvent OnPointerActivityStatusChanged;

        public UnityEvent OnPointerGotActive;
        public UnityEvent OnPointerGotInactive;

        private Vector3 previousPointerPosition;

        public float TimeBetweenChecks = 0.2f;

        [ShowInInspector, SerializeField] private bool waitRealTime;

        public UnityEvent BoolChangedEvent => OnPointerActivityStatusChanged;

        public bool Value => isPointerActive;

        void OnEnable()
        {
            StartCoroutine(CheckActivityRegularly());
        }

        void OnDisable()
        {
            StopAllCoroutines();
        }

        private IEnumerator CheckActivityRegularly()
        {
            yield return CoroutineUtils.CallRegularly(TimeBetweenChecks,
                CheckPointerActivity, waitRealTime);
        }

        public void CheckPointerActivity()
        {
            if ((UnityEngine.Input.mousePosition - previousPointerPosition).sqrMagnitude > DistanceThreshold)
            {
                previousPointerPosition = UnityEngine.Input.mousePosition;
                if (!isPointerActive)
                {
                    isPointerActive = true;
                    OnPointerGotActive.Invoke();
                    OnPointerActivityStatusChanged.Invoke();
                }
            }
            else if (isPointerActive)
            {
                isPointerActive = false;
                OnPointerGotInactive.Invoke();
                OnPointerActivityStatusChanged.Invoke();
            }
        }
    }
}
