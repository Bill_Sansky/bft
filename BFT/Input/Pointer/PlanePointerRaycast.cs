﻿using System;
using UnityEngine;

namespace BFT
{
    public class PlanePointerRaycast : PointerInstantRaycast
    {
        [SerializeField] protected IPlaneGiver plane;


        protected override void Raycast(Ray ray)
        {
            float enter;
            var hit = plane.GetPlane.Raycast(ray, out enter);

            if (hit)
            {
                Vector3 hitPoint = ray.GetPoint(enter);
                transform.position = hitPoint + PositionOffset * plane.GetPlane.normal;
                lastHitPosition = transform.position;
            }
        }

        protected override void OnRaycast(bool isHit, RaycastHit hit, Ray ray)
        {
            //It should'nt be called as its callculated in RaycastMethod
            throw new AccessViolationException();
        }
    }
}
