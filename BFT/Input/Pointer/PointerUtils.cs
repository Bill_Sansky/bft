﻿using UnityEngine.EventSystems;

namespace BFT
{
    public static class PointerUtils
    {
        public static bool CheckForEventSystemObject()
        {
            return EventSystem.current &&
                   EventSystem.current.IsPointerOverGameObject(UnityEngine.Input.touchCount > 0 ? UnityEngine.Input.GetTouch(0).fingerId : -1);
        }
    }
}
