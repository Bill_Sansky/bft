﻿#if BFT_REWIRED
using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Input/Axis/Axis Transform Range Position Action")]
    public class TransformRangePositionAxisAction : AxisAction
    {
        public AxisBFT Axis;
        public bool LocalAxis;

        public float MinDistance = 0;

        public float Range = 1;
        public UnityEngine.Transform ToMove;

        public override void AxisValueAction(float value)
        {
            Vector3 axisVector = (LocalAxis) ? MathExt.Axis(Axis) : transform.TransformDirection(MathExt.Axis(Axis));

            if (LocalAxis)
                ToMove.localPosition = (axisVector * Range * value).GetClampedMagnitude(MinDistance, Range);
            else
                ToMove.position = (axisVector * Range * value).GetClampedMagnitude(MinDistance, Range);
        }
    }
}

#endif
