﻿#if BFT_REWIRED
using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Input/Axis/Axis Transform Move Action")]
    public class TransformMoverAction : AxisAction
    {
        public Vector3Value Direction;
        public FloatValue Force;

        public UnityEngine.Transform ToMove;

        public bool UseRealTime;

        public override void AxisValueAction(float value)
        {
            ToMove.Translate(Direction.Value * (Force.Value * value *
                                                (UseRealTime
                                                    ? UnityEngine.Time.unscaledDeltaTime
                                                    : UnityEngine.Time.deltaTime)));
        }
    }
}

#endif