﻿using UnityEngine;
using UnityEngine.Events;
#if BFT_REWIRED
using Rewired;

namespace BFT
{
    public class JoystickTriggerEvent : MonoBehaviour
    {
        public string ActionID;

        private Player controlPlayer;
        public string ControlPlayerID;
        public UnityEvent OnButtonReleased;

        public UnityEvent OnButtonTriggered;

        void Start()
        {
            controlPlayer = ReInput.players.GetPlayer(ControlPlayerID);
        }

        void OnEnable()
        {
            if (controlPlayer == null)
                controlPlayer = ReInput.players.GetPlayer(ControlPlayerID);
        }

        public void Update()
        {
            if (controlPlayer.GetButtonDown(ActionID))
            {
                OnButtonTriggered.Invoke();
            }
            else if (controlPlayer.GetButtonUp(ActionID))
            {
                OnButtonReleased.Invoke();
            }
        }
    }
}

#endif
