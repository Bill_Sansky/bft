﻿using System.Collections;

namespace BFT
{
    public class CoroutineCaller : SingletonMB<CoroutineCaller>
    {
        public void StartExternalCoroutine(IEnumerator coroutine)
        {
            StartCoroutine(coroutine);
        }
        
    }
}