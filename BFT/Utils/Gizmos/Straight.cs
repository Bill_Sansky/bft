﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    public partial class GizmosExt
    {
        public static IEnumerable<Vector3[]> GetStraightLine(float range)
        {
            var lines = new List<Vector3[]>
            {
                new[] {Vector3.zero, Vector3.forward * range}
            };
            lines.AddRange(EndArrow(range));

            return lines;
        }

        private static IEnumerable<Vector3[]> EndArrow(float range)
        {
            var lines = new List<Vector3[]>
            {
                new[] {Vector3.forward * range, Vector3.forward * range + Vector3.back * 0.2f + Vector3.left * 0.1f},
                new[] {Vector3.forward * range, Vector3.forward * range + Vector3.back * 0.2f + Vector3.right * 0.1f},
                new[] {Vector3.forward * range, Vector3.forward * range + Vector3.back * 0.2f + Vector3.up * 0.1f},
                new[] {Vector3.forward * range, Vector3.forward * range + Vector3.back * 0.2f + Vector3.down * 0.1f}
            };
            return lines;
        }
    }
}
