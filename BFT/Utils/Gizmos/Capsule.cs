﻿using UnityEngine;

namespace BFT
{
    public partial class GizmosExt
    {
        public static void DrawCapsule(Vector3 position, float height, float radius, Quaternion rotation)
        {
#if UNITY_EDITOR
            float size = radius * 2;
            Matrix4x4 cylinderTransform = Matrix4x4.TRS(position, rotation, new Vector3(size, size, height - (radius * 2)));
            Matrix4x4 oldGizmosMatrix = UnityEditor.Handles.matrix;

            UnityEditor.Handles.matrix *= cylinderTransform;
            UnityEditor.Handles.CylinderHandleCap(0, Vector3.zero, Quaternion.identity, 1, EventType.Repaint);
            UnityEditor.Handles.matrix = oldGizmosMatrix;

            Vector3 p0, p1;
            p0 = p1 = position;
            float heightDiff = height / 2f - radius;
            p0.y += heightDiff;
            p1.y -= heightDiff;
            UnityEditor.Handles.SphereHandleCap(0, p0, Quaternion.identity, radius * 2, EventType.Repaint);
            UnityEditor.Handles.SphereHandleCap(0, p1, Quaternion.identity, radius * 2, EventType.Repaint);
#endif
        }
    }
}
