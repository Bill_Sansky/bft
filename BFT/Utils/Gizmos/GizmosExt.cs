﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    partial class GizmosExt
    {
        public static void DrawStraightLine(float range, Vector3 translation, Vector3 rotation)
        {
            Draw(GetStraightLine(range), translation, rotation);
        }

        public static void DrawCone(float angle, float range, Vector3 translation, Vector3 rotation)
        {
            Draw(GetConeLines(angle, range), translation, rotation);
        }

        public static void Draw(IEnumerable<Vector3[]> lines, Vector3 translation, Vector3 rotation)
        {
            foreach (var line in lines)
            {
                line.ApplyRotation(rotation);
                line.ApplyTranslation(translation);

                for (int i = 1; i < line.Length; i++)
                {
                    Gizmos.DrawLine(line[i - 1], line[i]);
                }
            }
        }

        public static void Draw(IEnumerable<Vector3[]> lines)
        {
            foreach (var line in lines)
            {
                for (int i = 1; i < line.Length; i++)
                {
                    Gizmos.DrawLine(line[i - 1], line[i]);
                }
            }
        }
    }
}
