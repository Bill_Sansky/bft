﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    public partial class GizmosExt
    {
        public static IEnumerable<Vector3[]> GetConeLines(float angle, float range)
        {
            var lines = new List<Vector3[]>();
            lines.AddRange(EdgeLines(-angle / 2, range));
            lines.AddRange(EdgeLines(angle / 2, range));
            lines.AddRange(RangeLines(angle, range));
            lines.AddRange(CircleLines(angle, range));
            return lines;
        }

        private static IEnumerable<Vector3[]> CircleLines(float angle, float range)
        {
            var segments = 37;
            var horizontalLine = new Vector3[segments];
            var tmpAngle = 0;

            for (int i = 0; i < segments; i++)
            {
                var position = GetPositionForAngle(tmpAngle, range);

                var sizeScale = Mathf.Sin(0.5f * Mathf.Deg2Rad * angle);
                var distanceScale = Mathf.Cos(0.5f * Mathf.Deg2Rad * angle);
                horizontalLine[i] = new Vector3(position.x * sizeScale, position.y * sizeScale, distanceScale * range);
                tmpAngle += 10;
            }

            return new List<Vector3[]> {horizontalLine};
        }

        private static IEnumerable<Vector3[]> EdgeLines(float angle, float range)
        {
            var lenghtLines = new List<Vector3[]>();
            var line1 = new Vector3[2];
            var line2 = new Vector3[2];

            line1[0] = Vector3.zero;
            line2[0] = Vector3.zero;

            var pos = GetPositionForAngle(angle, range);

            line1[1] = new Vector3(pos.x, 0, pos.y);
            line2[1] = new Vector3(0, pos.x, pos.y);

            lenghtLines.Add(line1);
            lenghtLines.Add(line2);

            return lenghtLines;
        }

        private static IEnumerable<Vector3[]> RangeLines(float angle, float range)
        {
            var segments = (int) (angle / 5 + 2);

            var horizontalLine = new Vector3[segments];
            var verticalLine = new Vector3[segments];
            var tmpAngle = -angle / 2;

            for (int i = 0; i < segments; i++)
            {
                var position = GetPositionForAngle(tmpAngle, range);

                horizontalLine[i] = new Vector3(position.x, 0, position.y);
                verticalLine[i] = new Vector3(0, position.x, position.y);

                tmpAngle += angle / (segments - 1);
            }

            return new List<Vector3[]> {horizontalLine, verticalLine};
        }

        private static Vector2 GetPositionForAngle(float angle, float range)
        {
            return new Vector2(
                Mathf.Sin(Mathf.Deg2Rad * angle) * range,
                Mathf.Cos(Mathf.Deg2Rad * angle) * range);
        }
    }
}
