﻿using UnityEngine;

namespace BFT
{
    class DrawForward : MonoBehaviour
    {
        private void OnDrawGizmos()
        {
            GizmosExt.DrawStraightLine(1, transform.position, transform.rotation.eulerAngles);
        }
    }
}
