#if BFT_NATIVE
using Unity.Mathematics;
#endif
using UnityEngine;

namespace BFT
{
    public static class QuaternionExt
    {
#if BFT_NATIVE
        public static float4 ToFloat4(this Quaternion q)
        {
            return new float4(q.x, q.y, q.z, q.w);
        }

        public static Quaternion ToQuaternion(this float4 f)
        {
            return new Quaternion(f.x, f.y, f.z, f.w);
        }
#endif

        public static (float, float, float, float) SerializedForm(this Quaternion quat)
        {
            return (quat.w, quat.x, quat.y, quat.z);
        }

        public static void FromSerialized(this Quaternion quat, object serialized)
        {
            (float, float, float, float) save = ((float, float, float, float)) serialized;

            quat.w = save.Item1;
            quat.x = save.Item2;
            quat.y = save.Item3;
            quat.z = save.Item4;
        }
    }
}