﻿using System;

namespace BFT
{
    public static class EnumExt
    {
        public static bool HasFlag(this System.Enum e, System.Enum flag)
        {
            int ve = Convert.ToInt32(e);
            int fe = Convert.ToInt32(flag);
            return (ve & fe) == fe;
        }

        public static bool HasAnyFlag(this System.Enum e, System.Enum flag)
        {
            int ve = Convert.ToInt32(e);
            int fe = Convert.ToInt32(flag);
            return (ve & fe) != 0;
        }
    }
}
