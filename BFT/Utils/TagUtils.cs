﻿using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditorInternal;

#endif

namespace BFT
{
    public static class TagUtils
    {
#if UNITY_EDITOR
        public static ValueDropdownList<string> TagValueDropDown()
        {
            var dd= new ValueDropdownList<string>();
            foreach (var tag in InternalEditorUtility.tags)
            {
                dd.Add(tag);
            }

            return dd;
        }

#endif
    }
}
