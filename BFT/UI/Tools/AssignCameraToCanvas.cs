﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace BFT
{
    public class AssignCameraToCanvas : MonoBehaviour
    {
        [SerializeField] private CameraValue CameraToAssign;
        [SerializeField] private bool mainCamera;

        [FormerlySerializedAs("onAwake")] [SerializeField]
        private bool AssignOnAwake;

        [FormerlySerializedAs("onStart")] [SerializeField]
        private bool AssignOnStart;

        [FormerlySerializedAs("onStart")] [SerializeField]
        private bool AssignOnEnable;

        void Awake()
        {
            if (!AssignOnAwake) return;
            Assign();
        }

        void Start()
        {
            if (!AssignOnStart) return;
            Assign();
        }

        private void OnEnable()
        {
            if (AssignOnEnable)
                Assign();
        }

        [Button]
        void Assign()
        {
            var canvas = GetComponent<Canvas>();
            canvas.worldCamera = CameraToAssign.Value;
        }
    }
}