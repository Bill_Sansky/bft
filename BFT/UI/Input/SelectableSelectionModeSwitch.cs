﻿
using UnityEngine.EventSystems;
using UnityEngine.Serialization;
#if BFT_REWIRED
using UnityEngine;
using UnityEngine.UI;

namespace BFT
{
    /// <summary>
    ///     Switches between a pad like selection and a pointer like selection
    /// </summary>
    public class SelectableSelectionModeSwitch : MonoBehaviour
    {
        public Selectable DefaultSelectable;
        public PointerActivityCondition PointerActivityTracker;
        [FormerlySerializedAs("PointerPosition")] public PointerPositionToRectPositionSetter pointerPositionToPositionSetter;
        public TransformRayCastScreenToWorldSetter rayCastScreenSetter;

        public TransformSelectedObjectCopy SelectionCopier;

        public void Awake()
        {
            PointerActivityTracker.OnPointerGotActive.AddListener(RayCastPointer);
            PointerActivityTracker.OnPointerGotInactive.AddListener(RayCastPad);
        }

        public void ActivateSelectionNavigation()
        {
            SelectionCopier.enabled = true;
            rayCastScreenSetter.enabled = false;
        }

        public void ActivateRaycastNavigation()
        {
            SelectionCopier.enabled = false;
            rayCastScreenSetter.enabled = true;
        }

        private void RayCastPad()
        {
            pointerPositionToPositionSetter.enabled = false;
            if (!EventSystem.current.currentSelectedGameObject)
                EventSystem.current.SetSelectedGameObject(DefaultSelectable.gameObject);
        }

        private void RayCastPointer()
        {
            pointerPositionToPositionSetter.enabled = true;
        }
    }
}
#endif
