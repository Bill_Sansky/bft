﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace BFT
{
    /// <summary>
    ///     Tracks a change in the selection of objects
    /// </summary>
    public class SelectedObjectChangedEvent : SingletonMB<SelectedObjectChangedEvent>
    {
        [ShowInInspector, ReadOnly] private GameObject lastlySelected;

        public UnityEvent OnNoObjectSelected;
        public UnityEvent OnObjectSelectedChanged;

        [ShowInInspector, ReadOnly] private GameObject previousSelection;

        public GameObject PreviousSelection => previousSelection;

        public GameObject LastlySelected => lastlySelected;

        public override void Awake()
        {
            base.Awake();
            lastlySelected = EventSystem.current.firstSelectedGameObject;
        }

        void Update()
        {
            if (EventSystem.current.currentSelectedGameObject != LastlySelected)
            {
                lastlySelected = EventSystem.current.currentSelectedGameObject;
                previousSelection = LastlySelected;
                if (LastlySelected)
                    OnObjectSelectedChanged.Invoke();
                else
                {
                    OnNoObjectSelected.Invoke();
                }
            }
        }
    }
}
