﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace BFT
{
    public class SelectOnPointerEnter : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
    {
        private Selectable toSelect;

        public bool UnSelectOnPointerExit = true;

        public void OnPointerEnter(PointerEventData eventData)
        {
            toSelect.Select();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (UnSelectOnPointerExit && EventSystem.current.currentSelectedGameObject == gameObject)
            {
                EventSystem.current.SetSelectedGameObject(null);
            }
        }

        void Awake()
        {
            toSelect = GetComponent<Selectable>();
        }
    }
}
