﻿using System;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class PointerPositionToRectPositionSetter : MonoBehaviour
    {
        public bool SetOnEnable;
        public bool SetOnUpdate=true;

        private void OnEnable()
        {
            if (SetOnEnable)
                transform.position = Input.mousePosition;
        }

        void Update()
        {
            if (!SetOnUpdate)
                return;

            transform.position = Input.mousePosition;
        }
    }
}