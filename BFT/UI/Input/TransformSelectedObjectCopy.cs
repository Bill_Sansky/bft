﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;

namespace BFT
{
    public class TransformSelectedObjectCopy : MonoBehaviour
    {
        public bool CopyPosition;
        public bool CopyRotation;
        public bool CopyScale;

        public bool MoveTransformOnNoSelection = false;

        [ShowIf("MoveTransformOnNoSelection")] public UnityEngine.Transform NoSelectionTransform;

        void Awake()
        {
            SelectedObjectChangedEvent.Instance.OnObjectSelectedChanged.AddListener(TeleportTransform);
            SelectedObjectChangedEvent.Instance.OnNoObjectSelected.AddListener(TeleportTransformToDefault);
        }

        void Start()
        {
            if (EventSystem.current.firstSelectedGameObject)
                transform.Copy(EventSystem.current.firstSelectedGameObject.transform, CopyPosition, CopyRotation,
                    CopyScale);
        }

        private void TeleportTransformToDefault()
        {
            if (MoveTransformOnNoSelection)
                transform.Copy(NoSelectionTransform, CopyPosition, CopyRotation, CopyScale);
        }

        private void TeleportTransform()
        {
            transform.Copy(SelectedObjectChangedEvent.Instance.LastlySelected.transform
                , CopyPosition, CopyRotation, CopyScale);
        }
    }
}
