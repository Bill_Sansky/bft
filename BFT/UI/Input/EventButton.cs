﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace BFT
{
    public class EventButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler,
        IPointerExitHandler
    {
        public bool DebugLog;
        public UnityEvent OnPointerDownEvent;

        public UnityEvent OnPointerInsideEvent;
        public UnityEvent OnPointerOutsideEvent;
        public UnityEvent OnPointerUpInsideEvent;
        public UnityEvent OnPointerUpOutsideEvent;

        private bool pointerInside = false;

        public void OnPointerDown(PointerEventData eventData)
        {
            OnPointerDownEvent.Invoke();
            if (DebugLog)
                UnityEngine.Debug.Log("Pointer Down", this);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            pointerInside = true;
            OnPointerInsideEvent.Invoke();
            if (DebugLog)
                UnityEngine.Debug.Log("Pointer Enter", this);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            pointerInside = false;
            OnPointerOutsideEvent.Invoke();

            if (DebugLog)
                UnityEngine.Debug.Log("Pointer Exit", this);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (pointerInside)
            {
                OnPointerUpInsideEvent.Invoke();

                if (DebugLog)
                    UnityEngine.Debug.Log("Pointer Up Inside", this);
            }
            else
            {
                OnPointerUpOutsideEvent.Invoke();
                if (DebugLog)
                    UnityEngine.Debug.Log("Pointer Up Outside", this);
            }
        }
    }
}
