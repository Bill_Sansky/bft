﻿using UnityEngine;

namespace BFT
{
    public class ImageConditionColorSwitch : MonoBehaviour
    {
        public BoolValue Condition;
        
        public ColorValue ColorOnFalse;

        public ColorValue ColorOnTrue;

        private UnityEngine.UI.Image image;

        void Awake()
        {
            image = GetComponent<UnityEngine.UI.Image>();
        }

        public void SwitchColor()
        {
            if (image)
                image.color = (Condition.Value) ? ColorOnTrue.Value : ColorOnFalse.Value;
        }
    }
}
