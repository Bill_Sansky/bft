﻿#if BFT_DEFORM
#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections.Generic;
using System.Linq;
using Deform;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace BFT
{
    [ExecuteInEditMode]
    public class DeformableImage : MaskableGraphic
    {
        
        [SerializeField, BoxGroup("Settings")] public bool IsUpdating = true;
        [SerializeField, BoxGroup("Settings")] private Deformable deformable = null;
        [SerializeField, BoxGroup("Settings")] private Sprite m_Sprite = null;
        [SerializeField, BoxGroup("Settings")] private bool preserveAspectRatio = true;

        [SerializeField, HideInInspector] private string cachedMeshName;
        [SerializeField, HideInInspector] private Mesh cachedMesh;
        [SerializeField, HideInInspector] private Vector2[] cachedUv = new Vector2[0];
        [SerializeField, HideInInspector] private List<int> triangles = new List<int>();
        [SerializeField, HideInInspector] private Vector3[] cachedVertices = new Vector3[0];
        [SerializeField, HideInInspector] private Vector2 size;
        [SerializeField, HideInInspector] private float minX;
        [SerializeField, HideInInspector] private float maxX;
        [SerializeField, HideInInspector] private float minY;
        [SerializeField, HideInInspector] private float maxY;

        void Update()
        {
            if (IsUpdating)
                SetVerticesDirty();
        }

        [Button(ButtonSizes.Medium)]
        public void UpdateMesh()
        {
            SetVerticesDirty();
        }

        protected override void UpdateMaterial()
        {
            if (!this.IsActive())
                return;
            this.canvasRenderer.materialCount = 1;
            this.canvasRenderer.SetMaterial(material, 0);
            this.canvasRenderer.SetTexture(m_Sprite != null ? m_Sprite.texture : null);
        }

        protected override void OnPopulateMesh(VertexHelper vh)
        {
            if (deformable == null || deformable.GetMesh() == null) return;

            var sharedMesh = deformable.GetMesh();
            if (sharedMesh == null) return;
            var meshVertices = sharedMesh.vertices;

            if (cachedMesh == null)
            {
                cachedMesh = deformable.GetMesh();
            }

            if (cachedMeshName != sharedMesh.name ||
                triangles.Count != sharedMesh.triangles.Length ||
                cachedVertices.Length != sharedMesh.vertices.Length)
            {
                cachedMesh = deformable.GetMesh();
                CacheMeshData(sharedMesh, meshVertices);
            }


            Vector2 otherScale;
            if (preserveAspectRatio)
            {
                var r = new Rect(minX, minY, size.x * rectTransform.sizeDelta.x * 1f,
                    size.y * rectTransform.sizeDelta.y * 1f);
                var spriteRatio = size.x / size.y;
                var rectRatio = rectTransform.sizeDelta.x / rectTransform.sizeDelta.y;

                if (spriteRatio > rectRatio)
                {
                    var oldHeight = r.height;
                    r.height = rectTransform.sizeDelta.x * (1.0f / spriteRatio);
                    r.y += (oldHeight - r.height) * rectTransform.pivot.y;
                    otherScale = new Vector2(rectTransform.sizeDelta.x / size.x, r.size.y / size.y);
                }
                else
                {
                    var oldWidth = r.width;
                    r.width = rectTransform.sizeDelta.y * spriteRatio;
                    r.x += (oldWidth - r.width) * rectTransform.pivot.x;
                    otherScale = new Vector2(r.width / size.x, rectTransform.sizeDelta.y / size.y);
                }
            }
            else
            {
                otherScale = new Vector2(rectTransform.sizeDelta.x * 0.01f, rectTransform.sizeDelta.y * 0.01f);
            }

            var vCount = meshVertices.Length;
            var vertices = new List<UIVertex>();
            for (var i = 0; i < vCount; i++)
            {
                cachedVertices[i].x = meshVertices[i].x * otherScale.x;
                cachedVertices[i].y = meshVertices[i].y * otherScale.y;
                vertices.Add(new UIVertex { color = color, position = cachedVertices[i], uv0 = cachedUv[i] });
            }

            vh.Clear();
            vh.AddUIVertexStream(vertices, triangles);


            deformable.transform.localScale =
 new Vector3(size.x * otherScale.x * 0.01f, size.y * otherScale.y * 0.01f, 1);

        }

        private void CacheMeshData(Mesh sharedMesh, Vector3[] meshVertices)
        {
            cachedMeshName = cachedMesh.name;
            triangles = cachedMesh.triangles.ToList();
            cachedVertices = new Vector3[cachedMesh.vertexCount];
            var uvs = cachedMesh.uv;
            var uvCount = uvs.Length;
            cachedUv = new Vector2[uvCount];

            for (int i = 0; i < cachedMesh.vertexCount; i++)
            {
                cachedVertices[i] = meshVertices[i];
                cachedUv[i] = uvs[i];
            }

            minX = float.MaxValue;
            maxX = float.MinValue;
            minY = float.MaxValue;
            maxY = float.MinValue;

            for (int i = 0; i < cachedMesh.vertexCount; i++)
            {
                var vert = cachedMesh.vertices[i];
                if (vert.x < minX) minX = vert.x;
                if (vert.x > maxX) maxX = vert.x;
                if (vert.y < minY) minY = vert.y;
                if (vert.y > maxY) maxY = vert.y;
            }

            size = new Vector2(maxX - minX, maxY - minY);
        }

        protected override void OnRectTransformDimensionsChange()
        {
            base.OnRectTransformDimensionsChange();
            SetVerticesDirty();
            SetMaterialDirty();
        }

#if UNITY_EDITOR
        [MenuItem("GameObject/UI/Deformable Image", false)]
        static void Init()
        {
            var go = new GameObject("Deformable Image");
            var deformableObj = Instantiate(go, go.transform);

            var parent = Selection.activeGameObject != null ? Selection.activeGameObject.transform : null;
            go.transform.SetParent(parent);
            go.transform.localScale = Vector3.one;
            go.transform.localPosition = Vector3.zero;
            go.transform.localRotation = Quaternion.identity;
            var deformableImage = go.AddComponent<DeformableImage>();


            deformableObj.name = "Deformable";
            deformableObj.AddComponent<MeshFilter>();
            deformableImage.deformable = deformableObj.AddComponent<Deformable>();

            deformableObj.AddComponent<RectTransform>();
            Selection.activeGameObject = go;
        }
#endif
    }
}
#endif
