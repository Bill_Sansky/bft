﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace BFT
{
    public class ImageUpdater : MonoBehaviour
    {
        [BoxGroup("Options")] public bool AutoUpdate;

        [BoxGroup("References")] public Image Image;

        [BoxGroup("References")]
        public SpriteValue SpriteReference;

        [BoxGroup("Options")] public bool UpdateOnEnable;

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void UpdateImage()
        {
            if (SpriteReference != null)
                Image.sprite = SpriteReference.Value;
        }

       // [OnInspectorGUI]
        private void EditorUpdate()
        {
            if (!Application.isPlaying && AutoUpdate)
                UpdateImage();
        }

        public void Reset()
        {
            Image = GetComponent<UnityEngine.UI.Image>();
        }

        public void OnEnable()
        {
            if (UpdateOnEnable)
                UpdateImage();
            if (AutoUpdate)
                StartCoroutine(UpdateRegularly());
        }

        public void OnDisable()
        {
            StopAllCoroutines();
        }

        private IEnumerator UpdateRegularly()
        {
            while (true)
            {
                UpdateImage();
                yield return null;
            }
        }
    }
}
