﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [RequireComponent(typeof(UnityEngine.UI.Image))]
    public class ImagePercentFill : MonoBehaviour
    {
        public bool AutoFillImage = false;

        private IEnumerator coroutine;

        public bool FillOnEnable;

        private UnityEngine.UI.Image image;

        [Tooltip("When inverted, the image will fill according to 1-FloatVariable")]
        public bool Inverted;

        [BoxGroup("Reference")]
        public PercentValue Percent;

        void OnEnable()
        {
            if (!image)
                image = GetComponent<UnityEngine.UI.Image>();

            if (Percent != null)
                InitPercent();

            if (FillOnEnable)
                FillImage();
        }

        void OnDisable()
        {
            if (coroutine != null)
                StopCoroutine(coroutine);
        }

        public void InitPercent()
        {
            if (!AutoFillImage)
                return;

            StopAllCoroutines();

            if (image && gameObject.activeInHierarchy)
            {
                StartCoroutine(coroutine = FillImageCoroutine());
            }

            FillImage();
        }

        IEnumerator FillImageCoroutine()
        {
            while (true)
            {
                FillImage();
                yield return null;
            }
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public void FillImage()
        {
            if (Percent != null && image != null)
            {
                image.fillAmount = (Inverted) ? 1 - Percent.Value : Percent.Value;
            }
        }

        public void ForceFillImage(float forcedPercent)
        {
            image.fillAmount = (Inverted) ? 1 - forcedPercent : forcedPercent;
        }
    }
}
