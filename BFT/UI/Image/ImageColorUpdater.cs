﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace BFT
{
    public class ImageColorUpdater : MonoBehaviour
    {
        [BoxGroup("Options")] public bool AutoUpdate;

        [BoxGroup("References")]
        public ColorValue ColorReference;

        [BoxGroup("References")] public Graphic Image;

        [SerializeField] private ColorEvent OnValueChange;

        [SerializeField] private bool updateOnEnable = true;
        
        [BoxGroup("Options"), Button(ButtonSizes.Medium)]
        public void UpdateImage()
        {
            if (ColorReference != null)
                Image.color = ColorReference.Value;
        }

        [OnInspectorGUI]
        private void EditorUpdate()
        {
            if (!Application.isPlaying && AutoUpdate)
                UpdateImage();
        }

        public void Reset()
        {
            Image = GetComponent<UnityEngine.UI.Image>();
        }

        public void OnEnable()
        {
            if (updateOnEnable || AutoUpdate)
                UpdateImage();
            if (AutoUpdate)
                StartCoroutine(UpdateRegularly());
        }

        public void OnDisable()
        {
            StopAllCoroutines();
        }

        private IEnumerator UpdateRegularly()
        {
            while (true)
            {
                UpdateImage();
                yield return null;
            }
        }
    }
}
