﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace BFT
{
    [ExecuteInEditMode]
    public class ScrollLocallyLoopedElement : MonoBehaviour
    {
        private LayoutElement layoutElement;

        public UnityEvent OnElementGotInside;
        public UnityEvent OnElementGotOutside;
        public UnityEvent OnElementTeleportToBottom;

        public UnityEvent OnElementTeleportToTop;

        [ShowInInspector, ReadOnly] private bool outside;

        private RectTransform rect;

        public RectTransform Rect
        {
            get
            {
#if UNITY_EDITOR
                if (!Application.isPlaying)
                {
                    rect = GetComponent<RectTransform>();
                }
#endif
                return rect;
            }
        }

        public LayoutElement LayoutElement
        {
            get
            {
#if UNITY_EDITOR
                if (!Application.isPlaying)
                {
                    layoutElement = GetComponent<LayoutElement>();
                }
#endif
                return layoutElement;
            }
        }

        [ShowInInspector, ReadOnly] public ScrollLocallyLoopedElement PreviousElement { get; set; }

        [ShowInInspector, ReadOnly] public ScrollLocallyLoopedElement NextElement { get; set; }

        public bool Outside
        {
            get => outside;
            set
            {
                if (value && !outside)
                {
                    outside = true;
                    OnElementGotOutside.Invoke();
                }
                else if (!value && outside)
                {
                    outside = false;
                    OnElementGotInside.Invoke();
                }
            }
        }

        public void Awake()
        {
            if (!rect)
                rect = GetComponent<RectTransform>();
            if (!layoutElement)
                layoutElement = GetComponent<LayoutElement>();
        }

        void OnDestroy()
        {
#if UNITY_EDITOR

            ScrollZoneLocallyLooped zone = GetComponentInParent<ScrollZoneLocallyLooped>();
            if (zone && zone.Elements.Contains(this))
                zone.Elements.Remove(this);

#endif
        }
    }
}
