﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
#endif

namespace BFT
{
    [RequireComponent(typeof(ScrollRect))]
    public class ScrollZoneLocallyLooped : MonoBehaviour
    {
        public float BlockInitialSize = 2;
        public LayoutElement BottomBlock;
        [SerializeField] private float contentSize = 50;

        [FoldoutGroup("Debug")] public bool DebugColor;
        [FoldoutGroup("Debug")] public bool DebugLog;

        public float DetectionOffset = 2;

        public List<ScrollLocallyLoopedElement> Elements = new List<ScrollLocallyLoopedElement>();
        [ShowInInspector, ReadOnly] private ScrollLocallyLoopedElement firstElement;

        [ShowInInspector, ReadOnly] private ScrollLocallyLoopedElement lastElement;
        private ScrollRect scrollRect;


        public LayoutElement TopBlock;

        public VerticalLayoutGroup VerticalGroup;

        public ScrollRect ScrollRect
        {
            get
            {
                if (!scrollRect)
                {
                    scrollRect = GetComponent<ScrollRect>();
                }

                return scrollRect;
            }
        }

        public RectTransform ScrollZone
        {
            get
            {
#if UNITY_EDITOR
                if (!ScrollRect)
                    return null;
#endif
                return ScrollRect.content;
            }
        }

        public RectTransform ScrollViewPort
        {
            get
            {
#if UNITY_EDITOR
                if (!ScrollRect)
                    return null;
#endif
                return ScrollRect.viewport;
            }
        }

        public float ContentSize
        {
            get => contentSize;
            set
            {
                contentSize = value;
                BottomBlock.minHeight = Mathf.Max(BottomBlock.minHeight, contentSize);
            }
        }

        void Start()
        {
            TopBlock.minHeight = BlockInitialSize;
            BottomBlock.minHeight = ContentSize;

            scrollRect = GetComponent<ScrollRect>();

            List<ScrollLocallyLoopedElement> tempElements = new List<ScrollLocallyLoopedElement>(Elements);
            Elements.Clear();
            foreach (var elem in tempElements)
            {
                AddElement(elem);
            }
        }

        public void AddElement(ScrollLocallyLoopedElement element)
        {
            element.transform.SetParent(VerticalGroup.transform);

#if UNITY_EDITOR

            if (!Application.isPlaying)
            {
                Undo.SetTransformParent(element.transform, VerticalGroup.transform, "Moving element");
                element.transform.localScale = Vector3.one;
                element.transform.localPosition = Vector3.zero;
            }

#endif

            element.transform.SetSiblingIndex(BottomBlock.transform.GetSiblingIndex() - 1);

            if (Elements.Count > 0)
            {
                element.PreviousElement = lastElement;
                element.NextElement = firstElement;
                lastElement.NextElement = element;
                firstElement.PreviousElement = element;
            }
            else
            {
                firstElement = element;
            }

            Elements.Add(element);
            lastElement = element;

#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                TopBlock.transform.SetAsFirstSibling();
                BottomBlock.transform.SetAsLastSibling();
            }
#endif
            ElementUpdateLogic();
            LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform) VerticalGroup.transform);

            element.Outside = (element.Rect.offsetMax.y + ScrollZone.offsetMax.y
                               < -ScrollViewPort.rect.height - DetectionOffset ||
                               element.Rect.offsetMin.y + ScrollZone.offsetMax.y > DetectionOffset);
        }

        public void Update()
        {
            ElementUpdateLogic();
        }

        private void ElementUpdateLogic()
        {
            foreach (ScrollLocallyLoopedElement element in Elements)
            {
                if (!element)
                    continue;

                if (element.Rect.offsetMin.y + ScrollZone.offsetMax.y > DetectionOffset)
                {
                    if (element.Outside)
                        continue;
                    element.Outside = true;

                    if (DebugColor)
                        element.GetComponent<UnityEngine.UI.Image>().color = Color.red;

                    MoveFirstElement();
                }
                else if (element.Rect.offsetMax.y + ScrollZone.offsetMax.y < -ScrollViewPort.rect.height - DetectionOffset)
                {
                    if (element.Outside)
                        continue;
                    element.Outside = true;

                    if (DebugColor)
                        element.GetComponent<UnityEngine.UI.Image>().color = Color.magenta;

                    MoveLastElement();
                }
                else
                {
                    element.Outside = false;

                    CheckNextElements(element);

                    if (DebugColor)
                        element.GetComponent<UnityEngine.UI.Image>().color = Color.green;
                }
            }
        }

        private void CheckNextElements(ScrollLocallyLoopedElement element)
        {
            if (element.NextElement && element.NextElement.Outside && element == lastElement)
            {
                //then check if it's over that element: if so, try to teleport the last element 
                if (element.NextElement.transform.localPosition.y
                    > element.transform.localPosition.y)
                {
                    MoveFirstElement();
                }
            }
            else if (element.PreviousElement &&
                     element.PreviousElement.Outside && element == firstElement)
            {
                if (element.NextElement.transform.localPosition.y
                    < element.transform.localPosition.y)
                {
                    MoveLastElement();
                }
            }
        }

        public void MoveLastElement()
        {
            if (!lastElement.Outside || TopBlock.minHeight
                - lastElement.LayoutElement.minHeight - VerticalGroup.spacing < 0)
                return;

            lastElement.Rect.SetSiblingIndex(TopBlock.transform.GetSiblingIndex() + 1);

            BottomBlock.minHeight += lastElement.LayoutElement.minHeight + VerticalGroup.spacing;

            TopBlock.minHeight -= lastElement.LayoutElement.minHeight + VerticalGroup.spacing;

            lastElement.OnElementTeleportToTop.Invoke();

            firstElement = lastElement;
            lastElement = lastElement.PreviousElement;

            if (DebugLog)
                UnityEngine.Debug.Log("Last Element(" + lastElement.NextElement + ") Moved", this);
        }

        public void MoveFirstElement()
        {
            if (!firstElement.Outside || BottomBlock.minHeight
                - firstElement.LayoutElement.minHeight - VerticalGroup.spacing < 0)
                return;

            firstElement.Rect.SetSiblingIndex(BottomBlock.transform.GetSiblingIndex() - 1);
            TopBlock.minHeight += firstElement.LayoutElement.minHeight + VerticalGroup.spacing;
            BottomBlock.minHeight -= firstElement.LayoutElement.minHeight + VerticalGroup.spacing;
            BottomBlock.minHeight = Mathf.Max(BlockInitialSize, BottomBlock.minHeight);

            firstElement.OnElementTeleportToBottom.Invoke();

            lastElement = firstElement;
            firstElement = firstElement.NextElement;

            if (DebugLog)
                UnityEngine.Debug.Log("First Element (" + firstElement.PreviousElement + ") Moved", this);
        }
    }
}
