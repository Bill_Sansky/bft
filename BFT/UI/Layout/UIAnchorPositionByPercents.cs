﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public class UIAnchorPositionByPercents : MonoBehaviour
    {
        public bool InvertMaxX;
        public bool InvertMaxY;
        public bool InvertMinX;
        public bool InvertMinY;
        public PercentValue MaxX;
        public PercentValue MaxY;
        public PercentValue MinX;
        public PercentValue MinY;

        [SerializeField] private RectTransform rectTransform;

        void Reset()
        {
            rectTransform = GetComponent<RectTransform>();
        }

        [Button(ButtonSizes.Medium), BoxGroup("Tools")]
        public void UpdateAnchors()
        {
            rectTransform.anchorMin = new Vector2(InvertMinX ? 1 - MinX.Value : MinX.Value,
                InvertMinY ? 1 - MinY.Value : MinY.Value);
            rectTransform.anchorMax = new Vector2(InvertMaxX ? 1 - MaxX.Value : MaxX.Value,
                InvertMaxY ? 1 - MaxY.Value : MaxY.Value);
        }

        [OnInspectorGUI]
        private void UpdateAnchorEditor()
        {
            if (MinY != null && MinX != null && MaxX != null && MaxY != null)
            {
                UpdateAnchors();
            }
        }
    }
}
