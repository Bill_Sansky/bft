﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class RectTransformAngleToEdgesPosition : MonoBehaviour
    {
        [InfoBox("Given an angle, puts the Transform around the edge of a reference Rect")]
        public FloatValue Angle;

        public RectTransform ReferenceTransform;

        void Start()
        {
            GetPosition();
        }

        private Vector3 GetPosition()
        {
            if (!ReferenceTransform)
                return Vector3.zero;

            float radius = 0;
            Vector3[] corners = new Vector3[4];
            ReferenceTransform.GetWorldCorners(corners);

            float maxX = corners[2].x - ReferenceTransform.position.x;
            float maxY = corners[2].y - ReferenceTransform.position.y;
            float minX = corners[0].x - ReferenceTransform.position.x;
            float minY = corners[0].y - ReferenceTransform.position.y;

            foreach (var vec in corners)
            {
                Vector3 distance = (vec - ReferenceTransform.position);
                if (distance.magnitude > radius)
                {
                    radius = distance.magnitude;
                }
            }

            float y = Mathf.Clamp(Mathf.Sin(Angle.Value * Mathf.Deg2Rad) * radius, minY, maxY)
                      + ReferenceTransform.position.y;
            float x = Mathf.Clamp(Mathf.Cos(Angle.Value * Mathf.Deg2Rad) * radius, minX, maxX)
                      + ReferenceTransform.position.x;

            return new Vector3(x, y, ReferenceTransform.position.z);
        }

        void Update()
        {
            transform.position = GetPosition();
        }
    }
}
