﻿using UnityEngine;

namespace BFT
{
    [ExecuteAlways]
    public class RectTransformPercentPosition : MonoBehaviour
    {
        public RectTransform ReferenceTransform;

        [Range(0, 1)] public PercentValue XPercent;

        [Range(0, 1)] public PercentValue YPercent;

        void Start()
        {
            GetPosition();
        }

        private Vector3 GetPosition()
        {
            if (!ReferenceTransform)
            {
                return Vector3.zero;
            }

            Vector3[] corners = new Vector3[4];
            ReferenceTransform.GetWorldCorners(corners);
            Vector3 left = Vector3.Lerp(corners[0], corners[1], YPercent.Value);
            Vector3 right = Vector3.Lerp(corners[3], corners[2], YPercent.Value);

            return Vector3.Lerp(left, right, XPercent.Value);
        }

        void Update()
        {
            transform.position = GetPosition();
        }
    }
}
