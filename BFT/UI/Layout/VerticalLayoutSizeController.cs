﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace BFT
{
    public class VerticalLayoutSizeController : MonoBehaviour
    {
        public enum VerticalAlignment
        {
            Top,
            Bottom
        }

        [SerializeField, ShowIf("OversizeAutoAlignment")]
        private VerticalAlignment alignment;

        private int childrenCount = -1;

        [SerializeField] private float defaultHeight;

        [SerializeField] private float defaultPos;

        [SerializeField] private VerticalLayoutGroup layoutGroup;

        [SerializeField] private RectTransform layoutTransform;

        [SerializeField] private bool overrideDefaultValuesOnStart;

        public bool OversizeAutoAlignment = false;

        [SerializeField] private bool sameChildrenSize = true;

        private float? spacing, topPadding, bottomPadding, screenSize;

        private void AutoSet()
        {
            if (layoutGroup == null)
                layoutGroup = gameObject.GetComponent<VerticalLayoutGroup>();
            if (layoutTransform == null)
                layoutTransform = (RectTransform) layoutGroup.transform;
        }

        private void Reset()
        {
            AutoSet();
            SetDefaultValuesFromTransform();
        }

        private void Awake()
        {
            AutoSet();
        }

        private void Start()
        {
            if (overrideDefaultValuesOnStart)
            {
                SetDefaultValuesFromTransform();
            }
        }

        [Button]
        private void SetDefaultValuesFromTransform()
        {
            if (layoutTransform)
            {
                defaultHeight = layoutTransform.rect.height;
                defaultPos = layoutTransform.anchoredPosition.y;
            }
        }

        private void LateUpdate()
        {
            int childCount = layoutTransform.childCount;
            if (childrenCount != childCount || spacing != layoutGroup.spacing || topPadding != layoutGroup.padding.top ||
                bottomPadding != layoutGroup.padding.bottom || screenSize != Screen.height)
            {
                childrenCount = childCount;
                spacing = layoutGroup.spacing;
                topPadding = layoutGroup.padding.top;
                bottomPadding = layoutGroup.padding.bottom;

                if (screenSize.HasValue && screenSize != Screen.height)
                {
                    defaultPos = (defaultPos * Screen.height) / screenSize.Value;
                    defaultHeight = (defaultHeight * Screen.height) / screenSize.Value;
                }

                screenSize = Screen.height;

                float layoutSize = 0;

                if (sameChildrenSize)
                {
                    if (childCount > 0)
                        layoutSize = ((RectTransform) layoutTransform.GetChild(0)).rect.height * childCount;
                }
                else
                {
                    foreach (GameObject child in layoutTransform)
                    {
                        layoutSize += ((RectTransform) child.transform).rect.height;
                    }
                }

                if (childCount > 1)
                {
                    layoutSize += spacing.Value * (childCount - 1);
                }

                layoutSize += topPadding.Value + bottomPadding.Value;

                float size = Mathf.Max(defaultHeight, layoutSize);

                if (OversizeAutoAlignment)
                {
                    if (layoutSize > defaultHeight)
                    {
                        switch (alignment)
                        {
                            case VerticalAlignment.Top:
                            {
                                Vector2 pos = layoutTransform.anchoredPosition;
                                pos.y = defaultPos + ((defaultHeight - size) / 2f);
                                layoutTransform.anchoredPosition = pos;
                            }
                                break;
                            case VerticalAlignment.Bottom:
                            {
                                Vector2 pos = layoutTransform.anchoredPosition;
                                pos.y = defaultPos + (-(defaultHeight - size) / 2f);
                                layoutTransform.anchoredPosition = pos;
                            }
                                break;
                        }
                    }
                    else
                    {
                        Vector2 pos = layoutTransform.anchoredPosition;
                        pos.y = defaultPos;
                        layoutTransform.anchoredPosition = pos;
                    }
                }

                layoutTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size);
            }
        }
    }
}
