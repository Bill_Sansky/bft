﻿using UnityEngine;
using UnityEngine.UI;

namespace BFT
{
    public class LayoutForceRebuilder : MonoBehaviour
    {
        public bool DisableLayoutAfterRebuild = false;

        [SerializeField] private LayoutGroup layout;

        private void Reset()
        {
            layout = gameObject.GetComponent<LayoutGroup>();
        }

        public void Rebuild()
        {
            layout.CalculateLayoutInputHorizontal();
            layout.SetLayoutHorizontal();
            if (DisableLayoutAfterRebuild)
            {
                layout.enabled = false;
            }
        }
    }
}
