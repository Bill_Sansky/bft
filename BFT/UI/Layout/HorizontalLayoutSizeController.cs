﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

namespace BFT
{
    [ExecuteAlways]
    public class HorizontalLayoutSizeController : MonoBehaviour
    {
        public enum HorizontalAlignment
        {
            Left,
            Right
        }

        [SerializeField, ShowIf("OversizeAutoAlignment")]
        private HorizontalAlignment alignment;

        private int childrenCount = -1;

        [SerializeField] private float defaultPos;

        [SerializeField] private float defaultWidth;

        [SerializeField] private HorizontalLayoutGroup layoutGroup;

        [SerializeField] private RectTransform layoutTransform;

        [SerializeField] private bool overrideDefaultValuesOnStart;

        public bool OversizeAutoAlignment = false;

        [SerializeField] private bool sameChildrenSize = true;

        private float? spacing, leftPadding, rightPadding, screenSize;

        private void AutoSet()
        {
            if (layoutGroup == null)
                layoutGroup = gameObject.GetComponent<HorizontalLayoutGroup>();
            if (layoutTransform == null)
                layoutTransform = (RectTransform) layoutGroup.transform;
        }

        private void Reset()
        {
            AutoSet();
            SetDefaultValuesFromTransform();
        }

        private void Awake()
        {
            AutoSet();
        }

        private void Start()
        {
            if (overrideDefaultValuesOnStart)
            {
                SetDefaultValuesFromTransform();
            }
        }

        [Button]
        private void SetDefaultValuesFromTransform()
        {
            if (layoutTransform)
            {
                defaultWidth = layoutTransform.rect.width;
                defaultPos = layoutTransform.anchoredPosition.x;
            }
        }

        private void LateUpdate()
        {
            int childCount = layoutTransform.childCount;
            if (childrenCount != childCount || spacing != layoutGroup.spacing || leftPadding != layoutGroup.padding.left ||
                rightPadding != layoutGroup.padding.right || screenSize != Screen.width)
            {
                childrenCount = childCount;
                spacing = layoutGroup.spacing;
                leftPadding = layoutGroup.padding.left;
                rightPadding = layoutGroup.padding.right;

                if (screenSize.HasValue && screenSize != Screen.width)
                {
                    defaultPos = (defaultPos * Screen.width) / screenSize.Value;
                    defaultWidth = (defaultWidth * Screen.width) / screenSize.Value;
                }

                screenSize = Screen.width;

                float layoutSize = 0;

                if (sameChildrenSize)
                {
                    if (childCount > 0)
                        layoutSize = ((RectTransform) layoutTransform.GetChild(0)).rect.width * childCount;
                }
                else
                {
                    foreach (GameObject child in layoutTransform)
                    {
                        layoutSize += ((RectTransform) child.transform).rect.width;
                    }
                }

                if (childCount > 1)
                {
                    layoutSize += spacing.Value * (childCount - 1);
                }

                layoutSize += leftPadding.Value + rightPadding.Value;

                float size = Mathf.Max(defaultWidth, layoutSize);

                if (OversizeAutoAlignment)
                {
                    if (layoutSize > defaultWidth)
                    {
                        switch (alignment)
                        {
                            case HorizontalAlignment.Left:
                            {
                                Vector2 pos = layoutTransform.anchoredPosition;
                                pos.x = defaultPos + (-(defaultWidth - size) / 2f);
                                layoutTransform.anchoredPosition = pos;
                            }
                                break;
                            case HorizontalAlignment.Right:
                            {
                                Vector2 pos = layoutTransform.anchoredPosition;
                                pos.x = defaultPos + ((defaultWidth - size) / 2f);
                                layoutTransform.anchoredPosition = pos;
                            }
                                break;
                        }
                    }
                    else
                    {
                        Vector2 pos = layoutTransform.anchoredPosition;
                        pos.x = defaultPos;
                        layoutTransform.anchoredPosition = pos;
                    }
                }

                layoutTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size);
            }
        }
    }
}
