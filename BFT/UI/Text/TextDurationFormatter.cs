﻿#if BFT_TEXTMESHPRO
using TMPro;
using UnityEngine;

namespace BFT
{
    [RequireComponent(typeof(TextMeshProUGUI)), ExecuteInEditMode]
    public class TextDurationFormatter : MonoBehaviour
    {
        public float Seconds;
        private TextMeshProUGUI text;

        void Awake()
        {
            text = GetComponent<TextMeshProUGUI>();
        }

        void Update()
        {
            text.text = TimeUtils.FormattedString(Seconds);
        }
    }
}
#endif
