﻿#if BFT_TEXTMESHPRO
using System.Collections;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace BFT
{
    public class TextColorUpdater : SerializedMonoBehaviour, IVariable<Color>
    {
        [BoxGroup("Options")] public bool AutoUpdate;

        [BoxGroup("References"), OnValueChanged("CheckIfSelf")]
        public IValue<Color> ColorReference;

        [SerializeField] private ColorEvent OnValueChange;

        [BoxGroup("References")] public TextMeshProUGUI Text;

        [SerializeField] private bool updateOnEnable = true;

        public Color Value
        {
            get => ColorReference.Value;

            set
            {
                IVariable<Color> variable = ColorReference as IVariable<Color>;
                if (variable != null)
                {
                    variable.Value = value;
                }

                OnValueChange.Invoke(value);
            }
        }

        private void CheckIfSelf()
        {
            if (ColorReference.Equals(this))
            {
                UnityEngine.Debug.LogWarning("You cannot reference an image updater to itself to avoid Stack Overflows", this);
                ColorReference = null;
            }
        }

        [BoxGroup("Options"), Button(ButtonSizes.Medium)]
        public void UpdateColor()
        {
            if (ColorReference != null)
                Text.color = ColorReference.Value;
        }

        [OnInspectorGUI]
        private void EditorUpdate()
        {
            if (!Application.isPlaying && AutoUpdate)
                UpdateColor();
        }

        public void Reset()
        {
            Text = GetComponent<TextMeshProUGUI>();
        }

        public void OnEnable()
        {
            if (updateOnEnable || AutoUpdate)
                UpdateColor();
            if (AutoUpdate)
                StartCoroutine(UpdateRegularly());
        }

        public void OnDisable()
        {
            StopAllCoroutines();
        }

        private IEnumerator UpdateRegularly()
        {
            while (true)
            {
                UpdateColor();
                yield return null;
            }
        }
    }
}
#endif