﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

#endif
namespace BFT
{
    [AddComponentMenu("Generation/Rectangle Populator")]
    public class RectanglePopulator : APopulatingGeom
    {
        public float height;

        public float width;

#if UNITY_EDITOR
        void OnDrawGizmosSelected()
        {
            // CoordinateBox box=new CoordinateBox(transform.position,new Vector3(width,height,0));
            Handles.DrawAAConvexPolygon(
                transform.position + transform.TransformDirection(new Vector3(-width / 2, 0, -height / 2)),
                transform.position + transform.TransformDirection(new Vector3(-width / 2, 0, height / 2)),
                transform.position + transform.TransformDirection(new Vector3(width / 2, 0, height / 2)),
                transform.position + transform.TransformDirection(new Vector3(width / 2, 0, -height / 2)));
        }
#endif

        public override List<Vector3> GivePossibleCoordinates(Populator populator)
        {
            int amountWidth = Mathf.FloorToInt(width / populator.Diameter);
            int amountHeight = Mathf.FloorToInt(height / populator.Diameter);
            List<Vector3> toReturn = new List<Vector3>(amountWidth * amountHeight);
            //now lets' find local coordinates
            for (int i = 0; i < amountWidth; i++)
            {
                for (int j = 0; j < amountHeight; j++)
                {
                    toReturn.Add(transform.position + transform.TransformDirection(new Vector3(
                                     (i + .5f) * populator.Diameter - width / 2, 0,
                                     (j + .5f) * populator.Diameter - height / 2)));
                }
            }

            return toReturn;
        }
    }
}
