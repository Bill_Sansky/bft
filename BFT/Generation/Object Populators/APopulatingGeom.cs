﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public abstract class APopulatingGeom : MonoBehaviour
    {
        public abstract List<Vector3> GivePossibleCoordinates(Populator populator);
    }
}
