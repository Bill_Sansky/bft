﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

#endif

namespace BFT
{
    public class Populator : MonoBehaviour
    {
        public int amountToGenerate;
        public float distanceOverlap;

        [SerializeField] private List<GameObject> generated = new List<GameObject>();

        public bool generatePerGeom = false;

        public APopulatingGeom[] geomsToPopulate;

        // the debugRadius that will be used to seperate all the objects during the placement.
        public float radiusOfPlacement;

        public int randomSeed;
        public GameObject[] toCopy;
        public GameObject whereToCreate;

        public float Diameter => radiusOfPlacement * 2;

#if UNITY_EDITOR
        public void OnDrawGizmosSelected()
        {
            Handles.color = Color.blue.Alphaed(.8f);
            Handles.DrawSolidDisc(transform.position, transform.up, radiusOfPlacement);
        }
#endif

        public void Populate()
        {
            List<Vector3> allPosition = new List<Vector3>();
            if (!generatePerGeom)
            {
                foreach (APopulatingGeom geom in geomsToPopulate)
                {
                    allPosition.AddRange(geom.GivePossibleCoordinates(this));
                }

                ToCopyId(allPosition);
            }
            else
            {
                foreach (APopulatingGeom geom in geomsToPopulate)
                {
                    allPosition = geom.GivePossibleCoordinates(this);
                    ToCopyId(allPosition);
                }
            }
        }

        private void ToCopyId(List<Vector3> allPosition)
        {
            int toCopyId = 0;
            int id = 0;
            int amountGenerated = 0;

            if (randomSeed == 0)
            {
                Random.InitState(randomSeed);
            }

            while ((randomSeed == 0 && id < amountToGenerate) ||
                   (randomSeed != 0 && amountGenerated < amountToGenerate))
            {
                GameObject copied = Instantiate(toCopy[toCopyId]);
                if (whereToCreate == null)
                    copied.transform.parent = transform;
                else
                {
                    copied.transform.parent = whereToCreate.transform;
                }

                copied.transform.position = allPosition[id];
                generated.Add(copied);

                amountGenerated++;

                toCopyId++;
                if (toCopyId >= toCopy.Count())
                    toCopyId = 0;

                if (randomSeed == 0)
                {
                    id++;

                    if (id >= allPosition.Count)
                        break;
                }
                else
                {
                    id = Random.Range(0, allPosition.Count);
                }
            }
        }

        public void Empty()
        {
            foreach (GameObject go in generated)
            {
#if UNITY_EDITOR
                DestroyImmediate(go);
#else
            Destroy(go);
#endif
            }
        }
    }
}
