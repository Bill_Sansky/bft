﻿using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Generation/Transform Randomizer")]
    public class TransformRandomizer : MonoBehaviour
    {
        [TabGroup("Random", "Scale"), ShowIf("RandomizeScale")]
        public bool KeepProportion = false;

        [TabGroup("Random", "Position"), ShowIf("RandomizePosition")]
        public float MaxDistanceFromCenter;


        [Range(-180, 180), TabGroup("Random", "Rotation"), ShowIf("RandomizeRotation")]
        public float MaxXRotation = 360;

        [TabGroup("Random", "Scale"), ShowIf("RandomizeScale")]
        public float MaxXScale = 1;

        [Range(-180, 180), TabGroup("Random", "Rotation"), ShowIf("RandomizeRotation")]
        public float MaxYRotation = 360;

        [TabGroup("Random", "Scale"), ShowIf("ShowAllScale")]
        public float MaxYScale = 1;

        [Range(-180, 180), TabGroup("Random", "Rotation"), ShowIf("RandomizeRotation")]
        public float MaxZRotation = 360;

        [TabGroup("Random", "Scale"), ShowIf("ShowAllScale")]
        public float MaxZScale = 1;

        [Range(-180, 180), TabGroup("Random", "Rotation"), ShowIf("RandomizeRotation")]
        public float MinXRotation;

        [TabGroup("Random", "Scale"), ShowIf("RandomizeScale")]
        public float MinXScale = 1;

        [Range(-180, 180), TabGroup("Random", "Rotation"), ShowIf("RandomizeRotation")]
        public float MinYRotation;

        [TabGroup("Random", "Scale"), ShowIf("ShowAllScale")]
        public float MinYScale = 1;

        [Range(-180, 180), TabGroup("Random", "Rotation"), ShowIf("RandomizeRotation")]
        public float MinZRotation;

        [TabGroup("Random", "Scale"), ShowIf("ShowAllScale")]
        public float MinZScale = 1;

        [TabGroup("Random", "Position"), ShowIf("RandomizePosition")]
        public Vector3 PositionFilter;

        [BoxGroup("Options")] public bool RandomizeOnAwake = true;

        [BoxGroup("Options")] public bool RandomizeOnEnable = false;

        [TabGroup("Random", "Position")] public bool RandomizePosition = false;
        [TabGroup("Random", "Rotation")] public bool RandomizeRotation = false;

        [TabGroup("Random", "Scale")] public bool RandomizeScale = false;

        [TabGroup("Random", "Position"), ShowIf("RandomizePosition")]
        public UnityEngine.Transform ReferenceCenter;

        private bool ShowAllScale => RandomizeScale && !KeepProportion;

        public void Reset()
        {
            ReferenceCenter = transform;
        }

        void Awake()
        {
            if (RandomizeOnAwake)
                Randomize();
        }

        void OnEnable()
        {
            if (RandomizeOnEnable)
                Randomize();
        }

        void OnDrawGizmosSelected()
        {
            if (ReferenceCenter != null)
            {
                Gizmos.color = Color.blue.Alphaed(.1f);
                Gizmos.DrawSphere(ReferenceCenter.position, MaxDistanceFromCenter);
            }
        }

        [BoxGroup("Tools"), Button(ButtonSizes.Medium)]
        public void Randomize()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                Undo.RecordObject(gameObject.transform, "Randomization");
#endif

            ModifyTransform(gameObject);
        }

        public void ModifyTransform(GameObject go)
        {
            if (RandomizeRotation)
                go.transform.rotation = Quaternion.Euler(Random.Range(MinXRotation, MaxXRotation),
                    Random.Range(MinYRotation, MaxYRotation), Random.Range(MinZRotation, MaxZRotation));

            if (RandomizeScale)
            {
                if (KeepProportion)
                    go.transform.localScale = Vector3.one * (Random.Range(MinXScale, MaxXScale));
                else
                {
                    go.transform.localScale = new Vector3(Random.Range(MinXScale, MaxXScale),
                        Random.Range(MinYScale, MaxYScale), Random.Range(MinZScale, MaxZScale));
                }
            }


            if (RandomizePosition && !Mathf.Approximately(MaxDistanceFromCenter, 0))
            {
                float radius = Random.Range(0, MaxDistanceFromCenter);
                float theta = Random.Range(-Mathf.PI / 2, Mathf.PI / 2);
                float phi = Random.Range(-Mathf.PI, Mathf.PI);
                go.transform.position = ReferenceCenter.position +
                                        new Vector3(radius * Mathf.Cos(theta) * Mathf.Cos(phi),
                                            radius * Mathf.Cos(theta) * Mathf.Sin(phi),
                                            radius * Mathf.Sin(theta)).Mult(PositionFilter);
            }
        }
    }
}
