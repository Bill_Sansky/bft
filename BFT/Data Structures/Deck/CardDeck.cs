﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace BFT
{
    [Serializable]
    public class CardDeck<T> : IEnumerable<T>
    {
        [BoxGroup("Deck Objects")] public List<T> CardsInDeck = new List<T>();

        [BoxGroup("Unity Events")] public UnityEvent OnDeckEmpty;

        [Tooltip("This event only gets called when cards are being removed as a group, and not when the deck is cleared")]
        [BoxGroup("Unity Events")]
        public UnityEvent OnLastCardRemoved;

        public CardDeck()
        {
            OnDeckEmpty = new UnityEvent();
            OnLastCardRemoved = new UnityEvent();
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (T value in CardsInDeck)
            {
                yield return value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void AddObjectsAtTheEnd(IEnumerable<T> toAdd)
        {
            CardsInDeck.AddRange(toAdd);
        }

        public void AddObjectsAfter(T after, IEnumerable<T> toAdd)
        {
            int id = CardsInDeck.IndexOf(after);
            if (id == -1)
                return;
            CardsInDeck.InsertRange(id + 1, toAdd);
        }

        public void AddObjectAtTheStart(T toAdd)
        {
            CardsInDeck.Insert(0, toAdd);
        }

        public void AddObjectAtTheEnd(T toAdd)
        {
            CardsInDeck.Add(toAdd);
        }

        public void AddObjectsAtTheStart(IEnumerable<T> toAdd)
        {
            CardsInDeck.InsertRange(0, toAdd);
        }

        public void InsertObjectsBetween(IEnumerable<T> toAdd, int minIndex, int maxIndex)
        {
            foreach (var value in toAdd)
            {
                CardsInDeck.Insert(Random.Range(minIndex, Mathf.Clamp(maxIndex, 0, CardsInDeck.Count - 1)), value);
                maxIndex++;
            }
        }

        public void AddShuffleObjects(IEnumerable<T> toAdd)
        {
            foreach (var value in toAdd)
            {
                CardsInDeck.Insert(Random.Range(0, CardsInDeck.Count), value);
            }
        }

        public void Clear()
        {
            CardsInDeck.Clear();
            OnDeckEmpty.Invoke();
        }

        public void Shuffle()
        {
            CardsInDeck.Shuffle();
        }

        public void Shuffle(int from, int count)
        {
            CardsInDeck.Shuffle(from, count);
        }

        public T TakeFirst()
        {
            if (CardsInDeck.Count == 0)
                return default(T);

            T toReturn = CardsInDeck[0];
            CardsInDeck.RemoveAt(0);

            if (CardsInDeck.IsEmpty())
                OnLastCardRemoved.Invoke();

            return toReturn;
        }

        public T SeeFirst()
        {
            if (CardsInDeck.Count == 0)
                return default(T);

            return CardsInDeck[0];
        }
    }
}
