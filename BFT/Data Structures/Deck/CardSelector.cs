﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sirenix.OdinInspector;

namespace BFT
{
    [Serializable]
    public class CardSelector<T>
    {
        private List<T> freeSelection = new List<T>();

        [BoxGroup("Selection")] public List<T> FullSelection = new List<T>();

        public List<T> FreeSelection => freeSelection ?? (freeSelection = FullSelection.ToList());


        public T GetNextAvailableCard(T current)
        {
            if (FreeSelection.Contains(current))
            {
                int i = FreeSelection.IndexOf(current) + 1;
                if (i >= FreeSelection.Count)
                    i = 0;
                return FreeSelection[i];
            }

            return FreeSelection[0];
        }

        public T GetPreviousAvailableCard(T current)
        {
            if (FreeSelection.Contains(current))
            {
                int i = FreeSelection.IndexOf(current) - 1;
                if (i < 0)
                    i = FreeSelection.Count - 1;
                return FreeSelection[i];
            }

            return FreeSelection[0];
        }

        public bool IsAnyCardAvailable()
        {
            return FreeSelection.Count > 0;
        }

        public bool IsCardAvailable(T toCheck)
        {
            return FreeSelection.Contains(toCheck);
        }

        public bool TakeCard(T card)
        {
            if (!IsCardAvailable(card))
                return false;

            FreeSelection.Remove(card);
            return true;
        }

        public void GiveBackCard(T card)
        {
            FreeSelection.Add(card);
        }
    }
}
