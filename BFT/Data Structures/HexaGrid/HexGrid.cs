﻿
using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

#if UNITY_EDITOR

using Sirenix.OdinInspector.Editor;
using UnityEditor;

#endif

namespace BFT
{
    public class HexGrid : SerializedMonoBehaviour, IEnumerable<Hex>
    {
#if UNITY_EDITOR
        void Reset()
        {
            HexPrefab = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Content/Grid/Prefab/Core Hex.prefab");
        }
#endif

        void Awake()
        {
            if (AutoFixGrid && IsBroken)
                FixGrid();
        }

        #region Hex Mesh

        private Mesh hexMesh;

        public Mesh HexMesh
        {
            get
            {
                if (hexMesh == null)
                    hexMesh = GetHexaMesh();
                return hexMesh;
            }
        }

        public Mesh GetHexaMesh()
        {
            Vector3 center = Vector3.zero;
            Quaternion sixty = Quaternion.AngleAxis(60, Vector3.up);
            Vector3 startingPoint = Vector3.right * HexRadius;

            List<Vector3> vertices = new List<Vector3>();
            List<int> triangleIndexes = new List<int>();
            List<Vector3> normals = new List<Vector3>();

            for (int i = 0; i < 6; i++)
            {
                AddTriangle(vertices, triangleIndexes, normals,
                    center,
                    center + startingPoint,
                    center + sixty * startingPoint
                );
                startingPoint = sixty * startingPoint;
            }

            Mesh mesh = new Mesh();
            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangleIndexes.ToArray();
            mesh.normals = normals.ToArray();

            return mesh;
        }

        private void AddTriangle(List<Vector3> vertices, List<int> triangles, List<Vector3> normals, Vector3 v1, Vector3 v2,
            Vector3 v3)
        {
            int vertexIndex = vertices.Count;
            vertices.Add(v1);
            vertices.Add(v2);
            vertices.Add(v3);
            normals.Add(Vector3.up);
            normals.Add(Vector3.up);
            normals.Add(Vector3.up);
            triangles.Add(vertexIndex);
            triangles.Add(vertexIndex + 1);
            triangles.Add(vertexIndex + 2);
        }

        [FoldoutGroup("Tools"), Button, PropertyOrder(Int32.MaxValue)]
        private void RegenerateHexMesh()
        {
            hexMesh = GetHexaMesh();
        }

        #endregion

        #region Data Structure

        public GameObject HexPrefab;

        public bool KeepPrefabInstanceLinking;

        [FoldoutGroup("Data Structure")] [SerializeField, ListDrawerSettings(NumberOfItemsPerPage = 5)]
        private List<Hex> hexes = new List<Hex>();

        [SerializeField, ListDrawerSettings(NumberOfItemsPerPage = 5)]
        private List<Hex> edges = new List<Hex>();

        [SerializeField, FoldoutGroup("Advanced Data Structure"), ShowIf("IsBroken")]
        private GridDictionary<Hex> hexByCoordinate;

        [FoldoutGroup("Data Structure")] public bool IsBroken => hexByCoordinate.ContainsNullRow;

        public Hex this[int i, int j] => hexByCoordinate[i, j];

        [FoldoutGroup("Data Structure"), SerializeField, ReadOnly]
        private Hex coreHex;

        public Hex CoreHex => coreHex;

        public bool AutoFixGrid = true;

        [FoldoutGroup("Tools"), Button(ButtonSizes.Medium)]
        public void FixGrid()
        {
            foreach (Hex hex in hexes)
            {
                hex.CheckIfEdge();
            }

            hexByCoordinate.Clear();

            foreach (Hex hex in hexes)
            {
                hexByCoordinate.Add(hex.HexI, hex.HexJ, hex);
            }

#if UNITY_EDITOR
            if (PrefabUtility.GetPrefabInstanceStatus(this) != PrefabInstanceStatus.NotAPrefab)
                OdinPrefabUtility.UpdatePrefabInstancePropertyModifications(gameObject, false);
#endif
        }

        #endregion

        #region Grid Size

        [FoldoutGroup("Grid Size"), OnValueChanged("RecalculateHexPositions"), MinValue(0.1f)]
        public float HexRadius = 1;

        [ShowInInspector, ReadOnly, FoldoutGroup("Grid Size")]
        public float SemiTriangleDistance => Mathf.Sqrt(HexRadius * HexRadius * 3 / 4);

        [ShowInInspector, ReadOnly, FoldoutGroup("Grid Size")]
        public int GridHexRadius
        {
            get
            {
                if (edges.Count > 0)
                    return Mathf.Abs(edges[0].HexJ);

                return 0;
            }
        }

        public IEnumerator<Hex> GetEnumerator()
        {
            foreach (Hex hex in hexes)
            {
                yield return hex;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion

        #region Coordinate Helpers

        public Vector3 WorldToGridCoordinates(Vector3 worldPosition)
        {
            Vector3 transformed = transform.InverseTransformPoint(worldPosition);
            return new Vector3(transformed.x / Mathf.Sin(120 * Mathf.Deg2Rad), transformed.y
                , transformed.z - transformed.x / Mathf.Tan(120 * Mathf.Deg2Rad));
        }

        public void FindGridCoordinate(Vector3 worldPosition, out int i, out int j)
        {
            Vector3 localCoord = WorldToGridCoordinates(worldPosition);

            i = Mathf.RoundToInt(localCoord.x / (SemiTriangleDistance * 2));

            j = Mathf.RoundToInt(localCoord.z / (SemiTriangleDistance * 2));
        }

        public Hex FindHex(Vector3 absolutePosition)
        {
            int i, j;
            FindGridCoordinate(absolutePosition, out i, out j);
            return hexByCoordinate[i, j];
        }

        #endregion

        #region Grid Expansion And Reduction

        public void AddNexHex(Hex hex)
        {
            if (!hexes.Contains(hex))
            {
                hexes.Add(hex);
                edges.Add(hex);
                hexByCoordinate.Add(hex.HexI, hex.HexJ, hex);
            }
        }

        public void RemoveHex(Hex hex)
        {
            if (hexes.Contains(hex))
            {
                hexes.Remove(hex);
            }

            if (edges.Contains(hex))
            {
                edges.Remove(hex);
            }

            if (hexByCoordinate.ContainsKeyAndNotNull(hex.HexI, hex.HexJ))
            {
                hexByCoordinate.RemoveAt(hex.HexI, hex.HexJ);
            }
        }

        public void NotifyEdge(Hex hex)
        {
            if (!edges.Contains(hex))
                edges.Add(hex);
        }

        public void NotifyNotAnEdge(Hex hex)
        {
            if (edges.Contains(hex))
                edges.Remove(hex);
        }

        public bool IsHexExists(int i, int j)
        {
            return hexByCoordinate.ContainsKeyAndNotNull(i, j);
        }

        #endregion

        #region Tools

#if UNITY_EDITOR

        [FoldoutGroup("Tools")] public int GridRadiusExpansion = 1;

        [FoldoutGroup("Tools")]
        [Button(ButtonSizes.Medium)]
        public void ExpendGrid()
        {
            if (!HexPrefab)
            {
                UnityEngine.Debug.LogWarning("First associate the prefab representing the hex!");
                return;
            }

            if (hexes.Count == 0)
            {
                GameObject go;

                if (KeepPrefabInstanceLinking)
                    go = PrefabUtility.InstantiatePrefab(HexPrefab) as GameObject;
                else
                    go = Instantiate(HexPrefab);

                if (go != null)
                {
                    go.name = "Core Hex";
                    Hex hex = go.GetComponent<Hex>();
                    go.transform.SetParent(transform, false);
                    go.transform.localPosition = new Vector3();
                    coreHex = hex;
                    hex.Grid = this;
                    AddNexHex(hex);
                }
            }

            for (int i = 0; i < GridRadiusExpansion; i++)
            {
                UnityEngine.Debug.Log("Creating Expansion #" + i);
                List<Hex> currentEdges = new List<Hex>(edges);
                foreach (var hex in currentEdges)
                {
                    if (hex != null)
                    {
                        hex.AddHexGroup(HexSide.ALL);
                        hex.CheckIfEdge();
                    }
                }
            }

            UnityEngine.Debug.Log("Expansion Done");

            if (!Application.isPlaying)
            {
                if (PrefabUtility.GetPrefabInstanceStatus(this) != PrefabInstanceStatus.NotAPrefab)
                    OdinPrefabUtility.UpdatePrefabInstancePropertyModifications(this, false);
            }

            UnityEngine.Debug.Log("Prefab Instance Saved");
        }

        public void RecalculateHexPositions()
        {
            if (!coreHex)
                return;
            List<Hex> repositionHexes = new List<Hex>();
            CoreHex.transform.localScale = Vector3.one * HexRadius;
            repositionHexes.Add(CoreHex);
            CoreHex.RepositionAllNeighbors(new List<Hex>(repositionHexes));
        }

        [FoldoutGroup("Debug")] public bool DebugGrid = true;

        [FoldoutGroup("Debug")] public Color HexLineDebugColor = Color.white;

        [FoldoutGroup("Debug")] public Color HexInsideDebugColor = Color.cyan.Alphaed(.1f);

        [FoldoutGroup("Debug")] public float DebugNonSelectedAlpha = 0.01f;

        public void DrawGrid()
        {
            foreach (Hex hex in hexes)
            {
                hex.HexGizmo(0.8f);
            }
        }

        [OnInspectorGUI]
        public void AutoFixGridMethod()
        {
            if (!Application.isPlaying && IsBroken && AutoFixGrid)
                FixGrid();
        }

#endif

        #endregion
    }
}
