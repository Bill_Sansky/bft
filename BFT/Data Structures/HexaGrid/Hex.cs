﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    [Flags]
    public enum HexSide
    {
        T = 1 << 1,
        TR = 1 << 2,
        BR = 1 << 3,
        B = 1 << 4,
        BL = 1 << 5,
        TL = 1 << 6,
        ALL = T | TR | BR | B | BL | TL,
        TOP = T | TR | TL,
        BOTTOM = B | BR | BL,
        LEFT = TL | BL,
        RIGHT = TR | BR,
        NONE
    }

    [ExecuteInEditMode]
    [AddComponentMenu("Grid/Hex Grid/Hex")]
    public class Hex : SerializedMonoBehaviour
    {
        public HexGrid Grid;

        [SerializeField, ShowInInspector] private bool isImpassable;

        [SerializeField, ShowInInspector] private bool isTaken;

        public Dictionary<HexSide, Hex> Neighbors = new Dictionary<HexSide, Hex>();
        public UnityEvent OnHexImpassableChange;

        public UnityEvent OnHexTakenChange;

        [HideInInspector] public int RangeHelperInt;

        public bool IsTaken
        {
            get => isTaken;
            set
            {
                isTaken = value;
                OnHexTakenChange.Invoke();
            }
        }

        public bool IsImpassable
        {
            get => isImpassable;
            set
            {
                isImpassable = value;
                OnHexImpassableChange.Invoke();
            }
        }

        [ShowInInspector, ReadOnly]
        public int HexJ
        {
            get
            {
                if (!Grid)
                    return 0;
                return Mathf.RoundToInt(Grid.WorldToGridCoordinates(transform.position).z
                                        / (Grid.SemiTriangleDistance * 2));
            }
        }

        [ShowInInspector, ReadOnly]
        public int HexI
        {
            get
            {
                if (!Grid)
                    return 0;
                return Mathf.RoundToInt(Grid.WorldToGridCoordinates(transform.position).x
                                        / (Grid.SemiTriangleDistance * 2));
            }
        }

        public void RemoveNeighbor(HexSide side)
        {
            Neighbors.Remove(side);

#if UNITY_EDITOR
            if (!Application.isPlaying && PrefabUtility.GetPrefabInstanceStatus(this) != PrefabInstanceStatus.NotAPrefab)
                Sirenix.OdinInspector.Editor.OdinPrefabUtility.UpdatePrefabInstancePropertyModifications(this, false);
#endif
        }

        public Hex GetClosestNeighbor(Vector3 position)
        {
            Vector3 distance = position - transform.position;
            float angle = MathExt.SignedVectorAngle(transform.right, distance, transform.up);
            if (angle >= 0)
            {
                if (angle < 60)
                    return Neighbors[HexSide.TR];

                if (angle < 120)
                    return Neighbors[HexSide.T];

                return Neighbors[HexSide.TL];
            }

            if (angle > -60)
            {
                return Neighbors[HexSide.BR];
            }

            if (angle > -120)
            {
                return Neighbors[HexSide.B];
            }

            return Neighbors[HexSide.BL];
        }


        public void AddHexGroup(HexSide side)
        {
            foreach (HexSide newSide in Enum.GetValues(typeof(HexSide)))
            {
                switch (newSide)
                {
                    case HexSide.ALL:
                    case HexSide.TOP:
                    case HexSide.BOTTOM:
                    case HexSide.LEFT:
                    case HexSide.RIGHT:
                    case HexSide.NONE:
                        break;
                    default:
                        if (side.HasFlag(newSide))
                            AddAndInstantiateHexNeighbor(newSide);
                        break;
                }
            }
        }

        public void AddAndInstantiateHexNeighbor(HexSide side)
        {
            if (Neighbors.ContainsKey(side))
                return;
            GameObject go;
#if UNITY_EDITOR
            if (Grid.KeepPrefabInstanceLinking)
                go = PrefabUtility.InstantiatePrefab(Grid.HexPrefab) as GameObject;
            else
                go = Instantiate(Grid.HexPrefab);
#else
                 go = Instantiate(Grid.HexPrefab);
#endif
            if (go != null)
            {
                Hex hex = go.GetComponent<Hex>();
#if UNITY_EDITOR
                Undo.SetTransformParent(go.transform, transform.parent, "Parent New Hex To Grid");
#else
              go.transform.SetParent(transform.parent);
#endif

                go.transform.position = transform.position + GetRelativeDistanceToOtherHexCenter(side);
                hex.Grid = Grid;
                go.name = " (" + hex.HexI + "," + hex.HexJ + ")";
                Neighbors.Add(side, hex);

                CheckIfEdge();

                hex.Neighbors.Add(GetOppositeHexSide(side), this);

                //now update all the references around

                //first cycle counterclockwise
                HexSide neighborSide = GetLeftNeighbor(side);
                if (Neighbors.ContainsKey(neighborSide))
                    Neighbors[neighborSide].UpdateHexReference(this, hex, GetRightNeighbor(side), true);
                //then cycle clockwise
                neighborSide = GetRightNeighbor(side);
                if (Neighbors.ContainsKey(neighborSide))
                    Neighbors[neighborSide].UpdateHexReference(this, hex, GetLeftNeighbor(side), false);

                Grid.AddNexHex(hex);

                hex.CheckIfEdge();

                //there is a chance the hex may not link to every possible hex in the situation of a convex graph:
                //that would need to be manually edited then.

#if UNITY_EDITOR

                hex.ToAdd = side;

                if (!Application.isPlaying &&
                    PrefabUtility.GetPrefabInstanceStatus(this) != PrefabInstanceStatus.NotAPrefab)
                {
                    PrefabUtility.RecordPrefabInstancePropertyModifications(this);
                    Sirenix.OdinInspector.Editor.OdinPrefabUtility
                        .UpdatePrefabInstancePropertyModifications(this, false);
                }
#endif
            }
        }

        //cycles through all the neighbors around the newly added Hex and update all the references
        public void UpdateHexReference(Hex startingHex, Hex newHex, HexSide relativeSide, bool cycleClockWise)
        {
            if (startingHex == this || Neighbors.ContainsKey(relativeSide))
                return;

            ClearNeighbors();

            Neighbors.Add(relativeSide, newHex);
            CheckIfEdge();
            newHex.Neighbors.Add(GetOppositeHexSide(relativeSide), this);

#if UNITY_EDITOR
            if (!Application.isPlaying && PrefabUtility.GetPrefabInstanceStatus(this) != PrefabInstanceStatus.NotAPrefab)
            {
                PrefabUtility.RecordPrefabInstancePropertyModifications(this);
                Sirenix.OdinInspector.Editor.OdinPrefabUtility
                    .UpdatePrefabInstancePropertyModifications(this, false);
            }
#endif


            HexSide neighborSide = (cycleClockWise) ? GetLeftNeighbor(relativeSide) : GetRightNeighbor(relativeSide);
            if (Neighbors.ContainsKey(neighborSide))
            {
                //then update the neighbor
                Neighbors[neighborSide].UpdateHexReference(startingHex, newHex
                    , (cycleClockWise) ? GetRightNeighbor(relativeSide) : GetLeftNeighbor(relativeSide), cycleClockWise);
            }
        }

        private void ClearNeighbors()
        {
            List<HexSide> neighborIndexesToRemove = new List<HexSide>();
            foreach (var neighbor in Neighbors)
            {
                if (!neighbor.Value)
                    neighborIndexesToRemove.Add(neighbor.Key);
            }

            foreach (HexSide hexSide in neighborIndexesToRemove)
            {
                Neighbors.Remove(hexSide);
            }
        }

        public void CheckIfEdge()
        {
            int i = 0;
            foreach (var neighborsKey in Neighbors.Keys)
            {
                switch (neighborsKey)
                {
                    case HexSide.ALL:
                    case HexSide.TOP:
                    case HexSide.BOTTOM:
                    case HexSide.LEFT:
                    case HexSide.RIGHT:
                    case HexSide.NONE:
                        break;
                    default:
                        i++;
                        break;
                }
            }

            if (i == 6)
            {
                //then it is not an edge
                Grid.NotifyNotAnEdge(this);
            }
            else
            {
                Grid.NotifyEdge(this);
            }
        }

        public void RepositionAllNeighbors(List<Hex> alreadyRepositioned)
        {
            ClearNeighbors();
            foreach (var neighbor in Neighbors)
            {
                if (!alreadyRepositioned.Contains(neighbor.Value))
                {
                    neighbor.Value.transform.position = transform.position
                                                        + GetRelativeDistanceToOtherHexCenter(neighbor.Key);
                    neighbor.Value.transform.localScale = Vector3.one * Grid.HexRadius;
                    alreadyRepositioned.Add(neighbor.Value);

                    neighbor.Value.RepositionAllNeighbors(alreadyRepositioned);
                }
            }
        }

        public Vector3 GetRelativeDistanceToOtherHexCenter(HexSide side)
        {
            return Grid.SemiTriangleDistance * 2 * GetVectorDirection(side);
        }

        public void RemoveHex()
        {
            if (Grid)
                Grid.RemoveHex(this);
            foreach (var neighbor in Neighbors)
            {
                if (neighbor.Value)
                {
                    neighbor.Value.RemoveNeighbor(GetOppositeHexSide(neighbor.Key));
                }
            }
        }

        #region HEX Helper Methods

        public static HexSide GetOppositeHexSide(HexSide side)
        {
            switch (side)
            {
                case HexSide.T:
                    return HexSide.B;
                case HexSide.TR:
                    return HexSide.BL;
                case HexSide.BR:
                    return HexSide.TL;
                case HexSide.B:
                    return HexSide.T;
                case HexSide.BL:
                    return HexSide.TR;
                case HexSide.TL:
                    return HexSide.BR;
                case HexSide.ALL:
                    return HexSide.NONE;
                case HexSide.TOP:
                    return HexSide.BOTTOM;
                case HexSide.BOTTOM:
                    return HexSide.TOP;
                case HexSide.LEFT:
                    return HexSide.RIGHT;
                case HexSide.RIGHT:
                    return HexSide.LEFT;
                default:
                    throw new ArgumentOutOfRangeException("side", side, null);
            }
        }

        public static HexSide GetRightNeighbor(HexSide side)
        {
            switch (side)
            {
                case HexSide.T:
                    return HexSide.TR;
                case HexSide.TR:
                    return HexSide.BR;
                case HexSide.BR:
                    return HexSide.B;
                case HexSide.B:
                    return HexSide.BL;
                case HexSide.BL:
                    return HexSide.TL;
                case HexSide.TL:
                    return HexSide.T;
                case HexSide.ALL:
                    return HexSide.ALL;
                case HexSide.TOP:
                    return HexSide.RIGHT;
                case HexSide.BOTTOM:
                    return HexSide.LEFT;
                case HexSide.LEFT:
                    return HexSide.BOTTOM;
                case HexSide.RIGHT:
                    return HexSide.TOP;
                case HexSide.NONE:
                    return HexSide.NONE;
                default:
                    throw new ArgumentOutOfRangeException("side", side, null);
            }
        }

        public static Vector3 GetVectorDirection(HexSide side)
        {
            switch (side)
            {
                case HexSide.T:
                    return Vector3.forward;
                case HexSide.TR:
                    return Quaternion.AngleAxis(60, Vector3.up) * Vector3.forward;
                case HexSide.BR:
                    return Quaternion.AngleAxis(120, Vector3.up) * Vector3.forward;
                case HexSide.B:
                    return -Vector3.forward;
                case HexSide.BL:
                    return Quaternion.AngleAxis(60, Vector3.up) * -Vector3.forward;
                case HexSide.TL:
                    return Quaternion.AngleAxis(120, Vector3.up) * -Vector3.forward;
                case HexSide.ALL:
                case HexSide.TOP:
                case HexSide.BOTTOM:
                case HexSide.LEFT:
                case HexSide.RIGHT:
                case HexSide.NONE:
                    return Vector3.zero;
                default:
                    throw new ArgumentOutOfRangeException("side", side, null);
            }
        }

        public static HexSide GetLeftNeighbor(HexSide side)
        {
            switch (side)
            {
                case HexSide.T:
                    return HexSide.TL;
                case HexSide.TR:
                    return HexSide.T;
                case HexSide.BR:
                    return HexSide.TR;
                case HexSide.B:
                    return HexSide.BR;
                case HexSide.BL:
                    return HexSide.B;
                case HexSide.TL:
                    return HexSide.BL;
                case HexSide.ALL:
                    return HexSide.ALL;
                case HexSide.TOP:
                    return HexSide.LEFT;
                case HexSide.BOTTOM:
                    return HexSide.RIGHT;
                case HexSide.LEFT:
                    return HexSide.TOP;
                case HexSide.RIGHT:
                    return HexSide.BOTTOM;
                case HexSide.NONE:
                    return HexSide.NONE;
                default:
                    throw new ArgumentOutOfRangeException("side", side, null);
            }
        }

        #endregion

#if UNITY_EDITOR

        [FoldoutGroup("Tools")] [EnumToggleButtons]
        public HexSide ToAdd;

        [FoldoutGroup("Tools")]
        [Button("Generate Hex Neighbors")]
        public void AddSelectedGroup()
        {
            Selection.activeGameObject = null;

            AddHexGroup(ToAdd);
        }

#endif

#if UNITY_EDITOR

        public void OnDrawGizmosSelected()
        {
            if (Grid)
                HexGizmo(1);
        }


        public void HexGizmo(float alpha)
        {
            Matrix4x4 trs = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
            Gizmos.matrix = trs;

            Quaternion sixty = Quaternion.AngleAxis(60, Vector3.up);

            Vector3 startingPoint = Vector3.right * Grid.HexRadius;

            Gizmos.color = Grid.HexLineDebugColor.Alphaed(alpha);

            for (int i = 0; i < 6; i++)
            {
                Gizmos.DrawLine(startingPoint, sixty * startingPoint);
                startingPoint = sixty * startingPoint;
            }

            Gizmos.color = Grid.HexInsideDebugColor.Alphaed(alpha);
            Gizmos.DrawMesh(Grid.HexMesh, Vector3.zero);

            if (IsTaken)
            {
                startingPoint = Vector3.right * Grid.HexRadius * 0.8f;
                Gizmos.color = Color.red.Alphaed(alpha);
                for (int i = 0; i < 6; i++)
                {
                    Gizmos.DrawLine(startingPoint + Vector3.up * 0.5f, sixty * startingPoint + Vector3.up * 0.5f);
                    startingPoint = sixty * startingPoint;
                }
            }

            if (IsImpassable)
            {
                Gizmos.color = Color.grey.Alphaed(alpha);
                Gizmos.DrawMesh(Grid.HexMesh, Vector3.zero);
            }

            if (Selection.Contains(gameObject))
            {
                //then draw the side helper
                Quaternion thirty = Quaternion.AngleAxis(30, Vector3.up);
                startingPoint = Vector3.right * Grid.HexRadius;
                string[] indexes = {"B", "BL", "TL", "T", "TR", "BR"};
                Handles.matrix = transform.localToWorldMatrix;
                Handles.color = Color.black;

                HandleExt.Text(Vector3.zero, "(" + HexI + "," + HexJ + ")");
                for (int i = 0; i < 6; i++)
                {
                    HandleExt.Text(thirty * sixty * startingPoint, indexes[i]);
                    startingPoint = sixty * startingPoint;
                }
            }
        }

#endif
    }
}
