﻿using System.Collections;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

namespace BFT
{
    [ExecuteInEditMode]
    [AddComponentMenu("Grid/Hex Grid/Hex Position")]
    public class HexPosition : MonoBehaviour
    {
        [FoldoutGroup("Grid")] public Hex CurrentHex;

        [SerializeField, FoldoutGroup("Grid")] private HexGrid grid;

        private Vector3 lastPosition;

        [FoldoutGroup("Grid")] public UnityEvent OnHexChanged;

        [FoldoutGroup("Grid")] public float UpdateFrequency = 0.5f;

        public int HexI
        {
            get
            {
                if (!CurrentHex)
                    return 0;
                return CurrentHex.HexI;
            }
        }

        public int HexJ
        {
            get
            {
                if (!CurrentHex)
                    return 0;
                return CurrentHex.HexJ;
            }
        }

        [FoldoutGroup("Grid"), ShowInInspector, ReadOnly]
        public Vector3 GridPosition
        {
            get
            {
                if (!Grid)
                    return Vector3.zero;
                return Grid.WorldToGridCoordinates(transform.position);
            }
        }

        public HexGrid Grid
        {
            get => grid;
            set
            {
                grid = value;
                CheckCurrentHexBasedOnTransform();
            }
        }

        void OnEnable()
        {
            StartCoroutine(UpdateGridPosition());
        }

        void OnDisable()
        {
            StopAllCoroutines();
        }

        public void TeleportToPosition(int x, int y)
        {
            if (!Grid || !Grid.IsHexExists(x, y))
                return;

            transform.position = Grid[x, y].transform.position;
            SetHex(Grid[x, y]);
        }

        public void TeleportToHex(Hex hex)
        {
            if (hex == null)
                return;

            transform.position = hex.transform.position;
            SetHex(hex);
        }

        public IEnumerator UpdateGridPosition()
        {
            while (true)
            {
                if (!Grid)
                {
                    yield return new WaitForSecondsRealtime(UpdateFrequency);
                    continue;
                }

                CheckCurrentHexBasedOnTransform();

                yield return new WaitForSecondsRealtime(UpdateFrequency);
            }
        }

        [Button(ButtonSizes.Medium)]
        public void CheckCurrentHexBasedOnTransform()
        {
            if (!grid)
                return;

            Vector3 projectedDistance = MathExt.ProjectPointOnPlane(Grid.transform.up,
                Grid.transform.position, transform.position);

            if (!CurrentHex ||
                (projectedDistance - lastPosition).sqrMagnitude > Grid.SemiTriangleDistance * Grid.SemiTriangleDistance)
            {
                //then we most likely are out of the current hex, find the nex one
                Hex newHex = Grid.FindHex(transform.position);
                if (!newHex)
                    return;
                SetHex(newHex);
                lastPosition = newHex.transform.position;
            }
        }

        public void SetHex(Hex newHex)
        {
            if (CurrentHex)
            {
                if (newHex == CurrentHex)
                    return;
                CurrentHex.IsTaken = false;
            }

            CurrentHex = newHex;
            OnHexChanged.Invoke();
            if (CurrentHex)
                CurrentHex.IsTaken = true;
        }

#if UNITY_EDITOR

        [SerializeField, FoldoutGroup("Tools")]
        private int teleportHexI;

        [SerializeField, FoldoutGroup("Tools")]
        private int teleportHexJ;

        [FoldoutGroup("Tools")]
        [Button]
        private void TeleportToPosition()
        {
            TeleportToPosition(teleportHexI, teleportHexJ);
        }
#endif

#if UNITY_EDITOR

        [FoldoutGroup("Debug", false, 9999)] public Color DebugColor = ColorExt.Color256(240, 128, 128, 150);


        public void OnDrawGizmosSelected()
        {
            if (!CurrentHex || !Grid)
                return;
            Gizmos.matrix = CurrentHex.transform.localToWorldMatrix;
            Gizmos.color = DebugColor;

            Gizmos.DrawMesh(Grid.HexMesh);
        }

#endif
    }
}
