﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    public abstract class Basic2DGrid : SerializedMonoBehaviour
    {
        [SerializeField, OnValueChanged("CellSizeChangedCallBack", true)]
        private float cellHeight = 1;

        [SerializeField, OnValueChanged("CellSizeChangedCallBack", true)]
        private float cellWidth = 1;


        [Header("Debug")] [TabGroup("Debug")] public bool DebugShowGrid;

        [TabGroup("Debug")] public float DebugZSize = 200;

        [SerializeField, OnValueChanged("GridSizeChangedCallBack", true), MinValue(1)]
        private int gridHeight = 1;

        [SerializeField, OnValueChanged("GridSizeChangedCallBack", true), MinValue(1)]
        private int gridWidth = 1;

        [TabGroup("Debug"), MaxValue(200)] public int MaxDebugCellCount = 200;


        public virtual float CellHeight => cellHeight;

        public virtual float CellWidth => cellWidth;

        public int GridLength => gridWidth;

        public int GridHeight => gridHeight;

        protected virtual void GridSizeChangedCallBack()
        {
        }

        protected virtual void CellSizeChangedCallBack()
        {
        }

#if UNITY_EDITOR

        public virtual void OnDrawGizmos()
        {
            if (!DebugShowGrid)
                return;

            Gizmos.color = Color.white;

            for (int i = 0; i < Mathf.Min(GridLength, MaxDebugCellCount); i++)
            {
                for (int j = 0; j < Mathf.Min(GridHeight, MaxDebugCellCount); j++)
                {
                    Gizmos.DrawWireCube(transform.position + new Vector3(cellWidth * (i + 0.5f), cellHeight * (j + 0.5f), 0)
                        , new Vector3(cellWidth, CellHeight, DebugZSize));
                }
            }
        }

        public virtual void DebugGridCell(int i, int j, bool full = true, bool forceGlobalPosition = false)
        {
            if (full)
            {
                Gizmos.DrawCube(transform.position + new Vector3(cellWidth * (i + 0.5f), cellHeight * (j + 0.5f), 0)
                    , new Vector3(cellWidth, CellHeight, DebugZSize));
            }
            else
            {
                Gizmos.DrawWireCube(transform.position + new Vector3(cellWidth * (i + 0.5f), cellHeight * (j + 0.5f), 0)
                    , new Vector3(cellWidth, CellHeight, DebugZSize));
            }
        }

#endif
    }
}
