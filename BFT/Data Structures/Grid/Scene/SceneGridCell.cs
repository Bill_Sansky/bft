﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class SceneGridCell : MonoBehaviour
    {
        [ReadOnly, TabGroup("Data")] private SceneGrid grid;

        [ReadOnly] public string GridNameReference = "";

        [SerializeField, ReadOnly, TabGroup("Data")]
        private int i;

        [SerializeField, ReadOnly, TabGroup("Data")]
        private int j;

        [ShowInInspector, ReadOnly, TabGroup("Data")]
        public double AbsoluteXPosition => (Grid) ? I * Grid.CellWidth : 0;

        [ShowInInspector, ReadOnly, TabGroup("Data")]
        public double AbsoluteYPosition => (Grid) ? J * Grid.CellHeight : 0;

        public int I
        {
            get => i;
            set => SetCoordinate(value, j);
        }

        public int J
        {
            get => j;
            set => SetCoordinate(i, value);
        }

        public SceneGrid Grid
        {
            get
            {
#if UNITY_EDITOR
                if (!grid && SceneGrid.ScenesByName.ContainsKey(GridNameReference))
                    grid = SceneGrid.ScenesByName[GridNameReference];
#endif
                return grid;
            }
            set
            {
#if UNITY_EDITOR
                if (value)
                {
                    GridNameReference = value.GridName;
                    value.NotifyCellAdded(this);
                    return;
                }
#endif
                grid = value;
                if (!grid)
                    return;

                GridNameReference = grid.GridName;
                grid.NotifyCellAdded(this);
            }
        }

        public void SetCoordinate(int newI, int newJ)
        {
            i = newI;
            j = newJ;
            gameObject.name = GridNameReference + " Cell (" + i + "," + j + ")";
        }

        void Awake()
        {
            if (GridNameReference != "")
            {
                if (!SceneGrid.ScenesByName.ContainsKey(GridNameReference) ||
                    SceneGrid.ScenesByName[GridNameReference] == null)
                {
                    SceneGrid.OnSceneAdded += UpdateSceneReference;
                }
                else
                {
                    Grid = SceneGrid.ScenesByName[GridNameReference];
                }
            }
        }

        private void UpdateSceneReference(SceneGrid newGrid)
        {
            if (newGrid.GridName == GridNameReference)
            {
                SceneGrid.OnSceneAdded -= UpdateSceneReference;
                Grid = newGrid;
                Grid.NotifyCellAdded(this);
            }
        }

        public bool IsInside(double localX, double localY)
        {
            return IsInside(transform.position + new Vector3((float) localX, (float) localY, 0));
        }

        public bool IsInside(Vector3 position)
        {
            Bounds bound = new Bounds(transform.position + new Vector3(grid.CellWidth / 2, grid.CellHeight / 2, 0)
                , new Vector3(grid.CellWidth, grid.CellHeight, 99999));
            return bound.Contains(position);
        }

        void OnDestroy()
        {
            if (grid)
                Grid.NotifyCellRemoved(this);
        }

        [Button, TabGroup("Utils")]
        public void SetGridCoordinateBasedOnName()
        {
            SetCoordinate(
                Convert.ToInt32(gameObject.name.Substring(6, gameObject.name.IndexOf(",", StringComparison.Ordinal) - 6))
                , Convert.ToInt32(gameObject.name.Substring(gameObject.name.IndexOf(",", StringComparison.Ordinal) + 1
                    , gameObject.name.IndexOf(")", StringComparison.Ordinal) - (gameObject.name.IndexOf(",", StringComparison.Ordinal) + 1))));
        }


        public void RepositionCellRepresentation()
        {
#if UNITY_EDITOR

            if (!Application.isPlaying)
            {
                if (grid && !grid.IsCellLoaded(i, j))
                {
                    grid.NotifyCellAdded(this, true);
                }
            }

#endif
            transform.position = GetRepositionPosition();
        }

        protected Vector3 GetRepositionPosition()
        {
            if (!grid)
                return Vector3.zero;
            //so far no need to transform the directions at all: the referential system is assumed to be the default one (no need for more yet)
            return new Vector3((float) (AbsoluteXPosition + grid.GridXRepresentation),
                (float) (AbsoluteYPosition + grid.GridYRepresentation),
                transform.position.z);
        }

#if UNITY_EDITOR
        public void OnDrawGizmosSelected()
        {
            Vector3 cellPosition = transform.position;
            Gizmos.DrawWireCube(new Vector3(Grid.CellWidth * (0.5f), Grid.CellHeight * (0.5f), 0) + cellPosition
                , new Vector3(Grid.CellWidth, Grid.CellHeight, Grid.DebugZSize));
        }
#endif
    }
}
