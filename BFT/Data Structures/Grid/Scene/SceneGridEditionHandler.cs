﻿#if UNITY_EDITOR

using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Utilities;
using UnityEditor;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    public class SceneGridEditionHandler : MonoBehaviour
    {
        private static SceneGridEditionHandler instance;


        private static string sceneTag = "SceneGridHelper";

        private readonly GridDictionary<List<UnityEngine.Transform>> toPositionOnCellLoaded =
            new GridDictionary<List<UnityEngine.Transform>>();

        public bool CreateGridCellBasedOnPosition = false;

        [OnValueChanged("LoadAndPositionAllGrids")]
        public List<SceneGridPosition> EditionPosition = new List<SceneGridPosition>();

        [TabGroup("Loading"), ValueDropdown("CurrentScenes")]
        public SceneGrid GridToLoad;

        [TabGroup("Loading"), OnValueChanged("LoadAndPositionAllGrids"), MinValue(1), MaxValue(25)]
        public int LoadingRange = 3;

        [AssetsOnly] public SceneGridPosition PositionPrefab;

        public static SceneGridEditionHandler Instance
        {
            get
            {
                if (!instance)
                {
                    try
                    {
                        GameObject go = GameObject.FindGameObjectWithTag("SceneGridHelper");
                        if (go)
                            instance = go.GetComponent<SceneGridEditionHandler>();
                    }
                    catch (UnityException)
                    {
                        return null;
                    }

                    if (!instance)
                    {
                        if (FindObjectsOfType<SceneGrid>().Length == 0
                            && FindObjectsOfType<SceneGridPosition>().Length == 0
                            && FindObjectsOfType<SceneGridCell>().Length == 0)
                            return null;

                        GameObject go = new GameObject("SceneGridHelper");
                        instance = go.AddComponent<SceneGridEditionHandler>();
                    }
                }

                return instance;
            }
        }

        void Reset()
        {
            PositionPrefab = AssetDatabase.LoadAssetAtPath<GameObject>
                ("Assets/Content/Main Structure/Prefabs/EDITION POSITION.prefab").GetComponent<SceneGridPosition>();
        }

        public void InitGrid(SceneGrid grid)
        {
            if (GetEditionPositionForGrid(grid) != null)
                return;
            SceneGridPosition pos = Instantiate(PositionPrefab);
            Undo.SetTransformParent(pos.transform, transform, "ReParent new position");
            pos.name = (grid.GridName.IsNullOrWhitespace() ? grid.gameObject.name : grid.GridName) +
                       " EDITION POSITION";
            pos.Grid = grid;
            EditionPosition.Add(pos);
            NotifyGridCoordinateChanged(pos);
            NotifyGridRepresentationPositionChanged(pos.Grid);
        }

        public void DestroyGrid(SceneGrid grid)
        {
            SceneGridPosition pos = GetEditionPositionForGrid(grid);
            if (!pos)
                return;

            EditionPosition.Remove(pos);
            Destroy(pos.gameObject);
        }

        [TabGroup("Loading"), Button(ButtonSizes.Medium), GUIColor(.2f, .8f, .2f)]
        public void LoadGrid()
        {
            foreach (SceneGridPosition pos in EditionPosition)
            {
                if (pos.Grid == GridToLoad)
                {
                    NotifyGridCoordinateChanged(pos);
                    NotifyGridRepresentationPositionChanged(pos.Grid);
                }
            }
        }

        [TabGroup("Loading"), Button(ButtonSizes.Medium), GUIColor(.8f, .2f, .2f)]
        public void UnloadGrid()
        {
            GridToLoad.UnloadGrid();
        }

        [TabGroup("Saving"), Button(ButtonSizes.Medium)]
        public void CreateCellsAroundPosition()
        {
            foreach (var position in EditionPosition)
            {
                for (int i = -LoadingRange + position.CellI; i <= LoadingRange + position.CellI; i++)
                {
                    for (int j = -LoadingRange + position.CellJ; j < LoadingRange + position.CellJ; j++)
                    {
                        if (!position.Grid.IsCellExist(i, j))
                            position.Grid.CreateNewCell(i, j);
                    }
                }
            }
        }

        [TabGroup("Saving"), Button(ButtonSizes.Medium)]
        public void CreateCellOnPositions()
        {
            foreach (var position in EditionPosition)
            {
                position.Grid.CreateNewCell(position.CellI, position.CellJ);
            }
        }

        [TabGroup("Saving"), Button(ButtonSizes.Medium)]
        public void RepositionAllObjectsOnGrids()
        {
            foreach (SceneGridPosition pos in EditionPosition)
            {
                pos.Grid.RepositionAllCellsObject();
            }
        }

        [TabGroup("Saving"), Button(ButtonSizes.Medium), GUIColor(.8f, .2f, .2f)]
        public void DeleteAllEmptyScenes()
        {
            foreach (var pos in EditionPosition)
            {
                pos.Grid.DeleteEmptyLoadedCells();
            }
        }

        public SceneGrid[] CurrentScenes()
        {
            List<SceneGrid> grids = new List<SceneGrid>();
            foreach (SceneGridPosition sceneGridPosition in EditionPosition)
            {
                if (sceneGridPosition && sceneGridPosition.Grid)
                    grids.Add(sceneGridPosition.Grid);
            }

            return grids.ToArray();
        }

#if UNITY_EDITOR

        void Awake()
        {
            if (Application.isPlaying)
                Destroy(this.gameObject);

            if (!instance)
                instance = this;
            if (!CompareTag(sceneTag))
                tag = sceneTag;

            Selection.activeGameObject = this.gameObject;

            foreach (var grid in FindObjectsOfType<SceneGrid>())
            {
                grid.LoadedCells.Clear();
                grid.LoadedCellsByCoordinate.Clear();
            }

            LoadAndPositionAllGrids();
        }

#endif

        public SceneGridPosition GetEditionPositionForGrid(SceneGrid grid)
        {
            if (EditionPosition != null)
                foreach (SceneGridPosition sceneGridPosition in EditionPosition)
                {
                    if (sceneGridPosition.Grid == grid)
                        return sceneGridPosition;
                }

            return null;
        }

        [TabGroup("Position"), Button(ButtonSizes.Medium)]
        public void LoadAndPositionAllGrids()
        {
            if (EditionPosition != null)
                foreach (SceneGridPosition sceneGridPosition in EditionPosition)
                {
                    NotifyGridCoordinateChanged(sceneGridPosition);
                    NotifyGridRepresentationPositionChanged(sceneGridPosition.Grid);
                }
        }


        public void AddTransformToRepositionAfterLoading(int i, int j,
            List<UnityEngine.Transform> toRepositionTransforms)
        {
            if (!toPositionOnCellLoaded.ContainsKeyAndNotNull(i, j))
                toPositionOnCellLoaded.Add(i, j, new List<UnityEngine.Transform>());
            toPositionOnCellLoaded[i, j].AddRange(toRepositionTransforms);
        }

        public void NotifyCellLoaded(SceneGridCell cell)
        {
            cell.Grid.PlaceTransformsIntoLoadedCell(cell.I, cell.J,
                (toPositionOnCellLoaded.ContainsKeyAndNotNull(cell.I, cell.J)
                    ? toPositionOnCellLoaded[cell.I, cell.J].ToArray()
                    : null));
            toPositionOnCellLoaded.RemoveAt(cell.I, cell.J);

            cell.RepositionCellRepresentation();
        }

        public void NotifyGridCoordinateChanged(SceneGridPosition position)
        {
            if (EditionPosition.Contains(position))
            {
                if (!position.Grid)
                    return;

                //load all the cell that are in the loading range
                for (int i = -LoadingRange + position.CellI; i <= LoadingRange + position.CellI; i++)
                {
                    for (int j = -LoadingRange + position.CellJ; j < LoadingRange + position.CellJ; j++)
                    {
                        if (!position.Grid.IsCellExist(i, j))
                        {
                            if (CreateGridCellBasedOnPosition && !position.Grid.IsCellExist(i, j))
                            {
                                position.Grid.CreateNewCell(i, j);
                            }
                        }
                        else if (!position.Grid.IsCellLoaded(i, j))
                            position.Grid.LoadCell(i, j);
                    }
                }

                //unload all the cells that are out of range
                foreach (SceneGridCell gridLoadedCell in new List<SceneGridCell>(position.Grid.LoadedCells))
                {
                    if (!gridLoadedCell)
                        continue;

                    if (gridLoadedCell.I > LoadingRange + position.CellI ||
                        gridLoadedCell.I < -LoadingRange + position.CellI ||
                        gridLoadedCell.J > LoadingRange + position.CellJ ||
                        gridLoadedCell.J < -LoadingRange + position.CellJ)
                        position.Grid.UnloadCell(gridLoadedCell.I, gridLoadedCell.J);
                }

                position.transform.position = position.LocalCellPosition;
                position.Grid.SetRepresentationPosition(-position.CellXPosition, -position.CellYPosition);
            }
        }

        public void NotifyGridRepresentationPositionChanged(SceneGrid grid)
        {
            if (!grid)
                return;

            //move cells' representation based on the reference position
            foreach (SceneGridCell gridLoadedCell in grid.LoadedCells)
            {
                if (gridLoadedCell != null)
                    gridLoadedCell.RepositionCellRepresentation();
            }
        }

        [TabGroup("Position"), Button(ButtonSizes.Medium)]
        public void RepositionAllGrid()
        {
            foreach (var pos in EditionPosition)
            {
                RepositionCellsOfGrid(pos.Grid);
            }
        }

        public void RepositionCellsOfGrid(SceneGrid grid)
        {
            foreach (var cell in grid)
            {
                cell.RepositionCellRepresentation();
            }
        }

        public void OnDrawGizmos()
        {
            Gizmos.color = Color.blue.DarkerBrighter(0.1f);
            foreach (SceneGridPosition sceneGridPosition in EditionPosition)
            {
                Gizmos.color = Gizmos.color.DarkerBrighter(.1f);
                if (sceneGridPosition.Grid)
                    foreach (SceneGridCell gridLoadedCell in sceneGridPosition.Grid.LoadedCells)
                    {
                        if (gridLoadedCell != null)
                            gridLoadedCell.OnDrawGizmosSelected();
                    }
            }
        }
    }
#endif

#if UNITY_EDITOR
    public class SaveReposition : UnityEditor.AssetModificationProcessor
    {
        static string[] OnWillSaveAssets(string[] paths)
        {
            SceneGridEditionHandler handler = SceneGridEditionHandler.Instance;
            if (!handler)
                return paths;

            foreach (var position in handler.EditionPosition)
            {
                position.Grid.RepositionAllCellsObject();
                position.Grid.LoadedCells.Clear();
                position.Grid.LoadedCellsByCoordinate.Clear();
            }

            return paths;
        }
    }
}
#endif
