﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

using Sirenix.Utilities;

#if UNITY_EDITOR

using UnityEditor;
using Sirenix.OdinInspector.Editor;
using UnityEditor.SceneManagement;

#endif

using UnityEngine;
using UnityEngine.SceneManagement;

namespace BFT
{
    /// <summary>
    ///     separates each cell into scenes, and hold a reference to scenes to load them.
    /// </summary>
    [ExecuteInEditMode]
    [AddComponentMenu("Grid/Scene Grid/Scene Grid")]
    public class SceneGrid : Basic2DGrid, IEnumerable
    {
        public delegate void LoadingAction(SceneGridCell cell);

        public delegate void SceneAction(SceneGrid grid);

        public static Dictionary<string, SceneGrid> ScenesByName = new Dictionary<string, SceneGrid>();


        [TabGroup("Grid Structure"), SerializeField]
        private GridDictionary<bool> cellExists = new GridDictionary<bool>();

        [OnValueChanged("Awake")] public bool EnableEdition = false;

        [OnValueChanged("UpdateSceneGlobalReference")]
        public string GridName;

        [SerializeField, TitleGroup("Representation"), OnValueChanged("NotifyGridRepresentationChanged")]
        private double gridXRepresentation;

        [SerializeField, TitleGroup("Representation"), OnValueChanged("NotifyGridRepresentationChanged")]
        private double gridYRepresentation;

        [SerializeField, ReadOnly, TabGroup("Grid Structure")]
        private List<SceneGridCell> loadedCells = new List<SceneGridCell>();

        [ReadOnly, TabGroup("Grid Structure"), SerializeField]
        private GridDictionary<SceneGridCell> loadedCellsByCoordinate = new GridDictionary<SceneGridCell>();

        public double GridYRepresentation
        {
            get => gridYRepresentation;
            set
            {
                gridYRepresentation = value;
                NotifyGridRepresentationChanged();
            }
        }

        public double GridXRepresentation
        {
            get => gridXRepresentation;
            set
            {
                gridXRepresentation = value;
                NotifyGridRepresentationChanged();
            }
        }

        public double TopRightX => GridLength * CellWidth;

        public double TopRightY => GridHeight * CellHeight;


        public GridDictionary<SceneGridCell> LoadedCellsByCoordinate
        {
            get
            {
#if UNITY_EDITOR
                if (!Application.isPlaying && loadedCellsByCoordinate.Count == 0)
                    return GetEditorCoordinateCells();
#endif

                return loadedCellsByCoordinate;
            }
        }

        public List<SceneGridCell> LoadedCells
        {
            get
            {
#if UNITY_EDITOR
                if (!Application.isPlaying && loadedCells.Count == 0)
                    return GetEditorCells();
#endif
                return loadedCells;
            }
        }

        public SceneGridCell this[int i, int j] =>
            LoadedCellsByCoordinate[Mathf.Clamp(i, 0, GridLength - 1), Mathf.Clamp(j, 0, GridHeight - 1)];

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public static event SceneAction OnSceneAdded;

        public string GetSceneName(int i, int j)
        {
            return GridName + " (" + i + "," + j + ")";
        }

        public event SceneAction OnRepresentationPositionChanged;

        private void NotifyGridRepresentationChanged()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
                SceneGridEditionHandler.Instance.NotifyGridRepresentationPositionChanged(this);
#endif
            if (OnRepresentationPositionChanged != null) OnRepresentationPositionChanged(this);
        }

        public void MoveRepresentationPosition(double xMovement, double yMovement)
        {
            gridXRepresentation += xMovement;
            gridYRepresentation += yMovement;
            NotifyGridRepresentationChanged();
        }

        public void SetRepresentationPosition(double xMovement, double yMovement)
        {
            gridXRepresentation = xMovement;
            gridYRepresentation = yMovement;
            NotifyGridRepresentationChanged();
        }

        public Vector3 GetCellRepresentationPosition(int i, int j)
        {
            return new Vector3((float) (gridXRepresentation + i * CellWidth),
                (float) (gridYRepresentation + j * CellHeight), transform.position.z);
        }

        public bool IsCellExist(int i, int j)
        {
            return cellExists[i, j];
        }

        public bool IsCellLoaded(int i, int j)
        {
            return LoadedCellsByCoordinate.ContainsKeyAndNotNull(i, j);
        }

#if UNITY_EDITOR
        public void Reset()
        {
            if (!Application.isPlaying)
                SceneGridEditionHandler.Instance.InitGrid(this);
        }
#endif

        public void Awake()
        {
#if UNITY_EDITOR

            UpdateSceneGlobalReference();

            if (!Application.isPlaying && EnableEdition)
                SceneGridEditionHandler.Instance.InitGrid(this);
#endif
        }

        public void NotifyCellAdded(SceneGridCell cell, bool silently = false)
        {
            if (!cell)
                return;

            if (!LoadedCellsByCoordinate.ContainsKey(cell.I, cell.J))
                LoadedCellsByCoordinate.Add(cell.I, cell.J, cell);

            LoadedCellsByCoordinate[cell.I, cell.J] = cell;
            LoadedCells.Add(cell);
            if (OnCellLoaded != null && !silently) OnCellLoaded(cell);

#if UNITY_EDITOR
            if (!Application.isPlaying && !silently)
            {
                SceneGridEditionHandler.Instance.NotifyCellLoaded(cell);
            }
#endif
        }

        protected override void CellSizeChangedCallBack()
        {
#if UNITY_EDITOR
            SceneGridEditionHandler.Instance.RepositionCellsOfGrid(this);
#endif
        }

        public void NotifyCellRemoved(SceneGridCell cell)
        {
            LoadedCellsByCoordinate.RemoveAt(cell.I, cell.J);
            LoadedCells.Remove(cell);

            if (OnCellDestroyed != null) OnCellDestroyed(cell);
        }

        public event LoadingAction OnCellLoaded;
        public event LoadingAction OnCellDestroyed;

        public void LoadCell(int i, int j)
        {
            if (!cellExists[i, j])
                return;

#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                try
                {
                    EditorSceneManager.OpenScene(SceneRootPath + GetSceneName(i, j) + ".unity", OpenSceneMode.Additive);
                }
                catch (ArgumentException)
                {
                    cellExists[i, j] = false;
                    UnityEngine.Debug.LogWarning("The scene holding Cell (" + i + "," + j + ") " +
                                     "was Deleted or cannot be found: if it was not deleted," +
                                     " you will have to add it back manually to the grid and mark it as existing");
                }

                return;
            }
#endif

            SceneManager.LoadSceneAsync(GetSceneName(i, j), LoadSceneMode.Additive);
        }

        public void UnloadCell(SceneGridCell cell)
        {
            UnloadCell(cell.I, cell.J);
        }

        public void UnloadCell(int i, int j)
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                EditorSceneManager.CloseScene(SceneManager.GetSceneByName(GetSceneName(i, j)), true);
                return;
            }
#endif
            if (LoadedCellsByCoordinate[i, j])
                SceneManager.UnloadSceneAsync(GetSceneName(i, j));
        }

        public SceneGridCell GetActualCellPosition(SceneGridPosition position)
        {
            int gridX = (int) (position.AbsoluteXPosition / CellWidth);
            int gridY = (int) (position.AbsoluteYPosition / CellHeight);
            gridX = Mathf.Clamp(gridX, 0, GridLength - 1);
            gridY = Mathf.Clamp(gridY, 0, GridHeight - 1);
            return LoadedCellsByCoordinate[gridX, gridY];
        }

        public double CellXPosition(SceneGridPosition position)
        {
            return position.CellI * CellWidth;
        }

        public double CellYPosition(SceneGridPosition position)
        {
            return position.CellJ * CellHeight;
        }


        public IEnumerator<SceneGridCell> GetEnumerator()
        {
            foreach (var cell in LoadedCellsByCoordinate)
            {
                yield return cell;
            }
        }

        public SceneGridCell GetCellByGridPosition(double xPosition, double yPosition, bool loadCellIfNotLoaded = true)
        {
            int gridX;
            int gridY;

            GetGridCellIndexesByGridPosition(xPosition, yPosition, out gridX, out gridY);

            if (this[gridX, gridY] == null && loadCellIfNotLoaded)
            {
                LoadCell(gridX, gridY);
                return null;
            }

            return this[gridX, gridY];
        }

        public void GetGridCellIndexesByGridPosition(double xPosition, double yPosition, out int gridX, out int gridY)
        {
            gridX = (int) (xPosition / CellWidth);
            gridY = (int) (yPosition / CellHeight);
            gridX = Mathf.Clamp(gridX, 0, GridLength - 1);
            gridY = Mathf.Clamp(gridY, 0, GridHeight - 1);
        }

        public void GetGridCellCoordinatesByGridPosition(double xPosition, double yPosition, out double gridX,
            out double gridY)
        {
            int i;
            int j;
            GetGridCellIndexesByGridPosition(xPosition, yPosition, out i, out j);

            gridX = i * CellWidth;
            gridY = j * CellHeight;
        }

#if UNITY_EDITOR

        private List<SceneGridCell> GetEditorCells()
        {
            SceneGridCell[] cells = FindObjectsOfType<SceneGridCell>();
            List<SceneGridCell> gridCells = new List<SceneGridCell>();

            foreach (var cell in cells)
            {
                if (cell.GridNameReference == GridName)
                    gridCells.Add(cell);
            }

            return gridCells;
        }

        private GridDictionary<SceneGridCell> GetEditorCoordinateCells()
        {
            SceneGridCell[] cells = FindObjectsOfType<SceneGridCell>();
            GridDictionary<SceneGridCell> gridCells = new GridDictionary<SceneGridCell>();

            foreach (var cell in cells)
            {
                if (cell.GridNameReference == GridName)
                    gridCells.Add(cell.I, cell.J, cell);
            }

            return gridCells;
        }


#endif

#if UNITY_EDITOR
        [OnValueChanged("CheckPathName")] public string SceneRootPath;

        private void CheckPathName()
        {
            if (!SceneRootPath.EndsWith("/"))
                SceneRootPath += "/";
        }


        private void UpdateSceneGlobalReference()
        {
            if (!string.IsNullOrEmpty(GridName) && (!ScenesByName.ContainsKey(GridName) || ScenesByName[GridName] == null))
            {
                if (ScenesByName.ContainsKey(GridName))
                    ScenesByName[GridName] = this;
                else
                    ScenesByName.Add(GridName, this);

                if (OnSceneAdded != null) OnSceneAdded(this);
            }
        }

#endif


#if UNITY_EDITOR
        public void CreateNewCell(int i, int j, bool keepLoaded = true, params GameObject[] gos)
        {
            if (cellExists[i, j] || i >= GridLength || j >= GridHeight || i < 0 || j < 0 || GridName.IsNullOrWhitespace())
                return;

            UnityEngine.SceneManagement.Scene scene = EditorSceneManager.NewScene(NewSceneSetup.EmptyScene, NewSceneMode.Additive);
            EditorSceneManager.SaveScene(scene, SceneRootPath + GetSceneName(i, j) + ".unity");
            GameObject obj = new GameObject("Cell (" + i + "," + j + ")");
            SceneGridCell cell = obj.AddComponent<SceneGridCell>();
            cellExists[i, j] = true;
            cell.SetCoordinate(i, j);
            cell.RepositionCellRepresentation();
            cell.Grid = this;

            SceneManager.MoveGameObjectToScene(obj, scene);

            foreach (GameObject go in gos)
            {
                Undo.SetTransformParent(go.transform, null, "UnParenting the object");
                SceneManager.MoveGameObjectToScene(go, scene);
                Undo.SetTransformParent(go.transform, obj.transform, "Parenting the object to the new cell");
            }

            EditorSceneManager.SaveScene(scene, SceneRootPath + GetSceneName(i, j) + ".unity");
            AddScenesToBuild(SceneRootPath + GetSceneName(i, j) + ".unity");

            if (!keepLoaded)
            {
                EditorSceneManager.CloseScene(scene, true);
            }
        }

        void AddScenesToBuild(string scenePath)
        {
            List<EditorBuildSettingsScene> scenesList = new List<EditorBuildSettingsScene>();
            scenesList.AddRange(EditorBuildSettings.scenes);

            List<string> scenesToAdd = new List<string>();
            scenesToAdd.Add(scenePath);

            foreach (var item in scenesList)
            {
                if (scenesToAdd.Contains(item.path))
                {
                    scenesToAdd.Remove(item.path);
                    item.enabled = true;
                }
            }

            foreach (var item in scenesToAdd)
            {
                scenesList.Add(new EditorBuildSettingsScene(item, true));
            }

            EditorBuildSettings.scenes = scenesList.ToArray();
        }


        [Button(ButtonSizes.Medium), TabGroup("Utils")]
        public void UnloadGrid()
        {
            foreach (var cell in new List<SceneGridCell>(LoadedCells))
            {
                UnloadCell(cell);
            }
        }

        [Button(ButtonSizes.Medium), TabGroup("Utils")]
        public void DeleteEmptyLoadedCells()
        {
            foreach (var cell in LoadedCells)
            {
                if (cell.transform.childCount == 0)
                {
                    cellExists.RemoveAt(cell.I, cell.J);

                    OdinPrefabUtility.UpdatePrefabInstancePropertyModifications(this, false);

                    EditorSceneManager.CloseScene(SceneManager.GetSceneByName(GetSceneName(cell.I, cell.J)), true);
                    AssetDatabase.DeleteAsset(SceneRootPath + GetSceneName(cell.I, cell.J) + ".unity");
                }
            }
        }

        [Title("Level Design")]
        [Button(ButtonSizes.Medium), TabGroup("Utils")]
        public void RepositionAllChildren()
        {
            RepositionGameObjects(transform.DirectChildren());
        }

        [Title("Level Design")]
        [Button(ButtonSizes.Medium), TabGroup("Utils")]
        public void RepositionAllCellsObject()
        {
            List<UnityEngine.Transform> toReposition = new List<UnityEngine.Transform>();

            toReposition.AddRange(transform.DirectChildren());
            foreach (var cell in LoadedCells)
            {
                toReposition.AddRange(cell.transform.DirectChildren());
            }

            RepositionGameObjects(toReposition);
        }


        public void RepositionGameObjects(IEnumerable<UnityEngine.Transform> trans)
        {
            SceneGridPosition editionPosition = SceneGridEditionHandler.Instance.GetEditionPositionForGrid(this);

            if (!editionPosition)
            {
                UnityEngine.Debug.LogWarning(
                    "You need a SceneGridPosition set in the SceneEditionHandler to be able to edit the grid locally");
                return;
            }

            GridDictionary<List<UnityEngine.Transform>> outOfLoadedRange = new GridDictionary<List<UnityEngine.Transform>>();
            GridDictionary<List<GameObject>> cellsToCreateWithObjectsToPosition = new GridDictionary<List<GameObject>>();

            foreach (UnityEngine.Transform go in trans)
            {
                double gridX = -GridXRepresentation + go.transform.position.x;
                double gridY = -GridYRepresentation + go.transform.position.y;

                int cellI;
                int cellJ;

                GetGridCellIndexesByGridPosition(gridX, gridY, out cellI, out cellJ);

                if (cellExists[cellI, cellJ])
                {
                    if (IsCellLoaded(cellI, cellJ))
                    {
                        //then transfer the object to the good cell
                        PlaceTransformsIntoLoadedCell(cellI, cellJ, go);
                    }
                    else
                    {
                        //then load the cell
                        LoadCell(cellI, cellJ);
                        if (!outOfLoadedRange.ContainsKeyAndNotNull(cellI, cellJ))
                            outOfLoadedRange.Add(cellI, cellJ, new List<UnityEngine.Transform>());
                        outOfLoadedRange[cellI, cellJ].Add(go);
                    }
                }
                else
                {
                    //then the cell does not exist, create it if still within the boundaries of the grid
                    if (!cellsToCreateWithObjectsToPosition.ContainsKeyAndNotNull(cellI, cellJ))
                        cellsToCreateWithObjectsToPosition.Add(cellI, cellJ, new List<GameObject>());
                    cellsToCreateWithObjectsToPosition[cellI, cellJ].Add(go.gameObject);
                }
            }

            //create the missing cells
            foreach (KeyValuePair<int, Dictionary<int, List<GameObject>>> rows in cellsToCreateWithObjectsToPosition
                .GridStructure)
            {
                foreach (KeyValuePair<int, List<GameObject>> objs in rows.Value)
                {
                    CreateNewCell(rows.Key, objs.Key, true, objs.Value.ToArray());
                }
            }

            foreach (KeyValuePair<int, Dictionary<int, List<UnityEngine.Transform>>> rows in outOfLoadedRange.GridStructure)
            {
                foreach (KeyValuePair<int, List<UnityEngine.Transform>> objs in rows.Value)
                {
                    SceneGridEditionHandler.Instance.AddTransformToRepositionAfterLoading(rows.Key, objs.Key, objs.Value);
                }
            }
        }

        public void PlaceTransformsIntoLoadedCell(int i, int j, params UnityEngine.Transform[] trans)
        {
            if (!IsCellLoaded(i, j))
                return;

            SceneGridCell cell = LoadedCellsByCoordinate[i, j];

            if (trans != null)
                foreach (UnityEngine.Transform go in trans)
                {
                    Undo.SetTransformParent(go, null, "UnParenting");
                    Undo.MoveGameObjectToScene(go.gameObject,
                        SceneManager.GetSceneByName(GetSceneName(cell.I, cell.J)), "Moving Object");
                    Undo.SetTransformParent(go, cell.transform, "UnParenting");
                    Undo.RecordObject(go, "Moving object to correct Scene");
                }
        }

        public void PlaceObjectIntoNewCell(GameObject go, int i, int j)
        {
            if (cellExists.ContainsKeyAndNotNull(i, j) || i >= GridLength || j >= GridHeight)
                return;

            CreateNewCell(i, j, go);
        }

        void Update()
        {
            UpdateSceneGlobalReference();
        }
#endif
    }
}
