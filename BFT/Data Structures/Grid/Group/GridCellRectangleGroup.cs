﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    [AddComponentMenu("Grid/Scene Grid/Group/Cell Group")]
    public class GridCellRectangleGroup : MonoBehaviour, IEnumerable<SceneGridCell>
    {
        public delegate void PositionAction(SceneGridCell cell);


        public bool debug;

        public bool editorConstantlyReloadRange = true;

        public SceneGrid grid;

        public bool loadCells;

        [ReadOnly] public List<SceneGridCell> LoadedCells = new List<SceneGridCell>();

        public GridDictionary<SceneGridCell> LoadedCellsOnGrid = new GridDictionary<SceneGridCell>();

        [SerializeField] private int maxX;

        [SerializeField] private int maxY;

        [SerializeField] private int minX;

        [SerializeField] private int minY;

        public int MinX
        {
            get => minX;
            protected set => minX = value;
        }

        public int MaxX
        {
            get => maxX;
            protected set => maxX = value;
        }

        public int MinY
        {
            get => minY;
            protected set => minY = value;
        }

        public int MaxY
        {
            get => maxY;
            protected set => maxY = value;
        }


        public IEnumerator<SceneGridCell> GetEnumerator()
        {
            foreach (var cell in LoadedCells)
            {
                yield return cell;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public event PositionAction OnCellRemoved;
        public event PositionAction OnCellAdded;


        private void Start()
        {
            //add all objects within current set boundaries
            LoadCurrentRange();
        }

        [Button]
        private void LoadCurrentRange()
        {
            if (!grid)
                return;
            LoadedCells.Clear();
            LoadedCellsOnGrid.Clear();
            for (int i = MinX; i <= MaxX; i++)
            {
                for (int j = MinY; j <= MaxY; j++)
                {
                    SceneGridCell cell = grid[i, j];
                    LoadedCells.Add(cell);
                    LoadedCellsOnGrid[i, j] = cell;
                    AddedToGroupAction(cell);
                    if (OnCellAdded != null) OnCellAdded(cell);
                }
            }
        }

        public void SetNewBoundaries(int newMinX, int newMinY, int newMaxX, int newMaxY)
        {
            //first removals
            for (int i = MinX; i <= MaxX; i++)
            {
                for (int j = MinY; j <= MaxY; j++)
                {
                    if (i < newMinX || j < newMinY || i > newMaxX || j > newMaxY)
                    {
                        SceneGridCell cell = LoadedCellsOnGrid[i, j];
                        if (cell != null)
                        {
                            LoadedCells.Remove(cell);
                            RemovedFromGroupAction(cell);
                            if (OnCellRemoved != null) OnCellRemoved(cell);
                        }

                        LoadedCellsOnGrid.RemoveAt(i, j);
                    }
                }
            }

            for (int i = newMinX; i <= newMaxX; i++)
            {
                for (int j = newMinY; j <= newMaxY; j++)
                {
                    if (i < MinX || j < MinY || i > MaxX || j > MaxY)
                    {
                        if (grid.IsCellLoaded(i, j))
                        {
                            SceneGridCell cell = grid[i, j];
                            LoadedCells.Add(cell);
                            LoadedCellsOnGrid[i, j] = cell;
                            AddedToGroupAction(cell);
                            if (OnCellAdded != null) OnCellAdded(cell);
                        }
                        else if (loadCells)
                        {
                            grid.LoadCell(i, j);
                            grid.OnCellLoaded += CellAddedNotification;
                        }
                    }
                }
            }

            MinX = newMinX;
            MaxX = newMaxX;
            MinY = newMinY;
            MaxY = newMaxY;
        }

        private void CellAddedNotification(SceneGridCell cell)
        {
            if (cell.I >= MinX && cell.J >= MinY && cell.I <= MaxX && cell.J <= MaxY && !LoadedCells.Contains(cell))
            {
                LoadedCells.Add(cell);
                LoadedCellsOnGrid[cell.I, cell.J] = cell;
                AddedToGroupAction(cell);
                if (OnCellAdded != null) OnCellAdded(cell);
            }
        }

        public virtual void RemovedFromGroupAction(SceneGridCell cell)
        {
            //do nothing by default
        }

        public virtual void AddedToGroupAction(SceneGridCell cell)
        {
            //do nothing by default
        }

#if UNITY_EDITOR

        public void OnDrawGizmosSelected()
        {
            if (!debug)
                return;

            for (int i = MinX; i <= MaxX; i++)
            {
                for (int j = MinY; j <= MaxY; j++)
                {
                    grid.DebugGridCell(i, j, false);
                }
            }
        }

        void Update()
        {
            if (!Application.isPlaying && (LoadedCells.IsEmpty() || editorConstantlyReloadRange))
            {
                LoadCurrentRange();
            }
        }
#endif
    }
}
