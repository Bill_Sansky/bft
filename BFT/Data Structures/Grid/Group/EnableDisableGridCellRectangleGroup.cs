﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Grid/Scene Grid/Group/Enable Disable Cell Group")]
    public class EnableDisableGridCellRectangleGroup : GridCellRectangleGroup
    {
        public bool disableOnLeave;

        [Button]
        public void ApplyRemovedActionOnAllButGroup()
        {
            foreach (var cell in grid)
            {
                if (!LoadedCells.Contains(cell))
                    RemovedFromGroupAction(cell);
            }
        }

        [Button]
        public void ApplyAddedToGroupActionToAllGroup()
        {
            foreach (var cell in grid)
            {
                if (LoadedCells.Contains(cell))
                    AddedToGroupAction(cell);
            }
        }

        public override void RemovedFromGroupAction(SceneGridCell cell)
        {
            if (cell)
                cell.gameObject.SetActive(!disableOnLeave);
        }

        public override void AddedToGroupAction(SceneGridCell cell)
        {
            if (cell)
                cell.gameObject.SetActive(disableOnLeave);
        }
    }
}
