﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [Serializable, ExecuteInEditMode]
    [AddComponentMenu("Grid/Scene Grid/Position/Constrain Box Position Updater")]
    public class ConstrainBoxGridPositionUpdater : MonoBehaviour
    {
        [SerializeField, ReadOnly] private double bottomLeftGridX;

        [SerializeField, ReadOnly] private double bottomLeftGridY;

        public Box2DTeleportConstrainer Box2DTeleportConstrainer;

        [SerializeField] private SceneGridPosition position;

        public bool TeleportOnInit;

        [SerializeField, ReadOnly] private double topRightGridX;

        [SerializeField, ReadOnly] private double topRightGridY;

        public SceneGridPosition Position
        {
            get => position;
            set => position = value;
        }

        public double BottomLeftGridX => bottomLeftGridX;

        public double BottomLeftGridY => bottomLeftGridY;

        public double TopRightX => topRightGridX;

        public double TopRightY => topRightGridY;


        protected void Awake()
        {
            if (!Box2DTeleportConstrainer)
                return;
            Box2DTeleportConstrainer.OnTeleportOccured += TransitionGrid;

            ResetBoxCoordinates();
            position.OnGridIndexesChanged += ResetBoxCoordinates;
            position.Grid.SetRepresentationPosition(-bottomLeftGridX + position.CellXPosition,
                -bottomLeftGridY + position.CellYPosition);

#if UNITY_EDITOR

            if (!Application.isPlaying)
                TeleportToPosition();
#endif
        }


        [Button]
        private void ResetBoxCoordinates()
        {
            Vector3 distance = position.transform.position - Box2DTeleportConstrainer.transform.position;
            bottomLeftGridX = position.AbsoluteXPosition + Box2DTeleportConstrainer.LeftDistance - distance.x;
            bottomLeftGridY = position.AbsoluteYPosition + Box2DTeleportConstrainer.BottomDistance - distance.y;
            topRightGridX = position.AbsoluteXPosition + Box2DTeleportConstrainer.RightDistance - distance.x;
            topRightGridY = position.AbsoluteYPosition + Box2DTeleportConstrainer.TopDistance - distance.y;
        }

        void OnEnable()
        {
            if (TeleportOnInit)
                TeleportToPosition();
        }

        public void TeleportToPosition()
        {
            Box2DTeleportConstrainer.Teleporter.SetTeleportPosition(Box2DTeleportConstrainer.transform.position);
            Box2DTeleportConstrainer.Teleporter.Teleport();
            ResetBoxCoordinates();
            TransitionGrid(BoxConstrainEnum.NONE);
        }

        [SerializeField]
        public void TransitionGrid(BoxConstrainEnum overlap)
        {
            Vector2 movement = new Vector2();
            switch (overlap)
            {
                case BoxConstrainEnum.NONE:
                    break;
                case BoxConstrainEnum.TOP:
                    movement = new Vector2(0, 1);
                    break;
                case BoxConstrainEnum.BOTTOM:
                    movement = new Vector2(0, -1);
                    break;
                case BoxConstrainEnum.RIGHT:
                    movement = new Vector2(1, 0);
                    break;
                case BoxConstrainEnum.LEFT:
                    movement = new Vector2(-1, 0);
                    break;
                case BoxConstrainEnum.TOP_RIGHT:
                    movement = new Vector2(1, 1);
                    break;
                case BoxConstrainEnum.TOP_LEFT:
                    movement = new Vector2(-1, 1);
                    break;
                case BoxConstrainEnum.BOTTOM_RIGHT:
                    movement = new Vector2(1, -1);
                    break;
                case BoxConstrainEnum.BOTTOM_LEFT:
                    movement = new Vector2(-1, -1);
                    break;
                default:
                    throw new ArgumentOutOfRangeException("overlap", overlap, null);
            }

            movement.Scale(new Vector2(Box2DTeleportConstrainer.Length, Box2DTeleportConstrainer.Height));

            bottomLeftGridX += movement.x;
            bottomLeftGridY += movement.y;
            topRightGridX += movement.x;
            topRightGridY += movement.y;

            RepositionGridPosition();

            position.Grid.SetRepresentationPosition(-bottomLeftGridX + Box2DTeleportConstrainer.LeftDistance
                , -bottomLeftGridY + Box2DTeleportConstrainer.BottomDistance);
        }

        void LateUpdate()
        {
#if UNITY_EDITOR

            if (!Box2DTeleportConstrainer || !Position)
                return;

            if (!Application.isPlaying)
            {
                Box2DTeleportConstrainer.OnTeleportOccured -= TransitionGrid;
                Box2DTeleportConstrainer.OnTeleportOccured += TransitionGrid;
                position.OnGridIndexesChanged -= ResetBoxCoordinates;
                position.OnGridIndexesChanged += ResetBoxCoordinates;
            }

#endif

            RepositionGridPosition();
        }

        private void RepositionGridPosition()
        {
            Vector3 distance = position.transform.position - Box2DTeleportConstrainer.BottomLeft;

            position.SetCoordinate(bottomLeftGridX - position.CellXPosition + distance.x,
                bottomLeftGridY + distance.y - position.CellYPosition);
        }

#if UNITY_EDITOR

        [TabGroup("Teleport")] public int gridCellITeleport;

        [TabGroup("Teleport")] public int gridCellJTeleport;

        [TabGroup("Teleport")] public double localXTeleport;

        [TabGroup("Teleport")] public double localYTeleport;

        [TabGroup("Teleport"), Button]
        public void TeleportToNewPosition()
        {
            position.SetCoordinate(gridCellITeleport, gridCellJTeleport, localXTeleport, localYTeleport);
            TeleportToPosition();
        }

#endif
    }
}
