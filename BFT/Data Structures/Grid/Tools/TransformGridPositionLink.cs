﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    [AddComponentMenu("Grid/Scene Grid/Position/Transform Grid Position Link")]
    public class TransformGridPositionLink : MonoBehaviour
    {
        public bool LocalOnly = true;

        public SceneGridPosition ReferencePosition;

        public bool UpdatePositionRealTime = true;

        [Button]
        public void CopyPositionIntoGridPosition()
        {
            transform.position = ReferencePosition.AbsolutePosition;
        }

        [Button]
        public void CopyPositionIntoLocalPosition()
        {
            transform.localPosition = ReferencePosition.LocalCellPosition;
        }


        void Update()
        {
            if (UpdatePositionRealTime && transform.hasChanged && ReferencePosition)
            {
                if (LocalOnly)
                {
                    ReferencePosition.SetCoordinate(transform.position.x, transform.position.y);
                }
                else
                {
                    int cellI;
                    int cellJ;
                    double localX;
                    double localY;

                    ReferencePosition.Grid.GetGridCellIndexesByGridPosition(transform.position.x,
                        transform.position.y, out cellI, out cellJ);

                    ReferencePosition.Grid.GetGridCellCoordinatesByGridPosition(transform.position.x
                        , transform.position.y, out localX, out localY);

                    localX = transform.position.x - localX + ReferencePosition.Grid.GridXRepresentation;
                    localY = transform.position.y - localY + ReferencePosition.Grid.GridYRepresentation;

                    ReferencePosition.SetCoordinate(cellI, cellJ, localX, localY);
                }
            }
        }
    }
}
