﻿using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    [AddComponentMenu("Grid/Scene Grid/Tools/Grid Parallax Tool")]
    public class GridParallaxTool : UpdateTypeSwitchable
    {
        public Vector3 BottomLeft;

        public bool DebugConstrains = true;


        [Range(0, 1)] public float DebugHeightPercent;

        public bool DebugNames = true;

        [Range(0, 1)] public float DebugWidthPercent;

        public SceneGridPosition ReferencePosition;
        public Vector3 TopRight;

        public bool UseDebugPercents = true;

        public float Length => TopRight.x - BottomLeft.x;
        public float Height => TopRight.y - BottomLeft.y;

        private float WidthPercent => (float) (ReferencePosition.AbsoluteXPosition / ReferencePosition.Grid.TopRightX);

        private float HeightPercent => (float) (ReferencePosition.AbsoluteYPosition / ReferencePosition.Grid.TopRightY);

        public override void UpdateMethod()
        {
            if (ReferencePosition)
                transform.position = new Vector3(Mathf.Lerp(BottomLeft.x, TopRight.x, WidthPercent),
                    Mathf.Lerp(BottomLeft.y, TopRight.y, HeightPercent), transform.position.z);
#if UNITY_EDITOR

            if (!Application.isPlaying && UseDebugPercents)
            {
                transform.position = new Vector3(Mathf.Lerp(BottomLeft.x, TopRight.x, DebugWidthPercent),
                    Mathf.Lerp(BottomLeft.y, TopRight.y, DebugHeightPercent), transform.position.z);
            }

#endif
        }

#if UNITY_EDITOR

        public void OnDrawGizmos()
        {
            if (!DebugConstrains)
                return;

            //top line
            Gizmos.color = Color.green.DarkerBrighter(0.1f);
            Gizmos.DrawLine(new Vector3(BottomLeft.x, TopRight.y, transform.position.z),
                new Vector3(TopRight.x, TopRight.y, transform.position.z));
            if (DebugNames)
                HandleExt.Text(new Vector3(Length / 2, TopRight.y, transform.position.z), "Top Distance");

            //bottom line
            Gizmos.color = Color.green;
            Gizmos.DrawLine(new Vector3(BottomLeft.x, BottomLeft.y, transform.position.z),
                new Vector3(TopRight.x, BottomLeft.y, transform.position.z));
            if (DebugNames)
                HandleExt.Text(new Vector3(Length / 2, BottomLeft.y, transform.position.z), "Bottom Distance");

            //right Line
            Gizmos.color = Color.red.DarkerBrighter(0.1f);
            Gizmos.DrawLine(new Vector3(TopRight.x, BottomLeft.y, transform.position.z),
                new Vector3(TopRight.x, TopRight.y, transform.position.z));
            if (DebugNames)
                HandleExt.Text(new Vector3(TopRight.x, Height / 2, transform.position.z), "Right Distance");

            //left Line
            Gizmos.color = Color.red;
            Gizmos.DrawLine(new Vector3(BottomLeft.x, BottomLeft.y, transform.position.z),
                new Vector3(BottomLeft.x, TopRight.y, transform.position.z));
            if (DebugNames)
                HandleExt.Text(new Vector3(BottomLeft.x, Height / 2, transform.position.z), "Left Distance");
        }

#endif
    }
}
