﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Grid/Scene Grid/Position/Scene Grid Position")]
    public class SceneGridPosition : MonoBehaviour
    {
        public delegate void GridCoordinateAction();

        [SerializeField] private int cellI;

        [SerializeField] private int cellJ;

        public Color DebugColor;

        public bool DebugPosition;

        [SerializeField, OnValueChanged("InvokeGridIndexesChanged")]
        private SceneGrid grid;

        [SerializeField] private double localXPosition;

        [SerializeField] private double localYPosition;

        public double LocalXPosition
        {
            get => localXPosition;
            set
            {
                localXPosition = value;
                if (grid && localXPosition > grid.CellWidth)
                    UpdateCurrentCell();
            }
        }

        public double LocalYPosition
        {
            get => localYPosition;
            set
            {
                localYPosition = value;
                if (grid && localXPosition > grid.CellWidth)
                    UpdateCurrentCell();
            }
        }

        public double CellXPosition => cellI * grid.CellWidth;

        public double CellYPosition => cellJ * grid.CellHeight;

        public double AbsoluteXPosition => CellI * Grid.CellWidth + LocalXPosition;

        public double AbsoluteYPosition => CellJ * Grid.CellHeight + LocalYPosition;

        public virtual Vector2 LocalCellPosition => new Vector2((float) LocalXPosition, (float) LocalYPosition);

        public Vector2 AbsolutePosition => new Vector2((float) AbsoluteXPosition, (float) AbsoluteYPosition);

        public int CellI
        {
            get => cellI;
            set
            {
                if (CellI != value)
                {
                    cellI = value;
                    UpdateCurrentCell();
                }
            }
        }

        public int CellJ
        {
            get => cellJ;
            set
            {
                if (cellJ != value)
                {
                    cellJ = value;
                    UpdateCurrentCell();
                }
            }
        }

        public SceneGrid Grid
        {
            get => grid;
            set => grid = value;
        }

        [Button("Update World Position")]
        public void UpdateWorldPosition()
        {
            UpdateCurrentCell();
        }

        void Awake()
        {
            grid.OnRepresentationPositionChanged += InvokeTransformRepresentationChange;
        }

        public void SetCoordinate(int newCellI, int newCellJ, double localX, double localY)
        {
            localXPosition = localX;
            localYPosition = localY;

            if (newCellI != cellI || newCellJ != cellJ ||
                (grid && (localXPosition > grid.CellWidth || localYPosition > grid.CellHeight || localXPosition < 0 ||
                          localYPosition < 0)))
            {
                cellI = newCellI;
                cellJ = newCellJ;

                UpdateCurrentCell();
            }
        }

        public void SetCoordinate(double localX, double localY)
        {
            localXPosition = localX;
            localYPosition = localY;

            if (grid && (localXPosition > grid.CellWidth || localYPosition > grid.CellHeight || localXPosition < 0 ||
                         localYPosition < 0))
                UpdateCurrentCell();
        }

        public void SetCoordinate(int newCellI, int newCellJ)
        {
            if (newCellI != cellI || newCellJ != cellJ)
            {
                cellI = newCellI;
                cellJ = newCellJ;
                UpdateCurrentCell();
            }
        }

        private void UpdateCurrentCell()
        {
            int gridI;
            int gridJ;

            grid.GetGridCellIndexesByGridPosition(AbsoluteXPosition, AbsoluteYPosition, out gridI, out gridJ);


            localXPosition = AbsoluteXPosition - gridI * grid.CellWidth;
            localYPosition = AbsoluteYPosition - gridJ * grid.CellHeight;

            cellI = gridI;
            cellJ = gridJ;

            InvokeGridIndexesChanged();

#if UNITY_EDITOR
            if (!Application.isPlaying && grid.EnableEdition)
                SceneGridEditionHandler.Instance.NotifyGridCoordinateChanged(this);
#endif
        }

        public event GridCoordinateAction OnGridIndexesChanged;
        public event GridCoordinateAction OnOutOfGrid;
        public event GridCoordinateAction OnGridTransformRepresentationChange;

        protected virtual void InvokeGridIndexesChanged()
        {
            if (OnGridIndexesChanged != null) OnGridIndexesChanged();
        }

        public virtual void InvokeTransformRepresentationChange(SceneGrid grid)
        {
            if (OnGridTransformRepresentationChange != null) OnGridTransformRepresentationChange();
        }

        protected void InvokeOutOfGrid()
        {
            if (OnOutOfGrid != null) OnOutOfGrid();
        }

#if UNITY_EDITOR

        public void OnDrawGizmos()
        {
            if (!DebugPosition)
                return;
            Gizmos.color = DebugColor;
            if (Grid)
                HandleExt.Text(transform.position, Grid.GridName + " Position: (" + cellI + "," + cellJ + ")", true);
        }

#endif
    }
}
