﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [ExecuteInEditMode]
    [AddComponentMenu("Grid/Scene Grid/Action/Cell Reposition")]
    public class LocalGridRepresentationPositionAction : GridRepresentationPositionAction
    {
        public bool editorConstantlyReposition;
        public GridCellRectangleGroup group;

        protected override void Awake()
        {
            base.Awake();
            group.OnCellAdded += Reposition;
        }

        [Button("Force Reposition")]
        protected override void Action()
        {
            foreach (SceneGridCell gridCell in group.LoadedCells)
            {
                Reposition(gridCell);
            }
        }

        private void Update()
        {
            if (!Application.isPlaying)
            {
                if (!group)
                    return;
                group.OnCellAdded -= Reposition;
                group.OnCellAdded += Reposition;

                if (editorConstantlyReposition)
                    Action();
            }
        }
    }
}
