﻿using System;
using UnityEngine;

namespace BFT
{
    [Flags]
    public enum EGridPositionActionEvent
    {
        GRID_INDEX_CHANGE = 1 << 1,
        GRID_TRANSFORM_REPRESENTATION_CHANGE = 1 << 2,
        GRID_OUT_OF_GRID_CHANGE = 1 << 3,
    }

    [ExecuteInEditMode]
    public abstract class AbstractGridPositionAction : MonoBehaviour
    {
        public bool ActionOnEnable;
        public EGridPositionActionEvent EventToBind;
        public SceneGridPosition ReferencePosition;

        protected virtual void Awake()
        {
            BindOnFlaggedEvents();
        }

        private void BindOnFlaggedEvents()
        {
            foreach (EGridPositionActionEvent eventType in Enum.GetValues(typeof(EGridPositionActionEvent)))
            {
                if (EventToBind.HasFlag(eventType))
                {
                    BindOnEvent(eventType);
                }
            }
        }

        private void UnbindOnFlaggedEvents()
        {
            foreach (EGridPositionActionEvent eventType in Enum.GetValues(typeof(EGridPositionActionEvent)))
            {
                if (EventToBind.HasFlag(eventType))
                {
                    UnbindOnEvent(eventType);
                }
            }
        }

        private void BindOnEvent(EGridPositionActionEvent eventType)
        {
#if UNITY_EDITOR
            if (!ReferencePosition)
                return;
#endif
            switch (EventToBind)
            {
                case EGridPositionActionEvent.GRID_INDEX_CHANGE:
                    ReferencePosition.OnGridIndexesChanged += Action;
                    break;
                case EGridPositionActionEvent.GRID_TRANSFORM_REPRESENTATION_CHANGE:
                    ReferencePosition.OnGridTransformRepresentationChange += Action;
                    break;
                case EGridPositionActionEvent.GRID_OUT_OF_GRID_CHANGE:
                    ReferencePosition.OnOutOfGrid += Action;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void UnbindOnEvent(EGridPositionActionEvent eventType)
        {
#if UNITY_EDITOR
            if (!ReferencePosition)
                return;
#endif


            switch (EventToBind)
            {
                case EGridPositionActionEvent.GRID_INDEX_CHANGE:
                    ReferencePosition.OnGridIndexesChanged -= Action;
                    break;
                case EGridPositionActionEvent.GRID_TRANSFORM_REPRESENTATION_CHANGE:
                    ReferencePosition.OnGridTransformRepresentationChange -= Action;
                    break;
                case EGridPositionActionEvent.GRID_OUT_OF_GRID_CHANGE:
                    ReferencePosition.OnOutOfGrid -= Action;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        void OnEnable()
        {
            if (ActionOnEnable)
                Action();
        }

        void Update()
        {
#if UNITY_EDITOR

            if (!Application.isPlaying)
            {
                UnbindOnFlaggedEvents();
                BindOnFlaggedEvents();
            }
#endif
        }

        protected abstract void Action();
    }
}
