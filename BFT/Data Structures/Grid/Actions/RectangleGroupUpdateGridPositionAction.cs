﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [AddComponentMenu("Grid/Scene Grid/Action/Rectange Group Update Action")]
    public class RectangleGroupUpdateGridPositionAction : AbstractGridPositionAction
    {
        [MinValue(0)] public int BottomRange = 2;

        public SceneGrid Grid;

        [MinValue(0)] public int LeftRange = 2;

        public GridCellRectangleGroup Range;

        [MinValue(0)] public int RightRange = 2;

        [MinValue(0)] public int TopRange = 2;

        [Button("ForceCoordinateChangeAction")]
        protected override void Action()
        {
            Range.SetNewBoundaries(Mathf.Clamp(ReferencePosition.CellI - LeftRange, 0, Grid.GridLength),
                Mathf.Clamp(ReferencePosition.CellJ - BottomRange, 0, Grid.GridHeight)
                , Mathf.Clamp(ReferencePosition.CellI + RightRange, 0, Grid.GridLength),
                Mathf.Clamp(ReferencePosition.CellJ + TopRange, 0, Grid.GridHeight));
        }
    }
}
