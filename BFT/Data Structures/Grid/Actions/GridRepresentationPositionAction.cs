﻿using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    public abstract class GridRepresentationPositionAction : AbstractGridPositionAction
    {
        private readonly List<SceneGridCell> toReposition = new List<SceneGridCell>();

        [Tooltip("ignores lateUpdateReposition if in the editor")]
        public bool DebugInstantReposition;

        private bool jobToDo = false;
        public bool LateUpdateReposition = true;

        public void Reposition(SceneGridCell cell)
        {
#if UNITY_EDITOR

            if (!Application.isPlaying && DebugInstantReposition)
            {
                if (cell)
                    cell.RepositionCellRepresentation();
            }
            else
            {
                NormalRepositionProcedure(cell);
            }

#else
        NormalRepositionProcedure(cell);
#endif
        }

        private void NormalRepositionProcedure(SceneGridCell cell)
        {
            if (LateUpdateReposition)
            {
                toReposition.Add(cell);

                if (!jobToDo)
                    jobToDo = true;
            }
            else
                cell.RepositionCellRepresentation();
        }

        void LateUpdate()
        {
            if (!jobToDo)
                return;

            foreach (SceneGridCell cell in toReposition)
            {
                if (cell)
                    cell.RepositionCellRepresentation();
            }

            toReposition.Clear();
            jobToDo = false;
        }
    }
}
