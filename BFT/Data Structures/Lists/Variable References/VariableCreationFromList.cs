using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

namespace BFT
{
    /// <summary>
    ///     Creates variable references from list elements
    ///     useful for generic UIs that may contain a certain amount of objects to link:
    ///     this way you can base part of the UI on a reference, and ignore where this reference comes from.
    ///     The amount of types is a mouthful unfortunately, but that's the only to have the data properly serialized
    /// </summary>
    /// <typeparam name="T"> the type of variable</typeparam>
    /// <typeparam name="T1">the type of list value</typeparam>
    /// <typeparam name="T2">the type of variable component that will be used to copy the data</typeparam>
    /// <typeparam name="T3">the type of variable</typeparam>
    [InfoBox(
        "This Component takes values from a list and creates prefabs under this objects that will hold a reference to each values of the list")]
    public class VariableCreationFromList<T, T1, T2, T3> : MonoBehaviour
        where T1 : IValue<List<T>>
        where T2 : VariableComponent<T, T3>
        where T3 : GenericVariable<T>
    {
        [BoxGroup("Status"), ReadOnly, HideInEditorMode]
        private readonly List<T2> currentComponents = new List<T2>();

        private readonly List<T2> notAssignedComponents
            = new List<T2>();

        public int PoolSize => notAssignedComponents.Count + currentComponents.Count;

        [BoxGroup("Options")] public bool PoolComponentsOnAwake;


        [FormerlySerializedAs("AssignOnEnable")] [BoxGroup("Options")]
        public bool AssignListOnEnable = true;

        [BoxGroup("Options")] public Transform PooledComponentsParent;
        [BoxGroup("Options")] public Transform CurrentComponentsParent;

        [InfoBox("The Component that will be instantiated and that will hold the value of each list entry")]
        [BoxGroup("References")]
        public T2 ComponentPrefab;

        [BoxGroup("Options"), ShowIf("PoolComponentsOnAwake")]
        public int InitialPoolSize = 5;

        [InfoBox("The list from which the values will be taken and put on the variables")] [BoxGroup("References")]
        public T1 List;

        [BoxGroup("Options")] public bool LogPoolExpanding = false;

        void Reset()
        {
            PooledComponentsParent = transform;
            CurrentComponentsParent = transform;
        }

        public void Awake()
        {
            if (PoolComponentsOnAwake)
                RaisePoolSize(InitialPoolSize);
        }

        public void OnEnable()
        {
            if (AssignListOnEnable)
                ReAssignAllComponents();
        }

        [BoxGroup("Tools")]
        [Button(ButtonSizes.Medium)]
        public void PoolAllComponents()
        {
            foreach (var variableComponent in currentComponents)
            {
                PoolComponent(variableComponent);
            }
        }

        [BoxGroup("Tools")]
        [Button(ButtonSizes.Medium)]
        public void ReAssignAllComponents()
        {
            if (PoolSize < List.Value.Count)
                RaisePoolSize(List.Value.Count - currentComponents.Count);

            if (currentComponents.Count > List.Value.Count)
            {
                for (int i = List.Value.Count; i < currentComponents.Count; i++)
                {
                    PoolComponent(i);
                }
            }

            for (var index = 0; index < List.Value.Count; index++)
            {
                var listValue = List.Value[index];
                AssignComponent(index, listValue);
            }
        }

        private void PoolComponent(int activeID)
        {
            var component = currentComponents[activeID];
            PoolComponent(component);
        }

        private void PoolComponent(T2 component)
        {
            component.Value = default;
            component.gameObject.SetActive(false);
            component.transform.SetParent(PooledComponentsParent);
            currentComponents.Remove(component);
            notAssignedComponents.Add(component);
        }

        [BoxGroup("Tools")]
        [Button(ButtonSizes.Medium)]
        public void AssignMissingValues()
        {
            if (currentComponents.Count < List.Value.Count)
                RaisePoolSize(List.Value.Count - currentComponents.Count);

            for (var index = 0; index < List.Value.Count; index++)
            {
                var listValue = List.Value[index];

                if (currentComponents[index].Value == null || !currentComponents[index].Value.Equals(listValue))
                {
                    AssignComponent(index, listValue);
                }
            }
        }

        private void TakeComponentFromPool()
        {
            if (notAssignedComponents.Count > 0)
            {
                currentComponents.Add(notAssignedComponents[0]);
                notAssignedComponents.RemoveAt(0);
            }
            else
            {
                if (LogPoolExpanding)
                    UnityEngine.Debug.LogFormat(this, "Pool Expending with {0} new elements", 1);

                T2 newComponent = Instantiate(ComponentPrefab, CurrentComponentsParent);
                currentComponents.Add(newComponent);
            }
        }

        public void RaisePoolSize(int amount)
        {
            if (LogPoolExpanding)
                UnityEngine.Debug.LogFormat(this, "Pool Expending with {0} new elements", amount);

            for (int i = 0; i < amount; i++)
            {
                var instantiated = Instantiate(ComponentPrefab, PooledComponentsParent);
                instantiated.gameObject.SetActive(false);
                notAssignedComponents.Add(instantiated);
            }
        }

        private void AssignComponent(int id, T content)
        {
            while (id >= currentComponents.Count)
            {
                TakeComponentFromPool();
            }

            currentComponents[id].Value = content;
            currentComponents[id].transform.SetParent(CurrentComponentsParent, true);
            currentComponents[id].gameObject.SetActive(true);
        }
    }
}