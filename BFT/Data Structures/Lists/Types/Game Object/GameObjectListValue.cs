using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace BFT
{
    [Serializable]
    public class GameObjectListValue : ListValue<GameObject>
    {
    }
}