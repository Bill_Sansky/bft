using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    public class GameObjectListValueFromChildren : MonoBehaviour, IValue<List<GameObject>>
    {
        public GameObject Parent;

        public bool GenerateListWheneverCalled = true;

        private List<GameObject> children;

        public int Count => Value.Count;

        public List<GameObject> Value
        {
            get
            {
                if (children == null || GenerateListWheneverCalled)
                    GenerateList();

                return children;
            }
        }

        private void GenerateList()
        {
            children = new List<GameObject>(Parent.transform.childCount);
            for (int i = 0; i < Parent.transform.childCount; i++)
            {
                children.Add(Parent.transform.GetChild(i).gameObject);
            }
        }
    }
}