using UnityEngine;

namespace BFT
{
    public class GameObjectValueFromListComponent : ValueFromListComponent<GameObject, GameObjectValueFromList,
        GameObjectListValue>
    {
    }
}