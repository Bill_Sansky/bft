using System;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class GameObjectValueFromList : ValueFromList<GameObject, GameObjectListValue>
    {
    }
}