﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;


namespace BFT
{
    /// <summary>
    ///     Gives a value from a list given a key id
    /// </summary>
    /// <typeparam name="T"> the type of data</typeparam>
    /// <typeparam name="T1">the type of dictionary</typeparam>
    [Serializable]
    public class ValueFromList<T, T1> : IntValue, IValue<T> where T1 : IValue<List<T>>
    {
        public bool LoopID = true;

        [FoldoutGroup("Value", false)] [SerializeField]
        private T defaultValue;


        [SerializeField] private T1 List;

        [HorizontalGroup]
        [HideIf("UseReference"), HideLabel, ShowInInspector]
        protected override int LocalValueInspected
        {
            get => LocalValue;
            set => LocalValue = value;
        }

        public int ID
        {
            get => Value;

            set => LocalValueInspected = value;
        }

        public T DefaultValue => defaultValue;

        public T ListValue

        {
            get
            {
                if (List.Value != null)
                {
                    if (LoopID)
                    {
                        if (List.Value.Count == 0)
                            return DefaultValue;

                        return List.Value[List.Value.LoopID(ID)];
                    }
                    else
                    {
                        return (ID >= 0 && ID < List.Value.Count) ? List.Value[ID] : DefaultValue;
                    }
                }

                return default(T);
            }

            //  set => ReferenceDictionary.Set(ID, value);
        }

        [FoldoutGroup("Value", false), ShowInInspector]
        T IValue<T>.Value => ListValue;

#if UNITY_EDITOR
        public override bool EditorDrawRef(bool value, GUIContent label)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("List Key");
            bool ret = base.EditorDrawRef(value, label);
            EditorGUILayout.EndHorizontal();
            return ret;
        }
#endif
    }
}