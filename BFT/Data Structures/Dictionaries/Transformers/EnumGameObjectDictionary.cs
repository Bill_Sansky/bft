using System;
using UnityEngine;
using UnityEngine.Rendering;

namespace BFT
{
    [Serializable]
    public class EnumGameObjectDictionary : SerializableDictionaryBase<EnumAssetValue, GameObject>
    {
        
    }
}