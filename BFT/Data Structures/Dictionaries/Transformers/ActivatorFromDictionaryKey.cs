﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    /// <summary>
    /// activates a specific object depending on the enum value
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    public class ActivatorFromDictionaryKey<T, T1, T2> : MonoBehaviour
        where T1 : SerializableDictionaryBase<T2, GameObject>
        where T2 : GenericValue<T>
    {
        public T2 EnumValue;

        public T1 StateDictionary;

        public bool ActivateStateOnEnable;

        private void OnEnable()
        {
            if (ActivateStateOnEnable)
                ActivateGoodState();
        }

        [Button(ButtonSizes.Medium)]
        public void ActivateGoodState()
        {
            foreach (var pair in StateDictionary)
            {
                if (pair.Key.Value.Equals(EnumValue.Value))
                {
                    pair.Value.SetActive(true);
                }
                else
                    pair.Value.SetActive(false);
            }
        }
    }
}