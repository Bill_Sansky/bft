﻿using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [CreateAssetMenu(menuName = "BFT/Data/Data Holders/Text/Text Dictionary", fileName = "Text Dictionary")]
    public class StringDictionaryAsset : DictionaryAsset<string, StringEntry>
    {
        public override StringEntry CreateNewDataHolder(object data)
        {
            return new StringEntry();
        }

        [Button(ButtonSizes.Medium),FoldoutGroup("Tools")]
        public void AddString(string name)
        {
            AddData(new StringEntry(){Data = name,NameID = name},null);
        }
    }
}
