using System;

namespace BFT
{
    [Serializable]
    public class IntDictionary : EntryDictionary<int, IntEntry>
    {
        public override IntEntry CreateNewDataHolder(object input)
        {
            return new IntEntry()
            {
                Data = (int) input
            };
        }
    }
}
