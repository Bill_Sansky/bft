using System;

namespace BFT
{
    [Serializable]
    public class IntValueFromDictionary : ValueFromDictionary<int, IntDictionaryValue, IntEntry, IntDictionary>
    {
    }
}
