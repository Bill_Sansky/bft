using System;

namespace BFT
{
    [Serializable]
    public class IntVariableComponentEntry : ObjectEntry<IntVariableComponent>, IDictionaryEntry<IVariable<int>>
    {
        IVariable<int> IDictionaryEntry<IVariable<int>>.Data
        {
            get => Data;
            set => Data = (IntVariableComponent) value;
        }


        public override JsonData ExportJsonData()
        {
            throw new System.NotImplementedException();
        }

        public override void ParseJsonData(JsonData data)
        {
            throw new System.NotImplementedException();
        }

        public override void NotifyJSonDataDeleteRequest()
        {
            throw new System.NotImplementedException();
        }
    }
}
