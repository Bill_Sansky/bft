using System;

namespace BFT
{
    [Serializable]
    public class IntVariableComponentDictionary : EntryDictionary<IntVariableComponent, IntVariableComponentEntry>
    {
        public override IntVariableComponentEntry CreateNewDataHolder(object input)
        {
            return new IntVariableComponentEntry() {Data = (IntVariableComponent) input};
        }
    }
}
