using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [Serializable]
    [InlineProperty]
    [HideReferenceObjectPicker]
    public class IntEntry : DictionaryEntry<int>
    {
        [SerializeField] private int field;
        [SerializeField] private string id;

        public override string NameID
        {
            get => id;

            set => id = value;
        }

        public override int Data
        {
            get => field;

            set => field = value;
        }


        public override void ParseJsonData(JsonData data)
        {
            base.ParseJsonData(data);
            field = int.Parse(data.DataByID["Value"]);
        }
        
    }
}