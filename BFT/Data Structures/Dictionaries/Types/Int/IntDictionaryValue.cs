using System;

namespace BFT
{
    [Serializable]
    public class IntDictionaryValue : EntryDictionaryValue<int, IntEntry, IntDictionary>
    {
    }
}
