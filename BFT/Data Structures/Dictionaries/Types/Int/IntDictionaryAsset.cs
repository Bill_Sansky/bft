﻿using UnityEngine;

namespace BFT
{
    [CreateAssetMenu(menuName = "BFT/Data/Data Holders/Int/Int Dictionary", fileName = "Int Dictionary")]
    public class IntDictionaryAsset : DictionaryAsset<int, IntEntry>
    {
        public override IntEntry CreateNewDataHolder(object data)
        {
            return new IntEntry();
        }
    }
}
