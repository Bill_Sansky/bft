﻿using UnityEngine;

namespace BFT
{
    [CreateAssetMenu(menuName = "BFT/Data/Data Holders/Float/Float Dictionary", fileName = "Float Dictionary")]
    public class FloatDictionaryAsset : DictionaryAsset<float, FloatEntry>
    {
        public override FloatEntry CreateNewDataHolder(object data)
        {
            return new FloatEntry();
        }
    }
}
