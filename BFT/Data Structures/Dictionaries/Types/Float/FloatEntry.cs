using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace BFT
{
    [Serializable]
    [InlineProperty]
    [HideReferenceObjectPicker]
    public class FloatEntry : DictionaryEntry<float>
    {
        [SerializeField] private float field;
        [SerializeField] private string id;

        public override string NameID
        {
            get => id;

            set => id = value;
        }

        public override float Data
        {
            get => field;

            set => field = value;
        }

        public override JsonData ExportJsonData()
        {
            JsonData data = new JsonData();
            data.DataByID.Add("Name ID", NameID);
            data.DataByID.Add("Value", field.ToString());

            return data;
        }

        public override void ParseJsonData(JsonData data)
        {
            NameID = data.DataByID["Name ID"];
            field = float.Parse(data.DataByID["Value"]);
        }

        public override void NotifyJSonDataDeleteRequest()
        {
            //nothing to do
        }
    }
}
