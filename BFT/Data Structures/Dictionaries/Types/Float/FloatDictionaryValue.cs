using System;

namespace BFT
{
    [Serializable]
    public class FloatDictionaryValue : EntryDictionaryValue<float, FloatEntry, FloatDictionary>
    {
    }
}
