using System;

namespace BFT
{
    [Serializable]
    public class FloatDictionary : EntryDictionary<float, FloatEntry>
    {
        public override FloatEntry CreateNewDataHolder(object input)
        {
            return new FloatEntry()
            {
                Data = (float) input
            };
        }
    }
}
