using System;

namespace BFT
{
    [Serializable]
    public class FloatVariableComponentEntry : ObjectEntry<FloatVariableComponent>, IDictionaryEntry<IVariable<float>>
    {
        IVariable<float> IDictionaryEntry<IVariable<float>>.Data
        {
            get => Data;
            set => Data = (FloatVariableComponent) value;
        }


        public override JsonData ExportJsonData()
        {
            throw new System.NotImplementedException();
        }

        public override void ParseJsonData(JsonData data)
        {
            throw new System.NotImplementedException();
        }

        public override void NotifyJSonDataDeleteRequest()
        {
            throw new System.NotImplementedException();
        }
    }
}
