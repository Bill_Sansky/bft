using System;

namespace BFT
{
    [Serializable]
    public class FloatVariableComponentDictionary : EntryDictionary<FloatVariableComponent, FloatVariableComponentEntry>
    {
        public override FloatVariableComponentEntry CreateNewDataHolder(object input)
        {
            return new FloatVariableComponentEntry() {Data = (FloatVariableComponent) input};
        }
    }
}
