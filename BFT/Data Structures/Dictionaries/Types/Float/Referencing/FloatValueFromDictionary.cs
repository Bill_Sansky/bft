using System;

namespace BFT
{
    [Serializable]
    public class FloatValueFromDictionary : ValueFromDictionary<float, FloatDictionaryValue, FloatEntry, FloatDictionary>
    {
    }
}
