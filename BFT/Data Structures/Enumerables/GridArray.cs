﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

namespace BFT
{
    [Serializable]
    public class GridArray<T> : IEnumerable<T>
    {
        [MinValue(1), ReadOnly] public int Height;

        public T[] Values;

        [MinValue(1), ReadOnly] public int Width;

        public GridArray(int width, int height, T[] toCopy = null)
        {
            this.Width = width;
            this.Height = height;
            this.Values = new T[width * height];

            if (toCopy != null)
            {
                int i = 0;
                int j = 0;
                foreach (var t in toCopy)
                {
                    if (i >= width || j >= height)
                        break;

                    Values[width * j + i] = t;

                    j++;
                    if (j > width)
                    {
                        j = 0;
                        i++;
                    }
                }
            }
        }

        public T this[int i, int j]
        {
            get => Values[Width * j + i];
            set => Values[Width * j + i] = value;
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (T t in Values)
            {
                yield return t;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
