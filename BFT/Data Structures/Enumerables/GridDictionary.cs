﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BFT
{
    [Serializable]
    public class GridDictionary<T> : IEnumerable<T>
    {
        [SerializeField]
        private Dictionary<int, Dictionary<int, T>> gridStructure = new Dictionary<int, Dictionary<int, T>>();

        public bool ContainsNullRow
        {
            get
            {
                foreach (Dictionary<int, T> dictionary in gridStructure.Values)
                {
                    if (dictionary == null)
                        return true;
                }

                return false;
            }
        }

        public int Count
        {
            get
            {
                int count = 0;

                foreach (Dictionary<int, T> keyValuePair in gridStructure.Values)
                {
                    foreach (T valuePair in keyValuePair.Values)
                    {
                        if (valuePair != null)
                            count++;
                    }
                }

                return count;
            }
        }

        public T this[int i, int j]
        {
            get
            {
                if (!GridStructure.ContainsKey(i) || GridStructure[i] == null)
                    return default(T);
                if (!GridStructure[i].ContainsKey(j))
                    return default(T);

                return GridStructure[i][j];
            }
            set
            {
                if (!GridStructure.ContainsKey(i))
                {
                    GridStructure[i] = new Dictionary<int, T>();
                }

                GridStructure[i][j] = value;
            }
        }

        public Dictionary<int, Dictionary<int, T>> GridStructure => gridStructure;

        public IEnumerator<T> GetEnumerator()
        {
            foreach (Dictionary<int, T> t in GridStructure.Values)
            {
                foreach (var value in t.Values)
                {
                    yield return value;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        public void Clear()
        {
            GridStructure.Clear();
        }

        public void Add(int i, int j, T type)
        {
            if (gridStructure == null)
                gridStructure = new Dictionary<int, Dictionary<int, T>>();
            if (!GridStructure.ContainsKey(i) || GridStructure[i] == null)
            {
                GridStructure[i] = new Dictionary<int, T>();
            }

            if (GridStructure[i].ContainsKey(j))
            {
                GridStructure[i][j] = type;
            }
            else
            {
                GridStructure[i].Add(j, type);
            }
        }

        public void RemoveAt(int i, int j)
        {
            if (!GridStructure.ContainsKey(i))
            {
                return;
            }

            if (GridStructure[i] == null)
            {
                GridStructure.Remove(i);
                return;
            }

            GridStructure[i].Remove(j);
            if (gridStructure[i].Count == 0 || GridStructure[i] == null)
                gridStructure.Remove(i);
        }

        public bool ContainsKey(int i, int j)
        {
            return GridStructure.ContainsKey(i) && GridStructure[i].ContainsKey(j);
        }


        public bool ContainsKeyAndNotNull(int i, int j)
        {
            return GridStructure != null
                   && GridStructure.ContainsKey(i) && GridStructure[i] != null
                   && GridStructure[i].ContainsKey(j) && this[i, j] != null;
        }
    }
}
