﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BFT
{
    [Serializable]
    public class PriorityList<T> : IEnumerable<T>
    {
        private T[] objects;
        private bool[] priorityTaken;

        public PriorityList(bool noPriority, params T[] objects)
        {
            this.objects = objects;
            priorityTaken = new bool[objects.Length];
            for (int i = 0; i < priorityTaken.Length; i++)
                priorityTaken[i] = !noPriority;
        }

        public T Holder
        {
            get
            {
                for (int i = 0; i < priorityTaken.Length; i++)
                {
                    if (priorityTaken[i])
                    {
                        return objects[i];
                    }
                }

                return objects[0];
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return ((IEnumerable<T>) objects).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void ReleasePriority(int priority)
        {
            priorityTaken[priority] = false;
        }

        public void ReleasePriority(T handler)
        {
            priorityTaken[Array.IndexOf(objects, handler)] = false;
        }

        public void TakePriority(int priority)
        {
            priorityTaken[priority] = true;
        }

        public void TakePriority(T handler)
        {
            priorityTaken[Array.IndexOf(objects, handler)] = true;
        }
    }
}
