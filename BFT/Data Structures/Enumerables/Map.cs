﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BFT
{
    [Serializable]
    public class Map<T1, T2> : IEnumerable<KeyValuePair<T1, T2>>
    {
        private readonly Dictionary<T2, T1> backward;
        private readonly Dictionary<T1, T2> forward;

        public Map(int size = 5)
        {
            forward = new Dictionary<T1, T2>(size);
            backward = new Dictionary<T2, T1>(size);
        }

        public int Count => Backward.Count;

        public T2 this[T1 key]
        {
            get => Forward[key];
            set
            {
                Forward[key] = value;
                Backward[value] = key;
            }
        }

        public T1 this[T2 key]
        {
            get => Backward[key];
            set
            {
                Backward[key] = value;
                Forward[value] = key;
            }
        }

        public ICollection<T1> Keys => Forward.Keys;

        public ICollection<T2> Values => Backward.Keys;

        public Dictionary<T1, T2> Forward => forward;

        public Dictionary<T2, T1> Backward => backward;

        IEnumerator<KeyValuePair<T1, T2>> IEnumerable<KeyValuePair<T1, T2>>.GetEnumerator()
        {
            return Forward.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return Forward.GetEnumerator();
        }

        public void Add(KeyValuePair<T1, T2> item)
        {
            Forward.Add(item.Key, item.Value);
            Backward.Add(item.Value, item.Key);
        }

        public void Add(KeyValuePair<T2, T1> item)
        {
            Backward.Add(item.Key, item.Value);
            Forward.Add(item.Value, item.Key);
        }

        public void Clear()
        {
            Backward.Clear();
            Forward.Clear();
        }

        public bool Contains(KeyValuePair<T2, T1> item)
        {
            return Backward.Contains(item);
        }

        public bool Remove(KeyValuePair<T2, T1> item)
        {
            return Backward.Remove(item.Key) && Forward.Remove(item.Value);
        }

        public bool Contains(KeyValuePair<T1, T2> item)
        {
            return Forward.Contains(item);
        }

        public bool Remove(KeyValuePair<T1, T2> item)
        {
            return Forward.Remove(item.Key) && Backward.Remove(item.Value);
        }

        public void Add(T1 key, T2 value)
        {
            Backward.Add(value, key);
            Forward.Add(key, value);
        }

        public bool Contains(T1 key)
        {
            return Forward.ContainsKey(key);
        }

        public bool Contains(T2 key)
        {
            return Backward.ContainsKey(key);
        }

        public bool Remove(T1 key)
        {
            return Backward.Remove(Forward[key]) && Forward.Remove(key);
        }

        public bool TryGetValue(T1 key, out T2 value)
        {
            return Forward.TryGetValue(key, out value);
        }

        public void Add(T2 key, T1 value)
        {
            Forward.Add(value, key);
            Backward.Add(key, value);
        }

        public bool ContainsKey(T2 key)
        {
            return Backward.ContainsKey(key);
        }

        public bool Remove(T2 key)
        {
            return Forward.Remove(Backward[key]) && Backward.Remove(key);
        }

        public bool TryGetValue(T2 key, out T1 value)
        {
            return Backward.TryGetValue(key, out value);
        }
    }
}
